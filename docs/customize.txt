=========================================

Customization Options For ActiveCampaign Email Marketing

=========================================


99% of the product is unencoded allowing you to edit the template and/or php files.


Template Files
--------------------
	
	Public Section

		/12all/templates 	(Directory) (You can modify any files in this directory)
			main.htm 	(The core layout)
		

	Admin Section
		/12all/admin/templates	(Directory) (You can modify any files in this directory)
			main.htm 	(the core admin layout)


CSS
--------------------

	Public Section
		/12all/css/default.css

	Admin Section
		/12all/admin/css/default.css




-----------------------------------------

For More Customization Details Visit:
	http://www.activecampaign.com/customize
