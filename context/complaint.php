<?php

require_once ac_global_functions("group.php");
require_once ac_admin("functions/abuse.php");
require_once ac_global_classes("select.php");

class complaint_context extends ACP_Page {

	function complaint_context() {
		$this->pageTitle = _a("Abuse Complaints");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		$gid    = (int)ac_http_param('g');
		$hash   = (string)ac_http_param('h');

		if ( !$gid or !$hash ) {
			ac_http_redirect(ac_site_plink() . '?err=c1');
		}

		// get group
		$group = ac_group_select_row($gid);
		//if ( !$group or !$group['abuseratio_overlimit'] ) {
		if ( !$group ) {
			ac_http_redirect(ac_site_plink() . '?err=c2');
		}
		$smarty->assign('group', $group);

		$abuse = abuse_select_row($gid);
		if ( !$abuse or $abuse['hash'] != $hash ) {
			ac_http_redirect(ac_site_plink() . '?err=c3');
		}
		$smarty->assign('abuse', $abuse);

		// display regular page with form inside
		$smarty->assign("content_template", "complaint.htm");
	}
}

?>
