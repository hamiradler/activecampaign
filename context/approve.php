<?php

require_once ac_global_classes("select.php");

class approve_context extends ACP_Page {

	function approve_context() {
		$this->pageTitle = _p("Approve Campaign Sending");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		$cid    = (int)ac_http_param('c');
		$aid    = (int)ac_http_param('a');
		$mid    = (int)ac_http_param('m'); // not used yet, but a working filter for split messages
		$hash   = (string)ac_http_param('h');

		if ( !$cid or !$aid or !$hash ) {
			ac_http_redirect(ac_site_plink());
		}

		// get approval
		$approval = approval_select_row($aid);
		// get campaign
		$campaign = campaign_select_row($cid, true, true, true);

		if ( !$campaign ) {
			ac_http_redirect(ac_site_plink());
		}

		// checks
		$approved = ( $approval and $approval['approved'] );
		$declined = !$approval;
		if ( !$approval or $approval['approved'] or $hash != $approval['hash'] ) {
			if ( !$declined and !$approval ) ac_http_redirect(ac_site_plink());
		}
		$smarty->assign('approved', $approved);
		$smarty->assign('declined', $declined);

		// get campaign's filter
		$campaign['filter'] = false;
		if ( $campaign['filterid'] ) $campaign['filter'] = filter_select_row($campaign['filterid']);

		// get campaign's user
		$origAdmin = ac_admin_get();
		$user = ac_admin_get_totally_unsafe($campaign['userid']);
		if ( !$user ) {
			ac_http_redirect(ac_site_plink());
		}
		$GLOBALS['admin'] = $origAdmin;
		$groupslist = implode("', '", $user['groups']);
		$groups = ac_sql_select_array("SELECT `id`, `title`, `descript` FROM #group WHERE `id` IN ('$groupslist')");

		// get campaign's message
		$messagekey = 0;
		if ( $mid ) {
			foreach ( $campaign['messages'] as $k => $v ) {
				if ( $v['id'] == $mid ) {
					$messagekey = $k;
					break;
				}
			}
		}
		$message = $campaign['messages'][$messagekey];

		$smarty->assign('approval', $approval);
		$smarty->assign('campaign', $campaign);
		$smarty->assign('message', $message);
		$smarty->assign('user', $user);
		$smarty->assign('groups', $groups);

		$type_array = campaign_type();
		$smarty->assign('type_array', $type_array);

		// get sample subscriber
		$subscriber = subscriber_dummy('', $campaign['lists'][0]['id']);
		$smarty->assign('subscriber', $subscriber);
		$smarty->assign('hash', ( $subscriber ? $subscriber['hash'] : '' ));

		// display regular page with form inside
		$smarty->assign("content_template", "approve.htm");
	}
}

?>
