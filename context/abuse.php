<?php

require_once ac_admin("functions/abuse.php");
require_once ac_global_functions("ajax.php");
require_once ac_global_classes("select.php");

class abuse_context extends ACP_Page {

	function abuse_context() {
		$this->pageTitle = _a("Report Abuse");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		$cid    = (int)ac_http_param('c');
		$mid    = (int)ac_http_param('m');
		$listid = (int)ac_http_param('nl');
		$hash   = (string)ac_http_param('s');

		if ( !$this->site['mail_abuse'] ) {
			ac_http_redirect(ac_site_plink() . '?err=ao');
		}

		if ( /*!$cid or !$mid or*/ !$listid or !$hash ) {
			ac_http_redirect(ac_site_plink() . '?err=hm');
		}

		// get campaign
		$campaign = campaign_select_row($cid, true, true, true);
		/*
		if ( !$campaign ) {
			ac_http_redirect(ac_site_plink() . '?err=cm');
		}
		*/

		// get subscriber
		$subscriber = subscriber_exists($hash, 0, 'hash'); // on any list
		if ( !$subscriber ) {
			$subscriber = subscriber_dummy(_a('_t.e.s.t_@example.com'), $listid);
			//ac_http_redirect(ac_site_plink());
		}

		$confirmed = ac_http_param_exists('abused');

		if ( $confirmed and $campaign ) {
			if ( !$campaign ) {
				$campaign = array(
					'id' => 0,
					'userid' => 1,
					'name' => '',
				);
			}
			abuse_complaint($subscriber, $campaign, $mid, $listid);
		}

		$smarty->assign('campaign', $campaign);
		$smarty->assign('subscriber', $subscriber);
		$smarty->assign('listid', $listid);
		$smarty->assign('messageid', $mid);
		$smarty->assign('confirmed', $confirmed);

		// display regular page with form inside
		$smarty->assign("content_template", "abuse.htm");
	}
}

?>
