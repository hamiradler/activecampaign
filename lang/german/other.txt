##### TRANSLATION STRINGS NOT SPECIFIC TO ADMIN OR PUBLIC SECTION #####
##### Example: "original string" = "your-translation-here" #####

"&copy; %s ActiveCampaign, Inc." = "&copy; %s ActiveCampaign, Inc."
"1-2-All" = "1-2-All"
"Email Marketing Software" = "Email Marketing Software"



##### 2009-08-13 #####

"ACEM" = ""



##### 2009-09-01 #####

"by ActiveCampaign" = ""



##### 2010-03-04 #####

"ActiveCampaign Email Marketing" = ""



##### 2010-07-02 #####

"ActiveCampaign Email Marketing Software" = ""



##### 2010-12-01 #####

"&copy; 2003-%s ActiveCampaign, Inc." = ""



##### 2011-07-13 #####

"quoted-printable" = ""



##### 2011-11-03 #####

"http://www.activecampaign.com/" = ""
