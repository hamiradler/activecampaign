##### TRANSLATION STRINGS #####
##### Example: "original string" = "your-translation-here" #####

"(Optional) Why did you decide to unsubscribe?" = "(Optioneel) Waarom wilt u zich uitschrijven?"
"(Subscribed %s)" = "(Ingeschreven %s)"
"A new subscriber has been added to your list." = "Een nieuwe inschrijver is toegevoegd aan uw lijst."
"A subscriber has been removed from your list." = "Een inschrijver is verwijderd van uw lijst."
"Archive" = "Archief"
"Cancel" = "Annuleer"
"Changes Saved." = "Wijzigingen opgeslagen."
"Clear" = "Legen"
"Comment from" = "Reactie van"
"Content" = "Inhoud"
"Create Date" = "Aanmaakdatum"
"Date/Time" = "Datum/Tijd"
"Date/Time:" = "Datum/Tijd:"
"E-mail address is invalid." = "E-mail adres is ongeldig."
"Email" = "E-mail"
"Error Occurred!" = "Fout opgetreden!"
"From:" = "Van:"
"HTML Editor" = "HTML editor"
"Here are the links to modify your accounts:" = "Hier zijn de links om uw accounts aan te passen:"
"Here is the link to modify your account:" = "Hier is de link om uw account aan te passen:"
"IP" = "IP"
"Invalid email address" = "Ongeldig e-mailadres"
"List Name" = "Lijstnaam"
"Loading..." = "Laden..."
"Message (HTML Version)" = "Bericht (HTML Versie)"
"Message (Text Version)" = "Bericht (Tekst Versie)"
"Message could not be previewed." = "Bericht kon niet worden voorvertoond."
"Message type not found." = "Type van bericht niet gevonden."
"Messages" = "Berichten"
"Name" = "Naam"
"Next" = "Volgende"
"OK" = "OK"
"Please also verify yourself by typing the text in the following image into the box below it." = "Verifieer uzelf door de tekst uit de volgende afbeelding over te typen in onderstaand veld."
"Please ensure all email addresses are valid." = "Zorg ervoor dat alle e-mailadressen geldig zijn."
"Please fill out at least one Friend email address and name." = "Vul aub minstens één e-mailadres en naam van een vriend in."
"Please include a text from the image." = "Vul aub een tekst uit de afbeelding in."
"Please include a valid email address." = "Vul aub een geldig e-mailadres in."
"Please include an email address." = "Vul aub een e-mailadres in."
"Please include your name." = "Vul aub uw naam in."
"Please type the correct text that appears in the image." = "Typ aub de correcte tekst uit de afbeelding in."
"Previous" = "Vorige"
"Public messages from List" = "Publieke berichten van lijst"
"Public messages from all Lists" = "Publieke berichten van alle lijsten"
"Search" = "Zoeken"
"Select Lists To Unsubscribe From" = "Selecteer lijsten om van uit te schrijven"
"Select a Campaign from List " = "Selecteer een campagne uit de lijst "
"Select a List" = "Selecteer een lijst"
"Send" = "Verzend"
"Set as Default" = "Instellen als standaard"
"Subject" = "Onderwerp"
"Subject:" = "Onderwerp:"
"Subscribe" = "Inschrijven"
"Subscriber ID is invalid." = "Inschrijver-ID is ongeldig"
"Subscription could not be processed since you did not select a list. Please select a list and try again." = "Inschrijving kon niet worden verwerkt aangezien u geen lijst heeft geselecteerd. Selecteer aub een lijst en probeer het opnieuw."
"Text Editor" = "Teksteditor"
"Thank you for confirming your subscription." = "Bedankt voor het bevestigen van uw inschrijving."
"Thank you for confirming your unsubscription." = "Bedankt voor het bevestigen van uw uitschrijving."
"The link to modify your account has been sent. Please check your email." = "De link om uw account aan te passen is verzonden. Controleer aub uw e-mail."
"The sender thought the mailing entitled '%s' would be of interest to you." = "De verzender dacht dat de mailing genaamd"
"This e-mail address cannot be added to list." = "Het e-mailadres kan niet worden toegevoegd aan de lijst."
"This e-mail address could not be unsubscribed." = "Het e-mailadres kon niet worden uitgeschreven."
"This e-mail address has been processed in the past to be subscribed, however your subscription was never confirmed." = "Het e-mailadres is verwerkt in het verleden om in te schrijven, maar is echter nooit bevestigd."
"This e-mail address has been processed. Please check your email to confirm your subscription." = "Dit e-mailadres is verwerkt. Controleer uw email om uw inschrijving te bevestigen."
"This e-mail address has been processed. Please check your email to confirm your unsubscription." = "Dit e-mailadres is verwerkt. Controleer uw email om uw uitschrijving te bevestigen."
"This e-mail address has been unsubscribed from the list." = "Dit e-mailadres is uitgeschreven van de lijst."
"This e-mail address has subscribed to the list." = "Dit e-mailadres is ingeschreven op de lijst."
"This e-mail address is already subscribed to this mailing list." = "Dit e-mailadres is reeds ingeschreven op deze mailinglijst."
"This e-mail address is on the global exclusion list." = "Dit e-mailadres staat op de globale uitsluitingslijst."
"This e-mail address was not subscribed to the list." = "Dit e-mailadres is niet ingeschreven op de lijst."
"This list is currently not accepting subscribers. This list has met its top number of allowed subscribers." = "Deze lijst is momenteel gesloten voor inschrijvers. Deze lijst heeft zijn maximale aantal inschrijvers bereikt."
"This message is being sent by:" = "Dit bericht wordt verzonden door:"
"This subscriber does not exist." = "Deze inschrijver bestaat niet."
"To view the mailing, please visit:" = "Om deze mailing te bekijken bezoekt u:"
"Unable to redirect you to this link. Please try again or contact your list admin." = "Kan u niet doorsturen naar deze link. Probeer het opnieuw of neem contact op met de administrator van uw lijst."
"Unknown response code. Please resubmit the subscription form." = "Onbekende antwoordcode. Verstuur aub het inschrijvingsformulier opnieuw."
"Unsubscribe" = "Uitschrijven"
"You are unable to be added to this list at this time." = "U kunt momenteel niet toegevoegd worden aan deze lijst."
"You selected a list that does not allow duplicates. This email is in the system already, please edit that subscriber instead." = "U heeft een lijst geselecteerd welke geen duplicaten toestaat. Deze e-mail staat reeds in het systeem. U kunt wel de bestaande inschrijver bewerken."
"Your changes have been saved." = "Uw wijzigingen zijn opgeslagen."
"Your e-mail address (Required)" = "Uw e-mailadres (vereist)"
"Your subscription request for this list could not be processed as you are missing required fields." = "Uw inschrijvingsverzoek voor deze lijst kon niet worden verwerkt omdat u enkele vereiste velden niet heeft ingevuld."
"Your subscription request for this list could not be processed as you must type your name." = "Uw inschrijvingsverzoek voor deze lijst kon niet worden verwerkt omdat u uw naam nog moet ingeven."
"[Unknown]" = "[Onbekend]"



##### 2009-07-10 #####




##### 2009-07-29 #####

"Subscribed to lists:" = "Ingeschreven op lijsten:"
"The image text you typed did not register. Please go back, reload the page, and try again." = "De afbeeldingstekst die u heeft ingegeven is incorrect. Laad de pagina opnieuw en probeer het dan opnieuw."
"Unsubscribed from lists:" = "Uitgeschreven van lijsten:"



##### 2009-08-13 #####

"Attachment not found." = "Bijlage niet gevonden."
"Attachments:" = "Bijlagen:"
"To:" = "Aan:"



##### 2009-10-06 #####

"Back" = "Terug"



##### 2009-10-14 #####

"Language changed" = "Taal gewijzigd"
"Rewrite Test" = "Herschrijftest"



##### 2009-12-21 #####

"Update Subscription Account" = "Bijwerken Lidmaatschap Account"



##### 2010-01-13 #####

"Abuse Complaint" = "Klacht over Misbruik"
"Abuse Complaints Reported:" = "Klacht over Misbruik gerapporteerd"
"Abuse Complaints have been removed." = "Klacht over Misbruik verwijderd."
"Abuse Ratio has been updated." = "Misbruik-ratio is bijgewerkt."
"Abuse Report Received." = "Misbruik-rapport ontvangen."
"Approve" = "Goedkeuren"
"Approve Campaign Sending" = "Goedkeuren campagne verzending"
"Campaign Approved." = "Campagne goedgekeurd."
"Campaign Declined." = "Campagne afgekeurd."
"Campaign for Approval" = "Campagne ter goedkeuring"
"Campaign:" = "Campagne:"
"Change Abuse Ratio" = "Wijzig misbruik-ratio"
"Click here to view and approve the campaign sending:" = "Klik hier om de campagne te bekijken en goed te keuren voor verzending:"
"Close" = "Sluiten"
"Current Abuse Ratio:" = "Huidige misbruik-ratio:"
"Decline" = "Afkeuren"
"Decline & Send Email" = "Afkeuren & e-mail verzenden"
"E-mail:" = "E-mail:"
"E-mails Sent:" = "E-mails verzonden:"
"FROM E-mail:" = "VAN E-mail:"
"FROM Name:" = "VAN Naam:"
"Group(s):" = "Groep(en)"
"Group:" = "Groep:"
"If you did not ever request to be contacted by this sender and/or about this information click the Report Abuse button to file your abuse report." = "Als u niet heeft gevraagd om te worden gemailed door deze verzender en/of omtrent deze informatie klik dan de Meld Misbruik-knop om een klacht over misbruik in te dienen."
"List(s):" = "Lijst(en):"
"Message:" = "Bericht:"
"Modify Campaign:" = "Wijzig campagne:"
"Name:" = "Naam:"
"New Abuse Ratio:" = "Nieuwe misbruik-ratio:"
"Notify Senders" = "Informeer verzenders"
"Notify user(s) in this group:" = "Informeer gebruiker(s) in deze groep:"
"Once you have updated your message you can try to send it again." = "Wanneer u uw bericht heeft bijgewerkt, kan u proberen het opnieuw te verzenden."
"Please check your message you were sending to ensure it meets our sending policy." = "Controleer aub uw bericht om er zeker van te zijn dat dit aan ons verzendbeleid voldoet."
"Please verify the information below. " = "Controleer aub onderstaande informatie."
"Private message." = "Privébericht."
"Report Abuse" = "Meld Misbruik"
"Reset Abuse Complaints" = "Stel klachten over misbruik opnieuw in"
"Segment:" = "Segment:"
"Send From:" = "Verzend Van:"
"Send Notification to " = "Verzend notificatie aan:"
"TO E-mail:" = "AAN E-mail:"
"TO Name:" = "AAN Naam:"
"The senders information is:" = "De informatie van de verzender is:"
"The subscriber information that this sender sent to is:" = "De informatie van de inschrijver waar deze verzender naar heeft verzonden is:"
"This campaign has already been accepted." = "Deze campagne is reeds goedgekeurd."
"This campaign has already been declined." = "Deze campagne is reeds afgekeurd."
"Type:" = "Type:"
"Update" = "Bijwerken"
"User:" = "Gebruiker:"
"View Abuses" = "Bekijk Misbruik"
"We take abuse reports very seriously and are in compliance of email marketing laws.  With your feedback we can prevent further emails from being sent by this sender.  By reporting this email as abuse you will not receive any further emails from this sender." = "Wij nemen klachten over misbruik erg serieus en volgen daarbij de wettelijke bepalingen omtrent e-mailmarketing. Met uw feedback kunnen we voorkomen dat er e-mails worden verzonden door deze afzender. Door het rapporteren van deze e-mail als misbruik zult u verder geen e-mails meer ontvangen van deze afzender."
"You have abused the mailing system %s" = "U heeft het mailing systeem %s misbruikt"
"Your Abuse Report Has Been Received" = "Uw misbruikrapport is ontvangen"
"Your campaign &quot;%s&quot; could not be approved at this time." = "Uw campagne &quot;%s&quot; kon momenteel niet worden goedgekeurd."
"Your campaign needs to be updated" = "Uw campagne dient te worden bijgewerkt"
"Your e-mail address has also been removed from this list. We apologize for any inconvenience this might have caused you." = "Uw e-mailadres is verwijderd van deze lijst. Wij verontschuldigen ons voor enig ongemak."

##### 2010-02-02 #####

"Abuse Reported." = "Misbruik gerapporteerd."
"Campaign Name:" = "Campagnenaam:"
"Click here to manage this user's abuse settings" = "Klik hier om de misbruikinstellingen van deze gebruiker te beheren"
"Click here to manage this user's abuse settings:" = "Klik hier om de misbruikinstellingen van deze gebruiker te beheren:"
"User %s has been suspended due to abuse reports" = "Gebruiker %s is tijdelijk geblokkeerd vanwege het aantal ingezonden klachten over misbruik."
"With a current limit of" = "Met een huidige limiet van"
"before being suspended." = "voordat deze tijdelijk wordt geblokkeerd."


##### 2010-03-01 #####

"Page %d of %d" = "Pagina %d van %d"



##### 2010-03-28 #####

"%s per page" = "%s per pagina"



##### 2010-07-02 #####

"%s Private Messages" = "%s Privéberichten"
"Private Messages published at %s." = "Privéberichten gepubliceerd op %s."
"Recipients:" = "Ontvangers:"



##### 2010-09-07 #####

"You have a new subscriber to your list." = "Je hebt een nieuwe inschrijver op je lijst."



##### 2010-12-01 #####




##### 2011-01-11 #####




##### 2011-02-23 #####

"Currently reading" = "Momenteel san het lezen"
"Social Share" = "Sociaal Delen"



##### 2011-12-12 #####

"You can only forward this campaign to up to %s people at a time." = ""



##### 2012-03-26 #####



##### 2012-11-06 #####

"Share with Your Friends" = ""
"More Sharing" = ""