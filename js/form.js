var form_act = {jsvar var=$act};
var form_obj = {jsvar var=$form};
{literal}

if (form_obj.target == 12) {
	// forward to a friend
	$J(document).ready(function() {
		$J("#_form_" + form_obj.id).submit(function() {
			var name = ac.str.trim($J("input[name=yrname]").val());

			if (name == "") {
				alert({/literal}'{"You must enter a name here"|alang|js}'{literal});
				return false;
			}

			var email = ac.str.trim($J("input[name=yremail]").val());

			if (email == "" || !ac.str.valid_email(email)) {
				// Unlikely to get here, as the input is read-only.
				alert({/literal}'{"Your email address must be valid."|alang|js}'{literal});
				return false;
			}

			var hasrecip = false;
			var allvalid = true;

			$J("input[name='rcpt_email[]']").each(function(k, v) {
				var addr = ac.str.trim(v.value);

				if (addr != "") {
					hasrecip = true;
					if (!ac.str.valid_email(addr)) {
						allvalid = false;
						return;
					}
				}
			});

			if (!allvalid) {
				alert({/literal}'{"Each of your recipient email addresses must be valid."|alang|js}'{literal});
				return false;
			}

			if (!hasrecip) {
				alert({/literal}'{"You must have entered at least one email address to forward."|alang|js}'{literal});
				return false;
			}

			return true;
		});
	});
}

function form_unsub_reason() {
	var post = {
		hash: $J("input[name=s][value!='']").val(),
		campaignid: $J("input[name=c]").val(),
		messageid: $J("input[name=m]").val(),
		listid: $J("input[name='nlbox[]']").val(),
		reason: $J("input[name=reason]:checked").val(),
		explanation: $J("textarea[name=explanation]").val()
	};

	ac.post("subscriber.subscriber_unsub_reason", post, function(data) {
		// Hide the radio buttons, and the "submit" button
		$J("#unsubreasons label").hide();
		$J("#unsubreasons textarea").hide();
		$J("input:button").hide();

		// Add something into the unsubreasons div to say thanks.
		$J("#unsubreasons").append(sprintf("<div class='_field _option'><b>%s</b></div>", {/literal}'{"Thanks for providing your feedback on why you unsubscribed."|alang|js}'{literal}));
		$J("#unsubreasons").append("<div>&nbsp;</div>");
		$J("#unsubreasons").append(sprintf("<div class='_field _option'>%s</div>", {/literal}'{"You have been unsubscribed from these emails."|alang|js}'{literal}));
	});
}

if (form_act == "unsub_result") {
	$J(document).ready(function() {
		$J("input:button").click(function() {
			form_unsub_reason();
		});
	});
}

$J(document).ready(function() {
	if (form_obj.widthpx > 0)
		$J("._form").css("width", form_obj.widthpx);
});

{/literal}
