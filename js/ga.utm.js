function _acUTM(f) {
	if(typeof(f._ga_utmz)!='undefined')return;
	var c=document.cookie;
	_acAN(f,'_ga_utmz',_acPC(c,'__utmz=',';'));
	_acAN(f,'_ga_utma',_acPC(c,'__utma=',';'));
	_acAN(f,'_ga_utmv',_acPC(c,'__utmv=',';'));
}
function _acAN(f,n,v) {
	var node=document.createElement('input');
	node.type='hidden';
	node.name=n;
	node.value=v;
	f.appendChild(node);
}
function _acPC(l,n,s) {
	if(!l || l=="" || !n || n=="" || !s || s=="")return "";
	var i,i2,i3,c="";
	i=l.indexOf(n);
	i3=n.indexOf("=")+1;
	if(i>-1){
		i2=l.indexOf(s,i);
		if(i2<0)i2=l.length;
		c=l.substring((i+i3),i2);
	}
	return c;
}