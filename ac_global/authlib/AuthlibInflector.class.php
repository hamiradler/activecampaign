<?php
require_once(ac_global('activerecord/InflectorInterface.class.php'));
class AuthlibInflector extends InflectorInterface {
	function Classify($table){
		// We only have one table and class
		if(strtolower($table) == "acp_globalauth"){
			require_once(ac_global('authlib/GlobalAuth.class.php'));
			return "GlobalAuth";
		} else {
			return "";
		}
	}

	function Tableize($class){
		if(strtolower($class) == "globalauth"){
			return "acp_globalauth";
		} else {
			return "";
		}
	}
}
?>
