// ihook.js

var ac_ihook_table = { };

function ac_ihook_define(key, func) {
	if ( !ac_ihook_exists(key) ) {
		ac_ihook_table[key] = [ ];
	}
	ac_ihook_table[key].push(func);
}

function ac_ihook_undefine(key, func) {
	if ( ac_ihook_exists(key) ) {
		for ( var i = 0; ac_ihook_table[key].length; i++ ) {
			if ( ac_ihook_table[key][i] == func ) {
				ac_ihook_table[key].splice(i, 1);
				return;
			}
		}
	}
}

function ac_ihook_exists(key) {
	return typeof ac_ihook_table[key] != 'undefined';
}

function ac_ihook(key, param) {
	var r = null;
	if ( ac_ihook_exists(key) ) {
		for ( var i in ac_ihook_table[key] ) {
			if ( !isNaN(parseInt(i)) ) {
	    		var func = ac_ihook_table[key][i];
	    		if ( typeof func == 'function' ) {
		            r = func(param);
	    		}
			}
		}
	}
	return r;
}

