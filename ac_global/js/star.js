function ac_star_clear(starObjId) {
	var elems = $(starObjId).getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++)
		elems[i].className = "ac_star_none";

	return elems;
}

function ac_star_hover(starObjId, limit) {
	var elems = ac_star_clear(starObjId);

	for (var i = 0; i < elems.length; i++) {
		elems[i].className = "ac_star_hover";

		if ((i+1) >= limit)
			break;
	}
}

function ac_star_render(starObjId) {
	var rating = $(starObjId + "_rating").innerHTML;
	var elems = ac_star_clear(starObjId);
	var cr = rating;		// Ratings counter
	var cls = "";

	for (var i = 0; i < elems.length; i++) {

		cls = "ac_star_none";
		if (cr >= 1.0)
			cls = "ac_star_full";
		else if (cr >= 0.5)
			cls = "ac_star_half";

		elems[i].className = cls;
		cr -= 1.0;
	}
}

function ac_star_callback(xml) {
	var ary = ac_dom_read_node(xml, null);

	ac_star_set(ary.prefix, 0, ary.rating);
}

function ac_star_set(starObjId, relid, val) {
	var rateid = starObjId + "_rating";

	if ($(rateid) !== null) {
		$(rateid).innerHTML = val;
		ac_star_render(starObjId);
	}
}

function ac_star_get(starObjId) {
	var rateid = starObjId + "_rating";

	if ($(rateid) !== null) {
		return $(rateid).innerHTML;
	}
	return 0;
}

function ac_stars(rating) {
	var count = 5;
	var links = "";
	var cr = parseFloat(rating);
	var ci = 0;
	var cls;

	while (count--) {
		ci++;

		cls = "ac_star_none";
		if (cr >= 1.0)
			cls = "ac_star_full";
		else if (cr >= 0.5)
			cls = "ac_star_half";

		links += sprintf("<a class=\"%s\" href=\"javascript:void(0)\" style=\"cursor:default\">", cls);
		links += sprintf("<img style=\"padding: 0px\" border=\"0\" align=\"absmiddle\" src=\"%s/media/ac_star_clear.gif\" />", acgpath);
		links += "</a>";

		cr -= 1.0;
	}

	return "<span>" + links + "</span>";
}

function ac_star_disable(starObjId) {
	var rel = $(starObjId);
	if ( !rel ) return;
	var val = $(starObjId + '_rating').innerHTML;
	var stars = ac_stars(val);
	rel.innerHTML = stars;
}
