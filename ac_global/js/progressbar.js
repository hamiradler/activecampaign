// progressbar.js

// If DIV is used, produces progress bar. If anything else (such as SPAN), then it is a simple value updater (no bar)

var ac_progress_bars = { };
var ac_progress_timers = { };
var ac_progress_ongoing = { };

function ac_progressbar_register(divId, processId, initialValue, secondInterval, spawn, func, justValue) {
	var rel = $(divId);
	if ( !rel ) return;
	if ( !secondInterval || isNaN(secondInterval) ) {
		secondInterval = 0;
	}
	spawn = ( spawn ? 1 : 0 );
	// save this process to this div bar
	ac_progressbar_init(divId, initialValue, justValue);
	ac_progress_bars[divId] = processId;
	if ( secondInterval > 0 ) {
		if ( typeof ac_progress_timers[processId] == 'undefined' ) {
			ac_progress_timers[processId] = window.setInterval(
				function() {
					if ( typeof ac_progress_ongoing[processId] != 'undefined' ) return;
					ac_progress_ongoing[processId] = 1;
					// make an ajax call that should set all divs with new value for this process
					ac_ajax_call_cb(
						apipath,
						"process!ac_progressbar_update",
						function(xml) {
							var ary = ac_dom_read_node(xml);
							if ( !ary.id /*|| !ary.percentage*/ ) {
								ac_progressbar_unregister(divId);
								return;
							}
							if ( typeof ac_progress_ongoing[processId] != 'undefined' ) {
								delete ac_progress_ongoing[processId];
							}
							//if ( !ary.id ) return;
							//if ( !ary.percentage ) return;
							for ( var i in ac_progress_bars ) {
								var rel = $(i);
								if ( !rel ) continue;
								if ( ac_progress_bars[i] != ary.id ) continue;
								ac_progressbar_set(i, ary.percentage);
								if ( ary.remaining == 0 ) {
									ac_progressbar_unregister(i);
								}
							}
							if ( typeof func == 'function' ) {
								func(ary);
							}
						},
						processId,
						spawn
					);
				},
				secondInterval * 1000
			);
		}
	}
}

function ac_progressbar_unregister(divId) {
	if ( typeof ac_progress_bars[divId] == 'undefined' ) return;
	var pid = ac_progress_bars[divId];
	delete ac_progress_bars[divId];
	if ( typeof ac_progress_timers[pid] == 'undefined' ) return;
	var found = false;
	for ( var i in ac_progress_bars ) {
		if ( ac_progress_bars[i] == pid ) {
			found = true;
			break;
		}
	}
	if ( !found ) {
		window.clearInterval(ac_progress_timers[pid]);
		delete ac_progress_timers[pid];
	}
}

function ac_progressbar_init(divId, val, justValue) {
	if ( !val ) val = 0;
	var rel = $(divId);
	if ( !rel ) return;
	var value = ( Math.round(val * 100) / 100 ) + '%';
	// if using just a value, then no bar is created and just percentage goes into this divId
	if ( justValue ) {
		rel.innerHTML = value;
		return;
	}
	ac_dom_remove_children(rel);
	// add progress label
	rel.appendChild(
		Builder.node(
			'div',
			{ className: 'ac_progress_label', title: value },
			[
				Builder._text(value)
			]
		)
	);
	// add progress bar
	rel.appendChild(
		Builder.node(
			'div',
			{ className: 'ac_progress_bar', style: 'width: ' + value, title: value }
		)
	);
}

function ac_progressbar_set(divId, val) {
	var rel = $(divId);
	if ( !rel ) {
		ac_progressbar_unregister(divId);
		return;
	}
	if ( !val ) val = 0;
	var value = ( Math.round(val * 100) / 100 ) + '%';
	var divs = rel.getElementsByTagName('div');
	if ( divs.length != 2 ) {
		rel.title = value;
		rel.innerHTML = value;
		return;
		//ac_progressbar_init(divId, val);
	}
	// set label
	var lbl = divs[0];
	lbl.title = value;
	lbl.innerHTML = value;
	// set bar
	var bar = divs[1];
	bar.title = value;
	bar.style.width = val + '%';
}
