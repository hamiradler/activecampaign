// ui.js

var ac_ui_prompt_width 	= "300px";
var ac_ui_prompt_top	= "0px";
var ac_ui_prompt_left	= "0px";

// Create an input box for our "prompt".  Label is what you want the box to say above the
// input element.  Cb is the callback function.
//
// Builder has some problems with parsing functions or javascript for its events.
// Particularly, I've noticed, it has some problems with strings in cb.  The safest method
// I've experienced is make cb look like "func()", where func() is the actual callback.

function ac_ui_prompt_make(label, vbl) {
	// need to clear out vbl

	if (!ac_ui_prompt_echeck(vbl))
		return;

	eval(sprintf("%s = null;", vbl));

	var elab = Builder._text(label);
	var einp = Builder.node("input", { style: "border: 1px solid black; font-size: 10px", id: "ac_ui_prompt_input" });
	var esub = Builder.node("input", { type: "button", onclick: sprintf("%s = $('ac_ui_prompt_input').value; ac_ui_prompt_free()", vbl), value: "Submit", style: "font-size: 10px" });

	var ediv = Builder.node("div", { id: "ac_ui_prompt_div", style: sprintf("font-family: Verdana, San-Serif; text-align: center; border: 1px solid #cccccc; background: #eeeeee; padding: 5px; font-size: 10px; position: absolute; width: %s; top: %s; left: %s", ac_ui_prompt_width, ac_ui_prompt_top, ac_ui_prompt_left) });

	ediv.appendChild(elab);
	ediv.appendChild(Builder.node("br"));
	ediv.appendChild(einp);
	ediv.appendChild(Builder._text(" "));
	ediv.appendChild(esub);

	document.body.appendChild(ediv);
}

function ac_ui_prompt_free() {
	var elem = $("ac_ui_prompt_div");

	if (elem !== null)
		document.body.removeChild(elem);
}

function ac_ui_prompt_echeck(str) {
	return str.match(/^[a-zA-Z_][a-zA-Z0-9_]*$/);
}

function ac_ui_prompt(label, vbl, waitfor) {
	var val;

	if (waitfor == "" || !ac_ui_prompt_echeck(waitfor))
		val = "ok";		// anything will do
	else
		val = eval(waitfor);

	if (val !== null)
		ac_ui_prompt_make(label, vbl);
	else {
		window.setTimeout(function() { ac_ui_prompt(label, vbl, waitfor); }, 500);
	}
}

function ac_ui_prompt_waitdo(vars, func) {
	for (var i = 0; i < vars.length; i++) {
		if (!ac_ui_prompt_echeck(vars[i]))
			return;

		var val = eval(vars[i]);

		if (val === null) {
			window.setTimeout(function() { ac_ui_prompt_waitdo(vars, func); }, 500);
			return;
		}
	}

	func();
}

/*
var _a = null;
var _b = null;
var _c = null;

ac_ui_prompt("a", "_a", "");
ac_ui_prompt("b", "_b", "_a");
ac_ui_prompt("c", "_c", "_b");
ac_ui_prompt_waitdo(["_a", "_b", "_c"], function() { alert("done!"); });
*/


/*
	ANCHORS
*/

function ac_ui_anchor_set(newAnchor, data) {
	ac_anchor_old = newAnchor;
	window.location.hash = newAnchor;
}
function ac_ui_anchor_get() {
	return window.location.hash.substr(1);
}
function ac_ui_anchor_changed() {
	var newAnchor = ac_ui_anchor_get();
	if ( newAnchor != ac_anchor_old ) {
		if ( typeof(runPage) == 'function' )
			runPage();
	}
}
function ac_ui_anchor_init() {
	historyTimer = setInterval(ac_ui_anchor_changed, 200);
}
var ac_anchor_old = ac_ui_anchor_get();
var historyTimer = null;



/*
	Functions that trigger session ping method
	Usages:
	- admin  side: ac_ui_session_ping_admin();  // every 10 minutes
	- public side: ac_ui_session_ping_public(); // every 10 minutes
*/
// set session ping
function ac_ui_session_ping_admin() {
	setInterval(
		function() {
			ac_ajax_call_cb('../ac_global/api/public/ping.php', 'sessionping', ac_ui_session_ping_cb);
		},
		10 * 60 * 1000 // every 10 minutes
	);
}

function ac_ui_session_ping_cb(xml) {
	// do nothing
}

// set session ping
function ac_ui_session_ping_public() {
	setInterval(
		function() {
			ac_ajax_call_cb('ac_global/api/public/ping.php', 'sessionping', ac_ui_session_ping_cb);
		},
		10 * 60 * 1000 // every 10 minutes
	);
}


/*
	REAL SIMPLE HISTORY
*/
var ac_rsh = null;
var ac_rsh_enabled = true;

function ac_rsh_listener(newLocation, historyData) {
	// do something
	var msg = 'A history change has occurred!\n\n\nNew Location:\n' + newLocation + '\n\nHistory Data:\n' + historyData;
	alert(msg);
	//ac_loader_show(nl2br(msg));
}

function ac_ui_rsh_listenwrapper(func) {
	return function(loc, hist) {
		if (ac_rsh_enabled)
			func(loc, hist);
	};
}

function ac_ui_rsh_init(listenerFunction, firstTimeRun) {
	// initialize rsh
	ac_rsh = window.dhtmlHistory.create(
		{
			toJSON: function(o) {
				return Object.toJSON(o);
			},
			fromJSON: function(s) {
				return s.evalJSON();
			}
		}
	);
	// set fallback function in case function ain't provided
	if ( typeof(listenerFunction) != 'function' ) {
		listenerFunction = ac_rsh_listener;
	}

	listenerFunction = ac_ui_rsh_listenwrapper(listenerFunction);

	// prototype-style adding envent observers
	Event.observe(
		window,
		'load',
		function() {
			dhtmlHistory.initialize();
			dhtmlHistory.addListener(listenerFunction);
			if ( firstTimeRun && dhtmlHistory.isFirstLoad() ) {
				listenerFunction(dhtmlHistory.currentLocation, null);
			}
		}
	);
}

function ac_ui_rsh_stop() {
	ac_rsh_enabled = false;
}

function ac_ui_rsh_save(newLocation, historyData) {
	dhtmlHistory.add(newLocation, historyData);
}


/*
	AJAX API CALLS SUPPORTING FUNCTIONS
	(needs standardization, naming at least)
*/

// define default english strings if translatables are not provided
var jsAreYouSure = 'Are You Sure?';
var jsAPIfailed = 'Server call failed for unknown reason. Please try your action again...';
var jsLoading = 'Loading...';
var jsResult = 'Changes Saved.';
var jsResult = 'Error Occurred!';

// define vars used
var resultTimer = false; // used in API call functions
var processingDelay = 60; // seconds! used in API call functions (how long to wait?)
var printAPIerrors = false; // if false, will do alert(!), if {} it will discard, if function it will pass message as param or true DOM ref to print there (innerHTML)

// this function notifies about droppedd api call (after time interval has passed)
// it will stop the loading bar and print out the error if listed
function ac_ui_api_stop() {
	if ( resultTimer ) {
		window.clearTimeout(resultTimer); // we don't need this, done elsewhere
		resultTimer = false;
	} else {
		return;
	}
	if ( typeof(printAPIerrors) == 'function' ) {
		printAPIerrors(jsAPIfailed);
	} else if ( typeof(printAPIerrors) == 'object' ) {
		printAPIerrors.innerHTML = jsAPIfailed;
	} else {
		alert(jsAPIfailed);
	}
	ac_loader_hide();
}

// this function should be called right prior to ac_ajax_*
function ac_ui_api_call(customMessage, delay) {
	if ( !delay && typeof(delay) != 'number' ) delay = processingDelay;
	if ( delay == 0 ) delay = 60 * 60 * 24; // 24hrs in seconds
	resultTimer = window.setTimeout(ac_ui_api_stop, delay * 1000);
	ac_loader_show(customMessage);
}

// this function should be called right at the end of ajax callback function
function ac_ui_api_callback() {
	// reset the timer
	if ( resultTimer ) {
		window.clearTimeout(resultTimer);
		resultTimer = false;
	}
	// if processing is shown, hide it, since we got our response back
	ac_loader_hide();
}



/*
	RESULT/ERROR MESSAGES (THE SAME AS LOADER BAR FROM loader.js)
*/

function ac_result_show(txt) {
	// cleanup previous
	if ( ac_loader_visible() ) ac_loader_hide();
	if ( ac_error_visible() ) ac_error_hide();
	if ( txt == '' ) {
		if ( ac_result_visible() ) ac_result_hide();
		return;
	} else if ( !txt ) {
		$('ac_result_text').innerHTML = nl2br(jsResult);
	} else {
		$('ac_result_text').innerHTML = nl2br(txt);
	}
	$('ac_result_bar').className = 'ac_block';
	window.setTimeout(ac_result_hide, 6 * 1000);
}

function ac_result_hide() {
	$('ac_result_bar').className = 'ac_hidden';
}

function ac_result_visible() {
	return $('ac_result_bar').className == 'ac_block';
}

function ac_result_flip() {
	ac_dom_toggle_class('ac_result_bar', 'ac_hidden', 'ac_block');
}



function ac_error_show(txt) {
	// cleanup previous
	if ( ac_loader_visible() ) ac_loader_hide();
	if ( ac_result_visible() ) ac_result_hide();
	if ( txt == '' ) {
		if ( ac_error_visible() ) ac_error_hide();
		return;
	} else if ( !txt ) {
		$('ac_error_text').innerHTML = nl2br(jsError);
	} else {
		$('ac_error_text').innerHTML = nl2br(txt);
	}
	$('ac_error_bar').className = 'ac_block';
	window.setTimeout(ac_error_hide, 6 * 1000);
}

function ac_error_hide() {
	$('ac_error_bar').className = 'ac_hidden';
}

function ac_error_visible() {
	return $('ac_error_bar').className == 'ac_block';
}

function ac_error_flip() {
	ac_dom_toggle_class('ac_error_bar', 'ac_hidden', 'ac_block');
}


// menu init
function ac_ui_menu_init() {
	//if ( document.getElementsByClassName('trapperr').length == 0 )
		initjsDOMenu();
}


/* KEY STOPPERS */
// usage: $('inputID').onkeypress = ac_ui_stopkey_enter;

function ac_ui_stopkey_enter(evt) {
	var evt = ( evt ? evt : ( event ? event : null ) );
	if ( !evt ) return true;
	var node = ( evt.target ? evt.target : ( evt.srcElement ? evt.srcElement : null ) );
	// 13 == ENTER
	if ( evt.keyCode == 13 && node.type == "text" )  {
		// nope, don't submit
		return false;
	}
}



function ac_ui_tab_reset(ul) {
	var rel = $(ul);
	if ( !rel ) return;
	var li = rel.getElementsByTagName('li');
	for ( var i = 0; i < li.length; i++ ) {
		if ( li[i].id && li[i].id.substr(0, 9) == 'main_tab_' ) {
			li[i].className = "othertab";
			var tabname = li[i].id.substr(9);
			var tab = $(tabname);
			if ( tab ) {
				tab.className = 'ac_hidden';
			}
		}
	}
}

// can call it onkeyup
function ac_ui_isnumber(obj) {
	return obj.value.match(/^\d+$/);
}
function ac_ui_numbersonly(obj, allowBlank) {
	if ( obj.value == '' ) return allowBlank;
	// isn't a number
	if ( !ac_ui_isnumber(obj) ) {
		// cutoff the last digit
		obj.value = obj.value.replace(/[^\d]/g, '');
		if ( obj.value == '' ) return allowBlank;
		return ac_ui_isnumber(obj);
	}
	return true;
}

function ac_ui_openwindow(url) {
	var rand = Math.floor(Math.random() * 1000.0);
	var winname = "ac_ui_openwindow_" + rand.toString();
	var w = window.open(url, winname, "width=600,height=500,menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes");
	if ( !w ) return false;
	if ( w.focus ) {
		w.focus();
	}
	return winname;
}

function ac_ui_error_mailer(txt, modal2close) {
	ac_ui_api_callback();
	if ( jsErrorMailerBarMessage != '' ) ac_error_show(jsErrorMailerBarMessage);
	// first close the modal
	if ( modal2close ) ac_dom_toggle_display(modal2close, 'block');

	var msg = '';
	// try trapperr error
	var matches = txt.match(/<i>Message:<\/i> <b>(.*)<\/b><br \/>/);
	if ( matches && matches[1] ) {
		var err = matches[1].split(/<br \/>/);
		if ( err && err[1] ) {
			// use err[1] to populate the error
			msg = err[1];
		}
	// try default php error
	} else if ( matches = txt.match(/<b>Fatal error<\/b>:  <br \/>(.*)/) ) {
		var err = matches[1].split(/<br \/>/);
		if ( err && err[0] ) {
			// use err[1] to populate the error
			msg = err[0];
		}
	// try other default php error
	} else if ( matches = txt.match(/Fatal error: <br \/>(.*)/) ) {
		var err = matches[1].split(/<br \/>/);
		if ( err && err[0] ) {
			// use err[1] to populate the error
			msg = err[0];
		}
	} else {
		msg = nl2br(txt);
	}

	$('error_mailer_message').innerHTML = msg;
	$('error_mailer_message_box').className = ( msg != '' ? 'ac_block' : 'ac_hidden' );

	// show the error screen
	ac_dom_toggle_display('error_mailer', 'block');
	// now reset the text handler
	ac_ajax_handle_text = null;
}
