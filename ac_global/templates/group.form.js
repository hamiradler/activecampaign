var group_form_str_cant_update = '{"You do not have permission to edit groups"|alang|js}';
var group_form_str_cant_insert = '{"You do not have permission to add groups"|alang|js}';
var group_form_str_notitle     = '{"You need to give this group a name."|alang|js}';
{literal}
var group_form_id = 0;

function group_form_defaults() {
	$("form_id").value = 0;
	$("form_title").value = '';
	$("form_descript").value = '';

	if (typeof group_form_defaults_extended == "function")
		group_form_defaults_extended();
}

function group_form_load(id) {
	group_form_defaults();
	group_form_id = id;

	if (id > 0) {
		if (typeof group_can_edit == "function" && !group_can_edit()) {
			ac_ui_anchor_set(group_list_anchor());
			alert(group_form_str_cant_update);
			return;
		}
		ac_ui_api_call(jsLoading);
		if ($("form_submit")) {
			$("form_submit").className = "ac_button_update";
			$("form_submit").value = jsUpdate;
		}
		ac_ajax_call_cb("api.php", "group!ac_group_select_row", group_form_load_cb, id);
	} else {
		if (typeof group_can_add == "function" && !group_can_add()) {
			ac_ui_anchor_set(group_list_anchor());
			alert(group_form_str_cant_insert);
			return;
		}
		if ($("form_submit")) {
			$("form_submit").className = "ac_button_add";
			$("form_submit").value = jsAdd;
		}
		$("form").className = "ac_block";
	}
}

function group_form_load_cb(xml) {
	var ary = ac_dom_read_node(xml, null);
	ac_ui_api_callback();
	group_form_id = ary.id;

	$("form_id").value = ary.id;
	$("form_title").value = ary.title;
	$("form_descript").value = ary.descript;

	if (ary.id == 3) {
		$("form_admin_limitations").style.display = "";
	} else {
		$("form_admin_limitations").style.display = "none";
	}

	if (typeof group_form_load_cb_extended == "function")
		group_form_load_cb_extended(ary);

	$("form").className = "ac_block";
}

function group_form_save(id) {
	var post = ac_form_post($("form"));

	if (post.title == "" || post.title.match(/^ +$/)) {
		alert(group_form_str_notitle);
		return false;
	}

	if (typeof group_form_save_extended_check == "function") {
		if (!group_form_save_extended_check())
			return false;
	}

	ac_ui_api_call(jsSaving);

	if (id > 0)
		ac_ajax_post_cb("api.php", "group!ac_group_update_post", group_form_save_cb, post);
	else
		ac_ajax_post_cb("api.php", "group!ac_group_insert_post", group_form_save_cb, post);
}

function group_form_save_cb(xml) {
	var ary = ac_dom_read_node(xml, null);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(group_list_anchor());
	} else {
		ac_error_show(ary.message);
	}
}
{/literal}
