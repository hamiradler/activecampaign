{jsvar name=filterid var=$filterid}
{jsvar name=filterid_sent var=$filterid_sent}
var privatemessage_str_read = '{"Read"|alang|js}';
var privatemessage_str_unread = '{"Unread"|alang|js}';

{literal}
var privatemessage_table = new ACTable();
var privatemessage_list_sort = "0";
var privatemessage_list_offset = "0";
var privatemessage_list_filter = filterid;
var privatemessage_list_sort_discerned = false;

var setcol_counter = 0;

privatemessage_table.setcol(setcol_counter, function(row, td) {

	td = privatemessage_list_table_cell(row, td, false);

	return Builder.node("input", { type: "checkbox", name: "multi[]", value: row.id, onclick: "ac_form_check_selection_none(this, $('acSelectAllCheckbox'), $('selectXPageAllBox'))" });
});
setcol_counter++;

/* Options links
privatemessage_table.setcol(setcol_counter, function(row) {
	var view = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsOptionView);
	var dele = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsOptionDelete);
	var reply = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsOptionReply);

	var ary = [];

	ary.push(view);
	ary.push(" ");

	if (ac_js_admin.pg_privmsg_delete && $("privatemessage_filter").value == "Inbox") {
		ary.push(dele);
	}

	//ary.push(reply);

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});
setcol_counter++;
*/

privatemessage_table.setcol(setcol_counter, function(row, td) {

	td = privatemessage_list_table_cell(row, td, true);

	if (row.user_to == ac_js_admin["id"]) {
		// Inbox view
		return row.user_from_moreinfo[0].first_name + " " + row.user_from_moreinfo[0].last_name + " (" + row.user_from_moreinfo[0].username + ")";
	}
	else {
		// Sent view
		return row.user_to_moreinfo[0].first_name + " " + row.user_to_moreinfo[0].last_name + " (" + row.user_to_moreinfo[0].username + ")";
	}

});
setcol_counter++;

privatemessage_table.setcol(setcol_counter, function(row, td) {

	td = privatemessage_list_table_cell(row, td, true);

	if (row.threadid > 0) {
		var thread_image = Builder.node("img", {src: "images/thread.gif", title: "Replied"});

		var span_class = "privatemessage_thread";
	}
	else {
		var thread_image = "";

		var span_class = "";
	}

	var subject_link = Builder.node("a", { href: sprintf("#form-%d", row.id) }, row.title);

	var title_display = Builder.node("span", { onmouseover: "ac_tooltip_show('" + ac_b64_encode( strip_tags(row.content) ) + "', 150, '', true)", onmouseout: "ac_tooltip_hide()" }, subject_link);

	var ary = [];

	ary.push(thread_image);

	ary.push(title_display);

	return Builder.node("div", {}, ary);
});
setcol_counter++;

privatemessage_table.setcol(setcol_counter, function(row, td) {

	td = privatemessage_list_table_cell(row, td, true);

	return sql2date(row.cdate).format(ac_js_site["datetimeformat"]);
});
setcol_counter++;

privatemessage_table.setcol(setcol_counter, function(row, td) {

	td = privatemessage_list_table_cell(row, td, true);

	// Hide "Status" column if user is viewing their own Inbox
	if ( $("privatemessage_filter").value == "user_to" ) {
		td.className = 'ac_hidden';
		$("list_table_column_status").className = 'ac_hidden';
	}
	else {
		td.className = '';
		$("list_table_column_status").className = '';
	}

	var ary = [];

	if (row.is_read) {

		var str_read = Builder.node("span", { style: "color: green; font-weight: bold;", onmouseover: "ac_tooltip_show('" + ac_b64_encode( sql2date(row.rdate).format(ac_js_site["datetimeformat"]) ) + "', 150, '', true)", onmouseout: "ac_tooltip_hide()" }, privatemessage_str_read);
		ary.push(str_read);
	}
	else {

		var str_unread = Builder.node("span", { style: "color: red; font-weight: bold;" }, privatemessage_str_unread);

		ary.push(str_unread);
	}

	return Builder.node("div", {}, ary);
});
setcol_counter++;

function privatemessage_list_anchor() {
	return sprintf("list-%s-%s-%s", privatemessage_list_sort, privatemessage_list_offset, privatemessage_list_filter);
}

function privatemessage_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_table_rowgroup";

		$("list_delete_button").className = "ac_hidden";
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}

	$("list_noresults").className = "ac_hidden";

	$("list_delete_button").className = "ac_inline";
	$("loadingBar").className = "ac_hidden";
	$("acSelectAllCheckbox").checked = false;
	$("selectXPageAllBox").className = 'ac_hidden';

	ac_paginator_tabelize(privatemessage_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function privatemessage_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	if (privatemessage_list_filter != filterid && privatemessage_list_filter != filterid_sent)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";

	privatemessage_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(privatemessage_list_anchor());
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, privatemessage_list_sort, privatemessage_list_offset, this.limit, privatemessage_list_filter);

	$("list").className = "ac_block";
}

function privatemessage_list_clear() {
	privatemessage_list_sort = "0";
	privatemessage_list_offset = "0";
	privatemessage_list_filter = filterid;
	$("list_search").value = "";
	privatemessage_search_defaults();
	ac_ui_anchor_set(privatemessage_list_anchor());
}

function privatemessage_list_search() {
	var post = ac_form_post($("list"));

	if (post.privatemessage_filter == "all") {
		privatemessage_list_clear();
	}
	else {
		ac_ajax_post_cb("api.php", "privatemessage!ac_privatemessage_filter_post", privatemessage_list_search_cb, post);
	}
}

function privatemessage_list_search_cb(xml) {
	var ary = ac_dom_read_node(xml, null);

	privatemessage_list_filter = ary.filterid;

	ac_ui_anchor_set(privatemessage_list_anchor());
}

function privatemessage_list_chsort(newSortId) {
	var oldSortId = ( privatemessage_list_sort.match(/D$/) ? privatemessage_list_sort.substr(0, 2) : privatemessage_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( privatemessage_list_sort.match(/D$/) ) {
			// was DESC
			newSortId = privatemessage_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = privatemessage_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old privatemessage_list_sort
		oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	privatemessage_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(privatemessage_list_anchor());
	return false;
}

function privatemessage_list_discern_sortclass() {
	if (privatemessage_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", privatemessage_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (privatemessage_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	privatemessage_list_sort_discerned = true;
}

{/literal}
