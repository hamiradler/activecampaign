var cron_delete_str = '{"Are you sure you want to delete Cron Job %s?"|alang|js}';
var cron_delete_str_multi = '{"Are you sure you want to delete the following Cron Job?"|alang|js}';
var cron_delete_str_cant_delete = '{"You do not have permission to delete Cron Jobs."|alang|js}';
{literal}
var cron_delete_id = 0;
var cron_delete_id_multi = "";

function cron_delete_check(id) {
	if (ac_js_admin.id != 1 || id <= cron_protected) {
		ac_ui_anchor_set(cron_list_anchor());
		alert(cron_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		cron_delete_check_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "cron!ac_cron_select_row", cron_delete_check_cb, id);
}

function cron_delete_check_cb(xml) {
	var ary = ac_dom_read_node(xml);

	ac_dom_remove_children($("delete_list"));

	cron_delete_id = ary.id;
	$("delete_message").innerHTML = sprintf(cron_delete_str, ary.name);
	ac_dom_display_block("delete");	// can't use toggle here in IE
}

function cron_delete_check_multi() {
	if (ac_js_admin.id != 1) {
		ac_ui_anchor_set(cron_list_anchor());
		alert(cron_delete_str_cant_delete);
		return;
	}

	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(cron_list_anchor());
		return;
	}

	var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	// clean up protected ones
	for ( var i = 1; i <= cron_protected; i++ ) {
		sel = ac_array_remove(i, sel, true);
	}
	if ( sel.length == 0 ) {
		alert(jsNothingSelected);
		ac_ui_anchor_set(cron_list_anchor());
		return;
	}

	ac_ajax_call_cb("api.php", "cron!ac_cron_select_array", cron_delete_check_multi_cb, 0, sel.join(","));
	cron_delete_id_multi = sel.join(",");
}

function cron_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = cron_delete_str_multi;

	ac_dom_remove_children($("delete_list"));
	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].name ]));
	} else {
		$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}

	ac_dom_display_block("delete");
}

function cron_delete(id) {
	if (cron_delete_id_multi != "") {
		cron_delete_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "cron!ac_cron_delete", cron_delete_cb, id);
}

function cron_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(cron_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
}

function cron_delete_multi() {
	if (selectAllSwitch) {
		ac_ajax_call_cb("api.php", "cron!ac_cron_delete_multi", cron_delete_multi_cb, "_all", cron_list_filter);
		return;
	}
	ac_ajax_call_cb("api.php", "cron!ac_cron_delete_multi", cron_delete_multi_cb, cron_delete_id_multi);
	cron_delete_id_multi = "";
}

function cron_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(cron_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
	$("acSelectAllCheckbox").checked = false;
}
{/literal}
