var loginsource_list_str_makedefault = '{"Make default"|alang|js}';
{literal}
var loginsource_table = new ACTable();
var loginsource_list_sort = "02";
var loginsource_list_offset = "0";
var loginsource_list_filter = "0";
var loginsource_list_sort_discerned = false;
//
var loginsource_list_length = 0;

loginsource_table.setcol(0, function(row) {
	var edit = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsOptionEdit);

	var ary = [];

	if (ary.file != 'local.php') {
		ary.push(edit);
	}

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});

loginsource_table.setcol(1, function(row) {
	if (!row.enabled)
		return row.order;

	return Builder.node("span", [
		row.order,
		" ",
		row.order > 1 ? Builder.node("a", { href: "#", onclick: sprintf("loginsource_list_reorder(%d, 'u'); return false", row.id) }, "up") : "",
		" ",
		row.order < loginsource_list_length ? Builder.node("a", { href: "#", onclick: sprintf("loginsource_list_reorder(%d, 'd'); return false", row.id) }, "down") : "",
	]);
});

loginsource_table.setcol(2, function(row) {
	if (row.enabled)
		return row.ident;
	else
		return Builder.node("span", [
			Builder.node("strike", row.ident),
			" ",
			Builder.node("em", "(disabled)")
		]);
});

function loginsource_list_anchor() {
	return sprintf("list-%s-%s-%s", loginsource_list_sort, loginsource_list_offset, loginsource_list_filter);
}

function loginsource_list_tabelize(rows, offset) {
	loginsource_list_length = rows.length;
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_table_rowgroup";
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	ac_paginator_tabelize(loginsource_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function loginsource_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	loginsource_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(loginsource_list_anchor());
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, loginsource_list_sort, loginsource_list_offset, this.limit, loginsource_list_filter);

	$("list").className = "ac_block";
}

function loginsource_list_chsort(newSortId) {
	var oldSortId = ( loginsource_list_sort.match(/D$/) ? loginsource_list_sort.substr(0, 2) : loginsource_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( loginsource_list_sort.match(/D$/) ) {
			// was DESC
			newSortId = loginsource_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = loginsource_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old loginsource_list_sort
		if ( oldSortObj ) oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	loginsource_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(loginsource_list_anchor());
	return false;
}

function loginsource_list_discern_sortclass() {
	if (loginsource_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", loginsource_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (loginsource_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	loginsource_list_sort_discerned = true;
}

function loginsource_list_reorder(id, dir) {
	ac_ajax_call_cb("api.php", "loginsource!ac_loginsource_reorder", ac_ajax_cb(loginsource_list_reorder_cb), id, dir);
}

function loginsource_list_reorder_cb(ary) {
	paginators[1].paginate(loginsource_list_offset);
}

function loginsource_list_makedefault(id) {
	ac_ajax_call_cb("api.php", "loginsource!ac_loginsource_recognize", ac_ajax_cb(loginsource_list_makedefault_cb), id, 1);
}

function loginsource_list_makedefault_cb(ary) {
	paginators[1].paginate(loginsource_list_offset);
}

{/literal}
