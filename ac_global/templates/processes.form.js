var processes_form_str_cant_insert = '{"You do not have permission to add Process"|alang|js}';
var processes_form_str_cant_update = '{"You do not have permission to edit Process"|alang|js}';
var processes_form_str_cant_find   = '{"Process not found."|alang|js}';

var processes_form_id = 0;
var processes_form_ary = null;

var processes_form_spawn = false;

{literal}

function processes_form_defaults() {
	$("form_id").value = 0;
	// this is also for edit, not for add
	// fields
	$('restartField').checked = false;
	$('activeField').checked = false;
	$('scheduleField').checked = false;
	$('scheduleField').disabled = false;
	$('spawnField').checked = false;
	$('spawnField').disabled = false;
	// panels
	$('restartBox').className = 'ac_hidden';
	$('activeBox').className = 'ac_hidden';
	$('activeInBox').className = 'ac_hidden';
	$('scheduleBox').className = 'ac_hidden';
	$('dateBox').className = 'ac_hidden';
	$('spawnBox').className = 'ac_hidden';
}

function processes_form_load(id) {
	processes_form_defaults();
	processes_form_id = id;

	if (id > 0) {
		ac_ui_api_call(jsLoading);
		$("form_submit").className = "ac_button_update";
		$("form_submit").value = jsUpdate;
		ac_ajax_call_cb("api.php", "processes!ac_processes_select_row", processes_form_load_cb, id);
	} else {
		if ( !ary.id ) {
			ac_error_show(processes_form_str_cant_find);
			ac_ui_anchor_set(processes_list_anchor());
			return;
		}
		$("form_submit").className = "ac_button_add";
		$("form_submit").value = jsAdd;
		$("form").className = "ac_block";
	}
}

function processes_form_load_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if ( !ary.id ) {
		ac_error_show(processes_form_str_cant_find);
		ac_ui_anchor_set(processes_list_anchor());
		return;
	}
	processes_form_id  = ary.id;
	processes_form_ary = ary;

	// labels
	$('nameLabel').innerHTML = ary.name;
	if ( ary.descript && ary.descript != '' ) {
		$('descriptLabel').innerHTML = ary.descript;
		$('descriptLabel').className = 'ac_block';
	} else {
		$('descriptLabel').className = 'ac_hidden';
	}
	$('cdateLabel').innerHTML = ary.cdate;
	$('ldateLabel').innerHTML = ( ary.ldate ? ary.ldate : jsNotAvailable );
	$('percentageLabel').innerHTML = ary.completed + ' / ' + ary.total;
	$('statusLabel').innerHTML = process_status(ary);

	// progress bar
	ac_progressbar_register(
		'progressBar',
		ary.id,
		ary.percentage,
		( ary.remaining > 0 ? 10 : 0 ),
		( processes_form_spawn && ary.stall && ary.stall > 4 * 60 ),
		processes_form_progress_ihook
	);
	if ( ary.remaining == 0 ) {
		ac_progressbar_unregister('progressBar');
	}

	// fields
	process_form_fields(ary);

	// panels
	$('activeBox' ).className = ( ary.remaining != 0 ? 'ac_block' : 'ac_hidden' );
	$('restartBox').className = ( ary.remaining == 0 ? 'ac_block' : 'ac_hidden' );
	process_form_panels(ary);

	$("form").className = "ac_block";
}

function processes_form_save(id) {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);

	if (id > 0)
		ac_ajax_post_cb("api.php", "processes!ac_processes_update_post", processes_form_save_cb, post);
	else
		ac_ajax_post_cb("api.php", "processes!ac_processes_insert_post", processes_form_save_cb, post);
}

function processes_form_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(processes_list_anchor());
	} else {
		ac_error_show(ary.message);
	}
}



function processes_form_active_changed() {
	$('scheduleField').checked = ( processes_form_ary.stall < 0 );
	$('ldateField').value = ( processes_form_ary.stall < 0 ? processes_form_ary.ldate : '' );
	process_form_panels(processes_form_ary);
}

function processes_form_schedule_changed() {
	$('dateBox').className = ( $('scheduleField').checked ? 'ac_inline' : 'ac_hidden' );
	if ( $('scheduleField').checked ) {
		// date box
		$('ldateField').value = '';
		// spawn box
		$('spawnField').checked = false;
		$('spawnField').disabled = true;
	} else {
		// release spawn box
		$('spawnField').disabled = false;
	}
}

function processes_form_spawn_changed() {
	if ( $('spawnField').checked ) {
		// schedule box
		$('scheduleField').checked = false;
		$('scheduleField').disabled = true;
		$('dateBox').className = 'ac_hidden';
	} else {
		// release schedule box
		$('scheduleField').disabled = false;
	}
}

function process_form_fields(ary) {
	// fields
	$('form_id').value = ary.id;
	$('activeField').checked = ( ary.remaining != 0 && ary.ldate );
	$('scheduleField').checked = ( ary.remaining > 0 && ary.ldate && ary.stall < 0 );
	$('ldateField').value = ( $('scheduleField').checked ? ary.ldate : '' );
}

function process_form_panels(ary) {
	// panels
	$('activeInBox').className = ( $('activeField').checked ? 'ac_block' : 'ac_hidden' );
	if ( $('activeField').checked ) {
		$('scheduleBox').className = 'ac_block';
		$('dateBox').className = ( ary.remaining == 0 || ary.stall < 0 ? 'ac_inline' : 'ac_hidden' );
		$('spawnBox').className = ( ary.remaining == 0 || ary.stall > 4 * 60 ? 'ac_block' : 'ac_hidden' );
	}
}

function processes_form_progress_ihook(ary) {
	var rel = $('percentageLabel');
	if ( rel ) rel.innerHTML = ary.completed + ' / ' + ary.total;
	var rel = $('statusLabel');
	if ( rel ) rel.innerHTML = process_status(ary);
	if ( ary.remaining == 0 ) {
		processes_form_defaults();
		processes_form_ary = ary;
		process_form_fields();
		$('activeBox' ).className = ( ary.remaining != 0 ? 'ac_block' : 'ac_hidden' );
		$('restartBox').className = ( ary.remaining == 0 ? 'ac_block' : 'ac_hidden' );
		process_form_panels();
	}
}

{/literal}
