<?php
if (!@ini_get("zlib.output_compression")) @ob_start("ob_gzhandler");

define('ACPUBLIC', true);
define('ACP_USER_NOAUTH', true);

// require main include file
require_once(dirname(__FILE__) . '/admin/prepend.inc.php');
require_once(ac_global_functions('smarty.php'));

// Preload the language file
ac_lang_get('public');

$smarty = new AC_Smarty('public');
$smarty->assign('build', $thisBuild);
$smarty->assign("version", str_replace(".", "", $site['version']));

$action = ac_http_param('action');
$smarty->assign("action", $action);

$smarty->assign("_", $site["p_link"]);

$smarty->assign('site', $site);
$smarty->assign('plink', ac_site_plink());

$smarty->assign('jsSite', ac_array_keys_remove($site, array('serial', 'av', 'avo', 'ac', 'acu', 'acec', 'acar', 'acad', 'acff', 'accj', 'smpass')));
$smarty->assign('jsAdmin', ac_array_keys_remove($admin, array('password')));

$smarty->sendCTheader('Content-Type: text/javascript; charset=' . _i18n("utf-8"));
$smarty->display("mainjs.inc.js");

?>
