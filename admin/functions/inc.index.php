<?php

// browser check for login screen: check if < IE8
$ua = ( isset($_SERVER['HTTP_USER_AGENT']) ? (string)$_SERVER['HTTP_USER_AGENT'] : '' );
$browser_alert = false;
$browser_version = false;
$browser_name = '';

if ( preg_match('|MSIE ([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 9 );
	$browser_name = 'explorer';
} elseif ( preg_match('|Chrome/([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 7 );
	$browser_name = 'chrome';
} elseif ( preg_match('|Firefox/([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 3 );
	$browser_name = 'firefox';
} elseif ( preg_match('|iPhone|', $ua, $matched) && preg_match('|Version/([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 4 );
	$browser_name = 'iphone';
} elseif ( preg_match('|iPod|', $ua, $matched) && preg_match('|Version/([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 4 );
	$browser_name = 'ipod';
} elseif ( preg_match('|iPad|', $ua, $matched) && preg_match('|Version/([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 4 );
	$browser_name = 'ipad';
} elseif ( preg_match('|Safari|', $ua, $matched) && preg_match('|Version/([0-9].[0-9]{1,2})|', $ua, $matched) ) {
	$browser_version = $matched[1];
	$browser_alert = ( $matched[1] < 4 );
	$browser_name = 'safari';
}
$smarty->assign('browser_version', $browser_version);
$smarty->assign('browser_alert', $browser_alert);
$smarty->assign('browser_name', $browser_name);



$cookie = ac_prefix("acp_globalauthuser_cookie");
$username = ( isset($_COOKIE[$cookie]) ? $_COOKIE[$cookie] : '' );
$smarty->assign('username', $username);


// dealing with error messages
$error_mesg = '';
// get error message if defined
if ( isset($_GET['error_mesg']) ) $error_mesg = $_GET['error_mesg'];
$smarty->assign('error_mesg', $error_mesg);


// pass main info to smarty
if(($site["acpow"])){ $site["acpow"] = base64_decode($site["acpow"]); } else { $site["acpow"] = 'Email Marketing'; }

$smarty->assign('site', $site);
$smarty->assign('admin', $admin);
$smarty->assign('languages', $languages);

$css = (string)ac_sql_select_one("SELECT admin_template_css FROM #branding WHERE groupid = '3'");
$smarty->assign("admin_template_css", $css);

$smarty->assign('thisURL', ac_http_geturl());
$smarty->assign('plink', ac_site_plink());

$smarty->assign('action', $action);

$smarty->assign('__ishosted', isset($GLOBALS['_hosted_account']));
$smarty->assign("hostedaccount", (isset($GLOBALS['_hosted_account']) ? $_SESSION[$GLOBALS["domain"]] : false));
$smarty->assign('build', $thisBuild);
$smarty->assign("version", str_replace(".", "", $site['version']));

require_once(ac_global_functions('browser.php'));
$smarty->assign('ieCompatFix', ac_browser_ie_compat());



?>