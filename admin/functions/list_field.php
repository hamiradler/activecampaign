<?php

function list_field_getfielddata(&$subscriber, &$fields) {
	$fieldlist = ac_array_extract($fields, "id");
	$fieldstr  = implode("','", $fieldlist);
	$ary       = ac_sql_select_array("
		SELECT
			fieldid,
			val
		FROM
			#field_value
		WHERE
			fieldid IN ('$fieldstr')
		AND
			relid    = '$subscriber[id]'
		ORDER BY
			fieldid
	");

	return $ary;
}

function list_field_getfields($campaignid) {
	$campaignid = intval($campaignid);
	return ac_sql_select_array("
		SELECT DISTINCT
			f.id,
			f.title
		FROM
			#field f,
			#field_rel r
		WHERE
			f.id = r.fieldid
		AND
			( r.relid IN (
				SELECT
					cl.listid
				FROM
					#campaign_list cl
				WHERE
					cl.campaignid = '$campaignid'
			)
			OR r.relid='0' )
		ORDER BY
			f.id
	");
}

function list_field_getdefaults($listids) {
	# Return an array of the fields which have default values according to the given $listids
	# (and including any global fields).

	$ary     = array();
	$liststr = implode("','", $listids);
	$rs      = ac_sql_query("
		SELECT
			f.id,
			IF(f.`type` = 2, f.expl, f.onfocus) AS a_default
		FROM
			#field f
		WHERE
			IF(f.`type` = 2, f.expl, f.onfocus) != ''
		AND
			f.id IN (
				SELECT
					r.fieldid
				FROM
					#field_rel r
				WHERE
					r.relid IN ('0', '$liststr')
			)
	");

	while ($row = ac_sql_fetch_assoc($rs)) {
		$ary[$row["id"]] = $row["a_default"];
	}

	return $ary;
}

function list_field_insert_post() {
	require_once ac_admin("functions/field.php");
	if (!isset($_POST["p"]) || !$_POST["p"]) {
		return ac_ajax_api_result(false, _a("Please provide at least one list."));
	}
	$lists = $_POST["p"]; // ac_http_param won't work because $_GET[p] is set
	$options = ac_http_param_forcearray("options");
	$title = ac_http_param("title");
	$perstag = ac_http_param("perstag");

	if (!$title) {
		return ac_ajax_api_result(false, _a("You must give your field a name"));
	}

	if (in_array(0, $lists) && !ac_admin_ismaingroup()) {
		return ac_ajax_api_result(false, _a("You are not allowed to add global custom fields"));
	}

	if ( field_title_taken($title, "", $lists) ) {
		return ac_ajax_api_result(false, sprintf(_a("There is already a field named '%s' -- choose another name"), $title));
	}

	if ( field_perstag_taken($perstag, "") ) {
		return ac_ajax_api_result(false, sprintf(_a("There is already a personalization tag named '%s' -- choose another name"), $perstag));
	}

	// 1 = Text Field, 2 = Text Box, 3 = Checkbox, 4 = Radio, 5 = Dropdown, 6 = Hidden field, 7 = List Box, 8 = Checkbox Group, 9 = Date
	$types = array(
		"1" => "text",
		"2" => "textarea",
		"3" => "checkbox",
		"4" => "radio",
		"5" => "dropdown",
		"6" => "hidden",
		"7" => "listbox",
		"9" => "date",
	);
	$type_id = ac_http_param("type");
	if ( !isset($types[$type_id]) ) {
		return ac_ajax_api_result(false, _a("Invalid field type"));
	}
	$type = $types[$type_id];

	$ary = array(
		"title" => $title,
		"type" => $type,
		"isrequired" => (int)ac_http_param("req"),
		"perstag" => $perstag,
		"show_in_list" => (int)ac_http_param("show_in_list"),
	);

	if ($type == "textarea") {
		$textarea_rows = (int)ac_http_param("rows");
		$textarea_cols = (int)ac_http_param("cols");
		if (!$textarea_rows) $textarea_rows = 5;
		if (!$textarea_cols) $textarea_cols = 40;
		$ary["rows"] = $textarea_rows;
		$ary["cols"] = $textarea_cols;
	}
//dbg($ary);

	$fieldid = ac_cfield_insert("#field", $ary);

	if ($ary["perstag"] == "") {
		$up = array(
			"perstag" => sprintf("PERS_%d", $fieldid),
		);

		ac_sql_update("#field", $up, "id = '$fieldid'");
	}

	// list relations
	foreach ($lists as $listid) {

		$ins = array(
			"fieldid" => $fieldid,
			"relid" => $listid,
		);

		$sql = ac_sql_insert("#field_rel", $ins);

		if (!$sql)
			return ac_ajax_api_result(false, _a("Could not relate field"));
	}

	// field options
	if ($type == "checkbox" || $type == "radio" || $type == "dropdown" || $type == "listbox") {
		if (count($options) > 0) {
			ac_sql_query("DELETE FROM #field_option WHERE fieldid = '$fieldid'");
			foreach ($options as $k => $option) {
				$ins = array(
					"fieldid" => $fieldid,
					"orderid" => $k + 1,
					"value" => $option,
					"label" => $k,
				);

				ac_sql_insert("#field_option", $ins);
			}
		}
	}

	$r = array();
	$r["fieldid"] = $fieldid;
	return ac_ajax_api_added(_a("Custom Field"), $r);
}

function list_field_update_post() {
	require_once ac_admin("functions/field.php");
	$fieldid = (int)ac_http_param("id");
	$lists = $_POST["p"]; // ac_http_param won't work because $_GET[p] is set
	$options = ac_http_param_forcearray("options");
	$title = ac_http_param("title");
	$perstag = ac_http_param("perstag");

	$original = ac_sql_select_row("SELECT * FROM #field WHERE id = '$fieldid'");

	if (!$original) {
		return ac_ajax_api_result(false, _a("Invalid field ID"));
	}

	if (!$title) {
		return ac_ajax_api_result(false, _a("You must give your field a name"));
	}

	if (in_array(0, $lists) && !ac_admin_ismaingroup()) {
		return ac_ajax_api_result(false, _a("You are not allowed to add global custom fields"));
	}

	if ( field_title_taken($title, $original["title"], $lists) ) {
		return ac_ajax_api_result(false, sprintf(_a("There is already a field named '%s' -- choose another name"), $title));
	}

	if ( field_perstag_taken($perstag, $original["perstag"]) ) {
		return ac_ajax_api_result(false, sprintf(_a("There is already a personalization tag named '%s' -- choose another name"), $perstag));
	}

	// 1 = Text Field, 2 = Text Box, 3 = Checkbox, 4 = Radio, 5 = Dropdown, 6 = Hidden field, 7 = List Box, 8 = Checkbox Group, 9 = Date
	$types = array(
		"1" => "text",
		"2" => "textarea",
		"3" => "checkbox",
		"4" => "radio",
		"5" => "dropdown",
		"6" => "hidden",
		"7" => "listbox",
		"9" => "date",
	);
	$type_id = ac_http_param("type");
	if ( !isset($types[$type_id]) ) {
		return ac_ajax_api_result(false, _a("Invalid field type"));
	}
	$type = $types[$type_id];

	$ary = array(
		"title" => $title,
		"type" => $type,
		"isrequired" => (int)ac_http_param("req"),
		"perstag" => $perstag,
		"show_in_list" => (int)ac_http_param("show_in_list"),
	);

	if ($type == "textarea") {
		$textarea_rows = (int)ac_http_param("rows");
		$textarea_cols = (int)ac_http_param("cols");
		if (!$textarea_rows) $textarea_rows = 5;
		if (!$textarea_cols) $textarea_cols = 40;
		$ary["rows"] = $textarea_rows;
		$ary["cols"] = $textarea_cols;
	}
//dbg($ary);

	if ($ary["perstag"] == "") {
		$ary["perstag"] = sprintf("PERS_%d", $fieldid);
	}

	ac_sql_update("#field", $ary, "id = '$fieldid'");

	// list relations
	ac_sql_query("DELETE FROM #field_rel WHERE fieldid = '$fieldid'");
	foreach ($lists as $listid) {
		$ins = array(
			"fieldid" => $fieldid,
			"relid" => $listid,
		);

		$sql = ac_sql_insert("#field_rel", $ins);

		if (!$sql)
			return ac_ajax_api_result(false, _a("Could not relate field"));
	}

	// field options
	ac_sql_query("DELETE FROM #field_option WHERE fieldid = '$fieldid'");
	if ($type == "checkbox" || $type == "radio" || $type == "dropdown" || $type == "listbox") {
		if (count($options) > 0) {
			foreach ($options as $k => $option) {
				$ins = array(
					"fieldid" => $fieldid,
					"orderid" => $k + 1,
					"value" => $option,
					"label" => $k,
				);

				ac_sql_insert("#field_option", $ins);
			}
		}
	}

	$r = array();
	return ac_ajax_api_updated(_a("Custom Field"), $r);
}

function list_field_delete($fieldid) {
	ac_cfield_delete_field('#field', '#field_rel', 'fieldid', $fieldid);
	$r = array();
	$r['fieldid'] = $fieldid;
	return ac_ajax_api_deleted(_a("Custom Field"), $r);
}

function list_field_select_nodata_rel($fieldids, $filters = array()) {
	if ($fieldids && $fieldids != "all") {
		$fieldids = explode(",", $fieldids);
		$lists_group = implode("','", $GLOBALS["admin"]["lists"]);
		// get the fields for this group's lists
		$fields_group = ac_cfield_select_nodata_rel('#field', '#field_rel', "r.relid IN ('$lists_group')");
		$fields_group_ids = array();
		foreach ($fields_group as $field) $fields_group_ids[] = $field["id"];
		foreach ($fieldids as $fieldid) {
			if ( preg_match("/^%/", trim($fieldid)) && preg_match("/%$/", trim($fieldid)) ) {
				// if it starts and ends with a percentage sign, it is a personalization tag
				// (you can pass a field ID, or pers tag, since both are unique)
				$pers_tag = ac_sql_escape(trim($fieldid, "%"));
				// check if this pers tag actually exists for a field
				$field_row_id = (int)ac_sql_select_one("SELECT id FROM #field WHERE perstag = '$pers_tag'");
				if ($field_row_id) {
					$fieldids[] = $field_row_id;
				}
				else {
					// invalid pers tag
					return ac_ajax_api_result(false, _a("Personalization tag") . " %" . $pers_tag . "% " . _a("cannot be found."));
				}
			}
			if (!in_array($fieldid, $fields_group_ids)) {
				// either a global field, or a field for another list (that this user does not have access to)
				$is_list_specific = (int)ac_sql_select_one("SELECT relid FROM #field_rel WHERE fieldid = '" . ac_sql_escape($fieldid) . "' LIMIT 1");
				if ($is_list_specific) {
					// relid > 0, so it's a list field that is not part of this user's group's lists
					return ac_ajax_api_result(false, _a("You do not have permission to access field ID " . $fieldid));
				}
				else {
					// global field
				}
			}
		}
		//$fieldids = array_merge($fields_group_ids, $fieldids);
		$fieldids = implode("','", $fieldids);
	}
	elseif ($fieldids == "all") {
	  if ( !in_array(0, $GLOBALS["admin"]["lists"]) ) {
	    // include global custom fields
      $GLOBALS["admin"]["lists"][0] = 0;
    }
    $lists_group = implode("','", $GLOBALS["admin"]["lists"]);
    // get the fields for this group's lists
    $fields_group = ac_cfield_select_nodata_rel('#field', '#field_rel', "r.relid IN ('$lists_group')");
		$fields_group_ids = array();
		foreach ($fields_group as $field) $fields_group_ids[] = $field["id"];
		$fieldids = implode("','", $fields_group_ids);
	}
	else {
	  $fieldids = "";
	}

	$where = "f.id IN ('" . $fieldids . "')";
	$fields = ac_cfield_select_nodata_rel("#field", "#field_rel", $where);

	foreach ($fields as $k => $field) {
		// get each field's lists (it only shows one list as a result of above function/query)
		$fields[$k]["lists"] = ac_sql_select_list("SELECT relid FROM #field_rel WHERE fieldid = '" . $field["id"] . "'");
	}

	return $fields;
}

?>
