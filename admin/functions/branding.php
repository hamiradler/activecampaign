<?php

require_once ac_global_classes("select.php");

function branding_select_query(&$so) {
	return $so->query("
		SELECT
			*
		FROM
			#branding
		WHERE
			[...]
	");
}

function branding_select_row($id, $fromgroup = 0) {
	$id = intval($id);
	if ( $id < 2 ) $id = 3;
	$so = new AC_Select;
	$so->push("AND groupid = '$id'");

	$row = ac_sql_select_row(branding_select_query($so));

	// If there is no branding row for the group, insert one, but only if that group ID exists
	if (!$row) {
		$ary = array(
			"groupid" => $id,
			"site_name" => 'Email Marketing Software',
			"site_logo" => '',
		);
		if ( isset($GLOBALS['__languageArray']) ) {
			$ary['site_name'] = _i18n('Email Marketing Software');
		}

		if ($fromgroup > 0) {
			$tmp = branding_select_row($fromgroup, 0);
			$ary["site_name"]         = $tmp["site_name"];
			$ary["site_logo"]         = $tmp["site_logo"];
			$ary["header_text"]       = $tmp["header_text"];
			$ary["header_text_value"] = $tmp["header_text_value"];
			$ary["header_html"]       = $tmp["header_html"];
			$ary["header_html_value"] = $tmp["header_html_value"];
			$ary["footer_text"]       = $tmp["footer_text"];
			$ary["footer_text_value"] = $tmp["footer_text_value"];
			$ary["footer_html"]       = $tmp["footer_html"];
			$ary["footer_html_value"] = $tmp["footer_html_value"];
			$ary["admin_template_htm"] = $tmp["admin_template_htm"];
			$ary["admin_template_css"] = $tmp["admin_template_css"];
			$ary["public_template_htm"] = $tmp["public_template_htm"];
			$ary["public_template_css"] = $tmp["public_template_css"];
			$ary["copyright"]         = $tmp["copyright"];
			$ary["version"]           = $tmp["version"];
			$ary["links"]             = $tmp["links"];
			$ary["demo"]              = $tmp["demo"];
		}

		$sql = ac_sql_insert("#branding", $ary);

		$row = ac_sql_select_row(branding_select_query($so));
	}

	return $row;
}

?>
