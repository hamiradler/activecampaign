<?php

function form_send_message($listid, $target, $sub, $tags = array()) {
	// We're not sure exactly which form to use, so pick one based on target/list.

	$formid = (int)ac_sql_select_one("
		SELECT
			f.id
		FROM
			#form f,
			#form_list l
		WHERE
			l.formid = f.id
		AND
			l.listid = '$listid'
		AND
			f.target = '$target'
		ORDER BY
			f.id
		LIMIT
			1
	");

	if ($formid)
		form_send_message_exact($formid, $listid, $sub, $tags);
}

function form_send_message_exact($formid, $listid, $sub, $tags = array()) {
	$formid = (int)$formid;
	$listid = (int)$listid;

	$form = form_select_row($formid);
	$messageid = $form["messageid"];

	// Double check if we should even be here.
	if (!$form["sendoptin"])
		return;

	// No list?  Pick the useconf one.  If no useconf, there's probably only one list in the table
	// -- pick that.
	if ($listid == 0) {
		$rs = ac_sql_query("SELECT listid, useconf FROM #form_list WHERE formid = '$formid'");
		while ($row = ac_sql_fetch_assoc($rs)) {
			$listid = $row["listid"];
			if ($row["useconf"])
				break;
		}
	}

	// No message associated with this form.
	if ($messageid == 0 || $listid == 0)
		return;

	$message = message_select_row($messageid);

	// This is really weird.
	if (!$message)
		return;

	$admin = ac_admin_get();
	$list = list_select_row($listid);
	$options = array();

	$options['userid'] = $list['userid'];
	$options['charset'] = _i18n('utf-8');
	$options['encoding'] = _i18n('quoted-printable');

	// Bounces
	$options['bounce'] = ac_sql_select_one("email", "#bounce", "id = 1");
	$bso = new AC_Select();
	if (!isset($GLOBALS["_hosted_account"])) {
		$bso->push("AND l.listid IN ('$listid')");
	}
	require_once ac_admin("functions/campaign.select.php");
	$bounces = campaign_list_bounces($bso);
	if ( $bounces ) {
		$randombounce = array_rand($bounces);
		$options['bounce'] = $randombounce['email'];
	} else {
		$randombounce = ac_sql_select_row("SELECT * FROM #bounce b, #bounce_list l WHERE b.id = l.bounceid AND l.listid = '$list[id]'");
		if ( $randombounce ) $options['bounce'] = $randombounce['email'];
	}
	if ( isset($GLOBALS['_hosted_account']) ) {
		$options['bounce'] = str_replace("@", "-0@", $options['bounce']);
	}

	// Headers
	$hso = new AC_Select();
	$hso->push("AND l.listid IN (" . $list['id'] . ")");
	$options['headers'] = campaign_list_headers($hso);

	$header = $footer = $header_alt = $footer_alt = "";

	// Format
	$header_alt = ($admin["brand_header_text"]) ? $admin["brand_header_text_value"] : "";
	$altBody = $header_alt . $message["text"] . $footer_alt;
	$footer_alt = ($admin["brand_footer_text"]) ? $admin["brand_footer_text_value"] : "";

	if ( isset($GLOBALS['__hosted_footer_text']) ) {
		$tmpcontent = personalization_basic($header_alt . $altBody . $footer_alt, $message['subject']);
		//$abuseLink = ac_site_plink('index.php?action=abuse&nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid');
		$unsubLink = ac_site_plink('box.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2');
		$unsubLink2 = ac_site_plink('proc.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&act=unsub');
		$hasUnsub = ac_str_instr($unsubLink, $tmpcontent) || ac_str_instr($unsubLink2, $tmpcontent);
		//$hasAbuse = ac_str_instr($abuseLink, $tmpcontent);
		$hasSender = (ac_str_instr('%SENDER-INFO%', $tmpcontent) || ac_str_instr('%SENDER-INFO-SINGLELINE%', $tmpcontent));
		if ( !$hasUnsub or !$hasSender ) {
			$footer_alt .= hosted_footer_personalize($GLOBALS['__hosted_footer_text']);
		}
	}

	$options['altBody'] = subscriber_personalize($sub, $listid, $formid, $altBody, 'sub', 'text');

	$header = ($admin["brand_header_html"]) ? $admin["brand_header_html_value"] : "";
	$body = $message["html"];
	$footer = ($admin["brand_footer_html"]) ? $admin["brand_footer_html_value"] : "";

	if ( isset($GLOBALS['__hosted_footer_html']) ) {
		$tmpcontent = personalization_basic($header . $body . $footer, $message['subject']);
		//$abuseLink = ac_site_plink('index.php?action=abuse&nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid');
		$unsubLink = ac_site_plink('box.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2');
		$unsubLink2 = ac_site_plink('proc.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&act=unsub');
		$hasUnsub = ac_str_instr($unsubLink, $tmpcontent) || ac_str_instr($unsubLink2, $tmpcontent);
		//$hasAbuse = ac_str_instr($abuseLink, $tmpcontent);
		$hasSender = (ac_str_instr('%SENDER-INFO%', $tmpcontent) || ac_str_instr('%SENDER-INFO-SINGLELINE%', $tmpcontent));
		if ( !$hasUnsub or !$hasSender ) {
			$footer .= hosted_footer_personalize($GLOBALS['__hosted_footer_html']);
		}
	}

	$body = array(
		'html' => $header . $body . $footer,
		'text' => $header_alt . $altBody . $footer_alt,
	);

	$body = mail_sender_info($list["id"], $body);

	// If we have a %LINK% tag, replace it here.
	foreach ($tags as $key => $val) {
		$body['html'] = str_replace($key, $val, $body['html']);
		$body['text'] = str_replace($key, $val, $body['text']);
	}

	// Personalization tags
	$body['html'] = subscriber_personalize($sub, $listid, $formid, $body['html'], 'sub', 'html');
	$body['text'] = subscriber_personalize($sub, $listid, $formid, $body['text'], 'sub', 'text');

	$senderheader = false;
	if (isset($GLOBALS["_hosted_account"]))
		$senderheader = true;

	$site = ac_site_get();

	if ($senderheader && $site['onbehalfof'] && isset($GLOBALS["domain"])) {
		$info = $_SESSION[$GLOBALS["domain"]];
		$host = (string)ac_sql_select_one("SELECT host FROM #mailer WHERE id = '1'");

		if (preg_match('/(astirx.com|acemserv.com|acems\d.com)$/', $host)) {
			$h = sprintf('<%s@%s>', $info["account"], $host);
			if ( trim($message["fromname"]) )
				$h = '"' . trim($message["fromname"]) . '" ' . $h;
			$options['headers'][] = array('name' => "Sender", 'value' => $h);
			$options['headers'][] = array('name' => "X-Sender", 'value' => $h);
			$options['headers'][] = array('name' => "X-Report-Abuse", 'value' => 'Please report abuse at http://www.activecampaign.com/contact/?type=abuse');
		}
	}

	if ($message["fromemail"] == "")
		$message["fromemail"] = $admin["email"];

	$subject = subscriber_personalize($sub, $listid, 0, $message["subject"], 'sub', 'text');
	ac_mail_send($message["format"], $message["fromname"], $message["fromemail"], $body, $subject, $sub["email"], $sub["first_name"].' '.$sub["last_name"], $options);
}

function form_forward($subid, $cid, $mid, $name, $email, $rcpt_names, $rcpt_emails) {
	// Send the "forward-to-a-friend" message to people.

	if (forward_toomuch($subid))
		return;

	$campaign = campaign_select_row($cid);

	// We can't trust any user input for the message; rebuild it here.
	$body =
		_a("This message is being sent by") . " " . $email . "\n\n" .
		sprintf(_a("The sender thought the mailing entitled '%s' would be of interest to you."), $campaign["messages"][0]["subject"]) . "\n\n" .
		_a("To view the mailing, please visit") . "\n" .
		rewrite_plink("social", "c=" . md5($cid) . "." . $mid);

	$options = array();

	// Bounces
	$options['bounce'] = ac_sql_select_one("email", "#bounce", "id = 1");
	if ( isset($GLOBALS['_hosted_account']) ) {
		$options['bounce'] = str_replace("@", "-0@", $options['bounce']);
	}

	$senderheader = false;
	if (isset($GLOBALS["_hosted_account"]))
		$senderheader = true;

	$site = ac_site_get();

	if ($senderheader && $site['onbehalfof'] && isset($GLOBALS["domain"])) {
		$info = $_SESSION[$GLOBALS["domain"]];
		$host = (string)ac_sql_select_one("SELECT host FROM #mailer WHERE id = '1'");

		if (preg_match('/(astirx.com|acemserv.com|acems\d.com)$/', $host)) {
			$h = sprintf('<%s@%s>', $info["account"], $host);
			//if ( trim($message["fromname"]) ) $h = '"' . trim($message["fromname"]) . '" ' . $h;
			if ( trim($name) ) $h = '"' . trim($name) . '" ' . $h;
			$options['headers'][] = array('name' => "Sender", 'value' => $h);
			$options['headers'][] = array('name' => "X-Sender", 'value' => $h);
			$options['headers'][] = array('name' => "X-Report-Abuse", 'value' => 'Please report abuse at http://www.activecampaign.com/contact/?type=abuse');
		}
	}

	$subject = sprintf(_a("%s has forwarded you a message"), $name);

	for ($i = 0; $i < 10; $i++) {
		if (!isset($rcpt_emails[$i]) || !isset($rcpt_names[$i]))
			break;

		$rcpt_name = $rcpt_names[$i];
		$rcpt_email = $rcpt_emails[$i];

		if (ac_str_is_email($rcpt_email)) {
			if (forward_toomuch($subid))
				return;

			$user_ip = $_SERVER['REMOTE_ADDR'];
			// Save to DB
			$ary = array(
				"subscriberid" => $subid,
				"campaignid" => $cid,
				"messageid" => $mid,
				"email_from" => $email,
				"name_from" => $name,
				"email_to" => $rcpt_email,
				"name_to" => $rcpt_name,
				"brief_message" => '',
				"=tstamp" => "NOW()",
				"=ip" => "INET_ATON('$user_ip')",
			);
			$sql = ac_sql_insert("#forward", $ary);
			$did = (int)ac_sql_insert_id();
			// update campaign
			ac_sql_update_one("#campaign", "=forwards", "forwards + 1", "id = '$cid'");
			ac_sql_update_one("#campaign_deleted", "=forwards", "forwards + 1", "id = '$cid'");

			subscriber_action_dispatch("forward", subscriber_select_row($subid), null, $campaign, null);

			ac_mail_send("text", $name, $email, $body, $subject, $rcpt_email, $rcpt_name, $options);
			forward_log($subid);
			stats_activity_log('forward', $subid, $cid, $did);
		}
	}
}

?>
