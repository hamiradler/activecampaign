<?php

require_once ac_global_functions("ajax.php");
require_once ac_global_functions("htmltext.php");
require_once ac_admin("functions/html.php");
require_once ac_admin("functions/personalization.php");
require_once ac_admin("functions/message.php");

$GLOBALS["campaign_save_id"] = 0;
$GLOBALS["campaign_save_order"] = array(
	"type",
	"list",
	"template",
	"message",
	"text",
	"splitmessage",
	"splittext",
	"summary",
	"result",
);

# This is sort of a hack; we try to determine if someone hit the back button by looking at our position
# now versus their old position.
$GLOBALS["campaign_save_backbutton"] = false;
$GLOBALS["campaign_save_after"] = 'nothing';

// Save result (needed since we are redirecting at the end).
if (!isset($_SESSION["campaign_save_result"]))
	$_SESSION["campaign_save_result"] = array();

# Where we are.
if (!isset($_SESSION["campaign_save_position"]))
	$_SESSION["campaign_save_position"] = array();

function campaign_save() {
	$step   = (string)ac_http_param("step");
	$after  = (string)ac_http_param("aftersave");
	$id     = (int)ac_http_param("id");

	if (!$step) {
		# We need to know what we're saving.
		return;
	}

	if ($id > 0) {
		$GLOBALS["campaign_save_id"] = $id;

		// check if we should save this campaign
		$campaign = ac_sql_select_row("SELECT * FROM #campaign WHERE id = '$id'");
		if ( !$campaign ) return;
		// if not draft
		if ( !in_array($campaign['status'], array(0, 1, 3, 6)) ) {
			// it's NOT draft, scheduled, paused nor disabled
			campaign_save_error(_a("Campaign cannot be saved (it is neither a draft, scheduled, paused, nor disabled)."), $id);
			return;
		}
	}

	$GLOBALS["campaign_save_after"] = $after;

	switch ($step) {
		case "type":
			campaign_save_type();
			break;

		case "list":
			campaign_save_list();
			break;

		case "template":
			campaign_save_template();
			break;

		case "message":
			campaign_save_message();
			break;

		case "text":
			campaign_save_text();
			break;

		case "splitmessage":
			campaign_save_splitmessage();
			break;

		case "splittext":
			campaign_save_splittext();
			break;

		case "summary":
			campaign_save_summary();
			break;

		default:
			break;
	}

	$id = $GLOBALS['campaign_save_id'];
	if ( $id ) {
		$admin = ac_admin_get();
		$up = array(
			"userid" => $admin["id"],
			"=mdate" => "NOW()",
		);
		ac_sql_update("#campaign", $up, "id = '$id'");
	}
}

function campaign_save_markpos($which, $campaignid) {
	if (!isset($_SESSION["campaign_save_position"][$campaignid]))
		$_SESSION["campaign_save_position"][$campaignid] = "";

	$oldpos = $_SESSION["campaign_save_position"][$campaignid];
	$index  = array_search($oldpos, $GLOBALS["campaign_save_order"]);
	$newindex = array_search($which, $GLOBALS["campaign_save_order"]);

	if ($index !== false && $newindex !== false) {
		# We may have hit the back button.

		if ($index == ($newindex + 1)) {
			# Looks like we did.
			$GLOBALS["campaign_save_backbutton"] = true;
		}
	}

	# Save the position in the laststep field.
	if ($campaignid > 0) {
		$campaignid = (int)$campaignid;
		$current = (string)ac_sql_select_one("SELECT laststep FROM #campaign WHERE id = '$campaignid'");

		$cindex = array_search($current, $GLOBALS["campaign_save_order"]);

		if ($cindex === false || $cindex < $newindex) {
			$up = array(
				"laststep" => $which,
			);

			ac_sql_update("#campaign", $up, "id = '$campaignid'");
		}
	}

	$_SESSION["campaign_save_position"][$campaignid] = $which;
}

function campaign_save_wentback() {
	return $GLOBALS["campaign_save_backbutton"];
}

function campaign_save_default($name, $type) {
	$admin = ac_admin_get();

	$ins = array(
		"id" => campaign_nextid(),
		"name" => $name,
		"type" => $type,
		"userid" => $admin["id"],
		"laststep" => "type",
		"=cdate" => "NOW()",
		"tracklinksanalytics" => 0,
		"trackreadsanalytics" => 0,
	);

	if (isset($_SERVER["REMOTE_ADDR"]) && ac_str_noipv6($_SERVER["REMOTE_ADDR"]))
		$ins["=ip4"] = "INET_ATON('$_SERVER[REMOTE_ADDR]')";

	if (ac_http_param("debug"))
		$ins["mailer_log_file"] = 4;

	$r = ac_sql_insert("#campaign", $ins);
	cache_clear("canSendCampaign");

	if (!$r) {
		campaign_save_error(_a("Could not save campaign."));
		ac_log("Couldn't create campaign");
		return;
	}

	$GLOBALS["campaign_save_id"] = $id = ac_sql_insert_id();
	campaign_updatenextid($id);


}

function campaign_save_error($message, $campaignid = null, $arr = array()) {
	if ( is_null($campaignid) ) $campaignid = $GLOBALS["campaign_save_id"];
	$_SESSION["campaign_save_result"][$campaignid]["succeeded"] = false;
	$_SESSION["campaign_save_result"][$campaignid]["message"] = $message;
	if ( count($arr) ) $_SESSION["campaign_save_result"][$campaignid] = array_merge($_SESSION["campaign_save_result"][$campaignid], $arr);
}

function campaign_save_result($message, $campaignid = null, $arr = array()) {
	if ( is_null($campaignid) ) $campaignid = $GLOBALS["campaign_save_id"];
	$_SESSION["campaign_save_result"][$campaignid]["succeeded"] = true;
	$_SESSION["campaign_save_result"][$campaignid]["message"] = $message;
	if ( count($arr) ) $_SESSION["campaign_save_result"][$campaignid] = array_merge($_SESSION["campaign_save_result"][$campaignid], $arr);
}

# Redirect functions

function campaign_save_after() {
	$step  = (string)ac_http_param("step");
	$after = (string)ac_http_param("aftersave");

	$id    = (int)$GLOBALS["campaign_save_id"];

	# Nothing we can do if we have no id.
	if ($id == 0)
		$after = "exit";

	// handle form processing
	if ( isset($_SESSION["campaign_save_result"][$id]) ) {
		if ( !$_SESSION["campaign_save_result"][$id]["succeeded"] ) {
			$after = "nothing";
		}
	}

	switch ($after) {
		default:
		case "nothing":
			break;

		case "next":
			campaign_save_next($step);
			break;

		case "back":
			campaign_save_back($step);

		case "exit":
			ac_http_redirect("main.php");
			break;
	}
}

function campaign_save_next($step) {
	$id = $GLOBALS["campaign_save_id"];
	$order = $GLOBALS["campaign_save_order"];

	$campaign = ac_sql_select_row("SELECT * FROM #campaign WHERE id = '$id'");
	$text = ($campaign["type"] == "split") ? "splittext" : "text";
	$message = ($campaign["type"] == "split") ? "splitmessage" : "message";

	$index = array_search($step, $order);
	if ($index !== false) {
		if ($index < (count($order) - 1)) {
			# Some exceptions may be in order.

			switch ($step) {
				default:
					break;

				case "list":
					# We may need to skip to message, or to text, based on our
					# campaign type and on if we've chosen a base
					# message/template.

					# Automatically go to the text context, regardless of tpl/msg
					if ($campaign["type"] == "text")
						ac_http_redirect("main.php?action=campaign_new_$text&id=$id");

					$hasmessage = (int)ac_sql_select_one("SELECT COUNT(*) FROM #campaign_message WHERE campaignid = '$id'");
					if ($campaign["basetemplateid"] > 0 || $campaign["basemessageid"] > 0 || $hasmessage)
						ac_http_redirect("main.php?action=campaign_new_$message&id=$id");

					break;

				case "template":
					ac_http_redirect("main.php?action=campaign_new_$message&id=$id");
					break;

				case "text":
					ac_http_redirect("main.php?action=campaign_new_summary&id=$id");
					break;

				case "splitmessage":
					if (!$campaign["managetext"])
						ac_http_redirect("main.php?action=campaign_new_summary&id=$id");
					break;
			}

			$nextstep = $order[$index + 1];
			ac_http_redirect("main.php?action=campaign_new_$nextstep&id=$id");
		}
	}
}

function campaign_save_back($step) {
	$id = $GLOBALS["campaign_save_id"];
	$order = $GLOBALS["campaign_save_order"];

	$campaign = ac_sql_select_row("SELECT * FROM #campaign WHERE id = '$id'");
	$text = ($campaign["type"] == "split") ? "splittext" : "text";
	$message = ($campaign["type"] == "split") ? "splitmessage" : "message";

	$index = array_search($step, $order);
	if ($index !== false) {
		if ($index > 0) {
			# Check for exceptions.

			switch ($step) {
				default:
					break;

				case "summary":
					if ($campaign["type"] == "text" || $campaign["managetext"] == 1)
						ac_http_redirect("main.php?action=campaign_new_$text&id=$id");

					ac_http_redirect("main.php?action=campaign_new_$message&id=$id");

					break;

				case "text":
					# Skip message; go directly back to list.
					if ($campaign["type"] == "text")
						ac_http_redirect("main.php?action=campaign_new_list&id=$id");

					break;

				case "splitmessage":
				case "message":
					# If we're here, we've chosen a base template or message.
					# There's no need to go back to the template screen.
					ac_http_redirect("main.php?action=campaign_new_list&id=$id");
					break;

				case "splittext":
					$messageid = (int)ac_http_param("messageid");
					ac_http_redirect("main.php?action=campaign_new_$message&id=$id&m=$messageid");

				case "summary":
					if ($campaign["type"] == "text")
						ac_http_redirect("main.php?action=campaign_new_text&id=$id");

					if ($campaign["type"] == "split")
						ac_http_redirect("main.php?action=campaign_new_splitmessage&id=$id");

					ac_http_redirect("main.php?action=campaign_new_message&id=$id");
					break;
			}

			$nextstep = $order[$index - 1];

			if ($index > 1)
				ac_http_redirect("main.php?action=campaign_new_$nextstep&id=$id");
			else
				ac_http_redirect("main.php?action=campaign_new&id=$id");
		}
	}
}

# Save-step functions

function campaign_save_type() {
	$name = trim((string)ac_http_param("name"));
	$type = (string)ac_http_param("type");

	// error checks
	if ( $GLOBALS["campaign_save_after"] == 'next' ) {
		if ( !in_array($type, array('single','recurring','split','responder','reminder','special','activerss','text')) ) {
			campaign_save_error(_a("Please select the campaign type first."));
			return;
		}
		if ( !$name ) {
			campaign_save_error(_a("Please enter the name of your campaign name first."));
			return;
		}
	}

	if ($GLOBALS["campaign_save_id"] == 0) {
		campaign_save_default($name, $type);
	} else {
		$up = array(
			"name" => $name,
			"type" => $type,
		);

		// Still use recurring if that's what we already are.  Unset willrecur if we're changing.
		$origtype = ac_sql_select_one("SELECT `type` FROM #campaign WHERE id = '$GLOBALS[campaign_save_id]'");
		if ($origtype == "recurring") {
		   	if ($type == "single")
				$up["type"] = $origtype;
			else
				$up["willrecur"] = 0;
		}

		if ($type != "split" && $origtype == "split") {
			// if changing from split to something else
			// make sure there is only one message in #campaign_message
			$messages = (int)ac_sql_select_one("SELECT COUNT(*) FROM #campaign_message WHERE campaignid = '$GLOBALS[campaign_save_id]'");
			if ($messages > 1) {
				// delete all messages, but one
				$delete = ac_sql_query( "DELETE FROM #campaign_message WHERE campaignid = '$GLOBALS[campaign_save_id]' LIMIT " . ($messages - 1) );
			}
		}

		if ($origtype == "responder" || $origtype == "activerss" || $origtype == "reminder")
			$up["=sdate"] = "NULL";

		if ($type == "text")
			$up["managetext"] = 1;

		ac_sql_update("#campaign", $up, "id = '$GLOBALS[campaign_save_id]'");
	}
}

function campaign_save_list() {
	global $admin;

	$id = $GLOBALS["campaign_save_id"];

	if ($id < 1)
		return;

	$listids = ac_http_param_forcearray("listid");
	$filterid = (int)ac_http_param("filterid");

	$listids = array_diff(array_map('intval', $listids), array(0));

	if ( !ac_admin_ismaingroup() ) {
		$listids = array_intersect($listids, array_values($admin['lists']));
	}

	// error checks
	if ( $GLOBALS["campaign_save_after"] == 'next' ) {
		if ( !count($listids) ) {
			campaign_save_error(_a("Please select at least one list to send this campaign to."), $id);
			return;
		}
		if ( $filterid and !filter_select_row($filterid) ) {
			campaign_save_error(_a("The segment you have selected does not seem to be valid."), $id);
			return;
		}
	}

	# Update the lists for this campaign.
	ac_sql_query("DELETE FROM #campaign_list WHERE campaignid = '$id'");

	foreach ($listids as $listid) {
		$ins = array(
			"campaignid" => $id,
			"listid" => $listid,
		);

		ac_sql_insert("#campaign_list", $ins);

		# If there are any list relations for the message, change them here.
		$messages = ac_sql_select_list("SELECT messageid FROM #campaign_message WHERE campaignid = '$id'");
		foreach ($messages as $mid) {
			ac_sql_query("DELETE FROM #message_list WHERE messageid = '$mid'");
			$ins = array(
				"messageid" => $mid,
				"listid" => $listid,
			);

			ac_sql_insert("#message_list", $ins);
		}
	}

	$lhs = ac_http_param_forcearray("filter_group_cond_lhs");
	if (count($lhs) > 1) {
		$rval = filter_insert_post();

		if (isset($rval["id"]) && $rval["id"] > 0)
			$filterid = $rval["id"];
	}

	$up = array(
		"filterid" => $filterid,
	);

	ac_sql_update("#campaign", $up, "id = '$id'");

	# Last ditch check; force them to go back if the count is zero.
	$type = (string)ac_sql_select_one("SELECT `type` FROM #campaign WHERE id = '$id'");
	if ($GLOBALS['campaign_save_after'] == "next" && !in_array($type, array("responder", "reminder")) && campaign_subscribers_fetch($listids, $filterid) < 1)
		ac_http_redirect("main.php?action=campaign_new_list&id=$id&nobody=1");
}

function campaign_save_template() {
	$id = $GLOBALS["campaign_save_id"];

	if ($id < 1)
		return;

	# Don't do anything.
	if ($GLOBALS["campaign_save_after"] == "back")
		return;

	$up = array(
		"basetemplateid" => (int)ac_http_param("basetemplateid"),
		"basemessageid" => (int)ac_http_param("basemessageid"),
	);

	ac_sql_update("#campaign", $up, "id = '$id'");

	# Remove any previous message content if it exists.  Necessary if someone
	# wants to start over with a new template.
	ac_sql_delete("#campaign_message", "campaignid = '$id'");
}

function campaign_save_message() {
	$id = $GLOBALS["campaign_save_id"];

	if ( $id < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."));
		return;
	}

	$mid = (int)ac_sql_select_one("SELECT messageid FROM #campaign_message WHERE campaignid = '$id' LIMIT 1");

	# Well, this is weird.
	if ( $mid < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."), $id);
		return;
	}

	$campaign = ac_sql_select_row("SELECT * FROM #campaign WHERE id = '$id'");
	# Well, this is weird.
	if ( !$campaign ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."), $id);
		return;
	}
	$message = ac_sql_select_row("SELECT * FROM #message WHERE id = '$mid'");

	$embed  = (int)ac_http_param("embed_images");

	# Update campaign.
	$up = array(
		"managetext" => (int)ac_http_param("managetext"),
		"embed_images" => $embed,
	);

	$managetext = $up["managetext"];	# Need this later.

	ac_sql_update("#campaign", $up, "id = '$id'");

	# Update the attachment rows, if any.
	$attach = ac_http_param_forcearray("attach");

	if (count($attach) > 0 && !$embed) {
		$attach = array_map('intval', $attach);
		$attachstr = implode("','", $attach);

		$up = array(
			"messageid" => $mid,
		);

		ac_sql_update("#message_file", $up, "id IN ('$attachstr')");
	} else {
		ac_sql_delete("#message_file", "messageid = '$mid'");
	}

	# Update message.
	$up = array(
		"fromname" => trim((string)ac_http_param("fromname")),
		"fromemail" => trim((string)ac_http_param("fromemail")),
		"reply2" => trim((string)ac_http_param("reply2")),
		"subject" => trim((string)ac_http_param("subject")),
		"html" => (string)ac_http_param("html"),
		"format" => "mime",
	);

	// Update campaign.
	$campup = array();

	$fetchurl = trim((string)ac_http_param("fetchurl"));
	$fetch    = ac_http_param("fetch");

	if ($fetch == "send" && $fetchurl != "http://" && strlen($fetchurl) > 8) {
		# 8 in case they use "https"
		$up["htmlfetch"] = campaign_save_fetchmethod($id, $fetch, $fetchurl);
		$up["html"] = "fetch:" . $fetchurl;
		$message["htmlfetchurl"] = $fetchurl;
	} else {
		# First, we know we're not doing any fetch-at-send, so kill any recurring settings.
		ac_sql_query("UPDATE #campaign SET willrecur = 0, `type` = 'single', `recurring` = 'day1' WHERE id = '$id' AND `type` = 'recurring'");

		# Fix any HTML problems in the message.
		$up["htmlfetch"] = "now";
		$up["html"] = ac_str_strip_malicious($up["html"]);
		$up["html"] = campaign_save_fixchars($up["html"]);
		$up["html"] = html_savefix($up["html"]);
		$up["html"] = message_link_revert($mid, $up['html']);
		$message["htmlfetchurl"] = "";
	}

	if ( $GLOBALS["campaign_save_after"] == 'next' ) {

		// perform checks
		if ( !$up["fromname"] ) {
			campaign_save_error(_a("Please enter sender name before proceeding to the next step."), $id);
			return;
		}
		if ( !ac_str_is_email($up["fromemail"]) ) {
			campaign_save_error(_a("Please enter a valid sender email address before proceeding to the next step."), $id);
			return;
		}
		if ( $up["reply2"] and !ac_str_is_email($up["reply2"]) ) {
			campaign_save_error(_a("Please enter a valid reply-to email address before proceeding to the next step."), $id);
			return;
		}
		if ( $up["htmlfetch"] == "now" && !$up["subject"] ) {
			campaign_save_error(_a("Please enter a message subject before proceeding to the next step."), $id);
			return;
		}
		if ( !ac_str_strip_tags($up["html"]) && $up["html"] == ac_str_strip_tag_short($up["html"], "img") ) {
			campaign_save_error(_a("It seems like you haven't entered any message content. Please enter some content before proceeding to the next step."), $id);
			return;
		}

		// activerss checks
		if ( $campaign['type'] == 'activerss' ) {
			$campup['activerss_url'] = trim((string)ac_http_param("activerss_url"));
			$campup['activerss_items'] = (int)ac_http_param("activerss_items");
			if ( !$campup['activerss_items'] ) $campup['activerss_items'] = 10;
			// check if url is provided
			if ( !ac_str_is_url($campup['activerss_url']) ) {
				campaign_save_error(_a("You do not have an RSS feed URL entered into the 'Insert your RSS feed' field. Please make sure that you entered a RSS feed URL and placed it into your message before proceeding to the next step."), $id);
				return;
			}

			$content = $up['html'];
			// check if exactly one rss feed is found
			preg_match_all('/%RSS-FEED\|/', $content, $feed_opening);
			preg_match_all('/%RSS-FEED%/', $content, $feed_closing);
			preg_match_all('/%RSS-LOOP\|/', $content, $loop_opening);
			preg_match_all('/%RSS-LOOP%/', $content, $loop_closing);
			// check if ONLY ONE of feed opening and closing tags is found
			if ( count($feed_opening[0]) != 1 || count($feed_closing[0]) != 1 ) {
				campaign_save_error(_a("You do not have RSS personalization tags included into your messsage, or you have more than one RSS feed referenced. Please make sure that you have one RSS feed placed into your message before proceeding to the next step."), $id);
				return;
			}
			// check if NO loop tags are found
			if ( count($loop_opening[0]) == 0 || count($loop_closing[0]) == 0 ) {
				campaign_save_error(_a("It seems like you haven't entered any LOOP tags to display your RSS feed items. Please add LOOP tags to your message before proceeding to the next step."), $id);
				return;
			}
			// check if any mismatched tags are found
			if ( count($feed_opening[0]) != count($feed_closing[0]) || count($loop_opening[0]) != count($loop_closing[0]) ) {
				campaign_save_error(_a("It seems like you haven't entered valid RSS personalization tags. Some tags seem to be missing, and your RSS feed would not be properly displayed. Please correct this before proceeding to the next step."), $id);
				return;
			}
		}
	}

	// look for SurveyMonkey personalization tags
	preg_match_all('/%SERVICE-SURVEYMONKEY-[^%]*%/', $up['html'], $service_surveymonkey);
	if ( isset($service_surveymonkey[0][0]) && $service_surveymonkey[0][0] ) {
		// get the unique ID from the pers tag, which is the URL ID for the survey
		$survey_url_id = preg_match("/[^-]*$/", $service_surveymonkey[0][0], $url_id_matches);
		if (isset($url_id_matches[0]) && $url_id_matches[0]) {
			$survey_url_id = substr($url_id_matches[0], 0, strlen($url_id_matches[0]) - 1);
		}
		$campup["survey"] = $survey_url_id;
	}

	// convert html to text
	if (!$managetext || ac_sql_select_one("text", "#message", "id = '$mid'") == '')
		$up["text"] = message_html2text(array_merge($message, $up));

	$sql = ac_sql_update("#message", $up, "id = '$mid'");
	if ( !$sql ) {
		campaign_save_error(_a("Message could not be saved due to a database error. Please contact support."), $id);
		return;
	}
	if ( $campup ) ac_sql_update("#campaign", $campup, "id = '$id'");
	// clear all message sources for this campaign
	campaign_source_clear(null, $mid, null);

	# Extract any links.
	$msg = message_select_row($mid);
	if ( $msg['htmlfetch'] == 'send' and $msg['htmlfetchurl'] ) {
		$msg['html'] = ac_http_get($msg['htmlfetchurl'], "UTF-8");
		$msg['html'] = message_link_resolve($msg['html'], $msg['htmlfetchurl']);
	}
	$msg["html"] = personalization_basic($msg["html"], $msg["subject"]);
	$links = message_extract_links($msg);

	// if links are present, turn on link tracking (they can still turn it off on the Summary page)
	if ($links) {
		$campup = array(
			"tracklinks" => 1,
		);
		ac_sql_update("#campaign", $campup, "id = '$id'");
	}

	// add read tracker rows
	$links[] = array(
	  "link" => "open",
	  "title" => "",
	);
	$links[] = array(
	  "link" => "open",
	  "messageid" => 0,
	  "title" => "",
	);

	$saved = array();
	$new_links = array();

	foreach ($links as $link) {
		if ($link["link"] != "open") {
			// $links contains duplicates - one for HTML and one for Text.
			// Only proceed with one instance of the link, so it uses the title attribute for the link name
			$link["link"] = preg_replace("/\&amp;/", "&", $link["link"]);
			if ( !in_array($link["link"], $new_links) ) {
				$new_links[] = $link["link"];
			} else {
				continue;
			}
		}

		$messageid = ( isset($link["messageid"]) ) ? $link["messageid"] : $mid;

		$esc = ac_sql_escape(message_link_internal($link["link"]));
		$linkid = (int)ac_sql_select_one('id', '#link', "campaignid = '$id' AND messageid = '$messageid' AND link = '$esc'");

		if (!$linkid) {
			$ins = array(
				"id" => 0,
				"campaignid" => $id,
				"messageid" => $messageid,
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
				"tracked" => 1,
			);

			$ins = campaign_save_fixlinkname($ins);

			$sql = ac_sql_insert("#link", $ins);
			$linkid = ac_sql_insert_id();
		} else {
			$up = array(
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
			);
			$up = campaign_save_fixlinkname($up);
			$sql = ac_sql_update("#link", $up, "id = '$linkid'");
			if ( !$sql ) {
			}
		}

		$saved[] = $linkid;
	}

	# Remove old links.
	$savedstr = implode("','", $saved);
	ac_sql_delete("#link", "campaignid = '$id' AND messageid = '$mid' AND id NOT IN ('$savedstr') AND link != 'open'");

	campaign_save_readtracking($id, $mid);

	// Request a screenshot.
	if (!ac_http_param("isauto")) {
		em_request_screenshot("campaign", $mid);
	}
}

function campaign_save_text() {
	$id = $GLOBALS["campaign_save_id"];

	if ( $id < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."));
		return;
	}

	$mid = (int)ac_sql_select_one("SELECT messageid FROM #campaign_message WHERE campaignid = '$id' LIMIT 1");

	# Well, this is weird.
	if ( $mid < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."), $id);
		return;
	}

	$type = ac_sql_select_one("SELECT `type` FROM #campaign WHERE id = '$id'");
	if ( $type == 'text' ) {
		# Update the attachment rows, if any.
		$attach = ac_http_param_forcearray("attach");

		if (count($attach) > 0) {
			$attach = array_map('intval', $attach);
			$attachstr = implode("','", $attach);

			// Force it to always be the first one.
			if (isset($GLOBALS['_hosted_account'])) {
				$attachstr = (string)$attach[0];
				$delstr = implode("','", array_slice($attach, 1));

				ac_sql_delete("#message_file", "id IN ('$delstr')");
				ac_sql_delete("#message_file_data", "fileid IN ('$delstr')");
			}

			$up = array(
				"messageid" => $mid,
			);

			ac_sql_update("#message_file", $up, "id IN ('$attachstr')");
		} else {
			ac_sql_delete("#message_file", "messageid = '$mid'");
		}
	}

	# Update message.
	$up = array(
		"text" => (string)ac_http_param("text"),
	);

	$fetchurl = trim((string)ac_http_param("fetchurl"));
	$fetch    = ac_http_param("fetch");

	if ($fetch == "send" && $fetchurl != "http://" && strlen($fetchurl) > 8) {
		# 8 in case they use "https"
		$up["textfetch"] = campaign_save_fetchmethod($id, $fetch, $fetchurl);
		$up["text"] = "fetch:" . $fetchurl;
	} else {
		# First, we know we're not doing any fetch-at-send, so kill any recurring settings.
		ac_sql_query("UPDATE #campaign SET willrecur = 0, `type` = 'single', `recurring` = 'day1' WHERE id = '$id' AND `type` = 'recurring'");

		$up["text"] = campaign_save_fixchars($up["text"]);
	}

	# Only update these if the fields exist--and they would exist only in a text-only campaign.
	if (ac_http_param("fromname")) {
		$up["fromname"] = (string)ac_http_param("fromname");
	}

	if (ac_http_param("fromemail")) {
		$up["fromemail"] = (string)ac_http_param("fromemail");
	}

	if (ac_http_param("reply2")) {
		$up["reply2"] = (string)ac_http_param("reply2");
	}

	if (ac_http_param("subject")) {
		$up["subject"] = (string)ac_http_param("subject");
	}

	if ($type == "text")
		$up["format"] = "text";
	else
		$up["format"] = "mime";

	if ( $GLOBALS["campaign_save_after"] == 'next' ) {
		if ( isset($up["fromemail"]) and !$up["fromname"] ) {
			campaign_save_error(_a("Please enter sender name before proceeding to the next step."), $id);
			return;
		}
		if ( isset($up["fromemail"]) and !ac_str_is_email($up["fromemail"]) ) {
			campaign_save_error(_a("Please enter a valid sender email address before proceeding to the next step."), $id);
			return;
		}
		if ( isset($up["reply2"]) and !ac_str_is_email($up["reply2"]) ) {
			campaign_save_error(_a("Please enter a valid reply-to email address before proceeding to the next step."), $id);
			return;
		}
		if ( (isset($up["textfetch"]) && $up["textfetch"] == "now") && isset($up["subject"]) && !$up["subject"] ) {
			campaign_save_error(_a("Please enter a message subject before proceeding to the next step."), $id);
			return;
		}
		if ( !trim($up["text"]) ) {
			campaign_save_error(_a("It seems like you haven't entered any message content. Please enter some content before proceeding to the next step."), $id);
			return;
		}

		// if they are hitting next button, they edited the message and they should have a new round of tests
		if (isset($GLOBALS['_hosted_account']) && isset($_SESSION["campaign_send_test"]) && $_SESSION["campaign_send_test"]) {
				$_SESSION["campaign_send_test"] = array();
		}
	}

	ac_sql_update("#message", $up, "id = '$mid'");
	// clear all message sources for this campaign
	campaign_source_clear(null, $mid, null);

	# Extract any links.
	$msg = message_select_row($mid);
	if ( $msg['textfetch'] == 'send' and isset($msg['textfetchurl']) and $msg['textfetchurl'] ) {
		$msg['text'] = ac_http_get($msg['textfetchurl'], "UTF-8");
		$msg['text'] = message_link_resolve($msg['text'], $msg['textfetchurl']);
	}
	$msg["text"] = personalization_basic($msg["text"], $msg["subject"], 'text');
	$links = message_extract_links($msg);
	$saved = array();
	$new_links = array();

	foreach ($links as $link) {
		if ($link["link"] != "open") {
			// $links contains duplicates - one for HTML and one for Text.
			// Only proceed with one instance of the link, so it uses the title attribute for the link name
			if ( !in_array($link["link"], $new_links) ) {
				$new_links[] = $link["link"];
			} else {
				continue;
			}
		}

		$messageid = ( isset($link["messageid"]) ) ? $link["messageid"] : $mid;

		$esc = ac_sql_escape(message_link_internal($link["link"]));
		$linkid = (int)ac_sql_select_one('id', '#link', "campaignid = '$id' AND messageid = '$messageid' AND link = '$esc'");

		if (!$linkid) {
			$ins = array(
				"id" => 0,
				"campaignid" => $id,
				"messageid" => $messageid,
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
				"tracked" => 1,
			);

			$ins = campaign_save_fixlinkname($ins);

			$sql = ac_sql_insert("#link", $ins);
			$linkid = ac_sql_insert_id();
		} else {
			$up = array(
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
			);
			$up = campaign_save_fixlinkname($up);
			$sql = ac_sql_update("#link", $up, "id = '$linkid'");
			if ( !$sql ) {
			}
		}

		$saved[] = $linkid;
	}

	// Remove old links -- but, here, only do so if this is a text-only campaign.

	if ($type == "text") {
		$savedstr = implode("','", $saved);
		ac_sql_delete("#link", "campaignid = '$id' AND messageid = '$mid' AND id NOT IN ('$savedstr') AND link != 'open'");
	}

	campaign_save_readtracking($id, $mid);
}

function campaign_save_splitmessage() {
	$id = $GLOBALS["campaign_save_id"];

	if ( $id < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."));
		return;
	}

	if ($GLOBALS["campaign_save_after"] == "next") {
		$c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #campaign_message WHERE campaignid = '$id'");
		if ($c < 2) {
			campaign_save_error(_a("Not enough messages. Please add another message before continuing."));
			return;
		}
	}

	$mid = (int)ac_http_param("messageid");

	# Well, this is weird.
	if ( $mid < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."), $id);
		return;
	}
	$message = ac_sql_select_row("SELECT * FROM #message WHERE id = '$mid'");

	$embed  = (int)ac_http_param("embed_images");

	# Update campaign.
	$up = array(
		"managetext" => (int)ac_http_param("managetext"),
		"split_type" => ac_http_param("splittype"),
		"split_offset" => (int)ac_http_param("splitoffset"),
		"split_offset_type" => ac_http_param("splitoffsettype"),
		"embed_images" => $embed,
	);

	$managetext = $up["managetext"];	# Need this later.

	if ($up["split_type"] == "winner") {
		$up["split_type"] = ac_http_param("splitwinnertype");
	}

	ac_sql_update("#campaign", $up, "id = '$id'");

	# Update the split ratios.
	$percids = ac_http_param_forcearray("splitmessageid");
	$perc = ac_http_param_forcearray("splitratio");

	if (count($percids) == count($perc)) {

		if ( count($perc) < 2 ) {
			campaign_save_error(_a("Unknown error occurred. It seems like you are trying to send only one message as a Split Test campaign. You need two or more messages to send a Split Test campaign."), $id);
			return;
		}

		if ( $up["split_type"] != "even" ) {
			$perc = array_map('intval', $perc);
			$totalperc = array_sum($perc);
			if ( $totalperc > 99 ) { // can't be 100 if winner
				campaign_save_error(_a("You need to allocate less than 100% of your subscribers so that a winner message can be selected, then sent to remaining subscribers."), $id);
				return;
			}
			if ( count($percids) != count(array_diff($perc, array(0))) ) {
				campaign_save_error(_a("You can not have zero (0) percent of subscribers allocated to a message, as then that message will not be sent to anyone."), $id);
				return;
			}

		} else {
			// reset always to equal amounts
			foreach ( $perc as $k => $v ) $perc[$k] = 1 / count($percids);
		}

		for ($i = 0; $i < count($percids); $i++) {
			$up = array(
				"percentage" => $perc[$i],
			);

			$percid = (int)$percids[$i];
			ac_sql_update("#campaign_message", $up, "campaignid = '$id' AND messageid = '$percid'");
		}
	}

	# Update the attachment rows, if any.
	$attach = ac_http_param_forcearray("attach");

	if (count($attach) > 0 && !$embed) {
		$attach = array_map('intval', $attach);
		$attachstr = implode("','", $attach);

		// Force it to always be the first one.
		if (isset($GLOBALS['_hosted_account'])) {
			$attachstr = (string)$attach[0];
			$delstr = implode("','", array_slice($attach, 1));

			ac_sql_delete("#message_file", "id IN ('$delstr')");
			ac_sql_delete("#message_file_data", "fileid IN ('$delstr')");
		}

		$up = array(
			"messageid" => $mid,
		);

		ac_sql_update("#message_file", $up, "id IN ('$attachstr')");
	} else {
		ac_sql_delete("#message_file", "messageid = '$mid'");
	}

	# Update message.
	$up = array(
		"fromname" => trim((string)ac_http_param("fromname")),
		"fromemail" => trim((string)ac_http_param("fromemail")),
		"reply2" => trim((string)ac_http_param("reply2")),
		"subject" => trim((string)ac_http_param("subject")),
		"html" => (string)ac_http_param("html"),
		"format" => "mime",
	);

	$fetchurl = trim((string)ac_http_param("fetchurl"));
	$fetch    = ac_http_param("fetch");

	if ($fetch == "send" && $fetchurl != "http://" && strlen($fetchurl) > 8) {
		# 8 in case they use "https"
		$up["htmlfetch"] = campaign_save_fetchmethod($id, $fetch, $fetchurl);
		$up["html"] = "fetch:" . $fetchurl;
		$message["htmlfetchurl"] = $fetchurl;
	} else {
		# First, we know we're not doing any fetch-at-send, so kill any recurring settings.
		ac_sql_query("UPDATE #campaign SET willrecur = 0, `type` = 'single', `recurring` = 'day1' WHERE id = '$id' AND `type` = 'recurring'");

		$up["htmlfetch"] = "now";
		# Fix any HTML problems in the message.
		$up["html"] = ac_str_strip_malicious($up["html"]);
		$up["html"] = campaign_save_fixchars($up["html"]);
		$up["html"] = html_savefix($up["html"]);
		$up["html"] = message_link_revert($mid, $up['html']);
		$message["htmlfetchurl"] = "";
	}

	if ( $GLOBALS["campaign_save_after"] == 'next' ) {

		// perform checks
		if ( !$up["fromname"] ) {
			campaign_save_error(_a("Please enter sender name before proceeding to the next step."), $id);
			return;
		}
		if ( !ac_str_is_email($up["fromemail"]) ) {
			campaign_save_error(_a("Please enter a valid sender email address before proceeding to the next step."), $id);
			return;
		}
		if ( $up["reply2"] and !ac_str_is_email($up["reply2"]) ) {
			campaign_save_error(_a("Please enter a valid reply-to email address before proceeding to the next step."), $id);
			return;
		}
		if ( $up["htmlfetch"] == "now" && !$up["subject"] ) {
			campaign_save_error(_a("Please enter a message subject before proceeding to the next step."), $id);
			return;
		}
		if ( !ac_str_strip_tags($up["html"]) && $up["html"] == ac_str_strip_tag_short($up["html"], "img") ) {
			campaign_save_error(_a("It seems like you haven't entered any message content. Please enter some content before proceeding to the next step."), $id);
			return;
		}

		// if they are hitting next button, they edited the message and they should have a new round of tests
		if (isset($GLOBALS['_hosted_account']) && isset($_SESSION["campaign_send_test"]) && $_SESSION["campaign_send_test"]) {
				$_SESSION["campaign_send_test"] = array();
		}

	}

	// convert html to text
	if (!$managetext || ac_sql_select_one("text", "#message", "id = '$mid'") == '')
		$up["text"] = message_html2text(array_merge($message, $up));

	ac_sql_update("#message", $up, "id = '$mid'");
	// clear all message sources for this campaign
	campaign_source_clear(null, $mid, null);

	# Extract any links.
	$msg = message_select_row($mid);
	if ( $msg['htmlfetch'] == 'send' and $msg['htmlfetchurl'] ) {
		$msg['html'] = ac_http_get($msg['htmlfetchurl'], "UTF-8");
		$msg['html'] = message_link_resolve($msg['html'], $msg['htmlfetchurl']);
	}
	$msg["html"] = personalization_basic($msg["html"], $msg["subject"]);
	$links = message_extract_links($msg);
	$saved = array();
	$new_links = array();

	foreach ($links as $link) {
		if ($link["link"] != "open") {
			// $links contains duplicates - one for HTML and one for Text.
			// Only proceed with one instance of the link, so it uses the title attribute for the link name
			$link["link"] = preg_replace("/\&amp;/", "&", $link["link"]);
			if ( !in_array($link["link"], $new_links) ) {
				$new_links[] = $link["link"];
			} else {
				continue;
			}
		}

		$messageid = ( isset($link["messageid"]) ) ? $link["messageid"] : $mid;

		$esc = ac_sql_escape(message_link_internal($link["link"]));
		$linkid = (int)ac_sql_select_one('id', '#link', "campaignid = '$id' AND messageid = '$messageid' AND link = '$esc'");

		if (!$linkid) {
			$ins = array(
				"id" => 0,
				"campaignid" => $id,
				"messageid" => $messageid,
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
				"tracked" => 1,
			);

			$ins = campaign_save_fixlinkname($ins);

			$sql = ac_sql_insert("#link", $ins);
			$linkid = ac_sql_insert_id();
		} else {
			$up = array(
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
			);
			$up = campaign_save_fixlinkname($up);
			$sql = ac_sql_update("#link", $up, "id = '$linkid'");
			if ( !$sql ) {
			}
		}

		$saved[] = $linkid;
	}

	# Remove old links.
	$savedstr = implode("','", $saved);
	ac_sql_delete("#link", "campaignid = '$id' AND messageid = '$mid' AND id NOT IN ('$savedstr') AND link != 'open'");

	campaign_save_readtracking($id, $mid);

	// Request a screenshot.
	if (!ac_http_param("isauto")) {
		em_request_screenshot("campaign", $mid);
	}
}

function campaign_save_splittext() {
	$id = $GLOBALS["campaign_save_id"];

	if ( $id < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."));
		return;
	}

	$mid = (int)ac_http_param("messageid");

	# Well, this is weird.
	if ( $mid < 1 ) {
		campaign_save_error(_a("Unknown error occurred. Please try repeating your action. We apologize for this inconvenience..."), $id);
		return;
	}

	# Update message.
	$up = array(
		"text" => (string)ac_http_param("text"),
	);

	$fetchurl = trim((string)ac_http_param("fetchurl"));
	$fetch    = ac_http_param("fetch");

	if ($fetch == "send" && $fetchurl != "http://" && strlen($fetchurl) > 8) {
		# 8 in case they use "https"
		$up["textfetch"] = campaign_save_fetchmethod($id, $fetch, $fetchurl);
		$up["text"] = "fetch:" . $fetchurl;
	} else {
		# First, we know we're not doing any fetch-at-send, so kill any recurring settings.
		ac_sql_query("UPDATE #campaign SET willrecur = 0, `type` = 'single', `recurring` = 'day1' WHERE id = '$id' AND `type` = 'recurring'");

		$up["text"] = campaign_save_fixchars($up["text"]);
	}

	if ( $GLOBALS["campaign_save_after"] == 'next' ) {
		/*
		if ( isset($up["fromemail"]) and !$up["fromname"] ) {
			campaign_save_error(_a("Please enter sender name before proceeding to the next step."), $id);
			return;
		}
		if ( isset($up["fromemail"]) and !ac_str_is_email($up["fromemail"]) ) {
			campaign_save_error(_a("Please enter a valid sender email address before proceeding to the next step."), $id);
			return;
		}
		if ( isset($up["reply2"]) and !ac_str_is_email($up["reply2"]) ) {
			campaign_save_error(_a("Please enter a valid reply-to email address before proceeding to the next step."), $id);
			return;
		}
		*/
		if ( isset($up["subject"]) and !$up["subject"] ) {
			campaign_save_error(_a("Please enter a message subject before proceeding to the next step."), $id);
			return;
		}
		if ( !trim($up["text"]) ) {
			campaign_save_error(_a("It seems like you haven't entered any message content. Please enter some content before proceeding to the next step."), $id);
			return;
		}

		// if they are hitting next button, they edited the message and they should have a new round of tests
		if (isset($GLOBALS['_hosted_account']) && isset($_SESSION["campaign_send_test"]) && $_SESSION["campaign_send_test"]) {
				$_SESSION["campaign_send_test"] = array();
		}
	}

	ac_sql_update("#message", $up, "id = '$mid'");
	// clear all message sources for this campaign
	campaign_source_clear(null, $mid, null);

	# Extract any links.
	$msg = message_select_row($mid);
	if ( $msg['textfetch'] == 'send' and isset($msg['textfetchurl']) and $msg['textfetchurl'] ) {
		$msg['text'] = ac_http_get($msg['textfetchurl'], "UTF-8");
		$msg['text'] = message_link_resolve($msg['text'], $msg['textfetchurl']);
	}
	$msg["text"] = personalization_basic($msg["text"], $msg["subject"], 'text');
	$links = message_extract_links($msg);
	$saved = array();
	$new_links = array();

	foreach ($links as $link) {
		if ($link["link"] != "open") {
			// $links contains duplicates - one for HTML and one for Text.
			// Only proceed with one instance of the link, so it uses the title attribute for the link name
			if ( !in_array($link["link"], $new_links) ) {
				$new_links[] = $link["link"];
			} else {
				continue;
			}
		}

		$messageid = ( isset($link["messageid"]) ) ? $link["messageid"] : $mid;

		$esc = ac_sql_escape(message_link_internal($link["link"]));
		$linkid = (int)ac_sql_select_one('id', '#link', "campaignid = '$id' AND messageid = '$messageid' AND link = '$esc'");

		if (!$linkid) {
			$ins = array(
				"id" => 0,
				"campaignid" => $id,
				"messageid" => $messageid,
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
				"tracked" => 1,
			);

			$ins = campaign_save_fixlinkname($ins);

			$sql = ac_sql_insert("#link", $ins);
			$linkid = ac_sql_insert_id();
		} else {
			$up = array(
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
			);
			$up = campaign_save_fixlinkname($up);
			$sql = ac_sql_update("#link", $up, "id = '$linkid'");
			if ( !$sql ) {
			}
		}

		$saved[] = $linkid;
	}

	campaign_save_readtracking($id, $mid);
}

function campaign_save_summary() {
	if (isset($GLOBALS['__is_our_demo']))
		return;

	$id = (int)ac_http_param("id");

	// fetch campaign
	$campaign = ac_sql_select_row("SELECT * FROM #campaign WHERE id = '$id'");
	if ( !$campaign ) {
		campaign_save_error(_a("Campaign not found."), $id);
		return;
	}
	// refetch all dependencies (google analytics, autoposting, tracking, public, etc)
	$listids = ac_sql_select_list("SELECT listid FROM #campaign_list WHERE campaignid = '$id'");
	$liststr = implode("','", $listids);

	# Figure out of we need to allow Google Analytics
	$showgread = 0;
	$showglink = 0;
	foreach ($listids as $listid) {
		$res = ac_sql_select_row("SELECT p_use_analytics_read, p_use_analytics_link FROM #list WHERE id = '$listid' AND analytics_ua != ''");

		if ($res) {
			if ($res["p_use_analytics_read"] && !$showgread)
				$showgread = 1;

			if ($res["p_use_analytics_link"] && !$showglink)
				$showglink = 1;
		}

		if ($showgread && $showglink)
			break;
	}
	// Check if we are supposed to use social sharing funcs
	$pass = function_exists('curl_init') && function_exists('hash_hmac') && (int)PHP_VERSION > 4;
	$pass_twitter = false;
	$pass_facebook = false;
	if ( $pass ) {
		$pass_twitter = (int)ac_sql_select_one("=COUNT(*)", "#list", "id IN ('$liststr') AND twitter_token != '' AND twitter_token_secret != ''");
		$pass_facebook = (int)ac_sql_select_one("=COUNT(*)", "#list", "id IN ('$liststr') AND facebook_session IS NOT NULL AND facebook_session != ''");
		if ( !$pass_twitter ) $campaign['tweet'] = 0;
		if ( !$pass_facebook ) $campaign['facebook'] = 0;
	} else {
		$campaign['tweet'] = $campaign['facebook'] = 0;
	}
	// can we offer archiving
	$isForPublic = (bool)ac_sql_select_one("=COUNT(*)", "#list", "id IN ('$liststr') AND private = 0");

	// tracking
	$links = ac_http_param_forcearray('linkname');
	$track = ac_http_param_forcearray('linktracked');
	//$tracking = (int)ac_http_param('tracking');
	$trackreads = (int)ac_http_param('trackreads');
	$tracklinks = ac_http_param('tracklinks') ? ( ac_http_param('linksselectall') ? 'all' : 'mime' ) : 'none';

	// define update array
	$up = array(
		"public" => (int)ac_http_param("public"),
		"tweet"  => (int)ac_http_param("tweet"),
		"facebook" => (int)ac_http_param("facebook"),
		"willrecur" => (int)ac_http_param("willrecur"),
		"trackreads" => $trackreads,
		"tracklinks" => $tracklinks,
		"trackreadsanalytics" => (int)ac_http_param("trackreadsanalytics"),
		"tracklinksanalytics" => (int)ac_http_param("tracklinksanalytics"),
		"schedule" => (int)ac_http_param("schedule"),
		"responder_existing" => (int)ac_http_param("responder_existing"),
		"responder_type" => "subscribe",
		"responder_offset" => ((int)ac_http_param("respondday") * 24) + (int)ac_http_param("respondhour"),
		"reminder_field" => ac_http_param("reminder_field"),
		"reminder_format" => ac_http_param("reminder_format"),
		"reminder_type" => ac_http_param("reminder_type"),
		"reminder_offset" => (int)ac_http_param("reminder_offset"),
		"reminder_offset_type" => ac_http_param("reminder_offset_type"),
		"reminder_offset_sign" => ac_http_param("reminder_offset_sign"),
		"recurring" => ac_http_param("recurring"),
		"activerss_interval" => ac_http_param("activerss_interval"),
	);

	// now fix it
	if ( !$pass_facebook ) $up["facebook"] = 0;
	if ( !$pass_twitter ) $up["tweet"] = 0;
	if ( !$showglink or $up['tracklinks'] == 'none' ) $up["tracklinksanalytics"] = 0;
	if ( !$showgread or !$up['trackreads'] ) $up["trackreadsanalytics"] = 0;
	if ( !$isForPublic ) $up['public'] = 0;
	$wl = array('hour0', 'hour1', 'hour2', 'hour6', 'hour12', 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
	if ( !in_array($up["activerss_interval"], $wl) ) $up['activerss_interval'] = 'year1';

	$type = $campaign["type"];

	if ($up["willrecur"]) {
		if ($type == "single")
			$up["type"] = "recurring";
	} else {
		if ($type == "recurring")
			$up["type"] = "single";
	}

	# This would be the "exactly" option, which is essentially +0
	if ($up["reminder_offset_sign"] == '=')
		$up["reminder_offset_sign"] = '+';

	# Schedule time
	$date = ac_http_param("scheduledate");
	$hour = ac_http_param("schedulehour");
	$minute = ac_http_param("scheduleminute");

	if ($up["schedule"] && $date != "") {
		$sdate = sprintf("%s %d:%d:00", date("Y-m-d", strtotime($date)), $hour, $minute);
		$sdate = strtotime($sdate) - (ac_date_offset_hour() * 3600);
		if ($campaign["status"] == 0 || $campaign["status"] == 1 || (in_array($campaign["type"], array("single", "split", "recurring", "text", "activerss")) && $sdate > time()))
			$up["sdate"] = date("Y-m-d H:i:s", $sdate);
		else
			$up["schedule"] = 0;
	} else {
		$up["=sdate"] = "NOW()";
		//$up["schedule"] = 0;
	}

	if ( $GLOBALS["campaign_save_after"] == 'next' ) {
		if ( 0 && $someError ) {
			campaign_save_error(_a("Some error occurred..."), $id);
			return;
		}
	}

	ac_sql_update("#campaign", $up, "id = '$id'");

	// save the links
	foreach ($links as $linkid => $linkname) {
		$linkid = (int)$linkid;
		$up2 = array(
			"name" => $linkname,
			"tracked" => (int)( $up['tracklinks'] != 'none' && isset($track[$linkid]) ),
		);
		ac_sql_update("#link", $up2, "id = '$linkid'");
	}

	if ($tracklinks == "none") {
	  // make sure "open" links are set to tracked=0, in this case
	  $up3 = array(
      "tracked" => 0,
	  );
	  ac_sql_update("#link", $up3, "campaignid = '$id' AND link = 'open'");
	}

	// save read tracking
	ac_sql_update_one("#link", "tracked", $up['trackreads'], "campaignid = '$id' AND link = 'open'");

	// refetch the campaign
	$campaign = ac_sql_select_row("SELECT * FROM #campaign WHERE id = '$id'");

	// initiate a campaign
	if ( $GLOBALS["campaign_save_after"] == 'next' ) {
		//dbg("campaign_init($id);");
		// send now?
		$sendnow = ( in_array($campaign['type'], array('single', /*'recurring',*/ 'split', 'text')) && $campaign["status"] == 0 && $campaign['sdate'] <= (string)ac_sql_select_one("SELECT NOW()") );
		if ( $sendnow ) {
			campaign_init($id, false);
		} elseif (in_array($campaign["type"], array("single", "split", "text")) && ($campaign["status"] == 3 || $campaign["status"] == 4)) {
			# This campaign is either paused or stopped; try to resume.
			ac_sql_update_one("#campaign", "status", CAMPAIGN_STATUS_SENDING, "id = '$id'");
		} else {
			// set campaign status
			ac_sql_update_one('#campaign', 'status', CAMPAIGN_STATUS_SCHEDULED, "id = '$id'");
			// if responder, try to deal with old subscribers (trigger a new campaign)
			if ( $campaign['type'] == 'responder' and $campaign['responder_existing'] ) {
				campaign_responder_oldies($id);
			}
		}
		//campaign_save_result(_a("Campaign Saved."), $id, array('sent' => $sendnow));
		campaign_save_result("", $id, array('sent' => $sendnow));
	}
}

function campaign_save_readtracking($campaignid, $messageid = 0) {
	$id0 = ac_sql_select_one("id", "#link", "campaignid = '$campaignid' AND messageid = 0 AND link = 'open'");
	$idm = ac_sql_select_one("id", "#link", "campaignid = '$campaignid' AND messageid = '$messageid' AND link = 'open'");

	$arr = array($id0, $idm);

	// add message 0
	$ins = array(
		"id" => $id0,
		"campaignid" => $campaignid,
		"messageid" => 0,
		"link" => 'open',
		"name" => _a("Read Tracker"),
		//"ref" => "",
		//"tracked" => 1,
	);

	# Do replace in case the id > 0
	ac_sql_replace("#link", $ins);
	if ( !$id0 ) $id0 = ac_sql_insert_id();

	// add message $mid
	$ins = array(
		"id" => $idm,
		"campaignid" => $campaignid,
		"messageid" => $messageid,
		"link" => 'open',
		"name" => _a("Read Tracker"),
		//"ref" => "",
		//"tracked" => 1,
	);

	# Do replace in case the id > 0
	ac_sql_replace("#link", $ins);
	if ( !$idm ) $idm = ac_sql_insert_id();

	return $id0;
}

function campaign_save_action() {
	$id = (int)ac_http_param("id");

	$campaignid = (int)ac_http_param("campaignid");
	$campaign_name = ac_sql_select_one("SELECT `name` FROM #campaign WHERE id = '$campaignid'");
	$linkid = (int)ac_http_param("linkid");
	if ( !$linkid ) {
		$linkid = (int)ac_sql_select_one("id", "#link", "campaignid = '$campaignid' AND messageid = 0 AND link = 'open'");
		if ( !$linkid ) {
			# Uh oh.
			return;
		}
	}

	if ($id > 0) {
		$up = array(
			"id" => $id,
			"campaignid" => $campaignid,
			"linkid" => $linkid,
			"type" => (string)ac_http_param("type"),
			"name" => _a("Actions for Campaign") . " \"$campaign_name\"",
		);

		# Update subscriber_action; remove any existing parts (we'll replace them in a bit).
		ac_sql_update("#subscriber_action", $up, "id = '$id'");
		ac_sql_delete("#subscriber_action_part", "actionid = '$id'");
	} else {
		$ins = array(
			"campaignid" => $campaignid,
			"linkid" => $linkid,
			"type" => (string)ac_http_param("type"),
			"name" => _a("Actions for Campaign") . " \"$campaign_name\"",
		);

		# Add new subscriber action row.
		ac_sql_insert("#subscriber_action", $ins);
		$id = (int)ac_sql_insert_id();
	}

	if ($id == 0) {
		# Uh oh.
		return;
	}

	# Now save all of the parts.

	$actions = ac_http_param_forcearray("linkaction");
	$value1  = ac_http_param_forcearray("linkvalue1");
	$value2  = ac_http_param_forcearray("linkvalue2");
	$value3  = ac_http_param_forcearray("linkvalue3");
	$value4  = ac_http_param_forcearray("linkvalue4");

	foreach ($actions as $action) {
		$v1 = array_shift($value1);
		$v2 = array_shift($value2);
		$v3 = array_shift($value3);
		$v4 = array_shift($value4);

		$ins = array(
			"actionid" => $id,
			"act" => $action,
		);

		switch ($action) {
			case "subscribe":
			case "unsubscribe":
			default:
				$ins["targetid"] = (int)$v1;
				break;

			case "send":
				$ins["targetid"] = (int)$v2;
				break;

			case "update":
				if (preg_match('/^[0-9]+$/', $v3))
					$ins["targetid"] = (int)$v3;
				else
					$ins["targetfield"] = $v3;

				$ins["param"] = $v4;
				break;
		}

		ac_sql_insert("#subscriber_action_part", $ins);
	}

	return array("count" => count($actions), "linkid" => (int)ac_http_param("linkid"));
}

function campaign_load_action($id, $campaignid) {
	$id = (int)$id;
	$campaignid = (int)$campaignid;

	if ($id == 0) {
		// This is likely a read action.
		$rval = ac_sql_select_row("SELECT * FROM #subscriber_action WHERE type = 'read' AND campaignid = '$campaignid'");
	} else {
		$rval = ac_sql_select_row("SELECT * FROM #subscriber_action WHERE linkid = '$id' AND campaignid = '$campaignid'");
	}

	if (!$rval)
		return array("linkid" => $id, "parts" => array());

	$rval["linkid"] = $id;
	$rval["parts"] = ac_sql_select_array("SELECT * FROM #subscriber_action_part WHERE actionid = '$rval[id]'");
	return $rval;
}

function campaign_delete_action() {
	$id = (int)ac_http_param("id");
	$partid = (int)ac_http_param("partid");
	$actionid = (int)ac_sql_select_one("SELECT id FROM #subscriber_action WHERE linkid = '$id'");

	if ($partid == 0) {
		# Delete everything.
		ac_sql_query("DELETE FROM #subscriber_action_part WHERE actionid = '$actionid'");
	} else {
		# Just this one part.
		ac_sql_query("DELETE FROM #subscriber_action_part WHERE id = '$partid' AND actionid = '$actionid'");
	}

	return array("linkid" => $id);
}

function campaign_save_fixchars($str) {
	$str = str_replace('„', '"', $str);
	$str = str_replace('“', '"', $str);
	$str = str_replace('‘', "'", $str);
	$str = str_replace('’', "'", $str);

	return $str;
}

function campaign_save_fixlinkname($link) {
	if ($link["name"] == "") {
		if (strpos($link["link"], "p_v.php") !== false)
			$link["name"] = _a("Web Copy Link");
		elseif (strpos($link["link"], "p_f.php") !== false)
			$link["name"] = _a("Forward to a Friend Link");
		elseif (strpos($link["link"], "p_m.php") !== false)
			$link["name"] = _a("Update Subscriber link");
		elseif (strpos($link["link"], "proc.php") !== false && strpos($link["link"], "act=unsub&ALL") !== false)
			$link["name"] = _a("Unsubscribe (All Lists) Link");
		elseif (strpos($link["link"], "proc.php") !== false && strpos($link["link"], "act=unsub") !== false)
			$link["name"] = _a("Unsubscribe Link");
	}

	return $link;
}

function campaign_save_fetchmethod($campaignid, $fetch, $url) {
	if ($fetch == "send") {
		$campaign = campaign_select_row($campaignid);

		# By default, $campaign will only have list-specific fields.  We'll
		# ignore them and grab all fields, list+global.
		$lists = ac_array_extract($campaign["lists"], "id");
		$campaign["fields"] = list_get_fields($lists, true);

		# subscriber_dummy doesn't assign the lists property, which we need in
		# subscriber_personalize_get for %LISTID% and some other related
		# things.
		$sub = subscriber_dummy(_a('_t.e.s.t_@example.com'));
		$sub["lists"] = $campaign["lists"];

		$old = $url;
		$new = str_replace(array_keys(subscriber_personalize_get($sub, $campaign)), '', $url);

		# If they differ, then there are pers tags in the URL.  Do fetch-personalized with cust.
		if ($old != $new)
			return "cust";

		# If not, we can get away with fetch-at-send without personalization (which is much faster).
		return "send";
	}

	# If we get here, $fetch is probably "now".  Just return whatever value we have for $fetch.
	return $fetch;
}

?>
