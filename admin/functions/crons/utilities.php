#!/usr/local/bin/php
<?php
// require main include file
define('ACADMINCRON', true);
require_once(dirname(dirname(dirname(__FILE__))) . '/prepend.inc.php');
require_once(ac_global_functions('cron.php'));

if ( !defined('AC_CRON') ) define('AC_CRON', 1);

// turning off some php limits
@ignore_user_abort(1);
@ini_set('max_execution_time', 950 * 60);
@set_time_limit(950 * 60);
$ml = ini_get('memory_limit');
if ( $ml != -1 and (int)$ml < 128 and substr($ml, -1) == 'M') @ini_set('memory_limit', '128M');
set_include_path('.');
@set_magic_quotes_runtime(0);

// admin permission reset (use admin=1!)
$admin = ac_admin_get_totally_unsafe(1);

// Preload the language file
ac_lang_get('admin');


$cron_run_id = ac_cron_monitor_start(basename(__FILE__, '.php')); // log cron start

//ac_sync_run_cron(); // this runs sync
// clear out #*log tables
ac_sql_delete('#bounce_log', "tstamp < SUBDATE(NOW(), INTERVAL 1 MONTH)");
ac_sql_delete('#cron_log', "sdate < SUBDATE(NOW(), INTERVAL 1 DAY)");
ac_sql_delete('#emailaccount_log', "tstamp < SUBDATE(NOW(), INTERVAL 1 DAY)");
ac_sql_delete('#error_source', "tstamp < SUBDATE(NOW(), INTERVAL 1 MONTH)");
ac_sql_delete('#link_log', "tstamp < SUBDATE(NOW(), INTERVAL 1 DAY)");

// looking for data that is not needed anymore
ac_sql_query('DELETE FROM em_subscriber_list WHERE (SELECT count(*) FROM em_subscriber c WHERE c.id = subscriberid) = 0');
ac_sql_query('DELETE FROM em_subscriber WHERE (SELECT COUNT(*) FROM em_subscriber_list WHERE em_subscriber_list.subscriberid = em_subscriber.id) = 0');
ac_sql_query('DELETE FROM em_link WHERE (SELECT count(*) FROM em_campaign c WHERE c.id = campaignid) = 0');
ac_sql_query('DELETE FROM em_link_data WHERE (SELECT count(*) FROM em_link c WHERE c.id = linkid) = 0');
ac_sql_query('DELETE FROM em_link_log WHERE (SELECT count(*) FROM em_link c WHERE c.id = linkid) = 0');
ac_sql_query('DELETE FROM em_log WHERE (SELECT count(*) FROM em_campaign c WHERE c.id = campaignid) = 0');
ac_sql_query('DELETE FROM em_bounce_log WHERE (tstamp + INTERVAL 60 DAY) < NOW()');
ac_sql_query("TRUNCATE TABLE em_campaign_source");
ac_sql_query("TRUNCATE TABLE em_campaign_source_data");

if ( !isset($GLOBALS['_hosted_account']) ) ac_sql_delete('#log', "tstamp < SUBDATE(NOW(), INTERVAL 3 MONTH)");

if (isset($GLOBALS['_hosted_account'])) {
	ac_sql_delete("#delay", "tstamp < SUBDATE(NOW(), INTERVAL 3 DAY)");
	ac_sql_delete("#process", "ldate IS NOT NULL AND ldate != '0000-00-00 00:00:00' AND ldate < SUBDATE(NOW(), INTERVAL 1 MONTH)");

	$rs = ac_sql_query("SELECT * FROM #campaign WHERE status = '7'");
	while ($row = ac_sql_fetch_assoc($rs)) {
		// Do we actually have a pending approval for this?
	}
}

if ( (int)ac_sql_select_one('=COUNT(*)', '#subscriber_import') > 100000 ) {
	ac_sql_query("TRUNCATE TABLE #subscriber_import");
} else {
	ac_sql_delete('#subscriber_import', "tstamp < SUBDATE(NOW(), INTERVAL 3 DAY)");
}
ac_sql_delete('#trapperrlogs', "tstamp < SUBDATE(NOW(), INTERVAL 1 MONTH)");

$rmlist = ac_sql_select_list("
	SELECT s.id
	FROM #campaign_source s, #campaign c
	WHERE s.campaignid = c.id
	AND c.ldate < SUBDATE(NOW(), INTERVAL 14 DAY)
");
foreach ( $rmlist as $rmid ) {
	ac_sql_delete('#campaign_source_data', "sourceid = '$rmid'");
	ac_sql_delete('#campaign_source', "id = '$rmid'");
}

// temporary query, to remove all deleted campaigns; we can remove this in a few days because campaign_delete() now purges the sources
$rmlist = ac_sql_select_list("
	SELECT s.id
	FROM #campaign_source s
	LEFT JOIN #campaign c
	ON s.campaignid = c.id
	WHERE c.id IS NULL
");
foreach ( $rmlist as $rmid ) {
	ac_sql_delete('#campaign_source_data', "sourceid = '$rmid'");
	ac_sql_delete('#campaign_source', "id = '$rmid'");
}

$rmlist = ac_sql_select_list("
	SELECT s.id
	FROM #campaign_spamcheck s, #campaign c
	WHERE s.campaignid = c.id
	AND c.ldate < SUBDATE(NOW(), INTERVAL 14 DAY)
");
foreach ( $rmlist as $rmid ) {
	ac_sql_delete('#campaign_spamcheck', "id = '$rmid'");
}

ac_file_delete_old(ac_cache_dir(), 14, '\.msg$');

$cron_run_id = ac_cron_monitor_stop(); // log cron end

?>
