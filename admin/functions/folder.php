<?php

require_once ac_global_classes("select.php");

function folder_select_query(&$so, $campcond = null) {
	global $admin;
	$ijoin = "";
	$cond = "";
	if ( $admin['id'] != 1 ) {
		$ijoin .= ", #campaign_list l";
		$cond  .= "AND l.campaignid = cf.campaignid AND l.listid IN ('" . implode("', '", $admin['groups']) . "')";
	}
	if ( $campcond ) {
		$ijoin .= ", #campaign c";
		$cond  .= "AND cf.campaignid = c.id AND $campcond";
	}
	$gid = current($admin['groups']);
	return $so->query("
		SELECT
			f.*,
			( SELECT COUNT(DISTINCT(cf.campaignid)) FROM #campaign_folder cf $ijoin WHERE cf.folderid = f.id $cond ) AS `count`
		FROM
			#folder f
		WHERE
			f.groupid = '$gid'
		AND
		[...]
	");
}

function folder_select_row($id) {
	$id = intval($id);
	$so = new AC_Select;
	$so->push("AND f.id = '$id'");

	return ac_sql_select_row(folder_select_query($so));
}

function folder_select_array($so = null, $ids = null, $campcond = null) {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		if ( !is_array($ids) ) $ids = explode(',', $ids);
		$tmp = array_diff(array_map("intval", $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND f.id IN ('$ids')");
	}
	return ac_sql_select_array(folder_select_query($so, $campcond));
}

function folder_select_array_paginator($id, $sort, $offset, $limit, $filter) {
	$admin = ac_admin_get();
	$so = new AC_Select;

	$filter = intval($filter);
	if ($filter > 0) {
		$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'folder'");
		$so->push($conds);
	}

	$so->count();
	$total = (int)ac_sql_select_one(folder_select_query($so));

	switch ($sort) {
		default:
		case '01':
			$so->orderby("folder"); break;
		case '01D':
			$so->orderby("folder DESC"); break;
		//case '02':
			//$so->orderby("type"); break;
		//case '02D':
			//$so->orderby("type DESC"); break;
		//case '03':
			//$so->orderby("descript"); break;
		//case '03D':
			$so->orderby("descript DESC"); break;
	}

	if ( (int)$limit == 0 ) $limit = 999999999;
	$limit  = (int)$limit;
	$offset = (int)$offset;
	$so->limit("$offset, $limit");
	$rows = folder_select_array($so);

	return array(
		"paginator"   => $id,
		"offset"      => $offset,
		"limit"       => $limit,
		"total"       => $total,
		"cnt"         => count($rows),
		"rows"        => $rows,
	);
}

function folder_filter_post() {
	$whitelist = array("f.folder");

	$ary = array(
		"userid" => $GLOBALS['admin']['id'],
		"sectionid" => "folder",
		"conds" => "",
		"=tstamp" => "NOW()",
	);

	if (isset($_POST["qsearch"]) && !isset($_POST["content"])) {
		$_POST["content"] = $_POST["qsearch"];
	}

	if (isset($_POST["content"]) and $_POST["content"] != "") {
		$content = ac_sql_escape($_POST["content"], true);
		$conds = array();

		if (!isset($_POST["section"]) || !is_array($_POST["section"]))
			$_POST["section"] = $whitelist;

		foreach ($_POST["section"] as $sect) {
			if (!in_array($sect, $whitelist))
				continue;
			$conds[] = "$sect LIKE '%$content%'";
		}

		$conds = implode(" OR ", $conds);
		$ary["conds"] = "AND ($conds) ";
	}
	if ( $ary['conds'] == '' ) return array('filterid' => 0);

	$conds_esc = ac_sql_escape($ary["conds"]);
	$filterid = ac_sql_select_one("
		SELECT
			id
		FROM
			#section_filter
		WHERE
			userid = '$ary[userid]'
		AND
			sectionid = 'folder'
		AND
			conds = '$conds_esc'
	");

	if (intval($filterid) > 0)
		return array("filterid" => $filterid);
	ac_sql_insert("#section_filter", $ary);
	return array("filterid" => ac_sql_insert_id());
}

function folder_insert_post() {
	global $admin;
	$gid = current($admin['groups']);
	if ( !$admin['pg_message_add'] and !$admin['pg_message_edit'] and !$admin['pg_message_delete'] ) {
		return ac_ajax_api_result(false, _a("You do not have a permission to manage Folders."));
	}
	$ary = folder_prepare_post();

	if ( $ary['folder'] == '' ) {
		return ac_ajax_api_result(false, _a("Folder not provided."));
	}
	if ( ac_sql_value_exists("#folder", "folder", $ary['folder'], "AND groupid = '$gid'") ) {
		return ac_ajax_api_result(false, _a("Folder name already exists."));
	}

	$sql = ac_sql_insert("#folder", $ary);
	if ( !$sql ) {
		return ac_ajax_api_result(false, _a("Folder could not be added."));
	}
	$ary['id'] =
	$id = ac_sql_insert_id();

	return ac_ajax_api_added(_a("Folder"), $ary);
}

function folder_update_post() {
	global $admin;
	$gid = current($admin['groups']);
	if ( !$admin['pg_message_add'] and !$admin['pg_message_edit'] and !$admin['pg_message_delete'] ) {
		return ac_ajax_api_result(false, _a("You do not have a permission to manage Folders."));
	}
	$ary = folder_prepare_post();
	$id = intval($_POST["id"]);

	if ( $ary['folder'] == '' ) {
		return ac_ajax_api_result(false, _a("Folder not provided."));
	}
	if ( ac_sql_value_exists("#folder", "folder", $ary['folder'], "AND groupid = '$gid' AND id != '$id'") ) {
		return ac_ajax_api_result(false, _a("Folder name already exists."));
	}

	$sql = ac_sql_update("#folder", $ary, "id = '$id'");
	if ( !$sql ) {
		return ac_ajax_api_result(false, _a("Folder could not be updated."));
	}

	$ary['id'] = $id;
	return ac_ajax_api_updated(_a("Folder"), $ary);
}

function folder_delete($id) {
	global $admin;
	$gid = current($admin['groups']);
	if ( !$admin['pg_message_add'] and !$admin['pg_message_edit'] and !$admin['pg_message_delete'] ) {
		return ac_ajax_api_result(false, _a("You do not have a permission to manage Folders."));
	}
	$id = intval($id);
	ac_sql_query("DELETE FROM #folder WHERE id = '$id' AND groupid = '$gid'");
	ac_sql_query("DELETE FROM #campaign_folder WHERE folderid = '$id'");
	return ac_ajax_api_deleted(_a("Folder"));
}

function folder_delete_multi($ids, $filter = 0) {
	global $admin;
	//$gid = current($admin['groups']);
	if ( !$admin['pg_message_add'] and !$admin['pg_message_edit'] and !$admin['pg_message_delete'] ) {
		return ac_ajax_api_result(false, _a("You do not have a permission to manage Folders."));
	}
	if ( $ids == '_all' ) {
		$tmp = array();
		$so = new AC_Select();
		$filter = intval($filter);
		if ($filter > 0) {
			$admin = ac_admin_get();
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'folder'");
			$so->push($conds);
		}
		$all = folder_select_array($so);
		foreach ( $all as $v ) {
			$tmp[] = $v['id'];
		}
	} else {
		$tmp = array_map("intval", explode(",", $ids));
	}
	foreach ( $tmp as $id ) {
		$r = folder_delete($id);
	}
	return $r;
}

function folder_prepare_post() {
	global $admin;
	$gid = current($admin['groups']);
	$r = array(
		'groupid' => $gid,
		'folder' => trim((string)ac_http_param('folder')),
		//'type' => (string)ac_http_param('type'),
		//'descript' => trim((string)ac_http_param('descript'))
	);
	return $r;
}

function folder_campaign_add($folderid, $campaignid) {
	$folderid = (int)$folderid;
	$campaignid = (int)$campaignid;

	$folder = folder_select_row($folderid);
	if ( !$folder ) {
		return ac_ajax_api_result(false, _a("Folder does not exist."));
	}

	$campaign = campaign_select_row($campaignid);
	if ( !$campaign ) {
		return ac_ajax_api_result(false, _a("Campaign does not exist."));
	}

	$found = (int)ac_sql_select_one("=COUNT(*)", "#campaign_folder", "folderid = '$folderid' AND campaignid = '$campaignid'");
	if ( $found ) {
		return ac_ajax_api_result(false, _a("This campaign is already in this folder."));
	}

	$in = array(
		'id' => 0,
		'folderid' => $folderid,
		'campaignid' => $campaignid,
	);
	ac_sql_insert("#campaign_folder", $in);

	return ac_ajax_api_result(true, _a("Campaign added to folder."), $folder);
}

function folder_campaign_remove($folderid, $campaignid) {
	$folderid = (int)$folderid;
	$campaignid = (int)$campaignid;

	$folder = folder_select_row($folderid);
	if ( !$folder ) {
		return ac_ajax_api_result(false, _a("Folder does not exist."));
	}

	$campaign = campaign_select_row($campaignid);
	if ( !$campaign ) {
		return ac_ajax_api_result(false, _a("Campaign does not exist."));
	}

	$found = (int)ac_sql_select_one("=COUNT(*)", "#campaign_folder", "folderid = '$folderid' AND campaignid = '$campaignid'");
	if ( !$found ) {
		return ac_ajax_api_result(false, _a("This campaign is not in this folder."));
	}

	ac_sql_delete("#campaign_folder", "folderid = '$folderid' AND campaignid = '$campaignid'");

	return ac_ajax_api_result(true, _a("Campaign removed from folder."), $folder);
}

?>
