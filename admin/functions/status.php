<?php

$GLOBALS['_status_datecols'] = array('tstamp_created', 'tstamp_viewed', 'tstamp_clicked', 'tstamp_closed');

function status_get($limit = 8) {
	global $admin;

	$r = array();

	// get user's group
	reset($admin['groups']);
	$gid = (int)current($admin['groups']);

	$lim = "";
	$limit = (int)$limit;
	if ( $limit ) $lim = "LIMIT $limit";

	// fetch this group's statuses
	$qry = "SELECT * FROM #status WHERE groupid IN ('$gid', 0) AND tstamp_closed IS NULL ORDER BY tstamp_created DESC $lim";
	$sql = ac_sql_query($qry);
	while ( $row = ac_sql_fetch_assoc($sql, $GLOBALS['_status_datecols']) ) {
		$r[] = $row;
	}

	return $r;
}

function status_viewed() {
	global $admin;

	// get user's group
	reset($admin['groups']);
	$gid = (int)current($admin['groups']);

	ac_sql_update_one("#status", "=tstamp_viewed", "NOW()", "groupid IN ('$gid', 0) AND tstamp_viewed IS NULL");

	return ac_ajax_api_result(true, _a("Statuses have been marked as viewed."));
}

function status_clicked($id) {
	global $admin;

	$id = (int)$id;
	if ( !$id ) return ac_ajax_api_result(false, _a("Status not provided."));

	// get user's group
	reset($admin['groups']);
	$gid = (int)current($admin['groups']);

	$qry = "SELECT * FROM #status WHERE id = '$id' AND groupid IN ('$gid', 0)";
	$status = ac_sql_select_row($qry, $GLOBALS['_status_datecols']);
	if ( !$status ) return ac_ajax_api_result(false, _a("Status not found."));

	$up = array(
		"=tstamp_viewed" => "NOW()",
		"=tstamp_clicked" => "NOW()",
	);
	ac_sql_update("#status", $up, "id = '$id' AND groupid IN ('$gid', 0)", $GLOBALS['_status_datecols']);

	return ac_ajax_api_result(true, _a("Status has been marked as clicked."), $status);
}

function status_add($type, $groupid, $title, $descript, $link) {

	// does this status already exist
	$status = status_exists($groupid, $type, $title);

	if ( !$status ) {
		// if it doesn't, create a new one
		$suppress = 'delete';
		$ary = array(
			'id' => 0,
			'groupid' => $groupid,
			'type' => $type,
			'title' => $title,
			'descript' => $descript,
			'link' => $link,
			'suppress_on' => $suppress,
			'=tstamp_created' => 'NOW()',
			'=tstamp_viewed' => 'NULL',
			'=tstamp_clicked' => 'NULL',
			'=tstamp_closed' => 'NULL',
		);
		$sql = ac_sql_insert("#status", $ary, $GLOBALS['_status_datecols']);
		if ( !$sql ) return false;
		$id = (int)ac_sql_insert_id();
		return $id;
	} else {
		// if it exists, move it up and reset previous actions
		$id = $status['id'];
		$up = array(
			'=tstamp_created' => 'NOW()',
			'=tstamp_viewed' => 'NULL',
			'=tstamp_clicked' => 'NULL',
			'=tstamp_closed' => 'NULL',
		);
		ac_sql_update("#status", $up, "id = '$id'", $GLOBALS['_status_datecols']);
	}
	cache_clear('main_statuses');

	return $id;
}

function status_remove($groupid, $type, $title) {
	$status = status_exists($groupid, $type, $title);
	if ( !$status ) return false;

	$id = (int)$status['id'];
	ac_sql_delete("#status", "id = '$status[id]'");
	return true;
}

function status_removed($id) {
	global $admin;

	$id = (int)$id;
	if ( !$id ) return ac_ajax_api_result(false, _a("Status not provided."));

	// get user's group
	reset($admin['groups']);
	$gid = (int)current($admin['groups']);

	$qry = "SELECT * FROM #status WHERE id = '$id' AND groupid IN ('$gid', 0)";
	$status = ac_sql_select_row($qry, $GLOBALS['_status_datecols']);
	if ( !$status ) return ac_ajax_api_result(false, _a("Status not found."));

	$up = array(
		"=tstamp_closed" => "NOW()",
	);
	ac_sql_update("#status", $up, "id = '$id' AND groupid IN ('$gid', 0)", $GLOBALS['_status_datecols']);

	return ac_ajax_api_result(true, _a("Status has been marked as closed."), $status);
}

function status_exists($groupid, $type, $title) {
	// conditions array to check if exists
	$cond = array(
		'groupid' => $groupid,
		'type' => $type,
		'title' => $title,
		//'=tstamp_closed' => 'NULL',
	);
	// does this status already exist
	$row = ac_sql_select_row("SELECT * FROM #status WHERE " . ac_sql_cond_str($cond));
	return $row;
}


?>
