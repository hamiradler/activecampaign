<?php

function form_message_default($file) {
	// Return the content, translated via smarty fetch, of $file located in /templates/emails
	$smarty = new AC_Smarty('admin');
	return $smarty->fetch('emails/' . $file);
}

function form_list_init($listid) {
	$listid = (int)$listid;
	$name = ac_sql_select_one("SELECT name FROM #list WHERE id = '$listid'");
	
	// Create all of the forms that any list SHOULD have.
	$forms = array(
		FORM_SUBSCRIBE_RESULT => form_list_init_target($listid, sprintf(_a("%s Subscriber result"), $name), FORM_SUBSCRIBE_RESULT),
		FORM_SUBSCRIBE_CONFIRM_RESULT => form_list_init_target($listid, sprintf(_a("%s Subscriber confirmation result"), $name), FORM_SUBSCRIBE_CONFIRM_RESULT),
		FORM_SUBSCRIBE_ERROR => form_list_init_target($listid, sprintf(_a("%s Subscriber error"), $name), FORM_SUBSCRIBE_ERROR),
		FORM_UNSUBSCRIBE => form_list_init_target($listid, sprintf(_a("%s Unsubscribe"), $name), FORM_UNSUBSCRIBE),
		FORM_UNSUBSCRIBE_ERROR => form_list_init_target($listid, sprintf(_a("%s Unsubscribe error"), $name), FORM_UNSUBSCRIBE_ERROR),
		FORM_UNSUBSCRIBE_RESULT => form_list_init_target($listid, sprintf(_a("%s Unsubscribe result"), $name), FORM_UNSUBSCRIBE_RESULT),
		FORM_DETAILS => form_list_init_target($listid, sprintf(_a("%s Update details"), $name), FORM_DETAILS),
		FORM_DETAILS_CONFIRM => form_list_init_target($listid, sprintf(_a("%s Update details confirmation"), $name), FORM_DETAILS_CONFIRM),
		FORM_DETAILS_REQUEST => form_list_init_target($listid, sprintf(_a("%s Update details request"), $name), FORM_DETAILS_REQUEST),
		FORM_DETAILS_REQUEST_CONFIRM => form_list_init_target($listid, sprintf(_a("%s Update details request confirmation"), $name), FORM_DETAILS_REQUEST_CONFIRM),
		FORM_ARCHIVE => form_list_init_target($listid, sprintf(_a("%s Archive"), $name), FORM_ARCHIVE),
		FORM_FORWARD => form_list_init_target($listid, sprintf(_a("%s Forward to a friend"), $name), FORM_FORWARD),
		FORM_FORWARD_RESULT => form_list_init_target($listid, sprintf(_a("%s Forward to a friend result"), $name), FORM_FORWARD_RESULT),
	);

	// Now create potential mail messages.  First up: unsubscribe result message
	form_list_init_message($forms[FORM_UNSUBSCRIBE], _a("Your unsubscription confirmation"), form_message_default('form_unsubscribe.tpl'));
	form_list_init_message($forms[FORM_DETAILS_REQUEST], _a("Update your subscription to") . " %LISTNAME%", form_message_default('form_details_request.tpl'));
}

function form_list_init_target($listid, $name, $target) {
	// Create all of the forms that a list SHOULD have by default.

	$ins = array(
		"name" => $name,
		"target" => $target,
		"theme" => "simple-blue",
	);

	if ($target == FORM_FORWARD || $target == FORM_ARCHIVE)
		$ins["widthpx"] = 600;

	ac_sql_insert("#form", $ins);
	$id = (int)ac_sql_insert_id();

	# Set up list relation
	$ins = array(
		"formid" => $id,
		"listid" => $listid,
	);

	ac_sql_insert("#form_list", $ins);

	// See if we need to add any form parts.
	switch ($target) {
		default:
			break;

		case FORM_SUBSCRIBE:
			$ins = array(
				"formid" => $id,
				"builtin" => "fullname",
				"header" => _a("Full Name"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "email",
				"header" => _a("Email"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Subscribe"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_SUBSCRIBE_RESULT:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Thanks for your subscription!"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => _a("A confirmation email is on its way to your inbox.") . " " . _a("Click the link there to confirm your subscription!"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_SUBSCRIBE_CONFIRM_RESULT:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("You are now subscribed"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => _a("Thanks!"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_SUBSCRIBE_ERROR:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("An error occurred..."),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "error",
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_UNSUBSCRIBE:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Unsubscribe"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "email",
				"header" => _a("Email"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Unsubscribe"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_UNSUBSCRIBE_ERROR:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("An error occurred..."),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "error",
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_UNSUBSCRIBE_RESULT:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("You have been unsubscribed"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => 
					_a("You have been unsubscribed from these emails.") . " " .
					_a("Please take a moment and let us know why you unsubscribed."),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "unsubreasonlist",
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Share Feedback"),
				"ordernum" => 3,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_DETAILS:
			// This one is tricky.  We should add email, name; sure.  But also 
			// any custom fields which are visible for this list.
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Update your account"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "email",
				"header" => _a("Email"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "fullname",
				"header" => _a("Full Name"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);

			$rs = ac_sql_query("SELECT f.id, f.title FROM #field f, #field_rel r WHERE f.id = r.fieldid AND r.relid = '0'");
			$i = 3;
			while ($field = ac_sql_fetch_assoc($rs)) {
				$ins = array(
					"formid" => $id,
					"fieldid" => $field["id"],
					"header" => $field["title"],
					"ordernum" => $i,
				);

				ac_sql_insert("#form_part", $ins);
				$i++;
			}

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Update"),
				"ordernum" => $i,
			);

			ac_sql_insert("#form_part", $ins);

			break;

		case FORM_DETAILS_REQUEST:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Update your account"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "email",
				"header" => _a("Email"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Submit"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_DETAILS_CONFIRM:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Thanks!"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => _a("Your subscriber information has been updated."),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_DETAILS_REQUEST_CONFIRM:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Please check your email"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => _a("We sent you an email with details on how you can update your subscription details."),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_ARCHIVE:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Archive"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "archive",
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_FORWARD:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Forward to a friend"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => _a("If you think a friend of yours would be interested in this message, send it to them!"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "_forward_name",
				"header" => _a("Your name"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "_forward_email",
				"header" => _a("Your email"),
				"ordernum" => 3,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "_forward_rcpt",
				"header" => _a("Recipients"),
				"ordernum" => 4,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "_forward_preview",
				"header" => _a("Message preview"),
				"ordernum" => 5,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "captcha",
				"header" => _a("Enter the value below"),
				"ordernum" => 6,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Forward"),
				"ordernum" => 7,
			);

			ac_sql_insert("#form_part", $ins);
			break;

		case FORM_FORWARD_RESULT:
			$ins = array(
				"formid" => $id,
				"builtin" => "header",
				"header" => _a("Thanks!"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "freeform",
				"content" => _a("Your message has been forwarded"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);
			break;
	}

	return $id;
}

function form_list_init_message($formid, $subject, $html) {
	$formid = (int)$formid;
	$admin = $GLOBALS['admin'];

	$ins = array(
		"userid" => $admin["id"],
		"=cdate" => "NOW()",
		"=mdate" => "NOW()",
		"fromname" => $admin["first_name"] . " " . $admin["last_name"],
		"fromemail" => $admin["email"],
		"reply2" => $admin["email"],
		"priority" => 3,
		"charset" => "utf-8",
		"encoding" => "quoted-printable",
		"format" => "mime",
		"subject" => $subject,
		"html" => $html,
		"htmlfetch" => "now",
		"textfetch" => "now",
	);

	$ins["text"] = ac_htmltext_convert($ins["html"]);

	ac_sql_insert("#message", $ins);
	$messageid = (int)ac_sql_insert_id();

	$up = array(
		"messageid" => $messageid,
	);

	ac_sql_update("#form", $up, "id = '$formid'");
}

?>
