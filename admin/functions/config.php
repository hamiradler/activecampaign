<?php

function config_id($key, $userid = 0) {
	// Return the id of a config row based on key.
	$key = ac_sql_escape($key);
	$rs = ac_sql_query("SELECT `id` FROM #config WHERE keyname = '$key' AND userid = '$userid'");

	if (!$rs || !ac_sql_num_rows($rs) || !($row = ac_sql_fetch_assoc($rs)))
		return null;

	return (int)$row["id"];
}

function config_get($key, $userid = 0) {
	// Return the val of a config row based on key.
	$key = ac_sql_escape($key);
	$rs = ac_sql_query("SELECT `val` FROM #config WHERE keyname = '$key' AND userid = '$userid'");

	if (!$rs || !ac_sql_num_rows($rs) || !($row = ac_sql_fetch_assoc($rs)))
		return null;

	return $row["val"];
}

function config_set($key, $val, $userid = 0) {
	// Assign a config key/val pair if one doesn't exist, or update the pair if one does exist.
	$id = config_id($key);

	if (!$id) {
		$ins = array(
			"keyname" => $key,
			"val" => $val,
			"userid" => $userid,
		);

		ac_sql_insert("#config", $ins);
	} else {
		$up = array(
			"val" => $val,
		);

		ac_sql_update("#config", $up, "id = '$id'");
	}
}

function config_set_post() {
	$key = ac_http_param("key");
	$val = ac_http_param("val");

	return config_set($key, $val);
}

function config_group($keypart, $userid = 0) {
	// Return an array of all config vals that start with 'keypart.*'.  For instance,
	// config_group('public') could return:
	//   public.defaultform => 1
	//   public.whatever => etc

	$keypart = ac_sql_escape($keypart);
	$rs = ac_sql_query("SELECT `keyname`, `val` FROM #config WHERE `keyname` LIKE '$keypart.%' AND userid = '$userid'");
	$rval = array();

	while ($row = ac_sql_fetch_assoc($rs)) {
		$rval[str_replace($keypart . ".", "", $row["keyname"])] = $row["val"];
	}

	return $rval;
}

?>
