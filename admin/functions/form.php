<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("smarty.php");
require_once ac_global_functions("custom_fields.php");
require_once ac_global("smarty_plugins/function.ac_cfield.php");
require_once ac_admin("functions/list.php");
require_once ac_admin("functions/html.php");

require_once ac_admin("functions/form_part.php");
require_once ac_admin("functions/form_init.php");
require_once ac_admin("functions/form_send.php");

// Some constants for the target field.
define("FORM_SUBSCRIBE", 1);
define("FORM_UNSUBSCRIBE", 2);
define("FORM_UNSUBSCRIBE_RESULT", 3);
define("FORM_DETAILS", 4);
define("FORM_DETAILS_REQUEST", 5);
define("FORM_DETAILS_CONFIRM", 6);
define("FORM_DETAILS_REQUEST_CONFIRM", 7);
define("FORM_SUBSCRIBE_CONFIRM_RESULT", 8);
define("FORM_SUBSCRIBE_RESULT", 9);
define("FORM_SUBSCRIBE_ERROR", 10);
define("FORM_ARCHIVE", 11);
define("FORM_FORWARD", 12);
define("FORM_FORWARD_RESULT", 13);
define("FORM_UNSUBSCRIBE_ERROR", 14);

// What we're compiling for.
// - Use "preview" if the admin-side version.
// - Use "working" if we're compiling for a real, usable form.
$GLOBALS["form_compile_view"] = "preview";

// The default image we're using for new image parts.
$GLOBALS["form_default_image"] = ac_admin("images/image_field_default.gif");

// The default alignment for any image.
$GLOBALS["form_default_image_align"] = "center";

// Whether to enable form submission or not.
$GLOBALS["form_enable_submit"] = true;

function form_select_query(&$so) {
	return $so->query("
		SELECT
			*
		FROM
			#form f
		WHERE
			[...]
	");
}

function form_select_row($id) {
	$id = intval($id);
	$so = new AC_Select;
	$so->push("AND id = '$id'");

	return ac_sql_select_row(form_select_query($so));
}

function form_select_row_listid($listid, $target) {
	$form = ac_sql_select_row(sprintf("
		SELECT
			f.*
		FROM
			#form f,
			#form_list l
		WHERE
			f.target = '%s'
		AND
			f.id = l.formid
		AND
			l.listid = '$listid'
		", $target));

	return $form;
}

function form_select_array($so = null, $ids = null) {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		if ( !is_array($ids) ) $ids = explode(',', $ids);
		$tmp = array_diff(array_map('intval', $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND id IN ('$ids')");
	}
	$rows = ac_sql_select_array(form_select_query($so));
	foreach ($rows as $k => $v) {
		$subscriptions = ac_sql_select_one("SELECT COUNT(*) FROM em_subscriber_list WHERE formid = '$v[id]'");
		$rows[$k]["subscriptions"] = $subscriptions;
	}
	//$rows = array();
	return $rows;
}

function form_delete($id) {
	$id = intval($id);
	ac_sql_query("DELETE FROM #form WHERE id = '$id'");
	form_delete_relations(array($id));

	if (Config::lookup("public.defaultform") == $id)
		Config::assign("public.defaultform", 0);

	return ac_ajax_api_deleted(_a("Form"));
}

function form_delete_multi($ids) {
	if ($ids == "_all") {
		ac_sql_query("TRUNCATE TABLE #form");
		form_delete_relations(null);
		return;
	}
	$tmp = array_map("intval", explode(",", $ids));

	$def = Config::lookup("public.defaultform");
	foreach ($tmp as $id) {
		if ($def == $id) {
			Config::assign("public.defaultform", 0);
			break;
		}
	}

	$ids = implode("','", $tmp);
	ac_sql_query("DELETE FROM #form WHERE id IN ('$ids')");
	form_delete_relations($tmp);
	return ac_ajax_api_deleted(_a("Form"));
}

function form_delete_relations($ids) {
	if ($ids === null) {		# delete all
		ac_sql_query("DELETE FROM #form_part");
		ac_sql_query("DELETE FROM #screenshot WHERE target = 'form'");
	} else {
		$idstr = implode("','", $ids);
		ac_sql_query("DELETE FROM #form_part WHERE formid IN ('$idstr')");
		ac_sql_query("DELETE FROM #screenshot WHERE target = 'form' AND targetid IN ('$idstr')");
	}
}

// --

function form_disable($id, $val) {
	$id = (int)$id;
	$up = array(
		"disabled" => $val,
	);

	ac_sql_update("#form", $up, "id = '$id'");
}

function form_save_name() {
	$id = (int)ac_http_param("id");
	$up = array(
		"name" => (string)ac_http_param("name"),
	);

	ac_sql_update("#form", $up, "id = '$id'");
}

function form_save_widthpx() {
	$id = (int)ac_http_param("id");
	$up = array(
		"widthpx" => (int)ac_http_param("widthpx"),
	);

	ac_sql_update("#form", $up, "id = '$id'");
}

function form_stop_redirecting() {
	$formid = (int)ac_http_param("id");
	$up = array(
		"redirecturl" => "",
	);

	ac_sql_update("#form", $up, "id = '$formid'");
}

function form_recognize_field($fieldid, $title = '', $visible = null) {
	$fieldid = (int)$fieldid;

	if ($visible === null || !$title) {
		$result = ac_sql_select_row("SELECT title, visible FROM #field WHERE id = '$fieldid'");
		$visible = $result["visible"];
		$title = $result["title"];
	}

	if (!$visible)
		return;

	$lists = ac_sql_select_list("SELECT relid FROM #field_rel WHERE fieldid = '$fieldid'");
	$liststr = implode("','", $lists);

	// Find any detail forms for these lists where this form part does not
	// appear to exist, and add it there.
	$rs = ac_sql_query("
		SELECT
			l.formid
		FROM
			#form_list l
		LEFT JOIN
			#form f
		ON
			f.id = l.formid
		LEFT JOIN
			#form_part p
		ON
			p.formid = l.formid AND p.fieldid = '$fieldid'
		WHERE
			f.target = 4
		AND
			l.listid IN ('$liststr')
		AND
			p.formid IS NULL
	");

	while ($row = ac_sql_fetch_assoc($rs)) {
		$ins = array(
			"formid" => $row["formid"],
			"fieldid" => $fieldid,
			"header" => $title,
			"ordernum" => form_bottom_order($row["formid"]),
		);

		ac_sql_insert("#form_part", $ins);
	}

	// Now find any details forms where this field doesn't belong any longer.
	$rs = ac_sql_query("
		SELECT
			DISTINCT p.id
		FROM
			#form_part p,
			#form_list l
		WHERE
			p.fieldid = '$fieldid'
		AND
			p.formid = l.formid
		AND
			l.listid NOT IN ('$liststr')
	");

	while ($row = ac_sql_fetch_assoc($rs)) {
		ac_sql_delete("#form_part", "id = '$row[id]'");
	}
}

function form_bottom_order($formid) {
	$formid = (int)$formid;
	$last = ac_sql_select_row("
		SELECT
			id,
			builtin,
			ordernum
		FROM
			#form_part
		WHERE
			formid = '$formid'
		ORDER BY
			ordernum DESC
	");

	if ($last["builtin"] == "subscribe") {
		// We want to keep the subscribe builtin at the bottom, so that needs
		// to move down one place and this needs to pop up above it.
		ac_sql_query("UPDATE #form_part SET ordernum = ordernum + 1 WHERE id = '$last[id]'");
		return $last["ordernum"];
	} else {
		// Subscribe isn't at the bottom, so we're clear to just add this field
		// at the bottom.
		return $last["ordernum"] + 1;
	}
}

function form_getforms() {
	$admin = ac_admin_get();
	$liststr = implode("','", $admin["lists"]);

	$so = new AC_Select;
	$so->push("AND (SELECT COUNT(*) FROM #form_list l WHERE l.formid = f.id AND l.listid IN ('$liststr')) > 0");
	$so->push("AND target = '1'");

	$rows = ac_sql_select_array(form_select_query($so));
	foreach ($rows as $k => $v) {
		$subscriptions = ac_sql_select_one("SELECT COUNT(*) FROM em_subscriber_list WHERE formid = '$v[id]'");
		$form_lists = ac_sql_select_list("SELECT listid FROM em_form_list WHERE formid = '$v[id]'");
		$rows[$k]["subscriptions"] = $subscriptions;
		$rows[$k]["lists"] = $form_lists;		
	}

	return $rows;
}

function form_apply_theme() {
	$id = (int)ac_http_param("id");
	$name = (string)ac_http_param("formname");

	if (!$id || !$name)
		return ac_ajax_api_result(false, _a("No such theme"));

	if (ac_str_instr("..", $name) || ac_str_instr("/", $name))
		return ac_ajax_api_result(false, _a("No such theme"));

	if (!file_exists(dirname(dirname(__FILE__)) . "/templates/form-themes/$name/style.css"))
		return ac_ajax_api_result(false, _a("No such theme"));

	$target = (int)ac_sql_select_one("SELECT target FROM #form WHERE id = '$id'");

	if ($target == FORM_SUBSCRIBE) {
		$up = array(
			"theme" => $name,
			"waitpreview" => 1,
		);

		ac_sql_update("#form", $up, "id = '$id'");
	} else {
		// If this isn't a subscription form, then we shouldn't request a
		// screenshot and we should apply the theme to forms for the list that
		// are not subscribe forms.
		$listid = (int)ac_sql_select_one("SELECT listid FROM #form_list WHERE formid = '$id' LIMIT 1");
		$allforms = ac_sql_select_list("SELECT formid FROM #form_list WHERE listid = '$listid'");
		$formstr = implode("','", $allforms);

		$up = array(
			"theme" => $name,
		);

		ac_sql_update("#form", $up, "id IN ('$formstr') AND target > 1");
	}
}

function form_optin_save() {
	$id = (int)ac_http_param("id");
	$messageid = (int)ac_http_param("messageid");
	$manage = (int)ac_http_param("managetext");

	$optin_checked = (int)ac_http_param("sendoptin");

	$up = array(
		"managetext" => $manage,
		"sendoptin" => $optin_checked,
	);

	ac_sql_update("#form", $up, "id = '$id'");

	$up = array(
		"fromname" => ac_http_param("fromname"),
		"fromemail" => ac_http_param("fromemail"),
		"reply2" => ac_http_param("reply2"),
		"subject" => ac_http_param("subject"),
		"html" => ac_http_param("html"),
		"text" => ac_http_param("text"),
	);

	# If they're not managing their own text version, do a quick convert on the html.
	if (!$manage) {
		$up["text"] = ac_htmltext_convert($up["html"]);
	}

	foreach ($up as $k => $v) {
		if ($k != "reply2" && !$v) {
			// if any value (besides reply2) is empty, don't save to em_message
			return;
		}
	}

	ac_sql_update("#message", $up, "id = '$messageid'");
}

function form_redirect($form, $sub, $action, $codes, $lists, $ask4reason, $extra) {
	switch ($form['target']) {
		case FORM_SUBSCRIBE:
		default:
			break;

		case FORM_UNSUBSCRIBE:
			if (isset($extra["error"])) {
				$_SESSION["unsubscribe_error_message"] = $extra["error"];
				return rewrite_plink("unsubscribe_error", "listid=$lists", "codes=$codes", "s=$sub[hash]");
			} else {
				return rewrite_plink("unsubscribe_result", "listid=$lists", "codes=$codes", "s=$sub[hash]", "c=$extra[c]", "m=$extra[m]");
			}

		case FORM_UNSUBSCRIBE_RESULT:
			return rewrite_plink("unsubscribe_result", "listid=$lists", "codes=$codes", "s=$sub[hash]", "result=thanks");

		case FORM_DETAILS_REQUEST:
			return rewrite_plink("update_request_confirm", "listid=$lists", "codes=$codes", "s=$sub[hash]");

		case FORM_DETAILS:
			return rewrite_plink("update_confirm", "listid=$lists", "codes=$codes", "s=$sub[hash]");

		case FORM_FORWARD:
			return rewrite_plink("forward_result", "listid=$lists");
	}

	// If we got here, this is probably for FORM_SUBSCRIBE.  Check if we have any errors.
	foreach ($extra as $listid => $info) {
		// We need something...
		if ($listid == 0)
			$listid = (int)ac_sql_select_one("SELECT listid FROM #form_list WHERE formid = '$form[id]' LIMIT 1");

		if ($info["error_code"] != 6) {
			$_SESSION["subscribe_error_message"] = $info["message"];
			return rewrite_plink("subscribe_error", "listid=$listid", "codes=$codes", "s=$sub[hash]");
		}
	}

	if (!$form['redirecturl']) {
		switch ($action) {
			default:
			case "sub":
				if ($form["sendoptin"])
					return rewrite_plink("subscribe_result", "listid=$lists", "codes=$codes", "s=$sub[hash]");
				else
					return rewrite_plink("subscribe_confirm_result", "listid=$lists", "codes=$codes", "s=$sub[hash]");

			case "csub":
				return rewrite_plink("subscribe_confirm_result", "listid=$lists", "codes=$codes", "s=$sub[hash]");

			case "forward":
				return rewrite_plink("forward_result", "listid=$lists", "codes=$codes", "s=$sub[hash]");
		}
	}

	return $form['redirecturl'];
}

function form_html($id, $extra = null) {
	if ( defined('AC_API_REMOTE') ) $id = (int)$id;
	if (is_int($id)) {
		$form = form_select_row($id);

		if (!$form)
			return "";

		$parts = form_compile($id);

		if (count($parts) == 0)
			return "";
	} else {
		$form = $id;
		$parts = $form["parts"];
	}

	$rval = "";
	$needlists = true;

	foreach ($parts as $part) {
		if ($part["builtin"] == "listselector")
			$needlists = false;

		if (preg_match('/_type_date/', $part["html"]))
			$extra["cal"] = true;

		$rval .= $part["html"];
	}

	if ($needlists) {
		$extra["lists"] = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$form[id]'");
	}

	$rval = form_header($form, $extra) . $rval . form_footer($form, $extra);

	$html = html_pprint($rval);

	if (defined('AC_API_REMOTE')) {
		echo $html;
		exit();
	}

	return $html;
}

function form_settings_save() {
	$id = (int)ac_http_param("id");
	$useconf = (int)ac_http_param("useconf");
	$listids = ac_http_param_forcearray("listid");
	$groups_lists = implode("','", $GLOBALS["admin"]["lists"]);

	# Update list relations.
	ac_sql_delete("#form_list", "formid = '$id' AND listid IN ('{$groups_lists}')");
	foreach ($listids as $listid) {
		$ins = array(
			"formid" => $id,
			"listid" => $listid,
		);

		if ($listid == $useconf)
			$ins["useconf"] = 1;

		ac_sql_insert("#form_list", $ins);
	}

	$messageid = (int)ac_sql_select_one("SELECT messageid FROM #form WHERE id = '$id'");
	ac_sql_delete("#message_list", "messageid = '$messageid' AND listid IN ('{$groups_lists}')");
	foreach ($listids as $listid) {
		$ins = array(
			"messageid" => $messageid,
			"listid" => $listid,
		);
		ac_sql_insert("#message_list", $ins);
	}

}

function form_set_default() {
	$key = ac_http_param("key");
	$val = ac_http_param("val");

	if ((int)$val == 0) {
		// disable public section if "Don't show any form by default" is chosen
		ac_sql_update("#backend", array("general_public" => 0), "id = 1");
	}
	else {
		ac_sql_update("#backend", array("general_public" => 1), "id = 1");
	}

	return Config::assign_post();
}

function form_copy($id, $listid) {
	// Copy form
	$form = ac_sql_select_row("SELECT * FROM #form WHERE id = '$id'");
	$form["name"] = _a("Copy of") . " " . $form["name"];
	unset($form["id"]);

	ac_sql_insert("#form", $form);
	$newid = (int)ac_sql_insert_id();

	// Copy form-list relation
	$rel = ac_sql_select_row("SELECT * FROM #form_list WHERE formid = '$id' LIMIT 1");
	$rel["formid"] = $newid;
	$rel["listid"] = $listid;
	unset($rel["id"]);

	ac_sql_insert("#form_list", $rel);

	// Copy form parts
	$rs = ac_sql_query("SELECT * FROM #form_part WHERE formid = '$id'");
	while ($row = ac_sql_fetch_assoc($rs)) {
		$row["formid"] = $newid;
		unset($row["id"]);

		ac_sql_insert("#form_part", $row);
	}
}

?>
