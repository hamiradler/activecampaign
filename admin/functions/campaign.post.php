<?php

define("CAMPAIGN_STATUS_DRAFT", 0);
define("CAMPAIGN_STATUS_SCHEDULED", 1);
define("CAMPAIGN_STATUS_SENDING", 2);
define("CAMPAIGN_STATUS_PAUSED", 3);
define("CAMPAIGN_STATUS_STOPPED", 4);
define("CAMPAIGN_STATUS_COMPLETED", 5);
define("CAMPAIGN_STATUS_DISABLED", 6);
define("CAMPAIGN_STATUS_PENDING_APPROVAL", 7);

require_once ac_global_functions("log.php");

function campaign_nextid() {
	return (int)ac_sql_select_one("SELECT maxcampaignid FROM #backend LIMIT 1") + 1;
}

function campaign_updatenextid($newmax) {
	$newmax = (int)$newmax;
	ac_sql_query("UPDATE #backend SET maxcampaignid = '$newmax'");
}

function campaign_create() {

	// gather important data
	if ( !isset($_POST['p']) ) $_POST['p'] = array();
	$lists = array_diff(array_map('intval', (array)$_POST['p']), array(0));
	$messages = array_diff(array_map('intval', (array)ac_http_param('m')), array(0));
	$methods = array_diff(array_map('intval', (array)ac_http_param('s')), array(0));
	$links = array_map('ac_b64_decode', (array)ac_http_param('linkurl'));
	$linknames = (array)ac_http_param('linkname');
	$linkmessages = (array)ac_http_param('linkmessage');

	$mid = 0;

	if ( is_array($messages) && count($messages) ) {
		if ( count($messages) == 1 ) {
			$mid = array_search( current($messages), $messages );
		}
		else {
			$tmp = array();
			foreach ( $messages as $k => $v ) $tmp[$v] = 100 / count($messages);
			$messages = $tmp;
		}
	}

	foreach ($methods as $mailerid) {
		if (!em_exists("#mailer", $mailerid))
			return ac_ajax_api_result(false, _a("Invalid mailer id in s parameter"));
	}

	foreach ($messages as $messageid => $ratio) {
		if (!em_exists("#message", $messageid))
			return ac_ajax_api_result(false, _a("Invalid message id in m parameter"));
	}

	foreach ($lists as $listid) {
		if (!em_exists("#list", $listid))
			return ac_ajax_api_result(false, _a("Invalid list id in p parameter"));
	}

	// insert read/open tracking links
	if ( (int)ac_http_param('trackreads') ) {
		$links[] = 'open';
		$linknames[] = '';
		$linkmessages[] = 0;

		$links[] = 'open';
		$linknames[] = '';
		$linkmessages[] = $mid;
	}

	// obtain links from message (you should not have to pass them individually anymore)
	/*
	foreach ($messages as $k => $v) {
		$message = message_select_row($k);
		if ($message) {
			$message["html"] = personalization_basic($message["html"], $message["subject"]);
			$message_links = message_extract_links($message);
			foreach ($message_links as $link_row) {
				// if not already in array of links parameter, add it
				if ( !in_array($link_row["link"], $links) ) {
					$links[] = $link_row["link"];
					$linknames[] = $link_row["title"];
					$linkmessages[] = $k;
				}
			}
		}
	}
	*/

	# Extract any links.
	$survey_url_id = "";
	foreach ($messages as $k => $v) {
		$msg = message_select_row($k);
		if ( !$msg ) continue;
		if ( $msg['htmlfetch'] == 'send' and $msg['htmlfetchurl'] ) {
			$msg['html'] = ac_http_get($msg['htmlfetchurl'], "UTF-8");
			$msg['html'] = message_link_resolve($msg['html'], $msg['htmlfetchurl']);
		}

		// look for SurveyMonkey personalization tags
		$survey_url_id = "";
		preg_match_all('/%SERVICE-SURVEYMONKEY-[^%]*%/', $msg['html'], $service_surveymonkey);
		if ( isset($service_surveymonkey[0][0]) && $service_surveymonkey[0][0] ) {
			// get the unique ID from the pers tag, which is the URL ID for the survey
			$survey_url_id = preg_match("/[^-]*$/", $service_surveymonkey[0][0], $url_id_matches);
			if (isset($url_id_matches[0]) && $url_id_matches[0]) {
				$survey_url_id = substr($url_id_matches[0], 0, strlen($url_id_matches[0]) - 1);
			}
		}

		$msg["html"] = personalization_basic($msg["html"], $msg["subject"]);
		$links = message_extract_links($msg);
	}

	$admin = $GLOBALS['admin'];

	// fetch campaign info
	$blank = ac_sql_default_row('#campaign');
	$blank_row = array();
	foreach ($blank as $field => $value) {
		if ($field != 'ip4' && $field != 'sdate') {
			$blank_row[$field] = $value;
		}
	}
	$blank = $blank_row;

	if (isset($_SERVER["REMOTE_ADDR"]) && ac_str_noipv6($_SERVER["REMOTE_ADDR"]))
		$blank["=ip4"] = "INET_ATON('$_SERVER[REMOTE_ADDR]')";

	$blank["survey"] = $survey_url_id;

	foreach ( $blank as $k => $v ) {
		if ($k != 'id') {
			if ( ac_http_param_exists($k) ) {
				$val = ac_http_param($k);
				if ( preg_match('/^\d+$/', $v) and !preg_match('/^\d+$/', $val) ) {
					$val = (int)$val;
				}
				if ($k == "tracklinks" && $val == "all") $val = "mime";
				$blank[$k] = $val;
			}
		}
	}

	$sdate = ac_http_param("sdate");
	// if status = 1, and sdate is greater than now, it is considered scheduled
	$blank['schedule'] = ( (int)ac_http_param("status") == 1 && $sdate > (string)ac_sql_select_one("SELECT NOW()") ) ? 1 : 0;
	if ($blank['schedule']) {
		// if scheduled, take the value they included as a parameter
		$blank["sdate"] = $sdate;
	}

	$blank['bounceid'] = (int)$blank['bounceid'];
	$blank['filterid'] = (int)$blank['filterid'];

	if ($blank["filterid"] > 0 && !em_exists("#filter", $blank["filterid"]))
		return ac_ajax_api_result(false, _a("Invalid filterid"));
	if ($blank["bounceid"] > 0 && !em_exists("#bounce", $blank["bounceid"]))
		return ac_ajax_api_result(false, _a("Invalid bounceid"));
	if ( !count($messages) ) {
	  return ac_ajax_api_result(false, _a("Campaign requires a message ID."));
	}
	if ( $blank['type'] == 'split' ) {
		if ( count($messages) < 2 ) {
			return ac_ajax_api_result(false, _a("Split test campaigns need at least two messages."));
		}
	} else {
		if ( count($messages) > 1 ) {
			return ac_ajax_api_result(false, _a("Only split test campaigns can be sent with more than one message."));
		}
	}

	$blank["source"] = "api";
	$blank["laststep"] = "summary";
	$blank["userid"] = $admin["id"];
	$blank["cdate"] = AC_CURRENTDATETIME;

	if ($blank["schedule"]) {
		// scheduled campaign
		$d = @strtotime($blank['sdate']);
		if (!$d || $d == -1) {
			// if scheduled campaign, and invalid date
			return ac_ajax_api_result(false, _a('Invalid send date provided.'));
		}
		// account for timezone being used
		$sdate = $d - (ac_date_offset_hour() * 3600);
		$blank["sdate"] = date("Y-m-d H:i:s", $sdate);
	}
	else {
		$blank["schedule"] = 0;
		$blank["=sdate"] = "NOW()";
	}

	if ( !isset($blank['ldate']) || !$blank['ldate'] ) {
		unset($blank['ldate']);
		$blank['=ldate'] = 'NULL';
	}
	if ( !$blank['reminder_last_cron_run'] ) {
		unset($blank['reminder_last_cron_run']);
		$blank['=reminder_last_cron_run'] = 'NULL';
	}
	/*
		CHECKING FOR RELATIONS
	*/
	// if no lists, dont even set it as draft -- this is not allowed
	if ( !count($lists) ) {
		return ac_ajax_api_result(false, _a('You did not provide any lists.'));
		$blank['status'] = 0;
	}
	/*
		CHECKING FOR ENUMS
	*/
	$wl = array('single', 'recurring', 'split', 'responder', 'reminder', 'special', 'activerss', 'text');
	if ( !in_array($blank['type'], $wl) ) {
		return ac_ajax_api_result(false, _a('You did not provide a valid campaign type.'));
	}
	if ( !in_array($blank['tracklinks'], array('mime', 'html', 'text', 'all')) ) $blank['tracklinks'] = 'none';
	if ( $blank['type'] == 'simple' ) {
		// simple campaign
	} elseif ( $blank['type'] == 'recurring' ) {
		$wl = array(/*'hour0', 'hour1', 'hour2', 'hour6', 'hour12',*/ 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
		if ( !in_array($blank['recurring'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid recurring interval.'));
		}
	} elseif ( $blank['type'] == 'split' ) {
		$wl = array('even', 'read', 'click');
		if ( !in_array($blank['split_type'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid split campaign type.'));
		}
		$wl = array('hour', 'day', 'week', 'month');
		if ( !in_array($blank['split_offset_type'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid duration for split winner calculation.'));
		}
	} elseif ( $blank['type'] == 'responder' ) {
		//if ( $blank['responder_type'] != 'unsubscribe' ) $data['responder_type'] = 'subscribe';
	} elseif ( $blank['type'] == 'reminder' ) {
		$wl = array('month_day', 'year_month_day');
		if ( !in_array($blank['reminder_type'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid format for a field needed for auto-reminders.'));
		}
		$wl = array('day', 'week', 'month', 'year');
		if ( !in_array($blank['reminder_offset_type'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid offset type for auto-reminders.'));
		}
		$wl = array('+', '-');
		if ( !in_array($blank['reminder_offset_sign'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid offset sign for auto-reminders.'));
		}
	} elseif ( $blank['type'] == 'special' ) {
	} elseif ( $blank['type'] == 'activerss' ) {
		$wl = array('hour0', 'hour1', 'hour2', 'hour6', 'hour12', 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
		if ( !in_array($blank['activerss_interval'], $wl) ) {
			return ac_ajax_api_result(false, _a('You did not provide a valid recurring interval.'));
		}
	}
	/*
		INSERT NEW CAMPAIGN
	*/
	$blank["id"] = campaign_nextid();
	$done = ac_sql_insert("#campaign", $blank);
	if ( !$done ) {
		return ac_ajax_api_result(false, _a('Campaign could not be added.'));
	}
	$id = (int)ac_sql_insert_id();
	cache_clear("canSendCampaign");
	campaign_updatenextid($id);

	// insert lists
	foreach ( $lists as $l ) {
		if ( $l = (int)$l ) {
			$arr = array(
				'id' => 0,
				'campaignid' => $id,
				'listid' => $l,
				'userid' => $admin['id'],
				'list_amt' => 0,
			);
			ac_sql_insert('#campaign_list', $arr);
		}
	}
	// copy mailer info
	$methodslist = implode("','", $admin['methods']);
	ac_sql_query("
		INSERT INTO
			#campaign_mailer
		(
			id, campaignid, mailerid
		)
			SELECT
				0 AS `id`,
				$id AS `campaignid`,
				`id` AS `mailerid`
			FROM
				#mailer
			WHERE
				`id` IN ('$methodslist')
	");

	// insert messages
	foreach ( $messages as $k => $v ) {
		if ( $k = (int)$k ) {
			foreach ( $lists as $l ) {
				if ( $l = (int)$l && !ac_sql_select_one('=COUNT(*)', '#message_list', "`messageid` = '$k' AND `listid` = '$l'") ) {
					$arr = array(
						'id' => 0,
						'messageid' => $k,
						'listid' => $l,
						'userid' => $admin['id'],
					);
					ac_sql_insert('#message_list', $arr);
				}
			}
			if ( $blank['type'] != 'split' or $blank['split_type'] == 'even' ) $v = 100;
			$arr = array(
				'id' => 0,
				'campaignid' => $id,
				'messageid' => $k,
				'percentage' => (float)$v,
				'sourcesize' => 0,
			);
			ac_sql_insert('#campaign_message', $arr);
			if ( $blank['type'] != 'split' or $blank['split_type'] == 'even' ) break;
		}
	}

	$saved = array();
	foreach ($links as $link) {
		$messageid = ( isset($link["messageid"]) ) ? $link["messageid"] : $mid;

		$esc = ac_sql_escape(message_link_internal($link["link"]));
		$linkid = (int)ac_sql_select_one('id', '#link', "campaignid = '$id' AND messageid = '$messageid' AND link = '$esc'");

		if (!$linkid) {
			$ins = array(
				"id" => 0,
				"campaignid" => $id,
				"messageid" => $messageid,
				"link" => $link["link"],
				"name" => $link["title"],
				"ref" => message_link_getref($link["link"]),
				"tracked" => 1,
			);

			$ins = campaign_save_fixlinkname($ins);

			$sql = ac_sql_insert("#link", $ins);
			$linkid = ac_sql_insert_id();
		} else {
			// update the name (from title="whatever" attribute)
			$up = array(
				"name" => $link["title"],
			);
			$sql = ac_sql_update("#link", $up, "id = '$linkid'");
			if ( !$sql ) {
			}
		}

		$saved[] = $linkid;
	}

	# Remove old links.
	$savedstr = implode("','", $saved);
	ac_sql_delete("#link", "campaignid = '$id' AND messageid = '$mid' AND id NOT IN ('$savedstr') AND link != 'open'");
	campaign_save_readtracking($id, $mid);

	// send now?
	$sendnow = ( in_array($blank['type'], array('single', /*'recurring',*/ 'split', 'text')) && !$blank['schedule'] && $blank['status'] );
	if ( $sendnow ) {
		campaign_init($id, false);
	} else {
		// set campaign status
		$status = (int)ac_http_param("status");

		if ($status !== 1)
			$status = 0;

		ac_sql_update_one('#campaign', 'status', $status, "id = '$id'");
		// if responder, try to deal with old subscribers (trigger a new campaign)
		if ( $status and $blank['type'] == 'responder' and ac_http_param('responder_do_oldies') ) {
			$oldies = ac_http_param('respondold');
			if ( $oldies != 'no' ) campaign_responder_oldies($id);
		}
	}


	return ac_ajax_api_saved(_a("Campaign"), array('id' => $id));
}

function campaign_prepare_post() {
	$r = array();
	// find basic campaign info
	$r['ary'] = campaign_prepare_post_ary();
	$r['actionid'] = ac_http_param("actionid");

	// find parents
	$r['lists'] = array();
	$p = ac_http_param('p');
	if ( is_array($p) and count($p) > 0 ) {
		$r['lists'] = array_map('intval', $p);
	} else {
		$r["lists"] = array((int)ac_http_param("p"));
	}

	// find messages
	$r['messages'] = array();
	$r['ratios'] = array();
	$messageid = ac_http_param('messageid');
	$splitratio = ac_http_param('splitratio');
	if ( is_array($messageid) ) {
		$r['messages'] = array_map('intval', $messageid);
	} else {
		if ( (int)$messageid > 0 ) {
			$r['messages'] = array((int)$messageid);
		}
	}
	if ( count($r['messages']) > 0 ) {
		if ( $r['ary']['type'] == 'split' ) {
			if ( $r['ary']['split_type'] != 'even' and is_array($splitratio) /*and count($splitratio) == count($r['messages'])*/ ) {
				$r['ratios'] = $splitratio;
			} else {
				$perc = round(100 / count($r['messages']));
				foreach ( $r['messages'] as $m ) {
					$r['ratios'][$m] = $perc;
				}
				// complement
				$r['ratios'][$m] += 100 - ( round(100 / count($r['messages'])) * count($r['messages']) );
			}
		} else {
			$r['ratios'][$r['messages'][0]] = 100;
		}
	}

	// find links and actions
	$r['linkactions'] = array();
	$r['links'] = array();
	$r['linknames'] = array();
	$r['linkmessages'] = array();
	$linkurl = ac_http_param('linkurl');
	$linkname = ac_http_param('linkname');
	$linkmessage = ac_http_param('linkmessage');
	$linkactions = ac_http_param("linkaction");
	if ( $r['ary']['tracklinks'] != 'none' || $r['ary']['tracklinksanalytics'] ) {
		if ( is_array($linkurl) and count($linkurl) > 0 ) {
			$r['links'] = array_map('trim', array_map('ac_b64_decode', $linkurl));
			if ( is_array($linkname) ) {
				$r['linknames'] = $linkname;
			}
			if ( is_array($linkmessage) ) {
				$r['linkmessages'] = $linkmessage;
			}
			if ( is_array($linkactions) ) {
				$r['linkactions'] = $linkactions;
			}
		}
	}
	if ( $r['ary']['trackreads'] and !in_array('open', $r['links']) ) {
		// add "read" link
		$r['linkactions'][] = 0;
		$r['links'][] = 'open';
		$r['linknames'][] = '';
		$r['linkmessages'][] = 0;
	}

	foreach ( $r['messages'] as $mid ) {
		$message = message_select_row($mid, implode(',', $r['lists']));
		if ( !$message ) continue;
		foreach ( $message['links'] as $link ) {
			// check if already added
			foreach ( $r['links'] as $k => $v ) {
				if ( $v == $link['link'] and $mid == $r['linkmessages'][$k] ) {
					// found already for this message
					continue(2);
				}
			}
			if (
				( (string)ac_http_param('tracklinks') == 'all' and count($r['links']) == $r['ary']['trackreads'] )
			)
			{
				$r['linkactions'][] = 0;
				$r['links'][] = $link['link'];
				$r['linknames'][] = '';
				$r['linkmessages'][] = $mid;
			}
		}
	}

	/*
	if ( $r['ary']['trackreads'] != 'none' || $r['ary']['trackreadsanalytics'] ) {
		$r['links'][] = 'open';
		if ( is_array($linkurl) and count($linkurl) > 0 ) {
			$r['links'] = array_map('trim', array_map('ac_b64_decode', $linkurl));
			if ( is_array($actions) ) {
				foreach ( $actions as $k => $v ) {
					$r['actions'][$k] = array();
					if ( $v != '' ) {
						$arr1 = explode('|**|', $v);
						foreach ( $arr1 as $a ) {
							$arr2 = array();
							list($arr2['action'], $arr2['value']) = explode('*||*', $a);
							$r['actions'][$k][] = $arr2;
						}
					}
				}
			}
			if ( is_array($linkname) ) {
				$r['linknames'] = $linkname;
			}
			if ( is_array($linkmessage) ) {
				$r['linkmessages'] = $linkmessage;
			}
		}
	}
	*/
	return $r;
}

function campaign_prepare_post_ary() {
	$step = (int)ac_http_param('step');
	// filter id
	$filterid = (int)ac_http_param('filterid');
	if ( !ac_http_param_exists('usefilter') ) $filterid = 0;
	// bounce id
	$bounceid = (int)ac_http_param('bounceid');
	if ( !ac_http_param_exists('usebounce') ) $bounceid = -1;
	// send date
	$sdate = ac_http_param('sdate');
	# Fix up the dates so that they match the MySQL format.
	$sdate = strtotime($sdate);
	$sdate -= (ac_date_offset_hour() * 3600);
	$sdate = strftime("%Y-%m-%d %H:%M:%S", $sdate);
	// link tracking
	$trackreads = (int)ac_http_param_exists('trackreads');
	$tracklinks = (string)ac_http_param('tracklinks');
	if ( $tracklinks != 'none' ) {
		if ( $step < 4 ) {
			// any links found
			$links = ac_http_param('linkurl');
			if ( !is_array($links) or count($links) == 0 ) {
				$tracklinks = 'none';
			} else {
				$links = array_map('ac_b64_decode', $links);
			}
		}
		if ( $tracklinks != 'none' ) {
			if ( $tracklinks == 'all' ) {
				$tracklinks = 'mime';
			} else {
				$tracklinks = ac_http_param('tracklinksformat' . ac_http_param('tracklinks')); // crazyness :)
			}
		}
	}
	if ( !in_array($tracklinks, array('mime', 'html', 'text')) ) $tracklinks = 'none';
	if ( $tracklinks == 'none' and $trackreads ) $tracklinks = 'html';
	// unsub link
	$htmlUnsubMissing = true;
	$textUnsubMissing = true;
	$r = array(
		'type' => trim((string)ac_http_param('campaign_type')),
		'filterid' => $filterid,
		'bounceid' => $bounceid,
		'name' => trim((string)ac_http_param('campaign_name')),
		'sdate' => $sdate,
		'public' => (int)ac_http_param_exists('public'),
		'tracklinks' => $tracklinks,
		'tracklinksanalytics' => (int)ac_http_param_exists('use_analytics_link'),
		'trackreads' => $trackreads,
		'trackreadsanalytics' => (int)ac_http_param_exists('use_analytics_read'),
		'tweet' => (int)ac_http_param_exists('tweet'),
		'facebook' => (int)ac_http_param_exists('facebook'),
		//'analytics_campaign_name' => '',
		'embed_images' => (int)ac_http_param_exists('embed_images'),
		'htmlunsub' => (ac_http_param('includeunsub') == 'yes' and $htmlUnsubMissing) ? 1 : 0,
		'htmlunsubdata' => ac_str_fixtinymce(trim((string)ac_http_param('includeunsubhtml'))),
		'textunsub' => (ac_http_param('includeunsub') == 'yes' and $textUnsubMissing) ? 1 : 0,
		'textunsubdata' => trim((string)ac_http_param('includeunsubtext')),
		'mailer_log_file' => (int)ac_http_param('debugging'),
		'total_amt' => 0,
	);
	$wl = array('single', 'recurring', 'split', 'responder', 'reminder', 'special', 'activerss', 'text');
	if ( !in_array($r['type'], $wl) ) $r['type'] = 'single';
	if ( $r['type'] == 'recurring' ) {
		$r['recurring'] = trim((string)ac_http_param('recurragain'));
		$wl = array(/*'hour0', 'hour1', 'hour2', 'hour6', 'hour12',*/ 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
		if ( !in_array($r['recurring'], $wl) ) $r['recurring'] = 'year1';
	} elseif ( $r['type'] == 'split' ) {
		if ( ac_http_param('schedule') == 'now' ) $r['sdate'] = (string)ac_sql_select_one("SELECT NOW()");
		$r['split_type'] = trim((string)ac_http_param('splittype'));
		if ( $r['split_type'] == 'winner' ) {
			$r['split_type'] = trim((string)ac_http_param('splitwinnertype'));
		}
		$wl = array('even', 'read', 'click');
		if ( !in_array($r['split_type'], $wl) ) $r['split_type'] = 'even';
		$r['split_offset'] = (int)ac_http_param('splitoffset');
		$r['split_offset_type'] = trim((string)ac_http_param('splitoffsettype'));
		$wl = array('hour', 'day', 'week', 'month');
		if ( !in_array($r['split_offset_type'], $wl) ) $r['split_offset_type'] = 'hour';
	} elseif ( $r['type'] == 'responder' ) {
		$r['public'] = 0;
		$r['responder_offset'] = (int)ac_http_param('responder_offset');
		//if ( $r['responder_type'] != 'unsubscribe' ) $r['responder_type'] = 'subscribe';
		$r['sdate'] = (string)ac_sql_select_one("SELECT NOW()");
	} elseif ( $r['type'] == 'reminder' ) {
		$r['public'] = 0;
		$r['reminder_field'] = trim((string)ac_http_param('reminder_field'));
		$r['reminder_format'] = trim((string)ac_http_param('reminder_format'));
		$r['reminder_type'] = trim((string)ac_http_param('reminder_type'));
		if ( $r['reminder_type'] != 'month_day' ) $r['reminder_type'] = 'year_month_day';
		$r['reminder_offset'] = (int)ac_http_param('reminder_offset');
		$r['reminder_offset_type'] = trim((string)ac_http_param('reminder_offset_type'));
		$wl = array('day', 'week', 'month', 'year');
		if ( !in_array($r['reminder_offset_type'], $wl) ) $r['reminder_offset_type'] = 'day';
		$r['reminder_offset_sign'] = trim((string)ac_http_param('reminder_offset_sign'));
		if ( $r['reminder_offset_sign'] != '-' ) $r['reminder_offset_sign'] = '+';
		$r['sdate'] = (string)ac_sql_select_one("SELECT NOW()");
	} elseif ( $r['type'] == 'activerss' ) {
		$r['activerss_interval'] = trim((string)ac_http_param('activerssagain'));
		$wl = array('hour0', 'hour1', 'hour2', 'hour6', 'hour12', 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
		if ( !in_array($r['activerss_interval'], $wl) ) $r['activerss_interval'] = 'year1';
	} elseif ( $r['type'] == 'special' ) {
		$r['public'] = 0;
	} else { // single
		if ( ac_http_param('schedule') == 'now' ) {
			$r['sdate'] = (string)ac_sql_select_one("SELECT NOW()");
		}
	}
	return $r;
}

function campaign_post2prepared($data = null) {
	if ( !$data ) $data = campaign_prepare_post();
	// campaign info
	$row = $data['ary'];
	//$row['id'] = $row['realcid'] = (int)ac_http_param('id');
	$row['id'] = $row['realcid'] = 0; // from post, it's always assumed 0
	$row['total_amt'] = 0;
	$row['send_amt'] = 0;
	$row['cdate'] =
	$row['sdate'] = (string)ac_sql_select_one("SELECT NOW()");
	$row['ldate'] = null;
	// set user permissions
	$cond = '';
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		if ( $admin['id'] != 1 ) {
			$cond = "AND l.id IN ('" . implode("', '", $admin['lists']) . "')";
		}
	}
	$listslist = implode("','", $data['lists']);
	// fetch all lists it belongs to
	$row['lists'] = ac_sql_select_array("
		SELECT
			*,
			id AS listid,
			0 AS campaignid,
			0 AS list_amt,
			0 AS relid
		FROM
			#list l
		WHERE
			l.id IN ('$listslist')
		$cond
	");
	if ( !$row['lists'] ) $row['lists'] = array();
	// calculate list limits
	$row['p_embed_image']        = 0;
	$row['p_use_scheduling']     = 0;
	$row['p_use_tracking']       = 0;
	$row['p_use_analytics_read'] = 0;
	$row['p_use_analytics_link'] = 0;
	$row['p_use_twitter'] = 0;
	$row['p_use_facebook'] = 0;
	$lists = array();
	foreach ( $row['lists'] as $l ) {
		$lists[] = $l['id'];
		if ( $l['p_embed_image'] )        $row['p_embed_image']        = $l['p_embed_image'];
		if ( $l['p_use_tracking'] )       $row['p_use_tracking']       = $l['p_use_tracking'];
		if ( $l['p_use_analytics_read'] ) $row['p_use_analytics_read'] = $l['p_use_analytics_read'];
		if ( $l['p_use_analytics_link'] ) $row['p_use_analytics_link'] = $l['p_use_analytics_link'];
		if ( $l['p_use_twitter'] )        $row['p_use_twitter']        = $l['p_use_twitter'];
		if ( $l['p_use_facebook'] )       $row['p_use_facebook']       = $l['p_use_facebook'];
	}
	$row['listslist'] = implode('-', $lists);
	// fetch all fields (for those lists only, globals should be prefetched elsewhere)
	$row['fields'] = list_get_fields($lists, false);
	// fetch all messages that belong to this campaign
	$row['ratios'] = $data['ratios'];
	$msgsfound_html = 0;
	$msgsfound_text = 0;
	$unsubsfound_html = 0;
	$unsubsfound_text = 0;
	$row['messages'] = message_select_array(null, $data['messages'], implode(',', $lists));
	foreach ( $row['messages'] as $k => $v ) {
		$row['messages'][$k]['percentage'] = $row['ratios'][$v['id']];
		// look for unsubscribe links so that we can hide the footer if all are found
		if ( $v['format'] != 'text' ) {
			// do htmls
			$msgsfound_html++;
			$unsubsfound_html += (int)( ac_str_instr('%UNSUBSCRIBELINK%', $v['html']) or ac_str_instr('/box.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2', $v['html']) or ac_str_instr('/proc.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2', $v['html']) );
		}
		if ( $v['format'] != 'html' ) {
			// do texts
			$msgsfound_text++;
			$unsubsfound_text += (int)( ac_str_instr('%UNSUBSCRIBELINK%', $v['text']) or ac_str_instr('/box.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2', $v['text']) or ac_str_instr('/proc.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2', $v['text']) );
		}
	}
	if ( $unsubsfound_html == $msgsfound_html ) {
		$row['htmlunsub'] = 0;
	}
	if ( $unsubsfound_text == $msgsfound_text ) {
		$row['textunsub'] = 0;
	}
	$row['messageslist'] = implode('-', array_keys($row['ratios']));
	// fetch all links for parsing
	$row['tlinks'] = array();
	// insert links relations
	foreach ( $data['links'] as $k => $l ) {
		if ( $l != '' ) {
			$linkid = count($row['tlinks']);
			$row['tlinks'][$linkid] = array(
				'id' => 0,
				'campaignid' => 0,
				'messageid' => $data['linkmessages'][$k],
				'link' => $l,
				'name' => $data['linknames'][$k],
				'actions' => array(),
			);
			# FIXME
		}
	}
	return $row;
}

function campaign_delete($id) {
	$id = intval($id);
	$admincond = '';
	$campaign = campaign_select_row($id);
	if ( !$campaign ) {
		return ac_ajax_api_result(false, _a("Campaign not found."));
	}

	$up = array(
		'status' => 0,
		'laststep' => 'summary',
	);
	ac_sql_update("#campaign", $up, "id = '$id' AND status = 7");
	if ( $campaign['status'] == 7 ) {
		ac_sql_delete("#campaign_count", "id = '$campaign[sendid]'");
	}

	ac_sql_query("
		INSERT INTO
			#campaign_deleted
		SELECT * FROM #campaign WHERE id = '$id'
	");

	ac_sql_delete('#campaign', "id = '$id'");
	ac_sql_delete('#campaign_folder', "campaignid = '$id'");

	// if campaign was not yet approved, remove the approval queue
	ac_sql_delete("#approval", "campaignid = '$id' AND approved = 0");

	// now remove all campaigns that are special and have this campaign as their "realcid"
	$relcamps = ac_sql_select_list("SELECT id FROM #campaign WHERE realcid = '$id' AND type = 'special'");
	foreach ( $relcamps as $rcid ) {
		campaign_delete($rcid);
	}

	// if campaign was sending at the time, we gotta do additional queries here
	ac_sql_query("DROP TABLE `#x$campaign[sendid]`");
	$serialized = ac_sql_escape(serialize($campaign["sendid"]));
	ac_sql_delete('#process', "`action` = 'campaign' AND `data` = '$serialized'");

	// remove any old sources
	campaign_source_clear($id, null, null);
	cache_clear("canSendCampaign");

	return ac_ajax_api_result(true, _a("Campaign deleted."));
}

function campaign_delete_multi($ids, $filter = 0) {
	if ( $ids == '_all' ) {
		$tmp = array();
		$so = new AC_Select();
		$filter = intval($filter);
		if ($filter > 0) {
			$admin = ac_admin_get();
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'campaign'");
			$so->push($conds);
		}
		$all = campaign_select_array($so);
		foreach ( $all as $v ) {
			$tmp[] = $v['id'];
		}
	} else {
		$tmp = array_map("intval", explode(",", $ids));
	}
	foreach ( $tmp as $id ) {
		$r = campaign_delete($id);
	}
	return ac_ajax_api_result(true, _a("Campaigns deleted."));
}

// switch cron job's status on/off
function campaign_status($id, $status) {
	$id = (int)$id;
	$status = (int)$status;
	$campaign = campaign_select_row($id);
	if ( !$campaign ) {
		return ac_ajax_api_result(false, _a("Campaign not found."));
	}
	if ( $status == 2 or $status == 3 ) {
		require_once(ac_global_functions('processes.php'));
		$campaign['processid'] = campaign_processid($campaign['id'], 'active');
		return ac_processes_trigger($campaign['processid'], ( $status == 3 ? 'pause' : 'resume' ));
	} else {
		// update the field
		$sql = ac_sql_update_one("#campaign", 'status', $status, "id = '$id'");
		if ( !$sql ) {
			return ac_ajax_api_result(false, _a("Campaign could not be updated."));
		}
	}
	// additional status commands
	if ( $status == 4 ) { // stop
		// remove the temp table
		ac_sql_query("DROP TABLE `#x$campaign[sendid]`");
		// remove the process
		$serialized = ac_sql_escape(serialize($campaign['sendid']));
		ac_sql_delete('#process', "`action` = 'campaign' AND `data` = '$serialized'");
		// mark campaign as cleaned up
		ac_sql_update_one("#campaign", 'mail_cleanup', 1, "id = '$id'");
		$countid = (int)ac_sql_select_one("id", "#campaign_count", "campaignid = '$id' ORDER BY id DESC");
		// if campaign is already in the sending stage
		if ( $campaign['mail_transfer'] ) {
			// set a new count
			ac_sql_update_one("#campaign_count", 'amt', $campaign['send_amt'], "id = '$countid'");
			ac_sql_delete("#approval", "campaignid = '$id' AND approved = 0");
		} else {
			// remove this campaign from counts
			ac_sql_update_one("#campaign_count", 'amt', 0, "id = '$countid'");
		}
	}
	// return result
	return ac_ajax_api_updated(_a("Campaign"));
}

function campaign_update_splittotal($campaignid, $total) {
	$campaignid = intval($campaignid);
	$ms         = ac_sql_select_array("
		SELECT
			*
		FROM
			#campaign_message
		WHERE
			campaignid = '$campaignid'
	");

	if ($total == 0)
		$total = (int)ac_sql_select_one("SELECT total_amt FROM #campaign WHERE id = '$campaignid'");

	$counter = $total;
	foreach ($ms as $m) {
		$ratio = $m["percentage"] / 100.0;
		$quot  = ceil($total * $ratio);
		if ($counter > $quot) {
			$counter -= $quot;
			ac_sql_query("UPDATE #campaign_message SET total_amt = $quot WHERE id = '$m[id]'");
		} else  {
			$quot = $counter;
			ac_sql_query("UPDATE #campaign_message SET total_amt = $quot WHERE id = '$m[id]'");
			break;
		}
	}
}

function campaign_update_splitsend($campaignid, $total) {
	$campaignid = intval($campaignid);
	$winner     = 0;
	$send       = 0;
	$ms         = ac_sql_select_array("
		SELECT
			*
		FROM
			#campaign_message
		WHERE
			campaignid = '$campaignid'
	");

	if ($total == 0) {
		$row = ac_sql_select_row("
			SELECT
				send_amt,
				total_amt,
				split_winner_awaiting,
				split_winner_messageid
			FROM
				#campaign
			WHERE
				id = '$campaignid'
		");
		if ($row) {
			$total = $row["total_amt"];
			$send  = $row["send_amt"];
			$overall = $row["total_amt"];
			$winner = $row["split_winner_messageid"];

			if (($row["split_winner_awaiting"] > 0 || $winner > 0) && $overall > $total)
				$total = $overall;
		}
	}

	$counter = $total;
	foreach ($ms as $m) {
		$ratio = $m["percentage"] / 100.0;
		$quot  = ceil($total * $ratio);
		# If this is the winner, then the send may still be going on for this message; we should
		# reflect that the total is a ratio of the send rather than of the overall total.
		if ($m["id"] == $winner)
			$quot = ceil($send * $ratio);

		if ($counter > $quot) {
			$counter -= $quot;
			ac_sql_query("UPDATE #campaign_message SET send_amt = $quot WHERE id = '$m[id]'");
		} else  {
			$quot = $counter;
			ac_sql_query("UPDATE #campaign_message SET send_amt = $quot WHERE id = '$m[id]'");
			break;
		}
	}
}

function campaign_copy($campaign, $data = array(), $reusemessage = false) {
	if ( !$campaign ) return 0;
	// create default campaign array (will use it as whitelist for new campaign)
	$blank = ac_sql_default_row('#campaign');
	// make clean insert array
	$insert = array();
	foreach ( $blank as $k => $v ) {
		if ( isset($campaign[$k]) ) $insert[$k] = $campaign[$k];
	}
	$insert = array_merge($insert, $data);
	if ( isset($insert['cdate']) ) {
		unset($insert['cdate']);
	}
	$insert['=cdate'] = 'NOW()';
	// add it as new id
	$insert['id'] = campaign_nextid();
	// reset sending engine counters
	$insert['send_amt'] =
	$insert['total_amt'] =
	$insert['mail_transfer'] =
	$insert['mail_send'] =
	$insert['mail_cleanup'] =
	$insert['opens'] =
	$insert['uniqueopens'] =
	$insert['linkclicks'] =
	$insert['uniquelinkclicks'] =
	$insert['subscriberclicks'] =
	$insert['forwards'] =
	$insert['uniqueforwards'] =
	$insert['hardbounces'] =
	$insert['softbounces'] =
	$insert['unsubscribes'] =
	$insert['sendid'] =
	$insert['split_winner_messageid'] =
	$insert['unsubreasons'] = 0;
	$insert['source'] = "copy";
	// other custom campaign info should already be prepared in $campaign array
	//...
	$wl = array('single', 'recurring', 'split', 'responder', 'reminder', 'special', 'activerss', 'text');
	if ( !in_array($insert['type'], $wl) ) {
		$insert['type'] = 'single';
	}
	$wl = array(/*'hour0', 'hour1', 'hour2', 'hour6', 'hour12',*/ 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
	if ( !in_array($insert['recurring'], $wl) ) {
		$insert['recurring'] = 'year1';
	}
	$wl = array('hour0', 'hour1', 'hour2', 'hour6', 'hour12', 'day1', 'day2', 'week1', 'week2', 'month1', 'month2', 'quarter1', 'quarter2', 'year1', 'year2');
	if ( !in_array($insert['activerss_interval'], $wl) ) {
		$insert['activerss_interval'] = 'year1';
	}
	// then copy the campaign as a new one
	if ( !ac_sql_insert("#campaign", $insert) ) return 0;
	$id = ac_sql_insert_id();
	cache_clear("canSendCampaign");
	campaign_updatenextid($id);
	// copy list relations
	ac_sql_query("
		INSERT INTO
			#campaign_list
		(
			id, campaignid, listid, userid, list_amt
		)
			SELECT
				0 AS id,
				$id AS campaignid,
				listid,
				userid,
				0 AS list_amt
			FROM
				#campaign_list
			WHERE
				campaignid = '$campaign[id]'
	");
	// copy mailer info
	ac_sql_query("
		INSERT INTO
			#campaign_mailer
		(
			id, campaignid, mailerid
		)
			SELECT
				0 AS `id`,
				$id AS `campaignid`,
				`mailerid`
			FROM
				#campaign_mailer
			WHERE
				`campaignid` = '$campaign[id]'
	");
	$messages = ac_sql_select_box_array("SELECT messageid, messageid FROM #campaign_message WHERE campaignid = '$campaign[id]'");
	// create default campaign array (will use it as whitelist for new campaign)
	$blankmsg = ac_sql_default_row('#message');
	$keysarr = array_keys($blankmsg);
	$valsarr = $keysarr;
	$valsarr[0] = "0 AS `id";
	$keys = "`" . implode("`, `", $keysarr) . "`";
	$vals = implode("`, `", $valsarr) . "`";
	foreach ( $messages as $k => $v ) {
		if ( $reusemessage ) {
			$v = $messages[$k] = $k;
		} else {
			// copy the message
			ac_sql_query("
				INSERT INTO
					#message
				(
					$keys
				)
					SELECT
						$vals
					FROM
						#message
					WHERE
						id = '$k'
			");
			$v = $messages[$k] = (int)ac_sql_insert_id();

			// copy message relations
			ac_sql_query("
				INSERT INTO
					#message_list
				(
					id, messageid, listid, userid
				)
					SELECT
						0 AS id,
						$v AS messageid,
						listid,
						userid
					FROM
						#message_list
					WHERE
						messageid = '$k'
			");
		}
		ac_sql_query("
			INSERT INTO
				#campaign_message
			(
				id, messageid, campaignid, percentage, sourcesize
			)
				SELECT
					0 AS id,
					$v AS messageid,
					$id AS campaignid,
					percentage,
					sourcesize
				FROM
					#campaign_message
				WHERE
					campaignid = '$campaign[id]'
				AND
					messageid = '$k'
		");
		// copy rss relations
		ac_sql_query("
			INSERT INTO
				#rssfeed
			(
				id, campaignid, messageid, url, type, lastcheck, howmany
			)
				SELECT
					0 AS id,
					$id AS campaignid,
					$v AS messageid,
					url,
					type,
					lastcheck,
					howmany
				FROM
					#rssfeed
				WHERE
					campaignid = '$campaign[id]'
				AND
					messageid = '$k'
		");

		// copy message attachments
		message_copy_attach($k, $v);
	}

	// copy link relations
	$links = ac_sql_select_box_array("SELECT id, messageid FROM #link WHERE campaignid = '$campaign[id]'");
	if ( count($links) > 0 ) {
		foreach ( $links as $l => $mid ) {
			$mid = isset($messages[$mid]) ? $messages[$mid] : $mid; // get the new message id
			// links
			ac_sql_query("
				INSERT INTO
					#link
					SELECT
						0 AS id,
						$id AS campaignid,
						$mid AS messageid,
						link,
						name,
						ref,
						tracked
					FROM
						#link
					WHERE
						id = '$l'
			");
			$relid = ac_sql_insert_id();

			# Subscriber actions
			$actions = ac_sql_select_list("SELECT id FROM #subscriber_action WHERE linkid = '$l'");

			if (count($actions) > 0) {
				foreach ($actions as $actionid) {
					ac_sql_query("
						INSERT INTO
							#subscriber_action
						SELECT
							0 AS id,
							filterid,
							listid,
							$id AS campaignid,
							$relid AS linkid,
							name,
							`type`
						FROM
							#subscriber_action
						WHERE
							id = '$actionid'
					");

					$newactionid = ac_sql_insert_id();

					ac_sql_query("
						INSERT INTO
							#subscriber_action_part
						SELECT
							0 AS id,
							$newactionid AS actionid,
							act,
							targetid,
							targetfield,
							param
						FROM
							#subscriber_action_part
						WHERE
							actionid = '$actionid'
					");
				}
			}
		}
	}

	// copy folder relations
	ac_sql_query("
		INSERT INTO #campaign_folder
			(id, campaignid, folderid)
		SELECT
			0 AS id, '$id' AS campaignid, folderid
		FROM
			#campaign_folder
		WHERE
			campaignid = '$campaign[id]'
	");

	return $id;
}

function campaign_share() {
	$addrto     = strval(ac_http_param("addrto"));
	$addrfrom   = strval(ac_http_param("addrfrom"));
	$nameto     = strval(ac_http_param("nameto"));
	$namefrom   = strval(ac_http_param("namefrom"));
	$subject    = strval(ac_http_param("subject"));
	$message    = strval(ac_http_param("message"));
	$campaignid = intval(ac_http_param("campaignid"));
	$arr = array('link' => '');

	if ($addrto != "" && $addrfrom != "" && $subject != "" && $message != "") {
		$campaign = campaign_share_get($campaignid, $addrto);
		$message  = str_replace('%REPORTLINK%', $campaign["sharelink"], $message);
		$admin = ac_admin_get();
		$options = array();
		if($admin) $options['userid'] = $admin['id'];
		if ( !isset($GLOBALS['demoMode']) ) { // check if demo mode is on
			ac_mail_send("text", $namefrom, $addrfrom, $message, $subject, $addrto, $nameto, $options);
		}
		$arr['link'] = $campaign['sharelink'];
		return ac_ajax_api_result(true, _a("Shared report sent"), $arr);
	} else {
		return ac_ajax_api_result(false, _a("Shared report could not be sent"), $arr);
	}
}

function campaign_rebuild_source($source) {
	$struct  = ac_mail_extract_components(ac_mail_extract($source));
	$headers = array();

	$headers[] = sprintf("Return-Path: %s", $struct["structure"]->headers["return-path"]);
	$headers[] = sprintf("To: %s", $struct["to"]);
	$headers[] = sprintf("From: %s", $struct["from"]);

	if (isset($struct->reply_to))
		$headers[] = sprintf("Reply-To: %s", $struct["reply_to"]);

	$headers[] = sprintf("Subject: %s", $struct["subject"]);
	$headers[] = sprintf("Date: %s", $struct["structure"]->headers["date"]);

	$headers[] = "MIME-Version: 1.0";
	$headers[] = sprintf("Content-Type: %s/%s;", $struct["structure"]->ctype_primary, $struct["structure"]->ctype_secondary);

	# This is a hack in case we have no ctype_parameters but we do (!!) have MIME parts.  This default
	# will probably never need to be used.

	$bound = "_=_swift-297934d6e971334c2c6.94225942_=_";

	foreach ($struct["structure"]->ctype_parameters as $param => $v) {
		if ($param == "boundary")
			$bound = $v;

		$headers[] = sprintf(" %s=\"%s\"", $param, $v);
	}

	$headers[] = "X-Priority: 3";
	$headers[] = "X-MSMail-Priority: Normal";
	$headers[] = "X-MimeOLE: Produced by SwiftMailer 3.3.2_4";

	$headers[] = sprintf("X-mid: %s", $struct["structure"]->headers["x-mid"]);
	$headers[] = "X-Mailer: ACEM";
	$headers[] = "User-Agent: ACEM";
	$headers[] = sprintf("X-Sender: %s", $struct["structure"]->headers["return-path"]);
	$headers[] = sprintf("List-Unsubscribe: %s", $struct["structure"]->headers["list-unsubscribe"]);
	$headers[] = sprintf("Message-ID: %s", $struct["structure"]->headers["message-id"]);

	$body = array();

	$body[] = "This is a message in multipart MIME format.  Your mail client should not";
	$body[] = "be displaying this. Consider upgrading your mail client to view this";
	$body[] = "message correctly.";

	foreach ($struct["structure"]->parts as $part) {
		$body[] = "--" . $bound;
		$body[] = sprintf("Content-Type: %s", $part->headers["content-type"]);
		$body[] = sprintf("Content-Transfer-Encoding: %s", $part->headers["content-transfer-encoding"]);
		$body[] = "";
		$body[] = ac_utf_conv($part->ctype_parameters["charset"], "UTF-8", $part->body);
	}

	if (count($struct["structure"]->parts) > 0)
		$body[] = "--" . $bound;

	$rval  = implode("\n", $headers);
	$rval .= "\n\n";
	$rval .= implode("\n", $body);

	return $rval;
}

function campaign_reminder_compile_post() {
	return array(
		"compile" => campaign_reminder_compile(
			(string)ac_http_param("field"),
			(string)ac_http_param("sign"),
			(int)ac_http_param("offset"),
			(string)ac_http_param("type")
		)
	);
}

function campaign_reminder_compile($field, $sign, $offset, $type) {
	# Some sanity checks.
	if ($sign != "+" && $sign != "-")
		$sign = "+";

	$offset = (int)$offset;

	switch ($type) {
		default:
		case "day":
			$type = "DAY";
			break;

		case "week":
			$type = "WEEK";
			break;

		case "month":
			$type = "MONTH";
			break;

		case "year":
			$type = "YEAR";
			break;
	}

	switch ($field) {
		case "sdate":
			$field = _a("Subscription Date");
			break;

		case "cdate":
			$field = _a("Creation Date");
			break;

		default:
			$field = (int)$field;
			$field = (string)ac_sql_select_one("SELECT title FROM #field WHERE id = '$field'");
			break;
	}

	$t_offset = ac_date_offset_hour();
	$site = ac_site_get();

	$today = (string)ac_sql_select_one("SELECT CURDATE()");

	$time = strtotime($today);
	$today = strftime($site["dateformat"], $time + ($t_offset * 3600));

	$thatday = (string)ac_sql_select_one($q = "SELECT CURDATE() $sign INTERVAL $offset $type");

	$time = strtotime($thatday);
	$thatday = strftime($site["dateformat"], $time + ($t_offset * 3600));

	return sprintf(_a("Example: When this campaign runs tomorrow (%s) it will look for subscribers with a %s matching (%s)",
		$today,
		$field,
		$thatday
	));
}

/*
$boundary = preg_match('/Content-Type: multipart/alternative; boundary=(.*)/', $source);
// first part is til the boundary header
// second part is from the boundary header to the end of the header
// third part is from the end of headers til the end of "this is a mime"
// fourth part is text-based
// sixth part is html-based
$parts = explode($boundary, $source, 6);
$source = implode($boundary, $parts) . "\n\n$boundary--";
*/
?>
