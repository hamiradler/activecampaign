<?php

require_once ac_admin("functions/form.php");

function field_insert_post() {
	$lists = ac_http_param_forcearray("lists");
	$options = ac_http_param_forcearray("options");

	$ins = array(
		"title" => ac_http_param("title"),
		"perstag" => strtoupper(trim(ac_http_param("perstag"))),
		"type" => ac_http_param("type"),
	);

	if ($ins["type"] == "textarea") {
		$ins["rows"] = (int)ac_http_param("rows");
		$ins["cols"] = (int)ac_http_param("cols");

		if (!$ins["rows"])
			$ins["rows"] = 5;

		if (!$ins["cols"])
			$ins["cols"] = 40;
	}

	if (!$ins["title"])
		return ac_ajax_api_result(false, _a("You must give your field a name"));

	if (field_title_taken($ins["title"], "", $lists))
		return ac_ajax_api_result(false, sprintf(_a("There is already a field named '%s' -- choose another name"), $ins["title"]));

	if (field_perstag_taken($ins["perstag"], ""))
		return ac_ajax_api_result(false, sprintf(_a("There is already a personalization tag named '%s' -- choose another name"), $ins["perstag"]));

	$sql = ac_sql_insert("#field", $ins);

	if (!$sql)
		return ac_ajax_api_result(false, _a("Could not create field"));

	$id = (int)ac_sql_insert_id();

	if ($ins["perstag"] == "") {
		$up = array(
			"perstag" => sprintf("PERS_%d", $id),
		);

		ac_sql_update("#field", $up, "id = '$id'");
	}

	foreach ($lists as $listid) {
		$ins = array(
			"fieldid" => $id,
			"relid" => $listid,
		);

		$sql = ac_sql_insert("#field_rel", $ins);

		if (!$sql)
			return ac_ajax_api_result(false, _a("Could not relate field"));
	}

	form_recognize_field($id);

	if (count($options) > 0) {
		ac_sql_query("DELETE FROM #field_option WHERE fieldid = '$id'");
		foreach ($options as $k => $option) {
			$ins = array(
				"fieldid" => $id,
				"orderid" => $k + 1,
				"value" => $option,
				"label" => $option,
			);

			ac_sql_insert("#field_option", $ins);
		}
	}

	if ( !isset($_SESSION['subscriber_importer']['newfields']) ) $_SESSION['subscriber_importer']['newfields'] = array();
	$_SESSION['subscriber_importer']['newfields'][] = array('fieldid' => $id, /*'columnid' => $r['column'],*/ 'type' => ac_http_param("type"));

	return ac_ajax_api_result(true, _a("Field added"), array("source" => ac_http_param("type"), "id" => $id, "title" => ac_http_param("title")));
}

function field_title_taken($title, $origname, $lists) {
	if ($title == $origname)
		return 0;

	$liststr = implode("','", $lists);
	$c = (int)ac_sql_select_one("
		SELECT
			COUNT(*)
		FROM
			#field f
		WHERE
			f.title = '$title'
		AND
			(SELECT COUNT(*) FROM #field_rel r WHERE r.fieldid = f.id AND r.relid IN ('$liststr', '0')) > 0
	");

	return $c > 0;
}

function field_perstag_taken($tag, $orig) {
	$subscriber = subscriber_dummy(_a('_t.e.s.t_@example.com'));
	// 2do: append campaign tags (such as %CAMPAIGNID%)
	$pers = subscriber_personalize_get($subscriber, $campaign = null);
	foreach ( $pers as $k => $v ) {
		$key = trim($k, "%");
		if ( strtolower($key) == strtolower($tag) ) return 1;
	}

	if ($tag == $orig)
		return 0;

	$tag = ac_sql_escape($tag);
	$c = (int)ac_sql_select_one("
		SELECT
			COUNT(*)
		FROM
			#field
		WHERE
			perstag = '$tag'
	");

	return $c > 0;
}

function field_loadname($listid) {
	$listid = (int)$listid;
	return array(
		"defname" => (string)ac_sql_select_one("SELECT to_name FROM #list WHERE id = '$listid'"),
	);
}

function field_savename() {
	$listid = (int)ac_http_param("listid");

	$up = array(
		"to_name" => (string)ac_http_param("defname"),
	);

	ac_sql_update("#list", $up, "id = '$listid'");
	return ac_ajax_api_result(true, _a("Name saved"));
}

function field_loadopts($id) {
	$rval = ac_sql_select_row("SELECT * FROM #field WHERE id = '$id'");
	$rval["lists"] = implode(",", ac_sql_select_list("SELECT relid FROM #field_rel WHERE fieldid = '$id'"));
	$rval["options"] = ac_sql_select_array("SELECT * FROM #field_option WHERE fieldid = '$id' ORDER BY orderid");

	return $rval;
}

function field_saveopts() {
	$fieldid = (int)ac_http_param("fieldid");
	$listid = (int)ac_http_param("listid");
	$inlists = ac_http_param_forcearray("inlist");

	$type = ac_sql_select_one("SELECT `type` FROM #field WHERE id = '$fieldid'");

	$up = array(
		"defval" => ac_http_param("defval"),
		"show_in_list" => (int)ac_http_param("show_in_list"),
		"visible" => (int)ac_http_param("visible"),
		"rows" => (int)ac_http_param("rows"),
		"cols" => (int)ac_http_param("cols"),
	);

	if (in_array($type, array("radio", "dropdown"))) {
		$optid = (int)ac_http_param("defval_dropdown");
		$up["defval"] = "";
		ac_sql_query("UPDATE #field_option SET isdefault = 0 WHERE fieldid = '$fieldid'");
		ac_sql_query("UPDATE #field_option SET isdefault = 1 WHERE id = '$optid'");
	} elseif (in_array($type, array("listbox", "checkbox"))) {
		$optids = ac_http_param_forcearray("defval_dropdown");
		$up["defval"] = "";
		ac_sql_query("UPDATE #field_option SET isdefault = 0 WHERE fieldid = '$fieldid'");

		foreach ($optids as $optid) {
			$optid = (int)$optid;
			ac_sql_query("UPDATE #field_option SET isdefault = 1 WHERE id = '$optid'");
		}
	}

	ac_sql_update("#field", $up, "id = '$fieldid'");

	$up = array();
	foreach ($inlists as $inlist) {
		list($k, $v) = explode(",", $inlist);
		$up[$k] = $v;
	}

	ac_log(var_export($up, true));

	$refresh = false;
	foreach ($up as $k => $v) {
		if ($v) {
			$c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #field_rel WHERE fieldid = '$fieldid' AND relid = '$k'");
			if (!$c) {
				$ins = array(
					"fieldid" => $fieldid,
					"relid" => $k,
				);
				ac_sql_insert("#field_rel", $ins);
			}
		} else {
			if ($k == $listid)
				$refresh = 1;

			$c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #field_rel WHERE fieldid = '$fieldid' AND relid = '$k'");
			if ($c) {
				ac_sql_query("DELETE FROM #field_rel WHERE fieldid = '$fieldid' AND relid = '$k'");
			}
		}
	}

	form_recognize_field($fieldid);

	return ac_ajax_api_result(true, _a("Options saved"), array("refresh" => $refresh));
}

function field_errorcheck() {
	$names = ac_http_param_forcearray("names");
	$orignames = ac_http_param_forcearray("orignames");
	$perstags = ac_http_param_forcearray("perstags");
	$origtags = ac_http_param_forcearray("origtags");
	$fieldids = ac_http_param_forcearray("fieldids");

	foreach ($names as $k => $name) {
		if (!isset($fieldids[$k]) || !isset($orignames[$k]))
			continue;

		$fieldid = (int)$fieldids[$k];
		$origname = $orignames[$k];
		$lists = ac_sql_select_list("SELECT relid FROM #field_rel WHERE fieldid = '$fieldid'");

		if (field_title_taken($name, $origname, $lists))
			return ac_ajax_api_result(false, sprintf(_a("Field name '%s' already in use -- choose another field name"), $name));
	}

	foreach ($perstags as $k => $tag) {
		if (isset($origtags[$k]) && field_perstag_taken($tag, $origtags[$k]))
			return ac_ajax_api_result(false, sprintf(_a("Personalization tag '%s' already in use -- choose another tag name"), $tag));
	}

	return ac_ajax_api_result(true, "");
}

?>
