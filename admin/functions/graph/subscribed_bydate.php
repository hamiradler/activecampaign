<?php

$period    = intval(ac_http_param("period"));
$from      = strval(ac_http_param("from"));
$to        = strval(ac_http_param("to"));


$listid    = (int)ac_http_param("id");
$mode      = (string)ac_http_param("mode");
$filterid  = (int)ac_http_param("filterid");

$range     = 'all';

$so = new AC_Select();
if ( $mode == 'report_list' ) {
	if ( $filterid ) {
		$so = select_filter_comment_parse($so, $filterid, $mode);
		if ( isset($so->graphfrom)     ) $from   = $so->graphfrom;
		if ( isset($so->graphto)       ) $to     = $so->graphto;
		if ( isset($so->graphperiod)   ) $period = $so->graphperiod;
		if ( isset($so->graphmode)     ) $range  = $so->graphmode;
	}
}

$series = array();
$graph  = array();

ac_graph_prepare_dateline($series, $graph, $period, $from, $to);

$cond = "";

if ( $mode == 'report_list' ) {
	$listarr = array();
	if ( !ac_admin_ismaingroup() ) {
		$admin = ac_admin_get();
		if ( $listid ) {
			if ( isset($admin['lists'][$listid]) ) {
				$listarr[] = $listid;
			} else {
				$listarr = array(0);
			}
		} else {
			$listarr = $admin['lists'];
		}
	}

	if ( $listarr ) {
		$liststr = implode("', '", $listarr);
	}

	if ( count($so->conds) > 1 ) {
		$f = $so->conds[1];
		// apply list filter
		$cond  = "AND sl.listid IN ( SELECT l.id FROM #list l WHERE 1 $f ) ";
	}
}


$rs   = ac_sql_query("
	SELECT
		DATE_FORMAT(sl.sdate, '%m/%d') AS cdate,
		DATEDIFF('$to', sl.sdate) AS diff,
		COUNT(*) AS count
	FROM
		#subscriber_list sl
	WHERE
	    DATE(sl.sdate) > '$from'
	AND sl.sdate < ('$to' + INTERVAL 1 DAY)
	$cond
	GROUP BY
		DATE(sl.sdate)
");

$count = 0;
$total = intval(ac_http_param("total")) > 0;

while ($row = ac_sql_fetch_assoc($rs)) {
	if ($total)
		$count += $row["count"];
	else
		$count = $row["count"];

	$series[$row["diff"]] = $row["cdate"];
	$graph[$row["diff"]] += $count;
}

if ($total) {
	$xcount = 0;
	foreach ($graph as $key => $count) {
		if ($count > $xcount)
			$xcount = $count;
		$graph[$key] = $xcount;
	}
}

if ( isset($_GET['json']) ) {
	krsort($series);
	krsort($graph);
	$series = array_values($series);
	$graph = array_values($graph);
}

$max = $cnt = $sum = $last = 0;
foreach ( $graph as $v ) {
	if ( $v > $max ) $max = $v;
	$sum += $v;
	$cnt++;
	$last = $v;
}
$avg = $cnt ? $sum / $cnt : 0;

$extras = array(
	'avg' => round($avg, 2),
	'max' => $max,
	'cnt' => $cnt,
	'sum' => $sum,
	'last' => $last,
	'empty' => !(bool)$sum,
);


if ( !$sum && isset($_GET['json']) ) {
	$graph[0] = 1;
	$graph[1] = 1.8;
	$graph[2] = 2.4;
	$graph[3] = 2.8;
	$graph[4] = 3.2;
	$graph[5] = 3.55;
	$graph[6] = 3.6;
	$graph[7] = 3.6;
	$graph[8] = 3.5;
	$graph[9] = 3.65;
	$graph[10] = 3.89;
	$graph[11] = 3.95;
	$graph[12] = 4.25;
	$graph[13] = 4.25;
	$graph[14] = 4.35;
	$graph[15] = 4.20;
	$graph[16] = 4.20;
	$graph[17] = 4.15;
	$graph[18] = 4.0;
	$graph[19] = 3.9;
	$graph[20] = 4.25;
	$graph[21] = 4.75;
	$graph[22] = 5.25;
	$graph[23] = 5.95;
	$graph[24] = 7;
	$graph[25] = 7.1;
	$graph[26] = 7.0;
	$graph[27] = 6.8;
	$graph[28] = 8.5;
	$graph[29] = 10;
}


//dbg($series,1);dbg($graph);


$smarty->assign("series", $series);
$smarty->assign("graph", $graph);

?>
