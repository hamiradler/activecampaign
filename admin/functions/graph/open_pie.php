<?php

$campaignid = intval(ac_http_param("id"));
$messageid  = intval(ac_http_param("messageid"));

if ($messageid > 0) {
	$open = (int)ac_sql_select_one("
		SELECT 
			`uniqueopens` 
		FROM 
			em_campaign_message 
		WHERE 
			messageid = '$messageid' 
		AND 
			campaignid = '$campaignid'
	");
} else {
	$open = (int)ac_sql_select_one("
		SELECT
			`uniqueopens`
		FROM
			#campaign
		WHERE
			id = '$campaignid'
	");
}

$bcond = "";
if ($messageid > 0)
	$bcond = "AND bd.messageid = '$messageid'";

$bounce = (int)ac_sql_select_one("
	SELECT
		COUNT(*)
	FROM
		#bounce_data bd
	WHERE
		bd.campaignid = '$campaignid'
		$bcond
");

if ($messageid > 0) {
	$total = (int)ac_sql_select_one("
		SELECT
			total_amt
		FROM
			#campaign_message
		WHERE
			campaignid = '$campaignid'
		AND
			messageid = '$messageid'
	");
} else {
	$total = (int)ac_sql_select_one("
		SELECT
			total_amt
		FROM
			#campaign
		WHERE
			id = '$campaignid'
	");
}

# The number of unopened emails equals the total less the number of opens and the number of
# bounces.
$unopen = ($total - $open - $bounce);

$pie = array(
	array(
		"title" => _a("Opened"),
		"val"   => $open,
	),
	array(
		"title" => _a("Unopened"),
		"val"   => $unopen,
	),
	array(
		"title" => _a("Bounced"),
		"val"   => $bounce,
	),
);

$smarty->assign("pie", $pie);

?>
