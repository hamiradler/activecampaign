<?php

$campaignid = intval(ac_http_param("id"));
$messageid = intval(ac_http_param("messageid"));
$listid    = (int)ac_http_param("listid");
$period  = intval(ac_http_param("period"));
$from    = strval(ac_http_param("from"));
$to      = strval(ac_http_param("to"));




$series = cache_get('subscriber_rate_series_'.$period.'_'.$listid.'_'.$from.'_'.$to);
$graph = cache_get('subscriber_rate_graph_'.$period.'_'.$listid.'_'.$from.'_'.$to);
$extras = cache_get('subscriber_rate_extras_'.$period.'_'.$listid.'_'.$from.'_'.$to);

if ( !($series) OR !($graph) OR !($extras) ) {



$series = array();
$graph  = array();

ac_graph_prepare_dateline($series, $graph, $period, $from, $to);

$dates = array();
$dummy = array();
ac_graph_prepare_dateline($dates, $dummy, $period, $from, $to, "Y-m-d");



// later we will add support for multiple lists


$cond = $subcond = "";
$listfilter = false; // if we'll filter by list
if ( $listid ) $listfilter = true; // if list provided, we will
if ( !ac_admin_ismain() ) $listfilter = true; // if not main admin, we will
if ( $listfilter ) { // if using list filter
	if ( ac_admin_ismain() ) { // for main admins
		$lists = array($listid); // just the filtered one
	} elseif ( $listid ) { // if filtered is provided
		$lists = array_intersect(array($listid), $admin['lists']); // check if in allowed lists
	} else { // otherwise
		$lists = $admin['lists']; // just admin's permissions are the filter
	}
	$liststr = implode("', '", $lists);
	//$subcond = "AND ( SELECT COUNT(*) FROM #campaign_list cl WHERE c.campaignid = cl.campaignid AND cl.listid IN ('$liststr') ) > 0 ";
	$cond = "AND listid IN ('$liststr') ";
}

$groupid = current($admin['groups']);

if ( $listfilter and $listid ) {
	$cachecond = "AND groupid = '0' AND listid IN ('$liststr')";
	$cgid = 0;
	$clid = $listid;
} else {
	$cachecond = "AND groupid = '$groupid' AND listid = 0";
	$cgid = $groupid;
	$clid = 0;
}



$query = "
	SELECT
		DATE_FORMAT(countdate, '%m/%d') AS formatted,
		subscriber_active AS cnt
	FROM
		#datecount
	WHERE
		1
	AND groupid = '$cgid'
	AND listid = '$clid'
	$cachecond
	ORDER BY countdate DESC
	LIMIT $period
";
$counts = ac_sql_select_box_array($query);

foreach ( $series as $k => $v ) {
	if ( isset($counts[$v]) and $counts[$v] ) {
		$graph[$k] = $counts[$v];
		continue;
	}
	//$res = 0;
	$curdate = $dates[$k];
	$qry = "
		SELECT
			COUNT(DISTINCT(subscriberid))
		FROM
			#subscriber_list
		WHERE
			sdate <= '$curdate 23:59:59'
		AND
		(
			udate IS NULL
		OR
			udate > '$curdate 23:59:59'
		)
		$cond
	";
	if ( ac_http_param_exists('dbg') and isset($_SESSION['ac_arc_login']) ) {
		dbg($qry,1);
	}

	$res = (int)ac_sql_select_one($qry);

	$graph[$k] = $res;
	if ( $dates[$k] == AC_CURRENTDATE ) continue; // don't cache today, there might be more hits
	if ( !isset($counts[$v]) ) {
		$in = array(
			'id' => 0,
			'groupid' => $cgid,
			'listid' => $clid,
			'countdate' => $dates[$k],
			'subscriber_active' => $res,
		);
		ac_sql_insert("#datecount", $in);
		//dbg($in,1);
	} else {
		$upcond = "groupid = '$groupid' AND countdate = '{$dates[$k]}' AND groupid = '$cgid' AND listid = '$clid'";
		ac_sql_update_one("#datecount", "subscriber_active", $res, $upcond);
	}
}


if ( isset($_GET['json']) ) {
	krsort($series);
	krsort($graph);
	$series = array_values($series);
	$graph = array_values($graph);
}

$min = $max = $cnt = $sum = $last = 0;
foreach ( $graph as $v ) {
	if ( $v < $min ) $min = $v;
	if ( $v > $max ) $max = $v;
	$sum += $v;
	$cnt++;
	$last = $v;
}
$avg = $cnt ? $sum / $cnt : 0;

$extras = array(
	'avg' => round($avg, 2),
	'min' => $min,
	'max' => $max,
	'cnt' => $cnt,
	'sum' => $sum,
	'last' => $last,
	'empty' => !(bool)$sum,
);

if ( isset($_GET['json']) ) {
	$cutoff = $min;
	if ( $min > 0 ) $cutoff = (int)($cutoff * .9);
	foreach ( $graph as $k => $v ) {
		$graph[$k] = $v - $cutoff;
	}
}
//dbg($graph);

if ( !$sum && isset($_GET['json']) ) {
	$graph[0] = 1;
	$graph[1] = 1.8;
	$graph[2] = 2.4;
	$graph[3] = 2.8;
	$graph[4] = 3.2;
	$graph[5] = 3.55;
	$graph[6] = 3.6;
	$graph[7] = 3.6;
	$graph[8] = 3.5;
	$graph[9] = 3.65;
	$graph[10] = 3.89;
	$graph[11] = 3.95;
	$graph[12] = 4.25;
	$graph[13] = 4.25;
	$graph[14] = 4.35;
	$graph[15] = 4.20;
	$graph[16] = 4.20;
	$graph[17] = 4.15;
	$graph[18] = 4.0;
	$graph[19] = 3.9;
	$graph[20] = 4.25;
	$graph[21] = 4.75;
	$graph[22] = 5.25;
	$graph[23] = 5.95;
	$graph[24] = 7;
	$graph[25] = 7.1;
	$graph[26] = 7.0;
	$graph[27] = 6.8;
	$graph[28] = 8.5;
	$graph[29] = 10;
}

	cache('subscriber_rate_extras_'.$period.'_'.$listid.'_'.$from.'_'.$to, $extras, '900');
	cache('subscriber_rate_graph_'.$period.'_'.$listid.'_'.$from.'_'.$to, $graph, '900');
	cache('subscriber_rate_series_'.$period.'_'.$listid.'_'.$from.'_'.$to, $series, '900');
}


$smarty->assign("series", $series);
$smarty->assign("graph", $graph);

?>
