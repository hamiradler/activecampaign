<?php

$campaignid = intval(ac_http_param("id"));
$messageid = intval(ac_http_param("messageid"));
$listid    = (int)ac_http_param("listid");
$period  = intval(ac_http_param("period"));
$from    = strval(ac_http_param("from"));
$to      = strval(ac_http_param("to"));

if ( !$period ) $period = 10;


$series = cache_get('read_rate_series_'.$period.'_'.$listid);
$graph = cache_get('read_rate_graph_'.$period.'_'.$listid);
$extras = cache_get('read_rate_extras_'.$period.'_'.$listid);

if ( !($series) OR !($graph) OR !($extras) ) {

$series = array();
$graph  = array();

//ac_graph_prepare_dateline($series, $graph, $period, $from, $to);


$campaigns = campaign_lastsent($period, $listid);

/*
// prefill the array if needed (if less than $period campaigns was found)
$diff = $period - count($campaigns);
if ( $diff ) {
	$series = array_fill(0, $diff, '');
	$graph = array_fill(0, $diff, 0);
}
*/

foreach ( $campaigns as $k => $v ) {
	$rate = round($v['uniqueopens'] / (int)$v['total_amt'] * 100, 2);
	if ( $rate > 100 ) $rate = 100;
	if ( !$rate ) continue;
	//$name = ac_sql_select_one("name", "#campaign", "id = '$v[campaignid]'");
	//if ( !$name ) $name = ac_sql_select_one("name", "#campaign_deleted", "id = '$v[campaignid]'");
	//$series[] = trim("$name @ $v[tstamp]");
	$series[] = trim($v['tstamp']);
	$graph[] = $rate;
}

/*
if ( isset($_GET['json']) ) {
	krsort($series);
	krsort($graph);
	$series = array_values($series);
	$graph = array_values($graph);
}
*/

$max = $cnt = $sum = $last = 0;
foreach ( $graph as $v ) {
	if ( $v > $max ) $max = $v;
	$sum += $v;
	$cnt++;
	$last = $v;
}
$avg = $cnt ? $sum / $cnt : 0;

// if only one row was found, chart can't deal with it, so we need another one just to make it connect two dots
if ( $cnt == 1 ) {
	$series[1] = $series[0];
	$graph[1] = $graph[0];
	$cnt = 2;
	//$sum *= 2;
}
$extras = array(
	'avg' => round($avg, 2),
	'max' => $max,
	'cnt' => $cnt,
	'sum' => $sum,
	'last' => $last,
	'empty' => !(bool)$sum,
);



if ( !$sum && isset($_GET['json']) ) {
	$graph[0] = 3;
	$graph[1] = 4;
	$graph[2] = 4.2;
	$graph[3] = 3.3;
	$graph[4] = 4.3;
	$graph[5] = 4.7;
	$graph[6] = 4.5;
	$graph[7] = 4.4;
	$graph[8] = 5.3;
	$graph[9] = 4.7;
	$series = array_fill(0, count($graph), '-');
}


	cache('read_rate_extras_'.$period.'_'.$listid, $extras, '900');
	cache('read_rate_graph_'.$period.'_'.$listid, $graph, '900');
	cache('read_rate_series_'.$period.'_'.$listid, $series, '900');
}

$smarty->assign("series", $series);
$smarty->assign("graph", $graph);

?>
