<?php

// define ihooks

ac_ihook_define('ac_process_handler', 'ihook_process_handler');
ac_ihook_define('ac_process_info', 'ihook_process_info');
ac_ihook_define('ac_process_actions', 'ihook_process_actions');
ac_ihook_define('ac_process_update', 'ihook_process_update');


// ihooks functions


function ihook_process_handler($process) {
	if ( $process['action'] == 'subscriber_import' ) {
		// include hooks
		require_once(ac_admin("functions/subscriber_import.php"));
		// run importer
		$test = ( isset($process['data']['test']) ? $process['data']['test'] : false );
		$_POST = $process['data']; // we gotta simulate post here :(
		return subscriber_import_run($process['data'], $test, $process['completed'], $prepareOnly = false);
		return ac_import_run($process['data'], $test, $process['completed'], $prepareOnly = false);
	} elseif ( $process['action'] == 'sync' ) {
		// include sync
		require_once(ac_global_functions("sync.php"));
		// overlap check
		$proc = ac_process_get($process['id']);
		if ( (int)$proc['stall'] < 4 * 60 and $proc['ldate'] != '0000-00-00 00:00:00' ) {
			// trigger an error
			return false;
			return ac_sync_run(array(), $process['data']['test'], $full = true, $process['completed']);
		}
		ac_process_update($process['id'], false);
		// run sync
		$test = ( isset($process['data']['test']) ? $process['data']['test'] : false );
		$_POST = $sync = $process['data']['sync']; // we gotta simulate post here :(
		ac_sync_log_init($sync);
		ac_sync_log_store("\nPicking up Cron Job (process #$sync[process_id]) at $process[completed] / $process[total]\n");
		return ac_sync_run($sync, $test, $full = true, $process['completed']);
	} elseif ($process["action"] == "database") {
		require_once ac_admin("functions/database.php");
		database_handle($process);
	} elseif ( $process['action'] == 'removebatch' ) {

		// action=batch, "Remove a select list of addresses"

		// Loop through supplied email addresses, and attempt to remove from current list
		$GLOBALS['subscriber_batch'] = 1;
		foreach ($process['data']['emails'] as $email) {

			// Loop through selected lists
			foreach ($process['data']['lists'] as $listid) {

				$subscriber = subscriber_exists(trim($email), $listid);

				if ($subscriber) {
					subscriber_list_remove($subscriber, $listid);
				}
			}
			// Run the process update X times - whatever the count of supplied emails is
			ac_process_update($process['id']);
		}
		cache_clear('subcnt');
		cache_clear("withinlimits_subscriber");
		unset($GLOBALS['subscriber_batch']);

	} elseif ($process['action'] == 'service_import_magento') {
		$process_data = $process["data"];
		require_once ac_admin("functions/subscriber.admin.php");
		foreach ($process_data as $subscriber) {
			$_POST = $subscriber;
			subscriber_sync("magento");
			ac_process_update($process['id']);
		}
	} elseif ($process['action'] == 'service_import_shopify') {
		$process_data = json_decode($process["data"]);
		foreach ($process_data->orders as $order) {
			$order = get_object_vars($order);
			if ( (int)$order["buyer_accepts_marketing"] ) {
				$_GET["p"] = array($process_data->lists_str, "shopify");
				$_POST = $order;
				$_POST["activecampaign_internal_submission"] = 1;
				$_POST["customer_sdate"] = $process_data->customer_sdate;
				require_once ac_admin("functions/subscriber.admin.php");
				// submit it just like Shopify does (when coming from the webhook). It will end up in service_subscriber_integration()
				subscriber_insert_post();
			}
			ac_process_update($process['id']);
		}
	} elseif ( $process['action'] == 'removenon' || $process['action'] == 'removeall' ) {

		// action=batch, "Remove all non-confirmed subscribers from these lists"
		// action=batch, "Remove all subscribers from these lists"

		$so = new AC_Select;
		$so->push($process['data']['conds']);
		$so->slist = array('s.id');
		$so->remove = false;
		$subscribers = subscriber_select_array($so);

		if ( !$subscribers ) {
			ac_process_end($process['id']);
			return;
		}

		// Loop through subscribers
		foreach ($subscribers as $subscriber) {
			// Loop through selected lists
			foreach ($process['data']['lists'] as $listid) {
				subscriber_list_remove($subscriber, $listid);
			}
			ac_process_update($process['id']);
			ac_flush('. ');
		}
		ac_flush('Completed.');
	} elseif ( $process['action'] == 'campaign' ) {
		require_once ac_admin("functions/campaign.php");
		$r = campaign_process($process);
		if ( !$r ) ac_process_remove($process['id']);
		return $r;
	} elseif ( $process['action'] == 'filter' ) {
		require_once ac_admin("functions/filter.php");
		filter_process($process);
	} elseif ($process["action"] == "iconv") {
		require_once ac_admin("functions/iconv.php");
		iconv_process($process);
	} elseif ($process["action"] == "reverify") {
		require_once ac_admin("functions/reverify.php");
		reverify_process($process);
	} else {
		// process is unknown, remove it
		ac_process_remove($process['id']);
		return ac_ajax_api_result(false, _a('Unknown Process - deleted'));
	}
	return true;
}

function ihook_process_info($process) {
	$r = array();
	$actions = ac_ihook('ac_process_actions');
	if ( !isset($actions[$process['action']]) ) return;
	$r['name'] = $actions[$process['action']];
	if ( $process['action'] == 'subscriber_import' ) {
		// stuff for importer
	} elseif ( $process['action'] == 'sync' ) {
		// stuff for sync
	} elseif ( $process['action'] == 'removebatch' ) {
		//
	} elseif ( $process['action'] == 'removenon' ) {
		//
	} elseif ( $process['action'] == 'removeall' ) {
		//
	} elseif ( $process['action'] == 'database' ) {
		//
	} elseif ( $process['action'] == 'campaign' ) {
		//
	} elseif ( $process['action'] == 'filter' ) {
		//
	} elseif ( $process['action'] == 'iconv' ) {
		//
	}
	return $r;
}

function ihook_process_actions() {
	return array(
		'subscriber_import' => _a('Import Subscribers'),
		'sync'              => _a('Database Synchronization'),
		'removeall'         => _a('Remove All Subscribers'),
		'removebatch'       => _a('Batch Remove Subscribers'),
		'removenon'         => _a('Remove Non-Confirmed Subscribers'),
		'database'          => _a('Database Utility'),
		'campaign'          => _a('Sending Engine'),
		'filter'            => _a('Subscriber Filtering'),
		'iconv'				=> _a("Convert to UTF-8"),
		'service_import_shopify' => _a("Import Previous Shopify Customers"),
	);
}

function ihook_process_update($process, $data) {
	$processdata = @unserialize($process['data']);
	if ( $process['action'] == 'campaign' ) {
		$sendid = (int)$processdata;
		$cid = (int)ac_sql_select_one("campaignid", "#campaign_count", "id = '$sendid'");
		// campaigns have their own run/pause switch
		$newstatus = ( isset($data['active']) ? 2 : 3 );
		ac_sql_update_one('#campaign', 'status', $newstatus, "`id` = '$cid'");
		//$oldstatus = ( $process['ldate'] ? 2 : 3 );
		//ac_sql_update_one('#campaign', 'status', $newstatus, "( `id` = '$cid' OR `processid` = '$process[id]' ) AND `status` = '$oldstatus'");
	}
}

?>
