<?php

require_once ac_global_classes("select.php");
require_once ac_global_functions("log.php");

function template_select_query(&$so) {
	if ( !ac_admin_ismaingroup() ) {
		$admin = ac_admin_get();
		if ( !isset($so->permsAdded) ) {
			$so->permsAdded = 1;
			//$admin['lists'][0] = 0;
			$liststr = implode("','", $admin["lists"]);
			$so->push("AND (SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid IN ('$liststr')) > 0");
		}
	}

	return $so->query("
		SELECT
			t.id,
			t.userid,
			t.name,
			t.subject,
			t.content,
			t.categoryid,
			t.used,
			t.waitpreview,
			(SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid = 0) > 0 AS is_global,
			(SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id) AS lists
		FROM
			#template t
		WHERE
			[...]
	");
}

function template_select_row($id, $campaignlists = null) {
	$id = intval($id);
	$so = new AC_Select;
	$so->push("AND t.id = '$id'");

	$r = ac_sql_select_row(template_select_query($so));

	if ( $r ) {
		$cond = '';
		if ( !ac_admin_ismaingroup() ) {
			$admin = ac_admin_get();
			if ( $admin['id'] != 1 ) {
				//$admin['lists'][0] = 0;
				$cond = "AND l.id IN ('" . implode("', '", $admin['lists']) . "')";
			}
		}

		$r["url"] = Screenshot::geturl("template", $r["id"]);
		$r['listscnt'] = $r['lists'];
		$r['lists'] = ac_sql_select_array("SELECT l.* FROM #template_list t, #list l WHERE t.templateid = '$id' AND t.listid = l.id $cond");

		if ( !$r['lists'] and $campaignlists ) {
			$listcond = str_replace('-', "', '", $campaignlists);
			$r['lists'] = ac_sql_select_array("SELECT l.* FROM #list l WHERE l.id IN ('$listcond') $cond");
		}
		$lists = array();
		foreach ( $r['lists'] as $l ) {
			$lists[] = $l['id'];
		}
		$r['listslist'] = implode('-', $lists);
		$so = new AC_Select();
		$listslist = implode(',', $lists);
		$so->push("AND (SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid IN ('$listslist')) > 0");
		$r['fields'] = list_get_fields($lists, false);
		$r['personalizations'] = list_personalizations($so);
		$tags = ac_sql_select_list("SELECT tag FROM #template_tag tt INNER JOIN #tag t ON tt.tagid = t.id WHERE tt.templateid = '$id' ORDER BY t.tag");
		$tags = array_map("ucfirst", $tags);
		$r['tags'] = $tags;
	}
	return $r;
}

function template_select_array($so = null, $ids = null) {
	if ($so === null || !is_object($so))
		$so = new AC_Select;

	if ($ids !== null) {
		if ( !is_array($ids) ) $ids = explode(',', $ids);
		$tmp = array_diff(array_map("intval", $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND t.id IN ('$ids')");
	}
	$r = ac_sql_select_array(template_select_query($so));

	return $r;
}

function template_select_array_paginator($id, $sort, $offset, $limit, $filter) {
	$admin = ac_admin_get();
	$so = new AC_Select;

	$filter = intval($filter);
	if ($filter > 0) {
		$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'template'");
		$so->push($conds);
	}

	//$so->count();
	$so->count();
	$total = (int)ac_sql_select_one($q = template_select_query($so));
	// Using template_select_query() strips out the JOIN stuff, but still passes "WHERE l.listid = ...", so total is always 0
	#$total = (int)ac_sql_select_one("SELECT COUNT(*) as count FROM #template t");

	switch ($sort) {
		default:
		case "01":
			$so->orderby("name"); break;
		case "01D":
			$so->orderby("name DESC"); break;
		case "03":
			$so->orderby("lists"); break;
		case "03D":
			$so->orderby("lists DESC"); break;
	}

	if ( (int)$limit == 0 ) $limit = 999999999;
	$limit  = (int)$limit;
	$offset = (int)$offset;
	$so->limit("$offset, $limit");
	$rows = template_select_array($so);

	foreach ($rows as $row) {
		if ($row["waitpreview"])
			em_request_screenshot("template", $row["id"]);
	}

	return array(
		"paginator"   => $id,
		"offset"      => $offset,
		"limit"       => $limit,
		"total"       => $total,
		"cnt"         => count($rows),
		"rows"        => $rows,
	);
}

// api
function template_select_list($limit = 0, $ids = null) {
	$so = new AC_Select();

	if ($ids !== null && $ids != 'all') {
		if ( !is_array($ids) ) $ids = explode(",", $ids);
		$tmp = array_diff(array_map("intval", $ids), array(0));
		$ids = implode("','", $tmp);
		$so->push("AND t.id IN ('$ids')");
	}

	if ( $limit = (int)$limit ) $so->limit($limit);
	$so->orderby('t.id DESC');
	$so->remove = false;
	return template_select_array($so);
}

function template_filter_post() {
	$whitelist = array(
		"name",
		"content",
	);

	$ary = array(
		"userid" => $GLOBALS['admin']['id'],
		"sectionid" => "template",
		"conds" => "",
		"=tstamp" => "NOW()",
	);

	if (isset($_POST["qsearch"]) && !isset($_POST["content"])) {
		$_POST["content"] = $_POST["qsearch"];
	}

	if (isset($_POST["content"]) and $_POST["content"] != "") {
		$content = ac_sql_escape($_POST["content"], true);
		$conds = array();

		if (!isset($_POST["section"]) || !is_array($_POST["section"]))
			$_POST["section"] = $whitelist;

		foreach ($_POST["section"] as $sect) {
			if (!in_array($sect, $whitelist))
				continue;
			$conds[] = "$sect LIKE '%$content%'";
		}

		$conds = implode(" OR ", $conds);
		$ary["conds"] = "AND ($conds) ";
	}

	if ( isset($_POST['listid']) ) {
		if ( defined('ACPUBLIC') ) {
			$_SESSION['nlp'] = $_POST['listid'];
		} else {
			$_SESSION['nla'] = $_POST['listid'];
		}
	}
	$nl = null;
	if ( isset($_SESSION['nlp']) and defined('ACPUBLIC') ) {
		$nl = $_SESSION['nlp'];
	} elseif ( isset($_SESSION['nla']) ) {
		$nl = $_SESSION['nla'];
	}
	if ( $nl ) {
		if ( is_array($nl) ) {
			if ( count($nl) > 0 ) {
				$ids = implode("', '", array_map('intval', $nl));
				$ary['conds'] .= "AND (SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid IN ('$ids')) > 0 ";
			} else {
				if ( defined('ACPUBLIC') ) {
					unset($_SESSION['nlp']);
				} else {
					unset($_SESSION['nla']);
				}
			}
		} else {
			$listid = (int)$nl;
			if ( $listid > 0 ) {
				$ary['conds'] .= "AND (SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid = '$listid') > 0 ";
			} else {
				if ( defined('ACPUBLIC') ) {
					unset($_SESSION['nlp']);
				} else {
					unset($_SESSION['nla']);
				}
			}
		}
	}
	if ( $ary['conds'] == '' ) return array('filterid' => 0);

	$conds_esc = ac_sql_escape($ary["conds"]);
	$filterid = ac_sql_select_one("
		SELECT
			id
		FROM
			#section_filter
		WHERE
			userid = '$ary[userid]'
		AND
			sectionid = 'template'
		AND
			conds = '$conds_esc'
	");

	if (intval($filterid) > 0)
		return array("filterid" => $filterid);
	ac_sql_insert("#section_filter", $ary);
	return array("filterid" => ac_sql_insert_id());
}

function template_insert_post() {

	if ( ac_http_param('template_scope') == 'all' ) {
		$l = 0;
	}
	else {
		// find parents
		if ( isset($_POST['p']) and is_array($_POST['p']) and count($_POST['p']) > 0 ) {
			$lists = array_map('intval', $_POST['p']);
		} else {
			return ac_ajax_api_result(false, _a("You did not select any lists."));
		}
	}

	$admin = ac_admin_get();

	// check group/list privileges - only Admin Group users can create global templates
	if ( !ac_admin_ismaingroup() ) {
		if (ac_http_param('template_scope') == 'specific') {
		  foreach ($lists as $l) {
		    if ( !in_array($l, $admin['lists']) ) {
		      return ac_ajax_api_result(false, _a("One or more lists you supplied are not accessible to you."));
		    }
		  }
		}
		elseif (ac_http_param('template_scope') == 'all') {
		  return ac_ajax_api_result(false, _a("You do not have privileges to create global templates."));
		}
	}

	$ary = template_post_prepare();
	$ary['id'] = 0;
	$ary['userid'] = (int)$admin['id'];
	$ary['used'] = 3;	# Default 3 used points for creating your own template
	$ary['waitpreview'] = 1;

	if ( $ary['name'] == '' ) {
		return ac_ajax_api_result(false, _a("Template Name can not be left empty. Please name this template."));
	}

	$sql = ac_sql_insert("#template", $ary);
	if ( !$sql ) {
		return ac_ajax_api_result(false, _a("Template could not be added."));
	}
	$id = ac_sql_insert_id();

	if ( ac_http_param('template_scope') == 'specific' ) {
		foreach ( $lists as $l ) {
			ac_sql_insert('#template_list', array('id' => 0, 'templateid' => $id, 'listid' => $l));
		}
	}
	else {
		ac_sql_insert('#template_list', array('id' => 0, 'templateid' => $id, 'listid' => $l));
	}

	// handle tags
	// first, delete tag relations for this template
	ac_sql_delete('#template_tag', "templateid = '$id'");
	// get tags from page
	$tags = ac_http_param("tags");
	if ($tags) {
		$tags = array_map("strtolower", $tags);
		foreach ($tags as $tag) {
			// check if it's a tag in the system; if not, enter it
			$tag_id = (int)ac_sql_select_one("SELECT id FROM #tag WHERE tag = '$tag'");
			if (!$tag_id) {
	      $insert = array(
	        "tag" => $tag,
	      );
	      $sql = ac_sql_insert("#tag", $insert);
	      $tag_id = ac_sql_insert_id();
	    }
	    // now add a relation for this template
      $insert = array(
      	"templateid" => $id,
        "tagid" => $tag_id,
      );
      $sql = ac_sql_insert("#template_tag", $insert);
      $template_tag_id = ac_sql_insert_id();
		}
	}

	em_request_screenshot("template", $id);

	return ac_ajax_api_added(_a("Template"), array("id" => $id));
}

function template_update_post() {
	if (ac_http_param('template_scope') != 'all' && ac_http_param('template_scope') != 'specific') {
		return ac_ajax_api_result(false, _a("Invalid value for template scope (only all or specific are allowed)."));
	}
	if ( ac_http_param('template_scope') == 'all' ) {
		$lists = array(0);
		$l = 0;
	}
	else {
		if ( isset($_POST['p']) and is_array($_POST['p']) and count($_POST['p']) > 0 ) {
			$lists = array_map('intval', $_POST['p']);
		} else {
			return ac_ajax_api_result(false, _a("You did not select any lists."));
		}
	}

	// check group/list privileges - only Admin Group users can create global templates
	if ( !ac_admin_ismaingroup() ) {
		if (ac_http_param('template_scope') == 'specific') {
		  $admin = ac_admin_get();
		  foreach ($lists as $l) {
		    if ( !in_array($l, $admin['lists']) ) {
		      return ac_ajax_api_result(false, _a("One or more lists you supplied are not accessible to you."));
		    }
		  }
		}
		elseif (ac_http_param('template_scope') == 'all') {
		  return ac_ajax_api_result(false, _a("You do not have privileges to create global templates."));
		}
	}

	$ary = template_post_prepare();
	$ary['waitpreview'] = 1;

	if ( $ary['name'] == '' ) {
		return ac_ajax_api_result(false, _a("Template Name can not be left empty. Please name this template."));
	}

	$id = intval($_POST["id"]);
	$sql = ac_sql_update("#template", $ary, "id = '$id'");

	if ( !$sql ) {
		return ac_ajax_api_result(false, _a("Template could not be updated."));
	}

	// list relations
	$cond = implode("', '", $lists);
	$admincond = '';
	if ( !ac_admin_ismaingroup() ) {
		$admin = ac_admin_get();
		$admincond = "AND listid IN ('" . implode("', '", $admin['lists']) . "')";
	}
	//ac_sql_delete('#template_list', "templateid = '$id' AND listid NOT IN ('$cond') $admincond");
	ac_sql_delete('#template_list', "templateid = '$id' $admincond");

	if (ac_http_param('template_scope') == 'specific') {
		foreach ( $lists as $l ) {
			if ( $l > 0 ) {
				if ( !ac_sql_select_one('=COUNT(*)', '#template_list', "templateid = '$id' AND listid = '$l'") )
					ac_sql_insert('#template_list', array('id' => 0, 'templateid' => $id, 'listid' => $l));
			}
		}
	}
	else {
	  ac_sql_insert('#template_list', array('id' => 0, 'templateid' => $id, 'listid' => $l));
	}

	// handle tags
	// first, delete tag relations for this template
	ac_sql_delete('#template_tag', "templateid = '$id'");
	// get tags from page
	$tags = ac_http_param("tags");
	if ($tags) {
		$tags = array_map("strtolower", $tags);
		foreach ($tags as $tag) {
			// check if it's a tag in the system; if not, enter it
			$tag_id = (int)ac_sql_select_one("SELECT id FROM #tag WHERE tag = '$tag'");
			if (!$tag_id) {
	      $insert = array(
	        "tag" => $tag,
	      );
	      $sql = ac_sql_insert("#tag", $insert);
	      $tag_id = ac_sql_insert_id();
	    }
	    // now add a relation for this template
      $insert = array(
      	"templateid" => $id,
        "tagid" => $tag_id,
      );
      $sql = ac_sql_insert("#template_tag", $insert);
      $template_tag_id = ac_sql_insert_id();
		}
	}

	em_request_screenshot("template", $id);

	return ac_ajax_api_updated(_a("Template"));
}

function template_delete($id) {
	$id = intval($id);
	ac_sql_query("DELETE FROM #template WHERE id = '$id'");
	template_delete_relations(array($id));
	return ac_ajax_api_deleted(_a("Template"));
}

function template_delete_multi($ids, $filter = 0) {
	if ( $ids == '_all' ) $ids = null;
	$so = new AC_Select();
	$so->slist = array('t.id');
	$so->remove = false;
	$filter = intval($filter);
	if ($filter > 0) {
		$admin = ac_admin_get();
		$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '$admin[id]' AND sectionid = 'template'");
		$so->push($conds);
	}
	$tmp = template_select_array($so, $ids);
	$idarr = array();
	foreach ( $tmp as $v ) {
		$idarr[] = $v['id'];
	}
	$ids = implode("','", $idarr);
	ac_sql_query("DELETE FROM #template WHERE id IN ('$ids')");
	template_delete_relations($ids);
	return ac_ajax_api_deleted(_a("Template"));
}

function template_delete_relations($ids) {
	$admincond = 1;
	$admincond2 = 1;
	if ( !ac_admin_ismain() ) {
		$admin = ac_admin_get();
		$admin['lists'][0] = 0;
		$admincond = "listid IN ('" . implode("', '", $admin['lists']) . "')";
		$users_templates = ac_sql_select_list("SELECT templateid FROM #template_list WHERE listid IN ('" . implode("','", $admin['lists']) . "')");
		$admincond2 = "templateid IN ('" . implode("','", $users_templates) . "')";
	}
	if ($ids === null) {		# delete all
		ac_sql_delete('#template_list', "$admincond");
		ac_sql_delete('#template_tag', "$admincond2");
		ac_sql_delete('#screenshot', "target = 'template'");
	} else {
		if (is_array($ids))
			$ids = implode("','", $ids);
		ac_sql_delete('#template_list', "`templateid` IN ('$ids') AND $admincond");
		ac_sql_delete('#template_tag', "`templateid` IN ('$ids') AND $admincond2");
		ac_sql_delete('#screenshot', "targetid IN ('$ids') AND target = 'template'");
	}
}

function template_post_prepare() {
	// template
	$ary = array();
	$ary['name'] = (string)ac_http_param('name');
	$ary['subject'] = (string)ac_http_param('subject');
	$ary['content'] = (string)ac_http_param('html');
	$ary['content'] = str_replace('&amp;', '&', $ary['content']);
	$ary['content'] = ac_str_fixtinymce($ary['content']);
	return $ary;
}

// api
function template_import_list() {
  $templates = array();
  $names = ac_http_param('names');
  $urls = ac_http_param('urls');
  if ( !is_array($names) ) return ac_ajax_api_result( false, _a('names needs to be an array of template names') );
  if ( !is_array($urls) ) return ac_ajax_api_result( false, _a('urls needs to be an array of URLs') );
  $imported = 0;
  foreach ($urls as $k => $url) {
    $name = $_POST['name'] = $names[$k];
    if ( ac_str_is_url($url) ) {
      $_POST['url'] = $url;
      $import = template_import_post();
      if ($import['succeeded']) {
        $templates[] = array('id' => $import['id'], 'name' => $name, 'url' => $url, 'result' => $import);
        $imported++;
      }
      else {
        $templates[] = array('id' => 0, 'name' => $name, 'url' => $url, 'result' => $import);
      }
    }
    else {
      $templates[] = array('id' => 0, 'name' => $name, 'url' => $url, 'result' => array('succeeded' => 0, 'message' => _a('This is not a valid URL')));
    }
  }
  if ($imported) {
    $result_code = true;
    $imported_message = 'Template(s) imported';
  }
  else {
    $result_code = false;
    $imported_message = 'No Template(s) imported';
  }
  return ac_ajax_api_result( $result_code, _a($imported_message), array('templates' => $templates) );
}

function template_import_post() {
	$site = ac_site_get();
	$admin = ac_admin_get();
	$ary = template_post_prepare();
	$ary['id'] = 0;
	$ary['userid'] = (int)$admin['id'];

	if ( $ary['name'] == '' ) {
		return ac_ajax_api_result(false, _a("Template Name can not be left empty. Please name this template."));
	}

	if ( ac_http_param_exists('url') ) {
	  $xml = @file_get_contents( ac_http_param('url') );
	}
	else {
		// now read in the XML file
		$file = ac_http_param('import');
		// check if file(s) uploaded properly
		$uploaded = false;
		$xml = '';
		$path = ac_http_param('path');
		if (!$path) $path = 'cache';
		if ($path == 'cache') {
			$path = ac_cache_dir() . "/";
		}
		elseif ($path == 'admin/sql/email-templates') {
			$path = ac_base('admin/sql/email-templates') . "/";
		}
		if ( !is_array($file) ) $file = array();
		foreach ( $file as $filename ) {
			if ( file_exists($path . $filename) ) {
				$xml = @file_get_contents($path . $filename);
				if ( $xml ) {
					$uploaded = true;
					break; // only one file at the time in importer
				}
			}
		}
		if ( !$uploaded ) {
			return ac_ajax_api_result(false, _a("You did not upload a file to import. Please do that first..."));
		}
	}

	require_once ac_global_pear("Unserializer.php");

	$unserializer = new XML_Unserializer();

	$unserializer->unserialize($xml);
	$data = $unserializer->getUnserializedData();
	//print_r($data); exit;
	if ( PEAR::isError($data) ) {
		return ac_ajax_api_result(false, _a("Uploaded XML file could not be parsed. Please ensure you are uploading a valid XML file."));
	}
	if ( ( !isset($data['html']) and !isset($data['content']) ) or ( !isset($data['tag']) && !isset($data['directory']) ) ) {
		return ac_ajax_api_result(false, _a("Your template could not be imported because important data was missing from XML file."));
	}

	//$directory = ac_admin('images/template_' . $data['tag']);
	if ( isset($data['tag']) ) $directory_str = $data['tag'];
	if ( isset($data['directory']) ) $directory_str = $data['directory'];
	$directory = ac_base('images/admin/template_' . $directory_str);

	if (isset($GLOBALS["_hosted_account"])) {
		if (isset($_SESSION[$GLOBALS["_hosted_account"]])) {
			$acname = $_SESSION[$GLOBALS["_hosted_account"]]["account"];
			$directory = "/images/" . $acname . "/admin/template_" . $directory_str;
		} else {
			$directory = "/images/" . $GLOBALS["_hosted_account"] . "/admin/template_" . $directory_str;
		}
	}

	if ( !file_exists($directory) ) {
		@mkdir($directory);
	}
	if ( !file_exists($directory) ) {
		return ac_ajax_api_result(false, sprintf(_a("Your template could not be imported because folder %s could not be created."), $directory));
	}

	if ( !isset($data['content']) ) $data['content'] = $data['html'];
	$html = base64_decode($data['content']);

	// Strip out <title> tag since it was getting embedded in the template body at the very beginning
	$html = ac_str_strip_tag($html, 'title');

	$html = str_replace('&amp;', '&', $html);
	$html = str_replace(' ', '&nbsp;', $html);
	$html = str_replace('©', '�', $html);
	$plink = $site['p_link'];
	if ( isset($_SESSION['ac_updater']['plink']) ) $plink = $_SESSION['ac_updater']['plink'];
	//$html = preg_replace('/images\//', $plink . '/images/admin/template_' . $data['tag'] . '/', $html);
	$html = preg_replace('/cid:(\w{32}\.\w+)/', $plink . '/images/admin/template_' . $directory_str . '/$1', $html);

	//print $html; exit;

	// set data
	$ary['content'] = $html;
	// set template category
	$ary['categoryid'] = isset($data['categoryid']) ? (int)$data['categoryid'] : 0;

	// Set importnum.
	$ary['importnum'] = (int)ac_sql_select_one("SELECT MAX(importnum) FROM #template") + 1;

	// if they chose "available for specific lists" radio, make sure they selected some lists
	if ( ac_http_param('template_scope2') == 'specific' ) {
		// get selected lists
		if ( isset($_POST['p']) and is_array($_POST['p']) and count($_POST['p']) > 0 ) {
			$parents = array_map('intval', $_POST['p']);
		} else {
			return ac_ajax_api_result(false, _a("You did not select any lists."));
		}
	}

	$sql = ac_sql_insert("#template", $ary);
	if ( !$sql ) {
		//spit( print_r($ary), 'em');
		return ac_ajax_api_result(false, _a("Template could not be added."));
	}
	$id = ac_sql_insert_id();

	ac_log(var_export($data, true));

	// set template preview if tags are in XML
	if ( isset($data["preview_mime"]) && $data["preview_mime"] && isset($data["preview_data"]) && $data["preview_data"] ) {
		$preview_mime = $data["preview_mime"];
		$preview_data = base64_decode($data["preview_data"]);
		//ac_sql_query("DELETE FROM #screenshot WHERE target = 'template' AND targetid = '$id'");
		$ins = array(
			"mime" => $preview_mime,
			"target" => "template",
			"targetid" => $id,
			"importname" => strtolower(str_replace(" ", "-", $ary["name"])),
		);
		ac_sql_insert("#screenshot", $ins);
		if (!isset($GLOBALS['_hosted_account']))
			@file_put_contents(ac_base("images/_screenshot_/template-$ins[importname].jpg"), $preview_data);
	}

	// handle tags
	if ( isset($data["tags"]["item"]) ) {
  	if ( is_array($data["tags"]["item"]) ) {
  	  // more then one tag
      $tags = array_map("base64_decode", $data["tags"]["item"]);
  	}
  	else {
  	  // just one tag
  	  $tags = array( base64_decode($data["tags"]["item"]) );
  	}
  	foreach ($tags as $tag) {
      $tagid = (int)ac_sql_select_one("SELECT id FROM #tag WHERE tag = '$tag'");
  	  if (!$tagid) {
        $insert = array(
          "tag" => $tag,
        );
        $sql = ac_sql_insert("#tag", $insert);
        $tagid = ac_sql_insert_id();
      }
      $tag_rel_exists = (int)ac_sql_select_one("SELECT COUNT(*) FROM #template_tag WHERE templateid = '$id' AND tagid = '$tagid'");
      if (!$tag_rel_exists) {
        $insert = array(
          "templateid" => $id,
          "tagid" => $tagid,
        );
        $sql = ac_sql_insert("#template_tag", $insert);
        $template_tag_id = ac_sql_insert_id();
      }
  	}
	}

	// if they chose "available for ALL lists and users" radio, put a 0 in #template_list
	if ( ac_http_param('template_scope2') == 'all' ) {
		$parents = array(0);
		$l = 0;
		ac_sql_insert('#template_list', array('id' => 0, 'templateid' => $id, 'listid' => $l));
	}
	else {
		// loop through each selected list
		foreach ( $parents as $l ) {
			if ( $l > 0 ) {
				if ( !ac_sql_select_one('=COUNT(*)', '#template_list', "templateid = '$id' AND listid = '$l'") )
					ac_sql_insert('#template_list', array('id' => 0, 'templateid' => $id, 'listid' => $l));
			}
		}
	}

	// save dependencies
	if ( isset($data['images']) && is_array($data['images']) && isset($data['images']['item']) && is_array($data['images']['item']) ) {
		/*
		if ( isset($data['images']['item']['id']) ) {
			$data['images']['item'] = array($data['images']['item']);
		}
		*/
		// just one image
		if ( isset($data['images']['item']['name']) ) {
			ac_file_put($directory . '/' . $data['images']['item']['name'], base64_decode($data['images']['item']['contents']));
		} else {
			// many images
			foreach ( $data['images']['item'] AS $img ) {
				if ( isset($img['name']) ) {
					ac_file_put($directory . '/' . $img['name'], base64_decode($img['contents']));
				}
			}
		}
	}

	if ( !ac_http_param_exists('url') ) {
	  $remove_cache_file = ac_file_upload_remove(ac_cache_dir(), '', $file[0]);
	}

	return ac_ajax_api_result( true, _a("Your template has been successfully imported."), array("id" => $id) );
}

function template_import_file_remove($id) {
	$r = array(
		'succeeded' => false,
		'message' => '',
		'id' => $id
	);
	$file = (string)substr($id, strlen('xmlimport-'));
	if ( !$file ) $file = 'noname.xml';
	$r['succeeded'] = ac_file_upload_remove(ac_cache_dir(), '', $id);
	if ( $r['succeeded'] ) {
		$r['message'] = sprintf(_a("File '%s' removed."), substr($id, strlen('xmlimport-')));
	} else {
		$r['message'] = sprintf(_a("File '%s' could not be removed."), substr($id, strlen('xmlimport-')));
	}
	return $r;
}

// api
function template_export_list($ids, $type) {
  $templates = array();
  if ( is_array($ids) && count($ids) ) {
    foreach ($ids as $id) {
      $template = template_export($id, $type, false, false);
      if ( $template ) $templates[] = array("id" => $id, "name" => $template["name"], "content" => $template["content"]);
    }
  }
  return ac_ajax_api_result( true, _a('Template(s) exported'), array('templates' => $templates) );
}

function template_export($id, $type = 'html', $download = true, $echo = true) {
	$template = template_select_row($id);
	if ( !$template ) {
		if ($echo) {
	    echo _a('Data not found.');
		}
		else {
		  return false;
		}
		exit;
	}
	$fileName = ac_str_urlsafe($template['name']);
	if ( $type != 'xml' ) $type = 'html';
	if ( $type == 'xml' ) {
		require_once ac_global_pear("Serializer.php");
		require_once ac_global_functions("mime.php");

		$directory = substr(md5(uniqid(rand())), 0, 10);

		// set the content body and fetch images from it
		$content_copy = $template['content'];
		$images = ac_mail_embed_images($content_copy, true, true);

		$content_copy = str_replace('©', '&copy;', $content_copy);

		$preview_mime = ac_sql_select_one("SELECT `mime` FROM #screenshot WHERE target = 'template' AND targetid = '$id'");
		$preview_data = ac_sql_select_one("SELECT `data` FROM #screenshot WHERE target = 'template' AND targetid = '$id'");
		if ($preview_data) $preview_data = base64_encode($preview_data);

		$data = array(
			'content' => base64_encode($content_copy),
			'images' => array(),
			'tags' => array_map( 'base64_encode', $template['tags'] ),
			'categoryid' => $template['categoryid'],
			'preview_mime' => $preview_mime,
			'preview_data' => $preview_data,
		  'directory' => $directory,
		);

		foreach ( $images as $name => $contents ) {
			$data['images'][] = array(
				'name' => $name,
				'contents' => base64_encode($contents),
			);
		}
		$options = array(
			XML_SERIALIZER_OPTION_INDENT            => '    ',
			'defaultTagName'                        => 'item',
			//XML_UNSERIALIZER_OPTION_COMPLEXTYPE   => 'array',
			XML_SERIALIZER_OPTION_RETURN_RESULT     => true,
			XML_SERIALIZER_OPTION_LINEBREAKS        => "\r\n",
			//XML_SERIALIZER_OPTION_CDATA_SECTIONS  => true,
		);

		$serializer = new XML_Serializer($options);
	}
	// send headers
	if ( $type == 'xml' ) {
		header("Content-type: application/xml; charset=" . _i18n("utf-8"));
		if ($download) header("Content-Disposition: attachment; filename=$fileName.xml");
	} elseif ( $type == 'html' ) {
		header("Content-type: text/html; charset=" . _i18n("utf-8"));
		if ($download) header("Content-Disposition: attachment; filename=$fileName.html");
	}
	header("Pragma: no-cache");
	header("Expires: 0");
	// print
	if ($echo) {
	  echo ( $type == 'html' ? $template['content'] : $serializer->serialize($data) );
	  exit; // end the script execution here!
	}
	else {
	  return ( $type == 'html' ? $template['content'] : array( 'name' => $template['name'], 'content' => $serializer->serialize($data) ) );
	}
}

// clear out the cache folder of any remnant template preview or import files, in case they get stuck there somehow
function template_cache_clear() {
	if ( $handle = opendir( ac_cache_dir() ) ) {
		while ( false !== ($file = readdir($handle)) ) {
			$filename = ac_file_basename($file);
			// if the file has an extension (there are some files without extensions in the cache folder)
			$file_ext = preg_match('/\./', $filename);
			if ($filename && $file_ext) {
				if ( preg_match('/^(tplimport|template_preview)/', $filename) ) {
					$remove_cache_file = ac_file_upload_remove(ac_cache_dir(), '', $filename);
				}
			}
		}
	}
}

function template_selector_tdisplay($tagid, $searchkey, $offset, $length, $context = "campaign", $listid = 0) {
	$tagid = (int)$tagid;
	$so = new AC_Select;
	$admin = ac_admin_get();

	if ($context == "template") {
		if ( !ac_admin_ismain() ) {
			$admin = ac_admin_get();
			if ( !ac_admin_ismaingroup() ) {
				if ( !isset($so->permsAdded) ) {
					$so->permsAdded = 1;
					//$admin['lists'][0] = 0;
					$liststr = implode("','", $admin["lists"]);
					$so->push("AND (SELECT COUNT(*) FROM #template_list l WHERE l.templateid = t.id AND l.listid IN ('$liststr')) > 0");
				}
			}
		}
	}

	if ($admin["id"] != 1) {
		$liststr = implode("','", array_merge(array(0), $admin["lists"]));
		$so->push("AND (SELECT COUNT(*) FROM #template_list tl WHERE tl.templateid = t.id AND tl.listid IN ('$liststr')) > 0");
	}

	if ($listid > 0) {
		// (we should already be filtering to this admin user's lists when displaying the drop-down), so allow any list ID here
		$so->push("AND (SELECT COUNT(*) FROM #template_list tl WHERE tl.templateid = t.id AND tl.listid = '$listid') > 0");
	}

	if ($tagid > 0) {
		$so->push("AND (SELECT COUNT(*) FROM #template_tag tag WHERE tag.tagid = '$tagid' AND tag.templateid = t.id) > 0");
	}

	$searchkey = trim($searchkey);
	if ($searchkey != "" && strlen($searchkey) > 2) {
		$searchkeyesc = ac_sql_escape($searchkey);
		$so->push("AND (name LIKE '%$searchkey%' OR content LIKE '%$searchkey%')");
	}

	# For the blank one later.
	if ($offset == 0)
		$length--;

	$query = $so->query("
		SELECT
			COUNT(*)
		FROM
			#template t
		WHERE
			[...]
	");

	$total = (int)ac_sql_select_one($query);

	$offset = (int)$offset;
	$length = (int)$length;
	$so->limit("$offset, $length");

	$query = $so->query("
		SELECT
			id,
			name,
			content != '' AS haspreview,
			waitpreview
		FROM
			#template t
		WHERE
			[...]
		ORDER BY
			t.`used` DESC
	");

	$rval = ac_sql_select_array($query);

	foreach ($rval as $k => $row) {
		if ($row["waitpreview"])
			em_request_screenshot("template", $row["id"]);

		$rval[$k]["url"] = Screenshot::geturl("template", $row["id"]);
	}

	if ($offset == 0 && !$tagid && $searchkey == "" && $context == "campaign") {
		array_unshift($rval, array("id" => 0, "name" => _a("Blank Message"), "url" => Screenshot::defaultfile("template-blank")));
	}

	return array("row" => $rval, "loadmore" => ($offset + $length) < $total);
}

function template_selector_cdisplay($tagid, $searchkey, $offset, $length) {
	$so = new AC_Select;

	$admin = ac_admin_get();

	if ($admin["id"] != 1) {
		$liststr = implode("','", $admin["lists"]);
		$so->push("AND (SELECT COUNT(*) FROM #campaign_list cl WHERE cl.campaignid = cm.campaignid AND cl.listid IN ('$liststr')) > 0");
	}

	$searchkey = trim($searchkey);
	if ($searchkey != "" && strlen($searchkey) > 2) {
		$searchkeyesc = ac_sql_escape($searchkey);
		$so->push("AND (SELECT COUNT(*) FROM #message m WHERE m.id = cm.messageid AND (m.subject LIKE '%$searchkey%' OR m.html LIKE '%$searchkey%' OR m.text LIKE '%$searchkey%')) > 0");
	}

	$query = $so->query("
		SELECT
			COUNT(DISTINCT messageid)
		FROM
			#campaign_message cm
		WHERE
			[...]
	");

	$total = (int)ac_sql_select_one($query);

	$offset = (int)$offset;
	$length = (int)$length;
	$so->limit("$offset, $length");

	$query = $so->query("
		SELECT
			cm.messageid AS id,
			m.subject,
			m.html != '' AS haspreview
		FROM
			#campaign_message cm,
			#message m,
			#campaign c
		WHERE
			[...]
		AND
			m.id = cm.messageid
		AND
			cm.campaignid = c.id
		AND
			c.status != 0
		GROUP BY
			cm.messageid
		ORDER BY
			m.cdate DESC
	");

	$rval = ac_sql_select_array($query);

	foreach ($rval as $k => $row) {
		$rval[$k]["url"] = Screenshot::geturl("campaign", $row["id"], Screenshot::F_BIG);
	}

	return array("row" => $rval, "loadmore" => ($offset + $length) < $total);
}

function template_post2preparedcampaign() {
	// find basic campaign info
	$row = campaign_new();
	if ( isset($GLOBALS['_hosted_account']) ) {
		$row['htmlunsub'] =
		$row['textunsub'] = 0;
		$row['htmlunsubdata'] =
		$row['textunsubdata'] = '';
		$row['bounceid'] = -1;
	}

	//turn analytics off for test message
	$row['tracklinksanalytics'] = 0;
	$row['trackreadsanalytics'] = 0;

	$templateid = (int)ac_http_param("templateid");
	$lists = ac_sql_select_list("SELECT listid FROM #template_list WHERE templateid = '$templateid'");
	$row['listslist'] = implode('-', $lists);

	if ($lists === array('0'))
		$row['lists'] = list_select_array(null);
	else
		$row['lists'] = list_select_array(null, implode(',', $lists));

	foreach ( $row['lists'] as $k => $v ) {
		$row['lists'][$k]['relid'] = 0;
	}

	// calculate list limits
	$row['p_embed_image']        = 0;
	$row['p_use_scheduling']     = ( $row['status'] == 3 or $row['status'] == 4 );
	$row['p_use_tracking']       = 0;
	$row['p_use_analytics_read'] = 0;
	$row['p_use_analytics_link'] = 0;
	$row['p_use_twitter']        = 0;
	$row['p_use_facebook']       = 0;
	foreach ( $row['lists'] as $l ) {
		if ( $l['p_embed_image'] )        $row['p_embed_image']        = $l['p_embed_image'];
		if ( $l['p_use_tracking'] )       $row['p_use_tracking']       = $l['p_use_tracking'];
		if ( $l['p_use_analytics_read'] ) $row['p_use_analytics_read'] = $l['p_use_analytics_read'];
		if ( $l['p_use_analytics_link'] ) $row['p_use_analytics_link'] = $l['p_use_analytics_link'];
		if ( $l['p_use_twitter'] )        $row['p_use_twitter']        = $l['p_use_twitter'];
		if ( $l['p_use_facebook'] )       $row['p_use_facebook']       = $l['p_use_facebook'];
	}

	// fetch all fields (for those lists only, globals should be prefetched elsewhere)
	$row['fields'] = list_get_fields($lists, false);

	$template = template_select_row((int)ac_http_param("templateid"));
	$message["id"] = -1;
	$message['userid'] = $GLOBALS['admin']['id'];
	$message['cdate'] = AC_CURRENTDATETIME;
	$message['fromname'] = $GLOBALS['admin']['fullname'];
	$message['fromemail'] = $GLOBALS['admin']['email'];
	$message['reply2'] = $message['fromemail'];
	$message['priority'] = 3;
	$message['charset'] = "utf-8";
	$message['encoding'] = "quoted-printable";
	$message['format'] = "mime";
	$message['subject'] = $template['subject'];
	if ($message['subject'] == '')
		$message['subject'] = $template['name'];
	$message['html'] = $template['content'];
	$message['text'] = ac_htmltext_convert($message['html']);
	$message['htmlfetch'] = 'now';
	$message['textfetch'] = 'now';
	$message['hidden'] = 0;
	$message['percentage'] = 100;
	$message['sourcesize'] = 0;
	$row['messages'] = array($message);
	$row['ratios'] = array(100);

	if ( $message['format'] != 'html' ) {
		if ( ac_str_instr('%UNSUBSCRIBELINK%', $message['text']) or ac_str_instr('/box.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2', $message['text']) or	ac_str_instr('/proc.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&act=unsub', $message['text']) ) {
			$row['textunsub'] = 0;
		}
	}
	if ( $message['format'] != 'text' ) {
		if ( ac_str_instr('%UNSUBSCRIBELINK%', $message['html']) or ac_str_instr('/box.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&funcml=unsub2', $message['html']) or	ac_str_instr('/proc.php?nl=currentnl&c=cmpgnid&m=currentmesg&s=subscriberid&act=unsub', $message['html']) ) {
			$row['htmlunsub'] = 0;
		}
	}

	$row['messageslist'] = -1;

	// fetch all links for parsing
	$row['tlinks'] = array();
	foreach ( $row['links'] as $k => $v ) {
		$row['tlinks'][] = array(
			'id' => 0,
			'campaignid' => 0,
			'messageid' => -1,
			'link' => $v['link'],
			'name' => '',
		);
	}

	return $row;
}

?>
