<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<HTML>
<body>
<title>
<xsl:value-of select="//rss/channel/title"/>
</title>


<xsl:for-each select="//rss/channel/item">

<xsl:value-of select="description"/>

</xsl:for-each>



</body>
</HTML>

</xsl:template>
</xsl:stylesheet>