<?php

if (!@ini_get("zlib.output_compression")) @ob_start("ob_gzhandler");

require_once(dirname(__FILE__) . '/prepend.inc.php');
require_once ac_global_functions("http.php");
require_once ac_global_functions("rss.php");
require_once(ac_admin('functions/stats.php'));

if (!isset($_SERVER["REMOTE_ADDR"]))
	die("No remote addr");

$ip    = ac_sql_escape($_SERVER["REMOTE_ADDR"]);

$attempt = ac_sql_select_row("
	SELECT
		id,
		attempts,
		ip,
		IF(NOW() < (tstamp + INTERVAL 15 MINUTE), 1, 0) AS a_lockedout
	FROM
		#rss_badattempt
	WHERE
		ip = INET_ATON('$ip')
");

if (!$attempt) {
	$attempt = array(
		"=ip"         => "INET_ATON('$ip')",
		"attempts"    => 0,
		"a_lockedout" => 0,
	);
}

if ($attempt["a_lockedout"] && $attempt["attempts"] >= 5) {
	logattempt();
}

$hash = ac_sql_escape(trim((string)ac_http_param("h")));
$lim  = (int)ac_http_param("l");
$filter = trim((string)ac_http_param("f"));

if (!$hash)
	logattempt();
if ($lim < 1)
	$lim = 6;
if (!$filter)
	$filter = null;
else
	$filter = array_map('trim', explode(',', $filter));

# No rows?  No match.
$info = ac_auth_hash_decode($hash, $_SERVER['SERVER_NAME']);
if (!$info)
	logattempt();

$admin = ac_admin_get_totally_unsafe($info['id']);
if (!$admin)
	logattempt();


# OK: we made it.  Time to prepare the RSS.

$stream = stats_activity_get(0, $lim, $filter);


# This expects $rss to be an array with the following keys:
#
# 	title
# 	link
# 	description
# 	items
# 	pubDate [[optional]]
# 	language [[optional]]
#
# Where items is itself an array, with the following keys:
#
# 	title
# 	link
# 	description
# 	pubDate [[optional]]

$items = array();
$lastdate = null;
$smarty = new AC_Smarty('admin');
$smarty->assign('site', ac_site_get());
$smarty->assign('admin', ac_admin_get());
foreach ( $stream as $row ) {
	if ( !$lastdate ) $lastdate = $row['last'];
	$smarty->assign('row', $row);
	$html = $smarty->fetch('ajax.activity_stream.rss.htm');
	if ( !$html ) continue;
	$row['html'] = $html;
	$items[] = array(
		"title"       => $row["options"]["text"],// . ': ' . $row['amount'] . ' - ' . $row["timeago"],
		"description" => $row["html"],
		"link"        => $row["options"]["url"],
		"pubDate"     => $row["last"],
	);
	$rows[] = $row;
}
if ( !$lastdate ) $lastdate = AC_CURRENTDATETIME;

$rss = array(
	"title"       => _a("Email Marketing Activity Stream"),
	"link"        => ac_site_alink("main.php"),
	"description" => _a("Email Marketing Activity Stream"),
	"item"        => $items,
	"pubDate"     => $lastdate,
	"css"         => ac_site_alink("css/rss.css"),
	"xsl"         => ac_site_alink("css/rss.xsl"),
);


ac_rss_echo($rss);

function logattempt() {
	$attempt = $GLOBALS["attempt"];
	$attempt["attempts"] += 1;
	$attempt["=tstamp"]   = "NOW()";
	unset($attempt["a_lockedout"]);
	ac_sql_replace("#rss_badattempt", $attempt);
	exit;
}

?>
