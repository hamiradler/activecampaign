<?php

require_once ac_global_classes("select.php");
require_once ac_global_functions("cfield.php");
require_once ac_admin("functions/field.php");

class field_context extends ACP_Page {

	function field_context() {
		$this->pageTitle = _a("Subscriber Fields");
		$this->sideTemplate = "side.list.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		ac_smarty_submitted($smarty, $this);
		$smarty->assign("content_template", "field.htm");

		if (ac_http_param("deletefield")) {
			$listid = (int)ac_http_param("listid");
			$fieldid = (int)ac_http_param("deletefield");

			if (!$listid || !$fieldid)
				ac_http_redirect("main.php?action=field");

			ac_sql_query("DELETE FROM #field WHERE id = '$fieldid'");
			ac_sql_query("DELETE FROM #field_rel WHERE fieldid = '$fieldid'");
			ac_sql_query("DELETE FROM #field_value WHERE fieldid = '$fieldid'");
			ac_sql_query("DELETE FROM #field_option WHERE fieldid = '$fieldid'");

			form_recognize_field($fieldid);

			ac_http_redirect("main.php?action=field&listid=$listid");
		}

		$admin = ac_admin_get();
		$liststr = implode("','", $admin["lists"]);

		if (!$liststr)
			ac_http_redirect("main.php?action=list&new");

		# Lists
		$lists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$liststr') ORDER BY name");
		$smarty->assign("lists", $lists);

		# Which list to use when displaying fields.
		$listid = (int)ac_http_param("listid");

		if (!$listid)
			$listid = $lists[0]["id"];

		$namereq = (int)ac_sql_select_one("SELECT require_name FROM #list WHERE id = '$listid'");

		$smarty->assign("listid", $listid);
		$smarty->assign("namereq", $namereq);

		# Fields
		$fields = ac_cfield_select_nodata_rel("#field", "#field_rel", "r.relid IN ('$listid', '0')", "", "r.dorder, f.title");

		foreach ($fields as $k => $v) {
			$fields[$k]["options"] = ac_sql_select_array("SELECT id, orderid, value FROM #field_option WHERE fieldid = '$v[id]' ORDER BY orderid");
		}

		$smarty->assign("fields", $fields);
	}

	function formProcess(&$smarty) {
		$perstags = ac_http_param_forcearray("perstag");
		$isrequired = ac_http_param_forcearray("isrequired");
		$names = ac_http_param_forcearray("fieldname");
		$choices = ac_http_param_forcearray("choice");
		$listid = (int)ac_http_param("listid");
		$up = array();
		$r = array('status' => 0, 'section' => 'generic', 'message' => _a('Import '));

		# Update field-specific stuff

		foreach ($perstags as $k => $v) {
			$k = (int)$k;
			if (field_perstag_taken($v, ac_sql_select_one("SELECT perstag FROM #field WHERE id = '$k'"))) {
				$r['message'] = sprintf(_a("The personalization tag '%s' is already taken -- choose another"), $v);
				return $r;
			}

			if (!isset($up[$k]))
				$up[$k] = array();

			$up[$k]["perstag"] = $v;
		}

		foreach ($isrequired as $k => $v) {
			if (!isset($up[$k]))
				$up[$k] = array();

			$up[$k]["isrequired"] = $v;
		}

		foreach ($names as $k => $v) {
			if (!isset($up[$k]))
				$up[$k] = array();

			$up[$k]["title"] = $v;
		}

		$movefieldid = (int)ac_http_param("movefieldid");
		$movefielddir = (string)ac_http_param("movefielddir");
		$dorder = -1;

		$i = 0;
		foreach ($up as $k => $v) {
			ac_sql_update("#field", $v, "id = '$k'");

			$relup = array(
				"dorder" => $i,
			);

			if ($k == $movefieldid)
				$dorder = $i;

			ac_sql_update("#field_rel", $relup, "fieldid = '$k' AND relid IN ('0', '$listid')");
			form_recognize_field($k);
			$i++;
		}

		if ($dorder > -1 && $movefieldid > 0 && in_array($movefielddir, array("up", "down"))) {
			switch ($movefielddir) {
				default:
				case "up":
					$swap = ac_sql_select_row("SELECT * FROM #field_rel WHERE relid IN ('0', '$listid') AND dorder < $dorder ORDER BY dorder DESC LIMIT 1");
					ac_sql_query("UPDATE #field_rel SET dorder = '$swap[dorder]' WHERE fieldid = '$movefieldid' AND relid IN ('0', '$listid')");
					ac_sql_query("UPDATE #field_rel SET dorder = '$dorder' WHERE id = '$swap[id]'");
					break;

				case "down":
					$swap = ac_sql_select_row("SELECT * FROM #field_rel WHERE relid IN ('0', '$listid') AND dorder > $dorder ORDER BY dorder ASC LIMIT 1");
					ac_sql_query("UPDATE #field_rel SET dorder = '$swap[dorder]' WHERE fieldid = '$movefieldid' AND relid IN ('0', '$listid')");
					ac_sql_query("UPDATE #field_rel SET dorder = '$dorder' WHERE id = '$swap[id]'");
					break;
			}
		}

		foreach ($choices as $fieldid => $opts) {
			ac_sql_query("DELETE FROM #field_option WHERE fieldid = '$fieldid'");
			foreach ($opts as $k => $v) {
				$ins = array(
					"fieldid" => (int)$fieldid,
					"orderid" => $k + 1,
					"value" => $v,
					"label" => $v,
				);

				ac_sql_insert("#field_option", $ins);
			}

			if (count($opts) > 0) {
				$d = ac_sql_select_one("SELECT defval FROM #field WHERE id = '$fieldid'");
				$d = ac_sql_escape($d);
				$c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #field_option WHERE fieldid = '$fieldid' AND value = '$d'");

				# We must have deleted the original defval option...
				if ($c == 0) {
					$up = array(
						"defval" => $opts[0],
					);
					ac_sql_update("#field", $up, "id = '$fieldid'");
				}
			}
		}

		# Update list-specific stuff

		$namereq = (int)ac_http_param("namereq");

		if ($namereq)
			ac_sql_query("UPDATE #list SET require_name = 1 WHERE id = '$listid'");
		else
			ac_sql_query("UPDATE #list SET require_name = 0 WHERE id = '$listid'");

		ac_http_redirect("main.php?action=field&listid=$listid");
	}

}

?>
