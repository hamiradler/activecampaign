<?php

require_once ac_admin("functions/recipient.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class recipient_context extends ACP_Page {

	function recipient_context() {
		$this->pageTitle = _a("Campaign Recipients");
		$this->sideTemplate = "side.subscriber.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "recipient.htm");

		// get send id
		$sid = (int)ac_http_param('id');
		if ( $sid < 1 ) {
			ac_http_redirect(ac_site_alink());
		}
		// check if X table exists
		$sql = ac_sql_query("SHOW TABLES LIKE 'em\_x$sid'");
		if ( !$sql or !mysql_num_rows($sql) ) {
			ac_http_redirect(ac_site_alink());
		}
		$smarty->assign('sid', $sid);

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(recipient_select_query($so, $sid));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=recipient');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'recipient.recipient_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "email", "label" => _a("Email Address")),
			array("col" => "name", "label" => _a("Subscriber Name")),
			array("col" => "sdate", "label" => _a("Subscribe Date")),
		);
		$smarty->assign("search_sections", $sections);

	}
}

?>
