<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");

class api_context extends ACP_Page {
	function api_context() {
		$this->pageTitle = _a("Form Settings");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "api.htm");

		// api doc file examples
		$api_example_files = array();
		if ( $handle = opendir( ac_base('docs/api-examples') ) ) {
			while ( false !== ($file = readdir($handle)) ) {
				$file = ac_file_basename($file);
				if ($file) {
					if ( substr($file, 0, 9) != 'branding_' ) {
						$api_example_files[] = $file;
					}
				}
			}
			closedir($handle);
		}
		sort($api_example_files);
		$smarty->assign("api_example_filenames", $api_example_files);
		// the very first filename, and content, in the array - pull its content now so we can pre-load it
		$smarty->assign("api_example_filename1", $api_example_files[0]);
		$file1 = ac_file_get( ac_base('docs/api-examples/' . $api_example_files[0]) );
		$file1 = str_replace('YOUR_USERNAME', $GLOBALS['admin']['username'], $file1);
		$file1 = str_replace('http://yourdomain.com/path/to/12all', $GLOBALS['site']['p_link'], $file1);
		$file1 = str_replace('ActiveCampaign Email Marketing', $GLOBALS['admin']['brand_site_name'], $file1);
		$smarty->assign("api_example_content1", $file1);
	}
}

?>
