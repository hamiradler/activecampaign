<?php

require_once ac_admin("functions/abuse.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class abuse_context extends ACP_Page {

	function abuse_context() {
		$this->pageTitle = _a("Abuse Complaints");
		$this->sideTemplate = "";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "abuse.htm");

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(abuse_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=abuse');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'abuse.abuse_select_array_paginator';
		$smarty->assign('paginator', $paginator);

	}
}

?>
