<?php

class help_context extends ACP_Page {

	function help_context() {
		$this->pageTitle = _a("Help & Support");
		$this->sideTemplate = "";
		$this->ACP_Page();
		$this->admin = $GLOBALS["admin"];
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "help.htm");

		if ( isset($GLOBALS['_hosted_account']) ) {
			$helpurl = "http://www.activecampaign.com/search/?a=" . $GLOBALS['_hosted_account'];
		} else {
			$helpurl = 'http://www.activecampaign.com/search/?_sn=' . base64_encode($this->site["site_name"]);
		}

		$addon = '';
		foreach ( $_GET as $k => $v ) {
			if ( $k == 'action' ) continue;
			if ( $k == '_action' ) $k = 'action';
			if ( is_array($v) ) continue;
			$addon .= '&' . $k . '=' . urlencode($v);
		}


		foreach ( $_POST as $k => $v ) {
			if ( $k == 'action' ) continue;
			if ( $k == '_action' ) $k = 'action';
			if ( is_array($v) ) continue;
			$addon .= '&' . $k . '=' . urlencode($v);
		}

		ac_http_redirect($helpurl . $addon);

		$smarty->assign("helpurl", $helpurl . $addon);

	}
}

?>
