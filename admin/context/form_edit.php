<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");

class form_edit_context extends ACP_Page {
	function form_edit_context() {
		$this->pageTitle = _a("Edit Form");
		//$this->sideTemplate = "side.message.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("ac_load_editor", "1");

		if (ac_http_param("new")) {
			$listid = (int)ac_http_param("listid");
			$name = ac_http_param("name");
			$admin = ac_admin_get();

			if (!$this->admin["pg_form_edit"]) {
				$smarty->assign('content_template', 'noaccess.htm');
				return;
			}

			$ins = array(
				"name" => $name,
				"theme" => "simple-blue",
			);

			ac_sql_insert("#form", $ins);
			$id = (int)ac_sql_insert_id();

			# Set up list relation
			$ins = array(
				"formid" => $id,
				"listid" => $listid,
			);

			ac_sql_insert("#form_list", $ins);

			# Set up opt-in message.
			$ins = array(
				"userid" => $admin["id"],
				"=cdate" => "NOW()",
				"=mdate" => "NOW()",
				"fromname" => $admin["first_name"] . " " . $admin["last_name"],
				"fromemail" => $admin["email"],
				"reply2" => $admin["email"],
				"priority" => 3,
				"charset" => "utf-8",
				"encoding" => "quoted-printable",
				"format" => "mime",
				"subject" => _a("Please confirm your subscription to").' %LISTNAME%',
				"html" => '<body style="margin:0; padding:0; background:#f5f5f5; font-size:14px; font-family:Arial,Helvetica; color:#333333; line-height:20px;">
    <div style="padding:15px; background:#f5f5f5;">
        <p style="margin:0; margin-bottom:20px; padding:0; font-size:16px; font-family:Arial,Helvetica; line-height:26px;"><strong style="font-weight:bold;">Thank you for subscribing to %LISTNAME%!</strong></p>
        <p style="line-height:20px;">You or someone had subscribe to this list on %SUBDATE% using the address %EMAIL%.</p>
        <p style="line-height:20px;">If you believe that this is a mistake and you did not intend on subscribing to this list, you can ignore this message and nothing else will happen.</p>
        <div style="margin:0; margin-top:35px; padding:0;">
        <div style="padding:5px; padding-bottom:20px; font-size:14px; background:#f4fbfe; border:1px solid #cbe1eb; color:#333333; font-family:Arial,Helvetica; text-align:center;">
            <p style="line-height:20px; margin-bottom:6px;"><strong style="font-weight:bold;">To confirm that you wish to be subscribed, please click the link below:</strong></p>
            <div style="margin:10px; text-align:center;">
                <p style="width:100%; margin-top:20px; margin-right:auto; margin-bottom:0; margin-left:auto; width:272px;">
                    <a href="%CONFIRMLINK%" style="display:block; margin-top:20px; margin-right:auto; margin-bottom:0; margin-left:auto; padding-top:0px; width:270px; background:#83c700; border:1px solid #669b00; font-size:18px; color:#ffffff; text-decoration:none!important;">
                        <span style="display:block; width:100%; border-top:1px solid #e9ffbf; height:0px; line-height:0px;"></span>
                        <strong style="display:block; padding:10px; font-weight:bold; color:#ffffff;">Confirm My Subscription</strong>
                    </a>
                    <span style="display:block; border-bottom:2px solid #bcd3dc; height:0px; line-height:0px;"></span>
                </p>
            </div>
        </div>
        </div>
    </div>
</body>',
				"htmlfetch" => "now",
				"textfetch" => "now",
			);

			$ins["text"] = ac_htmltext_convert($ins["html"]);

			ac_sql_insert("#message", $ins);
			$messageid = (int)ac_sql_insert_id();

			// insert message/list relation
			$ins = array(
				"messageid" => $messageid,
				"listid" => $listid,
			);
			ac_sql_insert("#message_list", $ins);

			$up = array(
				"messageid" => $messageid,
				"waitpreview" => 1,
			);

			ac_sql_update("#form", $up, "id = '$id'");

			em_request_screenshot("form", $id);

			ac_http_redirect("main.php?action=form_edit&id=$id");
		}

		if (!ac_http_param("id")) {
			ac_http_redirect("main.php?action=form#list-01-0-0");
		}

		ac_smarty_submitted($smarty, $this);

		$id = (int)ac_http_param("id");
		$form = form_select_row($id);

		if (!$form)
			ac_http_redirect("main.php?action=form#list-01-0-0");

		$lists = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$form[id]'");
		$liststr = implode("','", $lists);

		$fields = ac_cfield_select_nodata_rel("#field", "#field_rel", "r.relid IN ('0', '$liststr') AND f.visible = '1'");

		# See if we have any parts... add some automatically if not.
		$parts = ac_sql_select_array("SELECT id, fieldid, builtin FROM #form_part WHERE formid = '$id'");

		if (count($parts) == 0) {
			$ins = array(
				"formid" => $id,
				"builtin" => "fullname",
				"header" => _a("Full Name"),
				"ordernum" => 0,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "email",
				"header" => _a("Email"),
				"ordernum" => 1,
			);

			ac_sql_insert("#form_part", $ins);

			$ins = array(
				"formid" => $id,
				"builtin" => "subscribe",
				"content" => _a("Subscribe"),
				"ordernum" => 2,
			);

			ac_sql_insert("#form_part", $ins);
			$parts = ac_sql_select_array("SELECT id, fieldid, builtin FROM #form_part WHERE formid = '$id'");
		}

		# Grab any themes.
		$dp = @opendir(ac_admin("templates/form-themes"));
		if (!$dp)
			ac_http_redirect("main.php");

		$themes = array();
		while ($file = @readdir($dp)) {
			if ($file == "." || $file == "..")
				continue;

			$path = "templates/form-themes/$file";
			$themes[] = array(
				"preview" => sprintf("%s/preview.gif", $path),
				"style" => sprintf("%s/style.css", $path),
				"name" => $file,
				"selected" => ($file == $form["theme"]) ? 1 : 0,
			);
		}

		if (form_theme_safe($form["theme"]))
			$path = dirname(dirname(__FILE__)) . "/templates/form-themes/$form[theme]";

		if (file_exists($path . "/header.htm"))
			$smarty->assign("header", @file_get_contents($path . "/header.htm"));

		if (file_exists($path . "/header.htm"))
			$smarty->assign("footer", @file_get_contents($path . "/footer.htm"));

		$smarty->assign("form", $form);
		$smarty->assign("lists", $lists);
		$smarty->assign("fields", $fields);
		$smarty->assign("themes", $themes);
		$smarty->assign("showtheme", (int)ac_http_param("theme"));
		$smarty->assign("content_template", "form_edit.htm");
	}

	function formProcess(&$smarty) {
		if (ac_http_param("imageupload") == 1) {
			$partid = (int)ac_http_param("partid");
			$formid = (int)ac_http_param("id");
			$imagealt = (string)ac_http_param("imagealt");

			if ($formid == 0)
				ac_http_redirect("main.php?action=form");

			if ($partid == 0 || !isset($_FILES['imagefile']) || !isset($_FILES['imagefile']['tmp_name']))
				ac_http_redirect("main.php?action=form_edit&id=$formid");

			$content = @file_get_contents($_FILES['imagefile']['tmp_name']);
			$type = $_FILES['imagefile']['type'];
			$size = (int)$_FILES['imagefile']['size'];

			$up = array(
				"imagedata" => $content,
				"imagetype" => $type,
				"imagesize" => $size,
				"imagealt" => $imagealt,
				"imagealign" => (string)ac_http_param("imagealign"),
			);

			if ($up["imagedata"] == "") {
				unset($up["imagedata"]);
				unset($up["imagetype"]);
				unset($up["imagesize"]);
			}

			if (isset($up["imagedata"]) && function_exists('getimagesize')) {
				$rval = @getimagesize($_FILES['imagefile']['tmp_name']);
				$up["imagewidth"] = $rval[0];
				$up["imageheight"] = $rval[1];
			}

			ac_sql_update("#form_part", $up, "id = '$partid'");
			ac_http_redirect("main.php?action=form_edit&id=$formid");
		}
	}
}

?>
