<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");

class form_settings_context extends ACP_Page {
	function form_settings_context() {
		$this->pageTitle = _a("Form Settings");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);

		if (!ac_http_param("id")) {
			ac_http_redirect("main.php?action=form#list-01-0-0");
		}

		$id = (int)ac_http_param("id");
		$form = form_select_row($id);

		if (!$form)
			ac_http_redirect("main.php?action=form#list-01-0-0");

		$admin = ac_admin_get();
		$liststr = implode("','", $admin["lists"]);

		$lists = ac_sql_select_array("
			SELECT
				l.id,
				l.name,
				(SELECT COUNT(*) FROM #form_list f WHERE f.listid = l.id AND f.formid = '$id') > 0 AS `selected`,
				(SELECT COUNT(*) FROM #form_list f WHERE f.listid = l.id AND f.formid = '$id' AND f.useconf > 0) > 0 AS `useconf`
			FROM
				#list l
			WHERE
				l.id IN ('$liststr')
			ORDER BY
				l.name
		");

		if (count($lists) == 0) {
			// Quick sanity check, but -- it would be weird to get here.
			$smarty->assign('side_content_template', '');
			$smarty->assign('content_template', 'nolists.htm');
			return;
		}
				
		$checked = array_reduce($lists, create_function('$a, $b', 'return $a + $b["selected"];'), 0);
		$name = $lists[0]["name"];

		$smarty->assign("form", $form);
		$smarty->assign("lists_checked", $checked);
		$smarty->assign("lists_name", $name);
		$smarty->assign("lists", $lists);
		$smarty->assign("content_template", "form_settings.htm");

		ac_smarty_submitted($smarty, $this);
	}

	function formProcess(&$smarty) {

		$id = (int)ac_http_param("id");
		form_settings_save();

		switch (ac_http_param("next")) {
			default:
			case "list":
				ac_http_redirect("main.php?action=form#list-01-0-0");
				exit;

			case "settings":
				ac_http_redirect("main.php?action=form_settings&id=$id");

			case "edit":
				ac_http_redirect("main.php?action=form_edit&id=$id");
				exit;

			case "theme":
				ac_http_redirect("main.php?action=form_edit&id=$id&theme=1");
				exit;

			case "optin":
				ac_http_redirect("main.php?action=form_optin&id=$id");
				exit;

			case "code":
				ac_http_redirect("main.php?action=form_code&id=$id");
				exit;
		}
	}
}

?>
