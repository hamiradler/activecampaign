<?php

require_once ac_global_classes("select.php");

class form_facebook_context extends ACP_Page {

	function form_facebook_context() {
		$this->pageTitle = _a("Form Facebook");
		$this->sideTemplate = "side.form.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$admin = $GLOBALS["admin"];



		$pconf = new Config("public");
		$smarty->assign("pconf", $pconf->export());

		$smarty->assign("content_template", "form_facebook.htm");
	}
}

?>
