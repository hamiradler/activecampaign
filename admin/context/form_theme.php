<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");

class form_theme_context extends ACP_Page {
	function form_theme_context() {
		$this->pageTitle = _a("Edit Form");
		//$this->sideTemplate = "side.message.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("ac_load_editor", "1");

		if (!ac_http_param("id")) {
			ac_http_redirect("main.php?action=form#list-01-0-0");
		}

		$id = (int)ac_http_param("id");
		$form = form_select_row($id);

		if (!$form)
			ac_http_redirect("main.php?action=form#list-01-0-0");

		$smarty->assign("form", $form);
		$smarty->assign("content_template", "form_theme.htm");

		ac_smarty_submitted($smarty, $this);
	}

	function formProcess(&$smarty) {
	}
}

?>
