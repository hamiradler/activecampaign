<?PHP

/*
 *
 */
require_once(ac_global_classes('page.php'));

class about_context extends ACP_Page {

	var $thisVersion = '1.0';
	var $thisBuild = 1;

	// constructor
	function about_context() {
		require(ac_admin('functions/versioning.php'));
		$this->thisVersion  = $thisVersion;
		$this->thisBuild    = $thisBuild;
		$this->pageTitle    = sprintf(_a("About %s"), $GLOBALS['ac_app_name']);
		$this->sideTemplate = $GLOBALS['ac_sidemenu_settings'];
		parent::ACP_Page();
	}



	function process(&$smarty) {
 		$this->setTemplateData($smarty);
		// check for privileges first!
		if ( !ac_admin_ismaingroup() || isset($GLOBALS["_hosted_account"]) ) {
			// assign template
			ac_smarty_noaccess($smarty, $this);
			return;
		}

		if (ac_ihook_exists("ac_about_context_pre"))
			$smarty = ac_ihook("ac_about_context_pre", $smarty);

		$fetched = false;
		if (!isset($GLOBALS["_hosted_account"])) {
			$md5s = md5($this->site['serial']);
			$latest = (string)ac_http_get("http://www.activatelicense.com/update_checker.php?check_val=$GLOBALS[ac_app_id]&s=$md5s");
			if ( $latest != '' ) {
				$fetched = true;
				ac_sql_update_one('#backend', 'updateversion', $latest);
				$GLOBALS['site']['updateversion'] = $latest;
			}
			$shouldUpdate = ( ( $latest != 0 and $latest != '' ) ? version_compare($GLOBALS['site']['version'], $latest, '<' ) : false );
			$smarty->assign('latest', $latest);
			$smarty->assign('fetched', $fetched);
			$smarty->assign('shouldUpdate', $shouldUpdate);
		}

		$smarty->assign('appID', $GLOBALS['ac_app_id']);
		$smarty->assign('encoding', ac_php_encoding());
		$smarty->assign('hash', md5($this->site['serial']));

		$smarty->assign('build', $this->thisBuild);

		// assign template
		$smarty->assign('content_template', 'about.htm');
	}


}

?>
