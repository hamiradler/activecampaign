<?php

require_once(ac_global_classes('page.php'));

class login_oauth_context extends ACP_Page {

	var $thisVersion = '1.0';
	var $thisBuild = 1;

	// constructor
	function login_oauth_context() {
		$this->pageTitle = _a('Log-in with OAuth');
		$this->sideTemplate = "";
		$this->ACP_Page();
	}

	function process(&$smarty) {
 		$this->setTemplateData($smarty);

 		$site = ac_site_get();

		$smarty->assign("usemainmenu", 0);

		$service = ac_http_param("service");

		$smarty->assign("close_window", false);

		switch ($service) {
			case "twitter" :
				require_once ac_admin("functions/list.php");

			break;

			case "surveymonkey" :

				require_once ac_admin("functions/service.php");

				$access_token = ac_http_param("access_token");

				if ($access_token) {
					if (ac_ihook_exists("ac_service_oauth_token")) {
						ac_ihook("ac_service_oauth_token", $service, "service", $access_token);
					}
					$smarty->assign("close_window", true);
				}

			break;
		}

		// assign template
		$smarty->assign("content_template", "login_oauth.htm");
	}
}

?>
