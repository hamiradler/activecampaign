<?php

require_once ac_admin("functions/approval.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class approval_context extends ACP_Page {

	function approval_context() {
		$this->pageTitle = _a("Campaign Approval Queue");
		$this->sideTemplate = "";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "approval.htm");

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(approval_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=approval');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'approval.approval_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "sdate", "label" => _a("Submit Date")),
		);
		$smarty->assign("search_sections", $sections);

	}
}

?>
