<?php

require_once ac_admin("functions/form.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class form_context extends ACP_Page {

	function form_context() {
		$this->pageTitle = _a("Subscription Forms");
		$this->sideTemplate = "side.form.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "form.htm");

		if (!$this->admin["pg_form_edit"]) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		$listid = (int)ac_http_param("listid");
		$smarty->assign("listid", $listid);

		$delete = (int)ac_http_param("delete");

		if ($delete) {
			form_delete($delete);
			ac_http_redirect("main.php?action=form");
		}

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(form_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=form');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'form.form_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$admin = ac_admin_get();
		$liststr = implode("','", $admin["lists"]);
		$lists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$liststr') ORDER BY name");
		$smarty->assign("lists", $lists);

		// list ID filter
		if ( in_array($listid, $admin["lists"]) ) $liststr = $listid;

		$forms_query = "
			SELECT
				f.id,
				f.name,
				f.waitpreview,
				(SELECT COUNT(*) FROM #subscriber_list s WHERE s.formid = f.id) AS subcount
			FROM
				#form f
			WHERE
				f.target = '1'	-- FORM_SUBSCRIBE
		";

		if ($liststr) {
			$forms_query .= "
				AND
					(SELECT COUNT(*) FROM #form_list L WHERE L.formid = f.id AND L.listid IN ('$liststr')) > 0
			";
		}

		$forms = ac_sql_select_array($forms_query);

		foreach ($forms as $k => $form) {
			if ($form["waitpreview"]) {
				em_request_screenshot("form", $form["id"]);
			}

			$forms[$k]["formurl"] = rewrite_plink("form", "id=$form[id]");
			$forms[$k]["url"] = Screenshot::geturl("form", $form["id"]);
		}

		$smarty->assign("forms", $forms);

		$pconf = new Config("public");
		$smarty->assign("pconf", $pconf->export());

		$sections = array(
			array("col" => "name", "label" => _a("Name")),
		);
		$smarty->assign("search_sections", $sections);

	}
}

?>
