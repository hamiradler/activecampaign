<?php

require_once ac_admin("functions/template.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class template_context extends ACP_Page {

	function template_context() {
		$this->pageTitle = _a("Templates");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);
		$smarty->assign("ac_load_editor", "1");

		$admin = ac_admin_get();
		$site = ac_site_get();

		if (!$this->admin["pg_template_add"] && !$this->admin["pg_template_edit"] && !$this->admin["pg_template_delete"]) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		if ( list_get_cnt() == 0 ) {
			$smarty->assign('content_template', 'nolists.htm');
			return;
		}

		$smarty->assign("side_content_template", "side.campaign.htm");
		$smarty->assign("content_template", "template.htm");

		campaign_sidemenu($smarty, 'template');

		$so = new AC_Select;

		// list filter
		$filterArray = template_filter_post();
		$filter = $filterArray['filterid'];
		if ($filter > 0) {
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$this->admin['id']}' AND sectionid = 'template'");
			$so->push($conds);
		}
		$smarty->assign("filterid", $filter);
		$smarty->assign("listfilter", ( isset($_SESSION['nla']) ? $_SESSION['nla'] : null ));

		// get count
		//$so->count();
		//$total = (int)ac_sql_select_one(template_select_query($so, 0));
		// Using template_select_query() strips out the JOIN stuff, but still passes "WHERE l.listid = ...", so total is always 0
		$total = (int)ac_sql_num_rows(ac_sql_query("SELECT COUNT(*) as count FROM #template t INNER JOIN #template_list l ON t.id = l.templateid WHERE l.listid != 0 GROUP BY l.templateid"));
		$count = $total;

		# Lists
		$listsarr = $admin['lists'];
		if ( ac_admin_ismain() ) $listsarr[0] = 0;
		$liststr = implode("','", $listsarr);
		//if (!$liststr)
			//ac_http_redirect("main.php?action=list&new");
		$lists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$liststr') ORDER BY name");
		$smarty->assign("lists_count", count($lists));
		$smarty->assign("lists", $lists);

		# tags
		$templatelist = ac_sql_select_list("SELECT templateid FROM #template_list WHERE listid IN ('$liststr')");
		$templatestr = implode("','", $templatelist);
		$tags = ac_sql_select_array("
			SELECT
				t.id,
				t.tag,
				(SELECT COUNT(*) FROM #template_tag r WHERE r.tagid = t.id AND r.templateid IN ('$templatestr')) AS `count`
			FROM
				#tag t
			ORDER BY
				t.tag
		");
		$smarty->assign("tags", $tags);

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=template');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'template.template_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "name", "label" => _a("Template Name")),
			array("col" => "content", "label" => _a("Content")),
		);
		$smarty->assign("search_sections", $sections);

		// clear out any temporary template preview or import files in cache folder
		$cache_clear = template_cache_clear();

		$smarty->assign("templates_context", "template");
		$smarty->assign("templates_js_prefix", "");

		$fields = list_get_fields(array(), true); // no list id's, but global
		$smarty->assign("fields", $fields);
	}
}

?>
