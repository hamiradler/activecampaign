<?php

require_once ac_admin("functions/exclusion.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class exclusion_context extends ACP_Page {

	function exclusion_context() {
		$this->pageTitle = _a("Exclusion List");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$admin = $GLOBALS["admin"];

		if (!$admin["pg_list_edit"]) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		$this->setTemplateData($smarty);

		if ( !$this->admin['pg_subscriber_delete'] ) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		if ( list_get_cnt() == 0 ) {
			$smarty->assign('content_template', 'nolists.htm');
			return;
		}

		$smarty->assign("content_template", "exclusion.htm");
		$smarty->assign("side_content_template", "side.subscriber.htm");

		$so = new AC_Select;

		$so->push("AND hidden = 0");

		// list filter
		if ( isset($_GET['listid']) && (int)$_GET['listid'] ) $_POST['listid'] = (int)$_GET['listid'];
		$filterArray = exclusion_filter_post();
		$filter = $filterArray['filterid'];
		if ($filter > 0) {
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$this->admin['id']}' AND sectionid = 'exclusion'");
			$so->push($conds);
		}
		$smarty->assign("filterid", $filter);
		$smarty->assign("listfilter", ( isset($_SESSION['nla']) ? $_SESSION['nla'] : null ));

		// get count
		$so->count();
		$total = (int)ac_sql_select_one(exclusion_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=exclusion');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'exclusion.exclusion_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "email", "label" => _a("Email Matching Pattern")),
		);
		$smarty->assign("search_sections", $sections);
	}
}

?>
