$J(document).ready(function() {

	$J(document).click(function(event) {
		if (!$J(event.target).closest('.segments').length) {
			$J('.segments').hide();
		};
	});
	$J(document).mouseup(function(e) {
		if(!$J(e.target).hasClass("dropdown") && $J(e.target).parents("a.dropdown").length==0) {
			$J(".dropdown").removeClass("opendropdown");
			$J(".drop").hide();
		}
	});

	/*
	$J(".drop").mouseup(function() {
		return false;
	});
	*/

	// enable twipsy for links
	$J("a[rel=tip]").twipsy({});
	if ( $J("#list_list").length > 0 ) {
		// fetch lists
		paginators[1].paginate(0);
	}
	// populate the sender address fields in add list modal
	list_form_sender_populate($J('#formModal select[name=addressid]').val());
	// if they hit enter key in search box, run the filter
	$J('.filterSearch input[type=text]').keypress(function(event) {
		if ( event.which == 13 ) {
			list_list_search();
			event.preventDefault();
		}
	});

	if ( newform ) {
		list_form_open(0);
	}
	if ( settingsid > 0 ) {
		list_settings_open(settingsid);
		// ...
	}

});


function list_list_header() {
	var count = ac.str.number_format(paginators[1].total, 0, decimalDelim, commaDelim);
	var filter = "";
	if ( list_filter_id ) {
		filter = list_str_filter_search;
	}
	if ( filter ) filter = ' - ' + filter;
	var str_header = filter + " (" + count + ")";
	return str_header;
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function list_list_paginate(offset) {
	$J("#list_list").hide();
	$J("#list_list_count").hide();
	$J("#list_paginator").hide();
	$J("#list_noresults").hide();
	$J("#list_loader").show();

	//ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, '', offset, this.limit, list_filter_id);
	ac.get(this.ajaxAction, [ this.id, '01', offset, this.limit, list_filter_id ], paginateCBary);
}

// This function should not be called directly; it is called only by paginator's callback
function list_list_tabelize(rows, offset) {
	// hide the loader, we're done loading
	$J("#list_loader").hide();
	// set new count in title (h2)
	$J("#list_list_count").html(list_list_header()).show();
	// if nothing found
	if (rows.length < 1) {
		/*
		if (!list_filter_id || list_filter_id == 0) {
			ac_ui_api_callback();
			window.location = 'main.php?action=list_new';
			return;
		}
		*/
		// We may have some rows left if we just deleted the last row.
		$J("#list_list").hide().empty();
		$J("#list_noresults").show();
		$J("#list_paginator").hide();
		return;
	}

	$J("#list_noresults").hide();
	$J("#list_paginator").show();

	// remove old rows
	$J("#list_list").show().empty();

	// append new rows
	for ( var i = 0; i < rows.length; i++ ) {
		$J("#list_list").append(list_row(rows[i]));
		//list_charts_draw(rows[i]);
	}

	$J("a.seglist").click(function(){
		$J(".segments", this.parentNode.parentNode).toggle();
		return false;
	});

	$J('.graphcontent > div').hide();
	$J('.llft ul.graphnav a').click(function () {
		var box = $J(this).parents('div.listrow');
		$J('.graphcontent > div', box).hide().filter(this.hash).show();
		$J('.llft ul.graphnav a', box).removeClass('selected');
		$J(this).addClass('selected');
		var m = this.hash.split('-');
		var t = m[0];
		var lid = m[1];
		//alert($J('.graphcontent > div', box).attr('id'));
		if ( t == '#subs' ) {
			list_chart_draw('subscriber_rate', 'chart_subscriptions', lid);
		} else if ( t == '#intc' ) {
			list_chart_draw('interaction_rate', 'chart_interaction', lid);
		} else if ( t == '#unsubs' ) {
			list_chart_draw('unsubscribed_rate', 'chart_unsubscriptions', lid);
		}
		return false;
	//}).filter(':first').click();
	});
	$J('.llft ul.graphnav').each(function(i) { $J("a:first", $J(this)).click(); });

	$J('div.listrow:last').addClass('lastchild');

	// apply new
	$J("a[rel=tip]").twipsy({});
}


function list_row(row) {
	var str_cnt_subscribers = ac.str.number_format(row.subscribers, 0, decimalDelim, commaDelim);
	var str_cnt_segments = ac.str.number_format(row.segments.length, 0, decimalDelim, commaDelim);
	var str_segments = list_str_cnt_segments_none;
	if ( row.segments.length > 0 ) str_segments = sprintf(list_str_cnt_segments, str_cnt_segments);

	var r  = '';
	r += '	<div id="listrow_' + row.id + '" class="listrow">';
	r += '		<div class="llft">';
	r += '			<div class="graphcontent">';
	r += '				<div id="subs-' + row.id + '" class="set">';
	r += '					<div id="chart_subscriptions_blank_' + row.id + '" class="chartnodataoverlay" style="display:none;">' + list_str_bar_nodata + '</div>';
	r += '					<div id="chart_subscriptions_' + row.id + '" class="graph smallchart"></div>';
	r += '					' + list_str_bar_subs + '';
	r += '				</div>';
	r += '				<div id="intc-' + row.id + '" class="set">';
	r += '					<div id="chart_interaction_blank_' + row.id + '" class="chartnodataoverlay" style="display:none;">' + list_str_bar_nodata + '</div>';
	r += '					<div id="chart_interaction_' + row.id + '" class="graph smallchart"></div>';
	r += '					' + list_str_bar_interaction + '';
	r += '				</div>';
	r += '				<div id="unsubs-' + row.id + '" class="set">';
	r += '					<div id="chart_unsubscriptions_blank_' + row.id + '" class="chartnodataoverlay" style="display:none;">' + list_str_bar_nodata + '</div>';
	r += '					<div id="chart_unsubscriptions_' + row.id + '" class="graph smallchart"></div>';
	r += '					' + list_str_bar_unsubs + '';
	r += '				</div>';
	r += '			</div>';
	r += '			<ul class="graphnav">';
	r += '				<li><a href="#subs-' + row.id + '" data-placement="above" rel="tip" data-original-title="' + list_str_bar_subs + '">-</a></li>';
	r += '				<li><a href="#intc-' + row.id + '" data-placement="above" rel="tip" data-original-title="' + list_str_bar_interaction + '">-</a></li>';
	r += '				<li><a href="#unsubs-' + row.id + '" data-placement="above" rel="tip" data-original-title="' + list_str_bar_unsubs + '">-</a></li>';
	r += '			</ul>';
	r += '		</div><!--//llft-->';
	r += '		<div class="lrgt">';
	r += '			<div class="segmentlists">';
	r += '				<a class="seglist" href="#">' + str_segments + '</a>';
	r += '			</div><!--//segmentlists-->';
	r += '			<h2>' + row.name + '<span class="subcount">(<a href="main.php?action=subscriber&listid=' + row.id + '">' + str_cnt_subscribers + '</a>)</span></h2>';
	r += '			<div class="segments" style="display:none;">';
	for ( var i = 0; i < row.segments.length; i++ ) {
		var segment = row.segments[i];
	r += '				<a class="segment" href="main.php?action=subscriber&filterid=' + segment.id + '&listid=' + row.id + '">' + segment.name + '</a>';
	}
	r += '				<a class="segment addsegment" href="main.php?action=filter&listid=' + row.id + '#form-0"><span>' + list_str_cnt_segments_new + '</span></a>';
	r += '			</div>';
	r += '			<div class="avatars">';

	var avstream = stream_build(row.avatars, 'n');
	var href = ac_js_admin.pg_subscriber_import ? "main.php?action=subscriber_import&listid=" + row.id : "javascript:void(0)";

	if (avstream == "") {
		for (var i = 0; i < 15; i++) {
			avstream += sprintf('<a href="%s"><img src="' + ac_js_site.p_link + '/admin/images/gravatar_default_grey_25.png" height="25"></a>', href);
		}
	}

	r += '			</div><!--//avatars-->';
	r += '			<div class="options">';
	r += '				<ul class="optionbuttons">';
	if ( ac_js_admin.pg_subscriber_import ) {
	r += '					<li><a class="btnsmall" href="main.php?action=subscriber_import&listid=' + row.id + '">' + list_str_opt_import + '</a></li>';
	}
	if ( ac_js_admin.pg_subscriber_fields ) {
	r += '					<li><a class="btnsmall" href="main.php?action=field&listid=' + row.id + '">' + list_str_opt_field + '</a></li>';
	}
	if ( ac_js_admin.pg_subscriber_actions ) {
	r += '					<li><a class="btnsmall" href="main.php?action=subscriber_action&listid=' + row.id + '">' + list_str_opt_auto + '</a></li>';
	}
	if ( ac_js_admin.pg_form_edit ) {
	r += '					<li><a class="btnsmall" href="main.php?action=form&listid=' + row.id + '">' + list_str_opt_form + '</a></li>';
	}

	if ( ac_js_admin.pg_list_edit || pg_subscriber_delete.pg_form_edit || ac_js_admin.pg_list_headers || ac_js_admin.pg_list_emailaccount || ac_js_admin.pg_subscriber_sync || ac_js_admin.pg_list_delete || canAddList ) {
	r += '					<li><a class="btnsmall dropdown" href="#" onclick="$J(\'.drop\', this.parentNode).toggle();$J(\'.dropdown\', this.parentNode).toggleClass(\'opendropdown\');return false;"><span>' + jsOptions + '</span></a>';
	r += '						<ul class="drop">';
	if ( ac_js_admin.pg_list_edit ) {
	r += '							<li><a href="#" onclick="list_form_open(' + row.id + ');return false;">' + list_str_opt_editlist + '</a></li>';
	}
	if ( ac_js_admin.pg_subscriber_delete ) {
	r += '							<li><a href="main.php?action=exclusion&listid=' + row.id + '">' + list_str_opt_exclusion + '</a></li>';
	}
	if ( ac_js_admin.pg_list_headers ) {
	r += '							<li><a href="main.php?action=header&listid=' + row.id + '">' + list_str_opt_header + '</a></li>';
	}
	if ( ac_js_admin.pg_list_emailaccount ) {
	r += '							<li><a href="main.php?action=emailaccount&listid=' + row.id + '">' + list_str_opt_emailaccount + '</a></li>';
	}
	if ( ac_js_admin.pg_subscriber_sync ) {
	r += '							<li><a href="main.php?action=sync&listid=' + row.id + '">' + list_str_opt_sync + '</a></li>';
	}
	if ( canAddList ) {
	r += '							<li><a href="#" onclick="list_copy_open(' + row.id + ');return false;">' + list_str_opt_copy + '</a></li>';
	}
	if ( ac_js_admin.pg_list_edit ) {
	r += '							<li><a href="#" onclick="list_settings_open(' + row.id + ');return false;">' + list_str_opt_settings + '</a></li>';
	}
	if ( ac_js_admin.pg_list_delete ) {
	r += '							<li><a href="#" onclick="list_delete_open(' + row.id + ');return false;">' + jsDelete + '</a></li>';
	}
	r += '						</ul>';
	r += '					</li>';
	}
	r += '				</ul>';
	r += '			</div><!--//options-->';
	r += '		</div><!--//lrgt-->';
	r += '	</div><!--//listrow-->';
	return r;
}



function list_chart_draw(graphaction, divname, listid) {

	var divId = divname + '_' + listid;
	var blankId = '#' + divname + '_blank_' + listid;

	// small chart properties
	var smallchart = chart_small();
	smallchart.height = 74;
	smallchart.width = 150;

	// subscriber rate
	ac.graph(graphaction, { listid : listid }, function(data) {
		var rows = [];
		var max = 0;
		$J(data.series).each(function(i) {
			rows.push([this, data.graph[i]]);
			//alert(this + ' => ' + data.graph[i]);
			var val = parseInt(data.graph[i], 10);
			if ( val > max ) max = val;
		});
		$J('#' + divname + '_blank_' + listid).toggle(!!data.extras.empty);
		smallchart.colors[0] = data.extras.empty ? '#d9eff8' : '#52aed8';
		smallchart.vAxis.baselineColor = data.extras.empty ? '#cbe7f3' : '#cbe7f3';
		smallchart.max = max * 1.3;
		chart_draw(divId, rows, smallchart);
	});
}

function list_charts_draw(row) {
	list_chart_draw('subscriber_rate', 'chart_subscriptions', row.id);
	list_chart_draw('interaction_rate', 'chart_interaction', row.id);
	list_chart_draw('unsubscribed_rate', 'chart_unsubscriptions', row.id);
}


function list_list_clear() {
	/* qsearch : */ $J('.filterSearch input[type=text]').val('');
}

function list_list_search() {
	var post = {
		qsearch : $J('.filterSearch input[type=text]').val()
	};
	// hit up filterer
	ac.post('list.list_filter_post', post, list_list_search_cb);
}

function list_list_search_cb(ary) {
	list_filter_id = ary.filterid;
	paginators[1].paginate(0);
}


function list_delete_open(id) {
	// save the id somewhere
	list_delete_id = id;
	$J('#delete1').prop('checked', false);
	$J('#delete2').prop('checked', false);
	$J('#delete3').prop('checked', false);
	$J('#delete4').prop('checked', false);
	$J('#delete5').prop('checked', false);

	// open modal
	$J('#deleteModal').modal('show');
}

function list_delete(id) {

	$confirm1 = $J('#delete1').is(':checked');
	$confirm2 = $J('#delete2').is(':checked');
	$confirm3 = $J('#delete3').is(':checked');
	$confirm4 = $J('#delete4').is(':checked');
	$confirm5 = $J('#delete5').is(':checked');

	if(!$confirm1 || !$confirm2 || !$confirm3 || !$confirm4 || !$confirm5){
	alert(list_form_str_deleteerror);
	return false;
	}

	ac_ui_api_call(jsDeleting);
	ac.get(
		"list.list_delete",
		[ id ],
		function (data) {
			list_list_rebuild_callback(data);
			$J('#deleteModal').modal('hide');
		}
	);
}


function list_form_reset() {
	$J('#formModal input[name=name]').val('');
	$J('#formModal select[name=addressid]').selectedIndex = 0;
	list_form_sender_populate($J('#formModal select[name=addressid]').val());
	ac.get("list.list_addresses", [], list_sender_get_addresses_cb);
	$J('#formModal div.modal-header h3').html(list_form_str_title_add);
	$J('#formModal div.modal-footer a.btn.edit').html(list_form_str_button_add);
}

function list_form_open(id) {
	// save the id somewhere
	list_edit_id = id;
	if ( id == 0 ) {
		list_form_reset();
		$J('#formModal').modal('show');
	} else {
		ac.row('list', id, function(ary) {
			// populate name
			$J("#formModal input[name=name]").val(ary.name);
			// populate address
			$J("#formModal input[name=addressid]").val(ary.id);
			// sender info
			$J('#sendernameField').val(ary.sender_name);
			$J('#senderaddr1Field').val(ary.sender_addr1);
			$J('#senderaddr2Field').val(ary.sender_addr2);
			$J('#sendercityField').val(ary.sender_city);
			$J('#senderstateField').val(ary.sender_state);
			$J('#senderzipField').val(ary.sender_zip);
			$J('#sendercountryField').val(ary.sender_country);
			$J('#senderphoneField').val(ary.sender_phone);
			ac.get("list.list_addresses", [], list_sender_get_addresses_cb);
			// titles and buttons
			$J('#formModal div.modal-header h3').html(list_form_str_title_edit);
			$J('#formModal div.modal-footer a.edit').html(list_form_str_button_edit);
			// open modal
			$J('#formModal').modal('show');
		});
	}
	return false;
}

function list_save(id) {
	ac_ui_api_call(jsAdding);
	var post = ac.prepare('#formModal_form');
	post.id = id;
	ac.post(
		id > 0 ? "list.list_update_web" : "list.list_insert_web",
		post,
		function (data) {
			if ( id == 0 && data.succeeded ) {
				if ( $J("#list_list").length == 0 ) {
					// fetch lists
					window.location = 'main.php?action=list';
					return;
				}
			}
			list_list_rebuild_callback(data);
			if ( data.succeeded ) {
				list_form_reset();
				$J('#formModal').modal('hide');
			}
		}
	);
}


function list_settings_open(id) {
	ac_ui_api_call(jsLoading);
	$J("#twitter_confirmed").hide();
	ac.row('list', id, function(ary) {
		list_edit_id = id;
		$J("#settingsModal_listname").html(": " + ary.name);
		//$('usetrackingPField').checked = ary.p_use_tracking == 1;
		$J('#analyticsreadPField').attr("checked", (ary.p_use_analytics_read == 1));
		$J('#analyticsuaField').val(ary.analytics_ua);
		$J('#analyticslinkPField').attr("checked", (ary.p_use_analytics_link == 1));
		$J('#analyticssourceField').val( (ary.analytics_source) ? ary.analytics_source : ary.name );
		$J('#analyticssourceField').val( (ary.analytics_source) ? ary.analytics_source : ary.name );
		// filling analytics domains here
		if ( ary.analytics_domains ) {
			var domains = ary.analytics_domains.split('\n');
			for ( var i = 0; i < domains.length; i++ ) {
				if ( i > 0 ) clone_1st_div($('analyticsClonerDiv'));// first one is already there
				var alldivs = $J('#analyticsClonerDiv div');
				if ( alldivs[i] ) {
					$J("input:first", alldivs[i]).val(domains[i]);
				}
			}
		}

		$J('#twitterPField').attr("checked", ary.p_use_twitter == 1);
		$J('#twitter').show();
		$J('#twitter_checkbox').show();
		if (ary.twitter_token && ary.twitter_token_secret) {
			ac_ui_api_call(list_form_str_twitter5 + "...");
			ac.get("list.list_twitter_oauth_verifycredentials", [ary.twitter_token, ary.twitter_token_secret], list_form_external_twitter_verifycredentials_cb);
		}
		else {
			if (!ac_js_site.twitter_consumer_key || !ac_js_site.twitter_consumer_secret) {
				$J('#twitter').hide();
				$J('#twitter_checkbox').hide();
			}
			else {
				ac.get("list.list_twitter_oauth_init2", [id], list_twitter_oauth_init2_cb);
			}
		}

		$J('#facebookPField').attr("checked", (ary.p_use_facebook == 1 || ary.facebook_oauth_me));
		$J('#facebook').hide();
		if (ary.p_use_facebook == 1) $J('#facebook').show();
		$J('#facebook_confirmed').hide();
		$J('#facebook_unconfirmed').show();
		$J('#facebook_invalid').hide();
		if (ary.facebook_oauth_me) {
			// session valid for facebook
			$J('#facebook').show();
			$J('#facebook_confirmed').show();
			$J('#facebook_unconfirmed').hide();
			$J('#facebook_account_profile').attr("href", ary.facebook_oauth_me.link);
			$J('#facebook_accounts_toupdate_me').val(ary.facebook_oauth_me.id);
			$J('#facebook_account_profile').html(ary.facebook_oauth_me.name);
			$J('#facebook_account_logout_url').attr("href", ary.facebook_oauth_logout_url);
			$J('#facebook_accounts').show();
			$J('#facebook_accounts_available').empty();
			var pages_count = 0;
			// loop through ALL available accounts for this user, and append to DOM
			if (ary.facebook_oauth_me["accounts"]) {
				for (var i in ary.facebook_oauth_me["accounts"]["data"]) {
					var current_item = ary.facebook_oauth_me["accounts"]["data"][i];
					if (typeof current_item.category != 'undefined') {
						pages_count++;
						var checkbox = "<input type='checkbox' name='facebook_accounts_toupdate[]' id='facebook_accounts_toupdate_" + current_item.id + "' value='" + current_item.id + "' class='facebook_accounts_item' onchange='list_form_facebook_toupdate_toggle(this.checked);' />";
						var account_link = "<label for='facebook_accounts_toupdate_" + current_item.id + "' style='display: inline;'>" + current_item.name + "</label>";
						var account_type = " (" + current_item.category + ")";
						var item_ = "<div>" + checkbox + " " + account_link + " " + account_type + "</div>";
						$J('#facebook_accounts_available').append(item_);
					}
				}
			}

			if (!pages_count) {
				// accounts/data is empty, which can happen in the following ways:
				// 1. user has no pages to manage on Facebook.
				// 2. user allowed our AC Facebook app without the manage_pages permission (at one point we weren't asking for it).
				// 3. user once had permission to update certain pages, but those permissions were revoked on Facebook's end.
				// hide the accounts section (besides "me")
				$J('#facebook_accounts').hide();
			}

			$J('#facebook_accounts_toupdate_me').attr("checked", false);

			// loop through chosen/saved accounts and auto-check
			if (typeof(ary.facebook_oauth_me["accounts_toupdate"]) != "undefined" && ary.facebook_oauth_me["accounts_toupdate"].length > 0) {
				if (!pages_count) {
					// if there are previously saved Facebook accounts (in EM DB), but none were written to DOM (accounts/data is empty),
					// show message for them to disconnect, and re-connect
					$J("#facebook_accounts_none_error").show();
				}
				else {
					for (var i = 0; i < ary.facebook_oauth_me["accounts_toupdate"].length; i++) {
						var current_item = ary.facebook_oauth_me["accounts_toupdate"][i];
						if ($J('#facebook_accounts_toupdate_' + current_item).length == 0) {
							// if the DOM element cannot be found, it is likely the "me" checkbox (which has ID: "facebook_accounts_toupdate_me", but value of whatever the ID is, such as 12124332131232)
							// this ID will always be in the DOM
							$J('#facebook_accounts_toupdate_me').attr("checked", true);
						}
						else {
							// all other Facebook accounts should have a DOM ID that matches their actual account ID, IE: "facebook_accounts_toupdate_12124332131232"
							$J('#facebook_accounts_toupdate_' + current_item).attr("checked", true);
						}
					}
				}
			}
			else if (typeof(ary.facebook_oauth_me["accounts_toupdate"]) != "undefined" && ary.facebook_oauth_me["accounts_toupdate"].length == 0) {
				// usually when coming back to EM after signing-in on Facebook to confirm
				$J('#facebook_accounts_toupdate_me').attr("checked", true);
			}
		}
		else if (!ary.facebook_oauth_login_url && ary.facebook_oauth_logout_url) {
			// sometimes the login URL is not set (for example, if a user changes their Facebook password after connecting their account in EM)
			$J('#facebook_confirmed').show();
			$J('#facebook_unconfirmed').hide();
			$J('#facebook_account_logout_url').attr("href", ary.facebook_oauth_logout_url);
		}
		else {
			// session NOT valid for facebook - they need to log in
			$J('#facebook_account_login_url').attr("href", ary.facebook_oauth_login_url);
		}

		//$('embedimgPField').checked = ary.p_embed_image == 1;
		$J('#carboncopyField').val(ary.carboncopy);
		$J('#lastbroadcastPField').attr("checked", (ary.send_last_broadcast == 1));
		$J('#subscriptnotifyField').val(ary.subscription_notify);
		$J('#unsubscriptnotifyField').val(ary.unsubscription_notify);
		$J('#useridField').val(ary.luserid);

		// set the stage
		//$('main_tab_external').style.display = ( ary.p_use_tracking == 1 ? 'inline' : 'none' );
		$J('#analyticsread').attr("class", (ary.p_use_analytics_read == 1 ? 'ac_block' : 'ac_hidden'));
		$J('#analyticslink').attr("class", (ary.p_use_analytics_link == 1 ? 'ac_block' : 'ac_hidden'));
		if (ary.p_use_twitter == 1) {
			$J('#twitter').show();
		}
		else {
			$J('#twitter').hide();
		}

		//$('list_title_add').hide();
		//$('list_title_edit').show();

		// sender preview: use the text from the textboxes if anything is there. if not, use the default value
		for (var i = 0; i < sender_fields_actual.length; i++) {
			if ( $(sender_fields_actual[i]).value != "" ) {
				var new_value = $(sender_fields_actual[i]).value;
			}
			else {
				var new_value = sender_fields_default[i];
			}
			list_form_sender_livepreview(new_value, sender_fields_preview[i], 1);
		}

		// open modal
		$J('#settingsModal').modal('show');
	});
	return false;
}

function list_settings_save(id) {
	var post = ac.prepare('#settingsModal_form');
	post.id = id;

	if (typeof(post.p_use_analytics_read) != "undefined") {
		// Analytics Read Tracking: account number text field
		if (post.analytics_ua == "" || !post.analytics_ua.match(/^UA-\d+-\d+$/i)) {
			alert(list_form_str_analytics1);
			$("analyticsuaField").focus();
			return;
		}
	}
	if (typeof(post.p_use_analytics_link) != "undefined") {
		// Analytics Link Tracking: source name text field
		if (post.analytics_source == "") {
			alert(list_form_str_analytics2);
			$("analyticssourceField").focus();
			return;
		}
	}

	var facebook_checkboxes = $J(".facebook_accounts_item");
	var facebook_accounts_toupdate = [];
	for (var i = 0; i < facebook_checkboxes.length; i++) {
		var c = facebook_checkboxes[i];
		if (c.checked) {
			facebook_accounts_toupdate.push(c.value);
		}
	}
	// make sure at least one Facebook account is checked
	if (facebook_accounts_toupdate.length == 0) {
		alert(list_form_str_facebook1);
		return;
	}

	ac_ui_api_call(jsUpdating);

	ac.post(
		"list.list_settings_web",
		post,
		function (data) {
			list_list_rebuild_callback(data);
			$J('#settingsModal').modal('hide');
		}
	);
}


function list_copy_open(id) {
	list_copy_id = id;
	$J('#copyModal').modal('show');
	$J('#list_use_copy').attr('checked','checked');
	return false;
}

function list_copy(id) {
	ac_ui_api_call(jsAdding);
	var post = ac.prepare('#copyModal');
	post.id = id;
	
	// ac.prepare is not obtaining the checkbox values properly, for some reason (it returns 1 whether the checkbox is checked or not)
	post.copy_bounce = ($J("#copy_bounce").attr("checked")) ? 1 : 0;
	post.copy_exclusion = ($J("#copy_exclusion").attr("checked")) ? 1 : 0;
	post.copy_filter = ($J("#copy_filter").attr("checked")) ? 1 : 0;
	post.copy_header = ($J("#copy_header").attr("checked")) ? 1 : 0;
	post.copy_personalization = ($J("#copy_personalization").attr("checked")) ? 1 : 0;
	post.copy_template = ($J("#copy_template").attr("checked")) ? 1 : 0;
	post.copy_field = ($J("#copy_field").attr("checked")) ? 1 : 0;
	post.copy_form = ($J("#copy_form").attr("checked")) ? 1 : 0;
	post.copy_subscriber = ($J("#copy_subscriber").attr("checked")) ? 1 : 0;
	
	ac.post(
		"list.list_copy",
		post,
		function (data) {
			list_list_rebuild_callback(data);
			$J('#copyModal').modal('hide');
			$J('#copyModal input[type=checkbox]').attr('checked', 'checked');
			$J('#copyModal input[type=checkbox]:last').attr('checked', false);
		}
	);
}


function list_form_sender_populate(addressid) {
	addressid = parseInt(addressid, 10);
	if ( addressid == 0 ) return;

	if ( typeof addresses[addressid] == 'undefined' ) return;
	var address = addresses[addressid];

	$J('#sendernameField').val(address.sender_name);
	$J('#sender_company_display').html(address.sender_name);
	$J('#senderaddr1Field').val(address.sender_addr1);
	$J('#sender_address1_display').html(address.sender_addr1);
	$J('#senderaddr2Field').val(address.sender_addr2);
	$J('#sender_address2_display').html(address.sender_addr2);
	$J('#sendercityField').val(address.sender_city);
	$J('#sender_city_display').html(address.sender_city);
	$J('#senderstateField').val(address.sender_state);
	$J('#sender_state_display').html(address.sender_state);
	$J('#senderzipField').val(address.sender_zip);
	$J('#sender_zip_display').html(address.sender_zip);
	$J('#sendercountryField').val(address.sender_country);
	$J('#sender_country_display').html(address.sender_country);
}

// sender preview: shows a live preview when typing
function list_form_sender_livepreview(new_value, section, modify) {
	// modify can be 1 (overwrite) or 2 (append)
	if (modify == 1) {
		$J('#sender_' + section + '_display').html(new_value);
	}
	else if (modify == 2) {
		$J('#sender_' + section + '_display').append(new_value);
	}
}


function list_list_rebuild_callback(ary) {
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		if ( $J("#list_list").length > 0 ) {
			// rebuild this list
			paginators[1].paginate(0);
		}
	} else {
		ac_error_show(ary.message);
	}
}

function list_form_external_twitter_verifycredentials_cb(ary) {
	ac_ui_api_callback();

	if (ary.error) {
		ac_error_show(list_form_str_twitter3 + ": " + ary.error);
		$('twitter').hide();
		$('twitter_checkbox').hide();
		$('twitter_disabled').show();
		return;
	}

	$("twitter_confirmed_screenname").href = "http://twitter.com/" + ary.screen_name;
	$("twitter_confirmed_screenname").innerHTML = ary.screen_name;
	$("twitter_confirmed").show();
	$("twitter_unconfirmed").hide();
	if (ary.diff) {
		$("twitter_token_diff").show();
	}
	else {
		$("twitter_token_diff").hide();
	}
}

function list_form_external_twitter_confirm() {
	var post = ac_form_post($("form"));
	if (post.twitter_oauth_pin == "") {
		alert("Please provide a PIN number");
		return false;
	}
	post.savetodb = 1;
	ac_ui_api_call(list_form_str_twitter4 + "...");
	ac_ajax_post_cb("api.php", "list.list_twitter_oauth_getaccesstoken", list_form_external_twitter_confirm_cb, post);
}

function list_form_external_twitter_confirm_cb(ary) {
	ac_ui_api_callback();

	if ( ary.oauth_token == "" || ary.oauth_token_secret == "" ) {
		ac_error_show(list_form_str_twitter1);
	}
	else {
		ac_result_show(list_form_str_twitter2);
		ac_ui_api_call(list_form_str_twitter5 + "...");
		ac_ajax_call_cb("api.php", "list.list_twitter_oauth_verifycredentials", list_form_external_twitter_verifycredentials_cb, ary.oauth_token, ary.oauth_token_secret);
		// show or hide the link "Update this Twitter account for all lists" - depending on if any other list has different tokens already set
		if (ary.diff) {
			$("twitter_token_diff").show();
		}
		else {
			$("twitter_token_diff").hide();
		}
	}
}

// mirrors tokens from one list to all other lists
function list_form_external_twitter_mirror() {
	if (!confirm(list_form_str_twitter9)) {
		return;
	}
	var post = ac.prepare("form");
	post.id = list_edit_id;
	ac_ui_api_call(list_form_str_twitter6 + "...");
	ac.post("list.list_twitter_token_mirror", post, list_form_external_twitter_mirror_cb);
}

function list_form_external_twitter_mirror_cb(ary) {
	ac_ui_api_callback();
	if (ary.succeeded) {
		ac_result_show(list_form_str_twitter7);
		$("twitter_token_diff").hide();
	}
	else {
		ac_error_show(list_form_str_twitter8);
	}
}

function list_twitter_oauth_init2_cb(ary) {
	ac_ui_api_callback();

	if (ary.error) {
		ac_error_show(ary.error);
		$('twitter').hide();
		$('twitter_checkbox').hide();
		$('twitter_disabled').show();
	}
	else {
		$('twitter_register_url').href = ary.register_url;
	}
}

function list_form_external_facebook_toggle() {
	if ( $('facebook').style.display == '' ) {
		$('facebook').hide();
	}
	else {
		$('facebook').show();
	}
}

//when checking or unchecking Facebook pages
function list_form_facebook_toupdate_toggle(is_checked) {
	if (is_checked) {

	}
	else {

	}
}

// sender preview: shows a live preview when typing
function list_form_sender_livepreview(new_value, section, modify) {
	// modify can be 1 (overwrite) or 2 (append)
	if (modify == 1) {
		$('sender_' + section + '_display').innerHTML = new_value;
	}
	else if (modify == 2) {
		$('sender_' + section + '_display').innerHTML += new_value;
	}
}

function list_sender_get_addresses_cb(ary) {
	// list_edit_id is the list ID
	ac_ui_api_callback();
	if (ary.length == 0) {
		$('list_form_addresses').hide();
		$('list_form_address').show();
		return;
	}
	$('list_form_addresses').show();
	$J("#list_form_sender_addresses").empty();
	var new_options = [];
	var selected_id = "";
	var address_found = false;
	addresses = [];

	$J.each(ary, function() {
		var c = this;
		if (c.sender_name == "" && c.sender_addr1 == "" && c.sender_city == "" && c.sender_country == "") {
			return true; // continue
		}
		if ( typeof addresses[c.id] == 'undefined' ) {
			// add to smarty var if not there already (smarty var only refreshes on full page reload)
			addresses[c.id] = {
				"id": c.id,
				"userid": c.userid,
				"sender_name": c.sender_name,
				"sender_addr1": c.sender_addr1,
				"sender_addr2": c.sender_addr2,
				"sender_city": c.sender_city,
				"sender_state": c.sender_state,
				"sender_zip": c.sender_zip,
				"sender_country": c.sender_country,
				"sender_phone": c.sender_phone
			};
		}
		var option_string = c.sender_name + ", " + c.sender_addr1 + ", ";
		if (c.sender_addr2 != "") option_string += c.sender_addr2 + ", ";
		option_string += c.sender_city + ", " + c.sender_country;
		if (!selected_id) {
			if (
			  c.sender_name == $J("#sendernameField").val() &&
			  c.sender_addr1 == $J("#senderaddr1Field").val() &&
			  c.sender_addr2 == $J("#senderaddr2Field").val() &&
			  c.sender_city == $J("#sendercityField").val() &&
			  c.sender_state == $J("#senderstateField").val() &&
			  c.sender_zip == $J("#senderzipField").val() &&
			  c.sender_country == $J("#sendercountryField").val()
			) {
				// if this address matches the one in the em_list table, select it
				selected_id = "sender_address_" + c.id;
				address_found = true;
			}
			else if ( (i + 1) == ary.length ) {
				// else if last item in the loop, select it
				selected_id = "sender_address_" + c.id;
			}
		}
		new_options.push("<option id='sender_address_" + c.id + "' value='" + c.id + "'>" + option_string + "</option>");
		
	});

	new_options.push("<option id='sender_address_0' value='0'>" + list_form_str_address2 + "</option>");
	new_options = new_options.join(" ");
	$J("#list_form_sender_addresses").append(new_options);
	// if there are no addresses saved in DB at all, or if this list's address was NOT found in the DB
	if (list_edit_id && (!selected_id || !address_found)) {
		// select "Add New Address..." option
		selected_id = "sender_address_0";
		$J('#list_form_address').show();
	}
	else if (!list_edit_id) {
		// if we are Adding a new list (list_edit_id = 0)
		$J('#list_form_address').hide();
	}
	else {
		$J('#list_form_address').hide();
	}
	$J("#" + selected_id).prop("selected", true);
	if (!addresses.length) {
		$('list_form_addresses').hide();
		$('list_form_address').show();
	}
}
