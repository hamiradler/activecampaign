{literal}
report_user_table.setcol(0, function(row) {
	return Builder.node(
		'div',
		{ title: row.email },
		[
			Builder._text(row.username + ' - ' + row.first_name + ' ' + row.last_name)
		]
	);
});

report_user_table.setcol(1, function(row, td) {
	td.align = 'center';
	return ( row.campaigns ? row.campaigns : Builder.node('em', [ Builder._text('0') ]) );
});

report_user_table.setcol(2, function(row, td) {
	td.align = 'center';
	return ( row.emails ? row.emails : Builder.node('em', [ Builder._text('0') ]) );
});

report_user_table.setcol(3, function(row, td) {
	td.align = 'center';
	return ( row.epd ? ( Math.round(row.epd * 100) / 100 ) : Builder.node('em', [ Builder._text('0.00') ]) );
});

function report_user_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_block";
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	ac_paginator_tabelize(report_user_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function report_user_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	if (report_user_list_filter > 0)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";

	report_user_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(report_user_list_anchor());
	$("loadingBar").className = "ac_block";
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, report_user_list_sort, report_user_list_offset, this.limit, report_user_list_filter, report_user_id);

	$("general").className = "ac_block";
}

{/literal}
