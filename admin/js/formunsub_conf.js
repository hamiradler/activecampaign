var form_optin_message_str_noinfo = '{"Please enter something in the From Name and From Email fields before continuing."|alang|js}';
var form_optin_message_str_nomail_from = '{"Please enter a valid email address in the From Email field before continuing."|alang|js}';
var form_optin_message_str_nomail_reply2 = '{"Please enter a valid email address in the Reply To field before continuing."|alang|js}';
var form_optin_message_str_nosubj = '{"Please enter something in the Subject field before continuing."|alang|js}';
var form_optin_message_str_nobody = '{"Please enter something in the content area of your message before continuing."|alang|js}';
var form_optin_message_str_activerss_nofeed = '{"You do not have RSS personalization tags included into your messsage, or you have more than one RSS feed referenced. Please make sure that you have one RSS feed placed into your message before continuing."|alang|js}';
var form_optin_message_str_activerss_noloop = '{"It seems like you haven't entered any LOOP tags to display your RSS feed items. Please add LOOP tags to your message before continuing."|alang|js}';
var form_optin_message_str_activerss_mismatch = '{"It seems like you haven't entered valid RSS personalization tags. Some tags seem to be missing, and your RSS feed would not be properly displayed. Please correct this before continuing."|alang|js}';

var form_optin_fetch_str_insert = '{"Insert"|alang|js}';
var form_optin_fetch_str_save = '{"Save"|alang|js}';

ac_editor_init_word_object.plugins += ",fullpage";
{jsvar var=$message name=message_obj};
{jsvar var=$message.html name=default_editor_value};

{literal}

function form_optin_save(nextstep) {
	if ($("form_optin_fromname").value == "" || $("form_optin_fromemail").value == "") {
		alert(form_optin_message_str_noinfo);
		return false;
	}

	$("form_optin_fromemail").value = ac_str_trim($("form_optin_fromemail").value);
	if (!ac_str_email($("form_optin_fromemail").value)) {
		alert(form_optin_message_str_nomail_from);
		$("form_optin_fromemail").focus();
		return false;
	}
	$("form_optin_reply2").value = ac_str_trim($("form_optin_reply2").value);
	if ($("form_optin_reply2").value != '' && !ac_str_email($("form_optin_reply2").value)) {
		alert(form_optin_message_str_nomail_reply2);
		$("form_optin_reply2").focus();
		return false;
	}

	if ($("form_optin_subject").value == "") {
		alert(form_optin_message_str_nosubj);
		return false;
	}

	var html = ac_form_value_get($("messageEditor"));
	if (strip_tags(html) == "" && !html.match(/<img/i) ) {
		alert(form_optin_message_str_nobody);
		return false;
	}

	$J("#form_optin_next").val(nextstep);
	$("optinform").submit();
}

function form_optin_show_message(checked) {
	if (checked) {
		$J("#messagecontent").show();
	} else {
		$J("#messagecontent").hide();
	}
}

function form_optin_autosave() {
	var post = $("optinform").serialize(true);
	ac_ajax_post_cb("api.php", "form.form_optin_save", ac_ajax_cb(form_optin_autosave_cb), post);
}

function form_optin_autosave_cb(ary) {
}

function form_optin_changed() {
	if (ac_form_value_get($("messageEditor")) != default_editor_value && !default_editor_value.match(/^fetch:/)) {
		return true;
	}

	return false;
}

function form_optin_changed_safe() {
	default_editor_value = ac_form_value_get($("messageEditor"));
}

// set onload
ac_dom_onload_hook(form_optin_changed_safe);

function form_optin_attachafile() {
	$("attachafile").hide();
	$("attachmentsBox").show();
}

function form_optin_managetext(val) {
	$J("#form_optin_managetextid").val(val);

	$("askmanagetext").hide();
	$("willmanagetext").hide();
	$J("#textversion").hide();

	if (val) {
		$("willmanagetext").show();
		$J("#textversion").show();
	} else {
		$("askmanagetext").show();
		$J("#textversion").hide();
	}
}

// Personalization
function form_optin_personalization_show(id) {
	$("personalize_subinfo").hide();
	$("personalize_other").hide();

	$("subinfo_tab").className = "othertab";
	$("other_tab").className = "othertab";

	switch (id) {
		case "personalize_subinfo":
			$("subinfo_tab").className = "currenttab"; break;
		case "personalize_other":
			$("other_tab").className = "currenttab"; break;
	}

	$(id).show();
}

function form_optin_personalization_open() {
	$('personalize4').value = 'html';
	$('personalize2').value = 'messageEditor';

	$('message_personalize').toggle();
}

function form_optin_personalize_build(val) {
	// what type of code to build
	var type = $('personalize4').value;
	// now handle custom (html?) cases
	var text = '';
	// only today tag should be reset
	if ( val.match( /^%TODAY[+-]\d+%$/ ) ) {
		val = '%TODAY*%';
	}
	if ( val == '%CONFIRMLINK%' ) {
		text = strConfirmLinkText;
	} else if ( val == '%UNSUBSCRIBELINK%' ) {
		text = strUnsubscribeText;
	} else if ( val == '%UPDATELINK%' ) {
		text = strSubscriberUpdateText;
	} else if ( val == '%WEBCOPY%' ) {
		text = strWebCopyText;
	} else if ( val == '%FORWARD2FRIEND%' ) {
		text = strForward2FriendText;
	} else if ( val == '%SOCIALSHARE%' ) {
		//text = strForward2FriendText; // don't prompt for anything, just use val
	} else if ( val == '%TODAY*%' ) {
		var entered = prompt(strEnterRange, '+1');
		if ( !entered ) return;
		if ( !entered.match( /^[-+]?\d+$/ ) ) {
			alert(strEnterRangeInvalid);
			return;
		}
		if ( !entered.match(/^[-+].*$/) ) {
			entered = '+' + entered;
		}
		val = '%TODAY' + entered + '%';
	}
	if ( type == 'html' && text != '' ) {
		entered = prompt(strEnterText, text);
		if ( !entered ) entered = text;
		val = '<a href="' + val + '">' + entered + '</a>';
	}
	return val;
}

function form_optin_personalization_insert(value) {
	if ( value == '' ) {
		alert(strPersMissing);
		return;
	}
	// close the modal
	$('message_personalize').toggle();
	// build the code
	var code = form_optin_personalize_build(value);
	if ( code == '' ) return;
	// push it into needed editor
	ac_editor_insert($('personalize2').value, ( $('personalize4').value == 'html' ? nl2br(code) : code ));
}

// Editor functions
function form_optin_toggle_editor(id, action, settings) {
	if ( action == ac_editor_is(id + 'Editor') ) return false;
	ac_editor_toggle(id + 'Editor', settings);
	$(id + 'EditorLinkOn').className  = ( action ? 'currenttab' : 'othertab' );
	$(id + 'EditorLinkOff').className = ( !action ? 'currenttab' : 'othertab' );
	if ( action != ( ac_js_admin.htmleditor == 1 ) )
		$(id + 'EditorLinkDefault').show();
	else
		$(id + 'EditorLinkDefault').hide();
	/*
	if ( !$(id + 'Editor') ) tmpEditorContent = ac_form_value_get($(id + '_form')); else // heavy hack!!!
	tmpEditorContent = ac_form_value_get($(id + 'Editor'));
	*/
	return false;
}

function form_optin_setdefaulteditor(id) {
	var isEditor = ac_editor_is(id + 'Editor');
	if ( isEditor == ( ac_js_admin.htmleditor == 1 ) ) return false;
	// send save command
	// save new admin limit remotelly
	ac_ajax_call_cb('api.php', 'user.user_update_value', null, 'htmleditor', ( isEditor ? 1 : 0 ));
	$(id + 'EditorLinkDefault').hide();
	ac_js_admin.htmleditor = ( isEditor ? 1 : 0 );
	return false;
}

$J(document).ready(function() {
	window.setTimeout('form_optin_autosave()', 30000);
	default_editor_value = ac_form_value_get($("messageEditor"));
});

function form_switch_list(val) {
	window.location.href = "main.php?action=formunsub_conf&listid=" + val;
}

{/literal}
