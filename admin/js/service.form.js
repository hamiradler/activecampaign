var service_form_str_cant_insert = '{"You do not have permission to add External Services."|alang|js}';
var service_form_str_cant_update = '{"You do not have permission to edit External Services."|alang|js}';
var service_form_str_cant_update2 = '{"You do not have permission to edit this External Service."|alang|js}';
var service_form_str_cant_find = '{"External Service not found."|alang|js}';
var service_form_str_shopify1 = '{"Disconnect from Shopify"|alang|js}';
var service_form_str_shopify2 = '{"Save"|alang|js}';
var service_form_str_shopify3 = '{"Billing Address"|alang|js}';
var service_form_str_shopify4 = '{"Order Number"|alang|js}';
var service_form_str_shopify5 = '{"Total Price"|alang|js}';
var service_form_str_shopify6 = '{"Shopify"|alang|js}';
var service_form_str_shopify7 = '{"ActiveCampaign field ID"|alang|js}';
var service_form_str_shopify8 = '{"No mappings created. To map additional fields, disconnect then reconnect."|alang|js}';
var service_form_str_shopify9 = '{"External"|alang|js}';
var service_form_str10 = '{"Reset mappings"|alang|js}';
var service_form_str11 = '{"Save mappings"|alang|js}';
var service_form_str12 = '{"NOTE: Any LIST adjustments require re-saving the WebHook URL on the external service."|alang|js}';
var service_form_str13 = '{"Please provide your Wufoo account and API key."|alang|js}';
var service_form_str14 = '{"Choose"|alang|js}';
var service_form_str15 = '{"Connecting..."|alang|js}';
var service_form_str16 = '{"Wufoo"|alang|js}';
var service_form_str17 = '{"ActiveCampaign"|alang|js}';
var service_form_str_shopify18 = '{"Company"|alang|js}';
var service_form_str_shopify19 = '{"Phone Number"|alang|js}';

{jsvar name=user_password var=$user_password}
{jsvar name=fields var=$fields}
{jsvar name=fields_all var=$fields_all}

{literal}

var timeouts = [];

var customFieldsObj = new ACCustomFields({
	sourceType: "SELECT",
	sourceId: "parentsList",
	api: "service.service_list_change",
	responseIndex: "fields",
	includeGlobals: 0,
	additionalHandler: function(ary) {
		service_lists_change(ary.lists, ary.fields);
		// loop through fields that were checked
		for (var i in ary.fields) {
			var f = ary.fields[i];
			if (typeof f != "function") {
				//alert(f.type);
				// get the input
				var rel = $("custom" + f.id + "Field");
				// get the parent <tr> (where this field is held)
				var this_tr = rel.parentNode.parentNode.parentNode;
				this_tr.show();
				//rel.checked = ac_array_has(this.selection, f.id);
			}
		}
	}
});

// Load custom fields onto the page
customFieldsObj.addHandler("custom_fields_table", "list");
customFieldsObj.addHandler("custom_fields_list_shopify", "list");

var service_form_id = 0;

function service_form_defaults() {
	$("form_id").value = 0;
	if ( $("form_submit") ) $("form_submit").show();
	if($("service_facebook"))$("service_facebook").hide();
	if($("service_twitter"))$("service_twitter").hide();
	$("service_webhook").hide();
	$("service_webhook_url").hide();
	$("service_shopify").hide();
	$("service_surveymonkey").hide();
	$("service_shopify_auth_link").hide();
	$("service_webhook_fields_savebutton").hide();
	$('service_webhook_handshake').hide();
	$("service_webhook_customfields").hide();
	$("service_shopify_customfields_connected").hide();
	$("custom_fields_additional_unbounce1").hide();
	$("service_shopify_lists_connected").hide();
	if ($("service_facebook_id")) $("service_facebook_id").value = "";
	if ($("service_facebook_secret")) $("service_facebook_secret").value = "";
	if ($("service_twitter_key")) $("service_twitter_key").value = "";
	if ($("service_twitter_secret")) $("service_twitter_secret").value = "";
	$("service_shopify_password_no").hide();
	$("service_shopify_password_yes").hide();
	$("shopify_form_webhook_id").value = "";
	$("service_webhook_nolists").hide();
	$("service_webhook_instructions_unbounce").hide();
	$("service_webhook_instructions_wufoo").hide();
	$("service_shopify_customfields").hide();
	$("service_fields_connected").hide();
	$("fields_form_submit_type").value = "save";
	$("service_wufoo").hide();
	$("service_wufoo_lists").hide();
	$("service_wufoo_customfields").hide();
	$("service_wufoo_customfields_connected").hide();
	$("service_wufoo_form_chosen").hide();
	$("service_wufoo_oldversion").hide();
}

function service_form_load(id) {
	
	// check for lists first
	if (!total_lists_user) {
		$("form").hide();
		$("noforms_nolists").show();
		return;
	}
	
	service_form_defaults();
	service_form_id = id;

	if (id > 0) {
		/*
		if (ac_js_admin.__CAN_EDIT__ != 1) {
			ac_ui_anchor_set(service_list_anchor());
			alert(service_form_str_cant_update);
			return;
		}
		*/

		// if non-Admin Group user, and trying to access Facebook or Twitter settings, reject
		if ( !ac_admin_ismaingroup && (id == 1 || id == 2) ) {
			ac_ui_anchor_set(service_list_anchor());
			alert(service_form_str_cant_update2);
			return;
		}

		ac_ui_api_call(jsLoading);
		if ( $("form_submit") ) $("form_submit").className = "ac_button_update";
		if ( $("form_submit") ) $("form_submit").value = jsUpdate;
		ac_ajax_call_cb("api.php", "service.service_get", service_form_load_cb, id);
	} else {
		/*
		if (ac_js_admin.__CAN_ADD__ != 1) {
			ac_ui_anchor_set(service_list_anchor());
			alert(service_form_str_cant_insert);
			return;
		}
		*/

		// no adding allowed
		alert(service_form_str_cant_insert);
		return;

		if ( $("form_submit") ) $("form_submit").className = "ac_button_add";
		if ( $("form_submit") ) $("form_submit").value = jsAdd;
		$("form").className = "ac_block";
	}
}

function service_form_load_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if ( !ary.id ) {
		ac_error_show(service_form_str_cant_find);
		ac_ui_anchor_set(service_list_anchor());
		return;
	}
	service_form_id = ary.id;

	$("form_id").value = ary.id;
	$("service_header").innerHTML = ary.name;
	$("service_description").show();
	$("service_description").innerHTML = ary.description;

	if ( ac_admin_ismaingroup ) {
		$("service_facebook_id").value = ary.facebook_app_id;
		$("service_facebook_secret").value = ary.facebook_app_secret;
		$("service_twitter_key").value = ary.twitter_consumer_key;
		$("service_twitter_secret").value = ary.twitter_consumer_secret;
	}

	var list_inputs = $("parentsList_div").getElementsByTagName("input");

	if (ac_admin_ismaingroup && service_form_id == 1) {
		$("service_facebook").show();
	}
	else if (ac_admin_ismaingroup && service_form_id == 2) {
		$("service_twitter").show();
	}
	else if (service_form_id == 3) {
		service_form_load_cb_unbounce(ary);
	}
	else if (service_form_id == 4) {
		service_form_load_cb_wufoo(ary);
	}
	else if (service_form_id == 5) {
		service_form_load_cb_shopify(ary);
	}
	else if (service_form_id == 6) {
		service_form_load_cb_surveymonkey(ary);
	}

	// user's group may not be assigned to any lists
	if (list_inputs.length == 0) {
		$("service_webhook").hide();
		$("service_webhook_nolists").show();
	}

	$("form").className = "ac_block";
}

function service_form_save(id) {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);

	if (id > 0)
		ac_ajax_post_cb("api.php", "service.service_update_post", service_form_save_cb, post);
	else
		ac_ajax_post_cb("api.php", "service.service_insert_post", service_form_save_cb, post);
}

function service_form_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded && ary.succeeded == "1") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(service_list_anchor());
	} else {
		ac_error_show(ary.message);
	}
}

function service_form_lists_checked() {
	var inputs = $('parentsList_div').getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].checked) return inputs[i].value;
	}
}

// can only check one list at a time, so reset them all to unchecked besides the one checked
// if no listid is passed, then it will check the first list by default
function service_form_reset_lists(listid, list_div_id) {
	if (typeof(list_div_id) == "undefined") list_div_id = "parentsList_div";
	var inputs = $(list_div_id).getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].value != listid) inputs[i].checked = false;
	}
	var checked = 0;
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].checked) checked++;
	}
	// make sure at least one is checked
	// also load custom fields for whatever was checked
	if (!checked) {
		inputs[0].checked = true;
		customFieldsObj.fetch(0);
	}
}

// generate the URL based on the list they choose
function service_form_gen_url(listid) {
	$('service_form_url').value = ac_js_site.p_link + '/admin/api.php?api_key=' + ac_js_admin.apikey + '&api_action=subscriber_add&api_output=serialize&listid=' + listid;
}

function service_lists_change(lists, fields) {
	if ($("form_id").value == 3) {
		// unbounce
		var trs = $("custom_fields_table").getElementsByTagName("tr");
		for (var i = 0; i < trs.length; i++) {
			var tds = trs[i].getElementsByTagName("td");
			var td1_inputs = tds[1].getElementsByTagName("input"); // the second <td> in the <tr> (the one on the right side)
			var rel_custom_field_domid = td1_inputs[0].id; // the corresponding AC custom field DOM ID (that exists in the same <tr>)
			var input_element = Builder.node(
				"input",
				{id: rel_custom_field_domid + "_textbox", name: "fields_additional[]"}
			);
			for (var j = 0; j < tds.length; j++) {
				if (j == 0) tds[j].appendChild(input_element);
			}
		}
	}
	else if ($("form_id").value == 5) {
		// shopify
		var trs = $("custom_fields_list_shopify").getElementsByTagName("tr");
		for (var i = 0; i < trs.length; i++) {
			var tds = trs[i].getElementsByTagName("td");
			var select_options = [
			  Builder.node("option", {value: "company"}, service_form_str_shopify18),
			  Builder.node("option", {value: "billing_address"}, service_form_str_shopify3),
			  Builder.node("option", {value: "phone"}, service_form_str_shopify19),
			  Builder.node("option", {value: "order_number"}, service_form_str_shopify4),
			  Builder.node("option", {value: "total_price"}, service_form_str_shopify5)
			];
			var select_element = Builder.node(
				"select",
				{name: "shopify_fields_additional[]"},
				select_options
			);
			for (var j = 0; j < tds.length; j++) {
				if (j == 0) tds[j].appendChild(select_element);
			}
			// pre-select first option
			select_element.value = "billing_address";
		}
	}
}

{/literal}