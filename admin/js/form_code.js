var form_obj = {jsvar var=$form};
{literal}

function form_save_name() {
	ac.post("form.form_save_name", { id: form_obj.id, name: $J("#form_name").val() });
}

function form_code_submit(nextstep) {
	if (nextstep == "theme")
		nextstep = "edit&theme=1";
	window.location.href = sprintf("main.php?action=form_%s&id=%s", nextstep, form_obj.id);
}

function form_save_button() {
	$J(".saving").show('drop', { direction: "right" }, 500);
	ac.wait(1.5, function() {
		$J(".saving").hide('drop', { direction: "right" }, 500);
	});
}

function form_code_qr_changesize(form_id, qr_url, dimensions) {
	// update image source
	$("qrimg").src = "https://chart.googleapis.com/chart?cht=qr&chs=" + dimensions + "&chl=" + qr_url;
	// update download link
	$("form_code_qr_download_link").href = "main.php?action=form_code&id=" + form_id + "&qr=download&dimensions=" + dimensions;
}

{/literal}
