{literal}
function subscriber_search_defaults() {
	$("search_content").value = '';

	var boxes = $("search").getElementsByTagName("input");

	for (var i = 0; i < boxes.length; i++) {
		if (boxes[i].type == "checkbox")
			boxes[i].checked = true;
	}

	if ( subscriber_listfilter && typeof(subscriber_listfilter) == 'object' ) {
		ac_form_select_multiple($('JSListFilter'), subscriber_listfilter);
	} else if ( subscriber_listfilter > 0 ) {
		$('JSListFilter').value = subscriber_listfilter;
	} else {
		ac_form_select_multiple_all($('JSListFilter'));
	}
	ac_form_select_multiple_all($('JSStatusFilter'), true);
}

function subscriber_search_check() {
	$J("#search").show();
}

function subscriber_search() {
	var post = ac_form_post($("search"));

	$("list_search").value = post.content;
	subscriber_listfilter = post.listid;

	ac_ajax_post_cb("api.php", "subscriber.subscriber_filter_post", subscriber_search_cb, post);
}

function subscriber_search_cb(xml) {
	$J("#search").hide();
	subscriber_list_search_cb(xml);
}
{/literal}
