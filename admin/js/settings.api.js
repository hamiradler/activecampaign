var settings_api_button1 = '{"View API Response"|alang|js}';
var settings_api_button2 = '{"Next Step"|alang|js}';
var settings_api_general1 = '{"API App"|alang|js}';
var settings_api_general2 = '{"Done!"|alang|js}';
var settings_api_general3 = '{"Step"|alang|js}';
var settings_api_sequence_text1 = '{"Request URL"|alang|js}';
var settings_api_sequence_text2 = '{"Post Variables"|alang|js}';
var settings_api_sequence_text3 = '{"Response"|alang|js}';
var settings_api_sequence_text4 = '{"Next Request"|alang|js}';
var settings_api_sequence_text5 = '{"First Request"|alang|js}';
var settings_api_app1_title = '{"Create new email message, then send campaign"|alang|js}';
var settings_api_app2_title = '{"Add a list, then add a subscriber"|alang|js}';
var settings_api_app3_title = '{"Request single sign-on token, then copy link"|alang|js}';
var settings_api_app4_title = '{"Create new user group, then new user for that group, then SSO URL"|alang|js}';
var settings_api_app1_description = '{"Create a new email message, then send a campaign using that message."|alang|js}';
var settings_api_app2_description = '{"Create a new list, then add a subscriber to that list."|alang|js}';
var settings_api_app3_description = '{"Request single sign-on token for a user, then view/copy the link used to log-in as that user."|alang|js}';
var settings_api_app4_description = '{"Create a new user group, then add a new user for that group, then generate the SSO log-in URL for that user."|alang|js}';
{literal}

var api_apps = {

  1: {
  	title: settings_api_app1_title,
  	description: settings_api_app1_description,
  	enabled: 1,
  	total_steps: 2,
  	methods: {
  		1: {
  			method: "message_add",
  			actions: function(data) {}
  		},
  		2: {
  			method: "campaign_create",
  			actions: function(data) {
  				// set the message ID (textbox)
  				$J("#campaign_create_postvar_m_any").val(data.id);
  				$J("#campaign_create_postvar_m_any").change();
  			}
  		}
  	},
  	done: function(data) {}
  },

  2: {
  	title: settings_api_app2_title,
  	description: settings_api_app2_description,
  	enabled: 0,
  	total_steps: 2,
  	methods: {
  		1: {
  			method: "list_add",
  			actions: function(data) {}
  		},
  		2: {
  			method: "subscriber_add",
  			actions: function(data) {}
  		}
  	},
		done: function(data) {}
  },

  3: {
  	title: settings_api_app3_title,
  	description: settings_api_app3_description,
  	enabled: 1,
  	total_steps: 1,
  	methods: {
  		1: {
  			method: "singlesignon",
  			actions: function(data) {}
  		}
  	},
  	done: function(data) {
			var output = $J("#apiexample_sequence").html();
			var sso_login_url = ac_js_site.p_link + "/admin/main.php?_ssot=" + data.token;
			output += sso_login_url;
			// append the log-in link to the end of the sequence output
			$J("#apiexample_sequence").html(output);
			alert(sso_login_url);
  	}
  },
  
  4: {
  	title: settings_api_app4_title,
  	description: settings_api_app4_description,
  	enabled: 1,
  	total_steps: 3,
  	methods: {
  		1: {
  			method: "group_add",
  			actions: function(data) {}
  		},
  		2: {
  			method: "user_add",
  			actions: function(data) {
  				// set the Group ID textbox with the ID returned
  				$J("#user_add_postvar_group_any").val(data.group_id);
  			}
  		},
  		3: {
  			method: "singlesignon",
  			actions: function(data) {
  				// set the username textbox to the user that was just created
  				$J("#singlesignon_param_sso_user").val(data.username);
  			}
  		}
  	},
  	done: function(data) {
			var output = $J("#apiexample_sequence").html();
			var sso_login_url = ac_js_site.p_link + "/admin/main.php?_ssot=" + data.token;
			output += sso_login_url;
			// append the log-in link to the end of the sequence output
			$J("#apiexample_sequence").html(output);
			alert(sso_login_url);
  	}
  }

};

function settings_api_onload() {
	
	// runs on page load
	
	// populate Apps drop-down
	for (var i in api_apps) {
		var app = api_apps[i];
		if (app.enabled) {
			$J("#api_apps").append("<option value='" + i + "'>" + app.title + "</option>");
		}
	}
	
	// when clicking "View in new window" button
	$J("#apiexample_view_window").click(function() {
		window.open($J("#apiexample").val());
	});

	// "View Response" towards the bottom
	$J("#apiexample_view").click(function() {
		view_response();
	});
	
	$J("#api_common").change(function() {
		var this_value = $J(this).val();
		if (this_value) {
			$J("#api_methods").val(this_value);
		}
		else {
			// no option chosen ("Common Actions")
			$J("#api_methods").val("");
		}
		$J("#api_methods").change();
	});
	
	$J("#api_methods").change(function() {
		var this_value = $J(this).val();
		api_method_change("individual", this_value);
	});
	
	$J("#api_apps").change(function() {
		// reset/empty the output because you are starting over
		$J("#apiexample_sequence").html("");
		var this_value = $J(this).val();
		api_app(this_value);
	});
	
	// when clicking a List checkbox for subscriber_add
	$J("#api_parameters_subscriber_add [id^='subscriber_add_postvar_p_']").click(function() {
		if (!this.checked) {
			$J("#subscriber_add_postvar_status_" + this.value).attr("name", "xxx");
		}
		else {
			$J("#subscriber_add_postvar_status_" + this.value).attr("name", "subscriber_add_postvar_status[" + this.value + "]");
		}
	});
	
	// changing the campaign_create Type drop-down
	$J("#campaign_create_postvar_type").change(function() {
		$J("#api_parameters_campaign_create [id^='campaign_create_postvar_settings_']").each(function() {
			// hide them all first
			this.hide();
		});
		// now show the chosen one
		$J("#campaign_create_postvar_settings_" + this.value).show();
	});
	
	// changing the message_add HTML Constructor drop-down
	$J("#message_add_postvar_htmlconstructor").change(function() {
		$J("#api_parameters_message_add [id^='message_add_postvar_htmlsettings_']").each(function() {
			// hide them all first
			this.hide();
		});
		// now show the chosen one
		$J("#message_add_postvar_htmlsettings_" + this.value).show();
	});
	
	// trigger it on page-load (so it loads a default)
	$J("#message_add_postvar_htmlconstructor").change();
	
	// changing the message_add Text Constructor drop-down
	$J("#message_add_postvar_textconstructor").change(function() {
		$J("#api_parameters_message_add [id^='message_add_postvar_textsettings_']").each(function() {
			// hide them all first
			this.hide();
		});
		// now show the chosen one
		$J("#message_add_postvar_textsettings_" + this.value).show();
	});
	
	// the "Any Message ID" textbox when creating a campaign (so you can use any ID)
	$J("#campaign_create_postvar_m_any").change(function() {
		// reset it first
		$J("#campaign_create_postvar_m_any_pct").attr("name", "");
		if (this.value != "") {
			$J("#campaign_create_postvar_m_any_pct").attr("name", "campaign_create_postvar_m[" + this.value + "]");
			$J("#campaign_create_postvar_m_any_pct").val("100");
		}
	});
	
	// trigger it on page-load (so it loads a default)
	$J("#message_add_postvar_textconstructor").change();
	
	// campaign_create: when clicking a message checkbox
	$J("#api_parameters_campaign_create [id^='campaign_create_postvar_m_']").click(function() {
		campaign_create_m_toggle(this.id, this.checked);
	});
	
	// pre-selected option. run this so the description loads
	$J("#api_methods").change();
	
	// run when the page loads so it updates the Request URL right away
	view_response();
}

// when clicking any of the "View Response" buttons
function view_response() {

	$J("#apiexample_response_div").show();
	if (ac_js_site.brand_links) $J("#apiexample_response_parse_div").show();
	$J("#apiexample_response_postvars_div").hide();
	$J("#apiexample_response").html("");
	
	var method = $J("#api_methods").val();
	
	if ($J("#api_apps").val() == "") {
		apiexample_reset("individual", method);
	}

	var params_obj = {};
	
	// URL (GET) PARAMS: loop through all form elements inside this method's div
	// IE: "#api_parameters_user_view [name^='user_view_param_']"
	$J("#api_parameters_" + method + " [name^='" + method + "_param_']").each(function() {
		// count this part: "user_view_param_"
		var pre_length = (method + "_param_").length;
		// IE: "user_view_param_id" needs to be just "id" (that is the real parameter)
		// chop off "user_view_param_" 
		var param = this.name.substring(pre_length);
		var this_value = this.value;
		if (this.type == "checkbox" || this.type == "radio") {
			var this_checked = (this.checked) ? 1 : 0;
			if (!this_checked) {
				// don't include in params string if the radio/checkbox option is NOT checked
			}
			else {
				params_obj[param] = this_value;
			}
		}
		else {
			if (this_value) {
				params_obj[param] = this_value;
			}
		}
	});
	
	params_str = "";
	for (var i in params_obj) {
		params_str += "&" + i + "=" + params_obj[i];
	}
	
	// chop off preceding "&"
	if (params_str) params_str = params_str.substring(1);
//alert(params_str);
	
	// update the Example URL
	var example_url = $J("#apiexample").html();
	if (params_str) example_url += "&" + params_str;
	$J("#apiexample").html(example_url);
	
	var api_postvars_str = "";
	var postvars_obj = {};
	
	// POST VARS: loop through all form elements inside this method's div
	// IE: "#api_parameters_subscriber_add [name^='subscriber_add_postvar_']"
	var postvars_counter = 0;
	$J("#api_parameters_" + method + " [name^='" + method + "_postvar_']").each(function() {
		postvars_counter++;
		// count this part: "subscriber_add_postvar_"
		var pre_length = (method + "_postvar_").length;
		// IE: "subscriber_add_postvar_email" needs to be just "email" (that is the real parameter)
		// chop off "subscriber_add_postvar_"
		var postvar = this.name.substring(pre_length);
		if (postvar.match(/\[\]$/)) {
			// if there are brackets on the end (indicating an array). IE: group[]
			// remove the brackets below
			postvar = postvar.replace("[]", "");
			// now add the value inside the brackets
			// IE: group[] = 2 becomes group[2] = 2
			postvar += "[" + this.value + "]"
		}
		// look for "Set to Empty?" checkbox that corresponds to this field (typically used for setting custom field values to empty/nothing)
		var set_to_empty = ($(this.id + "_empty")) ? $(this.id + "_empty").checked : false;
		var this_value = this.value;
		include = true;
		if (this.type == "checkbox" || this.type == "radio") {
			// don't include if not checked/selected
			if (!this.checked) {
				include = false;
			}
		}
		if ((include && this_value) || (include && set_to_empty)) postvars_obj[postvar] = this_value;
	});
	
	// final adjustments
	postvars_obj = view_response_postvars_adjust(method, postvars_obj);

	if (postvars_counter > 0) {
		// show textarea that has POST vars printed out
		var postvars_json_string = JSON.stringify(postvars_obj, null, 2);
		$J("#apiexample_response_postvars").html(postvars_json_string);
		$J("#apiexample_response_postvars_div").show();
		api_postvars_str = ", $post_data";
	}
	
	var language = $J("#apiexample_response_parse_lang").val();
// the hidden div with default parse code
	var response_parse_default = $J("#apiexample_response_parse_default_" + language).html();
	
	var components = method.split("_");
	
	// adjustments
	
	var method_class = "";
	if (typeof(components[1]) != "undefined") {
		if (components.length > 2) {
			for (var i = 0; i < components.length; i++) {
				// skip the first item
				if (i > 0) {
					method_class += components[i];
					if (i < components.length - 1) method_class += "_";
				}
			}
		}
		else {
			method_class = components[1];
		}
	}
	
	if (method_class == "view_email" || method_class == "view_username" || method_class == "view_hash") {
		method_class = "view";
	}
	
	var component = components[0];
	
	if (component == "branding") {
		component = "design";
	}
	else if (component == "sync") {
		component = "subscriber";
		method_class = "sync";
	}
	else if (component == "singlesignon") {
		component = "auth";
		if (method_class == "") {
			method_class = "singlesignon";
		}
		else {
			method_class = "singlesignon_sameserver";
		}
	}

	var api_param = component + "/" + method_class;
	if (params_str) api_param += "?" + params_str;
	
	// "Request and parse" textarea
	if (api_postvars_str) response_parse_default += "\n\n$post_data = get_object_vars(json_decode('" + postvars_json_string + "'));";
	response_parse_default += "\n\n$response = $ac->api(\"" + api_param + "\"" + api_postvars_str + ");";
	response_parse_default += "\n\n" + $J("#apiexample_response_parse_default2_" + language).html();
	$J("#apiexample_response_parse").html(response_parse_default);

	var func_params = [$J("#settings_api_url").val(), $J("#settings_api_key").val(), component, method_class, encodeURIComponent(params_str)];

	if ($J("#apiexample_view_test").attr("checked")) {
		$J("#apiexample_response").html("Test mode.");
		return;
	}

	ac_ui_api_call(jsLoading, 30);
	
	if (postvars_counter > 0) {
		// POST request
		postvars_obj["func_params"] = func_params;
		ac.post("api.api_public_post", postvars_obj, function(data) {
			var json_string = JSON.stringify(data, null, 2);
			$J("#apiexample_response").html(json_string);
			ac_ui_api_callback();
			api_app($J("#api_apps").val(), data);
		});
	}
	else {
		// GET request
		ac.get("api.api_public", func_params, function(data) {
			var json_string = JSON.stringify(data, null, 2);
			$J("#apiexample_response").html(json_string);
			ac_ui_api_callback();
			api_app($J("#api_apps").val(), data);
		});
	}
}

function api_method_change(type, method) {
	// handles the process of changing the displayed method details (URL, parameters, etc) to another one.
	// type is either "individual" (for individual actions) or "sequence" (for sequential actions - or apps)
	
	// GET or POST
	var request_method = $J("#api_method_details_" + method + "_request_method").html();
	
	if (request_method == "POST") {
		// Hide "View in new window" (won't work for post requests)
		$J("#apiexample_view_window").hide();
	}
	else {
		$J("#apiexample_view_window").show();
	}
	
	if (type == "individual") {
		apiexample_reset("individual", method);
		// set the list of individual methods ("Common Actions") to the correct one, also
		$J("#api_common").val(method);
	}
	else if (type == "sequence") {
		$J("#app_step").show();
		$J("#app_description").show();
		$J("#api_common").val(method);
		$J("#api_methods").val(method);
		apiexample_reset_requesturl(method);
		$J("#apiexample_view").val(settings_api_button2);
	}

	$J("#apiexample_response_div").hide();
	$J("#apiexample_response_postvars_div").hide();
	$J("#apiexample_response_parse_div").hide();

	// TEMPORARY: only show the "View Response" button for those that are all set up (with parameters)
	var allowed = ["message_list", "subscriber_delete", "subscriber_delete_list", "form_getforms", "form_html", "message_delete_list", "message_delete", "message_template_import", "message_template_export", "message_template_delete_list", "message_template_delete", "message_template_list", "message_template_view", "message_template_edit", "message_template_add", "campaign_status", "singlesignon", "campaign_report_bounce_list", "subscriber_sync", "list_field_add", "list_field_edit", "list_field_view", "list_delete", "list_edit", "list_add", "group_edit", "message_view", "group_add", "user_me", "campaign_report_open_list", "campaign_report_totals", "campaign_list", "campaign_send", "message_edit", "message_add", "campaign_create", "subscriber_edit", "subscriber_add", "subscriber_list", "list_paginator", "list_list", "subscriber_paginator", "subscriber_view_hash", "subscriber_view_email", "subscriber_view", "user_add", "user_view", "user_view_email", "user_view_username", "user_list", "account_view", "branding_view", "user_edit", "user_delete_list", "user_delete"];
	if (jQuery.inArray(method, allowed) != -1) {
		$J("#apiexample_view").show();
	}
	else {
		$J("#apiexample_view").hide();
	}
	
	// hide them all first

	$J(".api_method_details").each(function() {
		$J(this).hide();
	});

	$J(".api_parameters_div").each(function() {
		$J(this).hide();
	});

	if (method) {
		// then select the one corresponding to the chosen option
		$J("#api_method_details_" + method).show();
		$J("#api_parameters_" + method).show();
		
		// run View Response, but in test mode
		$J("#apiexample_view_test").attr("checked", true);
		view_response();
	}
	else {
		// No method chosen ("Choose")
	}
}

function campaign_create_m_toggle(id, checked) {
	
	// NOT a Split type campaign.
	// UNcheck them all first.
	if ($J("#campaign_create_postvar_type").val() != "split") {
		// loop through each message checkbox
		$J("#api_parameters_campaign_create [id^='campaign_create_postvar_m_']").each(function() {
				// uncheck all but the one that was checked
				if (this.id != id) this.checked = false;
		});
		// Set all percent textboxes to empty
		// loop through each message percent textbox
		$J("#api_parameters_campaign_create [id^='campaign_create_postvar_r_']").each(function() {
			if (this.id != id) $J("#" + this.id).val("");
		});
		// set the corresponding textbox (to the one that was checked above it) to 100
		var id_pieces = id.split("_");
		var message_id = id_pieces[4];
		$J("#campaign_create_postvar_r_" + message_id).val("100");
	}
	else {
		// Split-type campaign.
		// find out how many are checked
		var total_checked = 0;
		// loop through each message checkbox (again)
		$J("#api_parameters_campaign_create [id^='campaign_create_postvar_m_']").each(function() {
			if (this.checked) total_checked++;
		});
		
		var split_percent = Math.round(100 / total_checked);
		
		// loop through each message percent textbox
		$J("#api_parameters_campaign_create [id^='campaign_create_postvar_r_']").each(function() {
			var id_pieces = this.id.split("_");
			var message_id = id_pieces[4];
			// if the corresponding checkbox above this textbox is checked, set it's value
			if ($J("#campaign_create_postvar_m_" + message_id).attr("checked")) {
				$J("#" + this.id).val(split_percent);
			}
			else {
				// if the corresponding checkbox above this textbox is NOT checked, set value to empty
				$J("#" + this.id).val("");
			}
		});
	}
}

function api_app(app_id, data) {
	if (app_id == "") return;
	
	// update displayed description
	var app_description = api_apps[app_id]["description"];
	$J("#app_description_text").html(app_description);

	if (app_id != $J("#app_id").val()) {
		// reset counter if they switch apps
		$J("#app_step_int").html("0");
		$J("#apiexample_sequence_div").hide();
	}
	
	app_id = parseInt(app_id, 10);
	
	// set the hidden element value, so we know when to reset things
	$J("#app_id").val(app_id);
	
	var step = $J("#app_step_int").html();
	step = parseInt(step, 10);
	// step is the step that just finished (API request went through)
	var next_step = step + 1;
	// update the DOM with the new Step #
	$J("#app_step_int").html(next_step + "");

	var app = api_apps[app_id];

	if (next_step > 1) {
		api_sequence_output();
		$J("#apiexample_sequence_div").show();
	}

	if (typeof app.methods[next_step] != "undefined") {
		var app_step = app.methods[next_step];
		// display the next API method (form parameters)
		api_method_change("sequence", app_step.method);
		// run any actions that need to happen between each step
		app_step.actions(data);
	}
	else {
		// past last step - done here
		$J("#app_step").html(settings_api_general2);
		$J("#apiexample_view").hide();
		// run any final DOM changes, or anything in JS
		app.done(data);
	}
}

function api_sequence_output() {
	
	var method = $J("#api_methods").val();
	var request_url = $J("#apiexample").html();
	var postvars = $J("#apiexample_response_postvars").html();
	var response = $J("#apiexample_response").html();
	
	var sequence_text = $J("#apiexample_sequence").html();

	if (sequence_text == "") {
		// main header
		var app_title = $J("#api_apps option:selected").text();
		var sequence_text_new = settings_api_general1 + ": ";
		sequence_text_new += app_title + ".\n\n";
		sequence_text_new += "# " + settings_api_sequence_text5 + "\n\n";
	}
	else {
		var sequence_text_new = "# " + settings_api_sequence_text4 + "\n\n";
	}

	sequence_text_new += "## " + settings_api_sequence_text1 + " (" + method + "):\n\n";
	sequence_text_new += request_url + "\n\n";
	sequence_text_new += "## " + settings_api_sequence_text2 + " (" + method + "):\n\n";
	sequence_text_new += postvars + "\n\n";
	sequence_text_new += "## " + settings_api_sequence_text3 + " (" + method + "):\n\n";
	sequence_text_new += response + "\n\n";
	
	// append
	sequence_text = sequence_text + sequence_text_new;
	$J("#apiexample_sequence").html(sequence_text);
}

function apiexample_reset_requesturl(method) {
	// update Request URL in textbox
	var new_url = $J("#apiexample_root").val();
	new_url += "&api_action=" + method + "&api_output=json";
	$J("#apiexample").html(new_url);
}

function apiexample_reset(type, method) {

	// reset things based on choices made in the drop-downs
	// type is either "individual" (for individual actions) or "sequence" (for sequential actions)
	
	// update Request URL in textbox
	apiexample_reset_requesturl(method);
	
	if (type == "individual") {
		$J("#api_apps").val("");
		$J("#app_step").hide();
		$J("#app_description").hide();
		// reset the button to "View API Response"
		$J("#apiexample_view").val(settings_api_button1);
		$J("#app_step_int").html("0");
		$J("#apiexample_sequence_div").hide();
		$J("#apiexample_view").show();
	}
	else if (type == "sequence") {
		$J("#app_step_int").html("0");
	}
}

function apiexample_response_parse_lang_toggle(lang) {
	// hide all first
	$J("#apiexample_response_parse_ref_php").hide();
	$J("#apiexample_response_parse_ref_rails").hide();
	// show the one
	$J("#apiexample_response_parse_ref_" + lang).show();
}

function view_response_postvars_adjust(method, postvars_obj) {
	// any final checks and adjustments
	var postvars_obj_new = {};

	// set up any vars to use in the loop
	if (method == "campaign_create") {
		var messages = 0; // messages counter
	}

	for (var i in postvars_obj) {

		var key = i;
		var value = postvars_obj[i];
		var add = true;
//alert(key + ": " + value);

		if (method == "campaign_create") {
			if (key.match(/^m\[/)) {
				// IE: "m[33]" or "m[2]" - just look for "m[" at the beginning
				messages++;
				
				// if we ever go above 1, then it's a split-type campaign, so we have to adjust things further
				if (messages > 1) {
					//add = false;
					// convert "m[33]" to "r[33]" (for all subsequent messages) 
					key = key.replace("m", "r");
				}
			}
		}

		if (add) postvars_obj_new[key] = value;
	}

/*for (var i in postvars_obj_new) {
	alert(i + ": " + postvars_obj_new[i]);
}*/
	
	return postvars_obj_new;
}

{/literal}
