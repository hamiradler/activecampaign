var exclusion_delete_str = '{"Are you sure you want to delete Exclusion Pattern %s?"|alang|js}';
var exclusion_delete_str_multi = '{"Are you sure you want to delete the following Exclusion Pattern?"|alang|js}';
var exclusion_delete_str_cant_delete = '{"You do not have permission to delete Exclusion Pattern"|alang|js}';
{literal}
var exclusion_delete_id = 0;
var exclusion_delete_id_multi = "";

function exclusion_delete_check(id) {
	if (ac_js_admin.pg_list_edit != 1) {
		ac_ui_anchor_set(exclusion_list_anchor());
		alert(exclusion_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		exclusion_delete_check_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "exclusion.exclusion_select_row", exclusion_delete_check_cb, id);
}

function exclusion_delete_check_cb(xml) {
	var ary = ac_dom_read_node(xml);

	ac_dom_remove_children($("delete_list"));

	exclusion_delete_id = ary.id;
	$("delete_message").innerHTML = sprintf(exclusion_delete_str, ary.email);
	ac_dom_display_block("delete");	// can't use toggle here in IE
}

function exclusion_delete_check_multi() {
	if (ac_js_admin.pg_list_edit != 1) {
		ac_ui_anchor_set(exclusion_list_anchor());
		alert(exclusion_delete_str_cant_delete);
		return;
	}

	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(exclusion_list_anchor());
		return;
	}

	var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	ac_ajax_call_cb("api.php", "exclusion.exclusion_select_array", exclusion_delete_check_multi_cb, 0, sel.join(","));
	exclusion_delete_id_multi = sel.join(",");
}

function exclusion_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = exclusion_delete_str_multi;

	ac_dom_remove_children($("delete_list"));
	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].email ]));
	} else {
		$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}

	ac_dom_display_block("delete");
}

function exclusion_delete(id) {
	if (exclusion_delete_id_multi != "") {
		exclusion_delete_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "exclusion.exclusion_delete", exclusion_delete_cb, id);
}

function exclusion_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(exclusion_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
}

function exclusion_delete_multi() {
	ac_ui_api_call(jsLoading);
	if (selectAllSwitch) {
		ac_ajax_call_cb("api.php", "exclusion.exclusion_delete_multi", exclusion_delete_multi_cb, "_all", exclusion_list_filter);
		return;
	}
	ac_ajax_call_cb("api.php", "exclusion.exclusion_delete_multi", exclusion_delete_multi_cb, exclusion_delete_id_multi);
	exclusion_delete_id_multi = "";
}

function exclusion_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(exclusion_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
	$("acSelectAllCheckbox").checked = false;
	$('selectXPageAllBox').className = 'ac_hidden'; 
}
{/literal}
