var campaign_str_confirm_enable = '{"Are you sure you wish to re-enable this Scheduled Campaign?"|alang|js}';
var campaign_str_confirm_disable = '{"Are you sure you wish to disable this Scheduled Campaign?"|alang|js}';
var campaign_str_confirm_run = '{"Are you sure you wish to run this Campaign?"|alang|js}';
var campaign_str_confirm_stop = '{"Are you sure you wish to stop this Campaign?  Once you stop a campaign you CANNOT resume it."|alang|js}';
var campaign_str_confirm_pause = '{"Are you sure you wish to pause this Campaign?"|alang|js}';
var campaign_str_confirm_resume = '{"Are you sure you wish to resume this Campaign?"|alang|js}';
var campaign_str_confirm_send = '{"Are you sure you wish to send this Campaign now?"|alang|js}';
var campaign_str_alert_scheduled = '{"This will turn off your scheduled campaign. You will have the option to resume/enable the scheduled campaign after doing this."|alang|js}';
var campaign_str_sendnow = '{"Send Now"|alang|js}';
var campaign_str_survey1 = '{"Survey Results"|alang|js}';
var campaign_str_ticket = '{"Ticket"|alang|js}';
var campaign_str_status_completed = '{"Sent to %s subscribers about %s ago"|alang|js}';
var campaign_str_status_scheduled = '{"Scheduled to send on %s at %s"|alang|js}';
var campaign_str_status_reminder = '{"Sends when %s matches current date %s"|alang|js}';
var campaign_str_status_responder_instant = '{"Sends instantly upon subscribing"|alang|js}';
var campaign_str_status_responder_delayed = '{"Sends %s after subscribing"|alang|js}';
var campaign_str_status_activerss = '{"Sends every %s if new RSS posts are found"|alang|js}';
var campaign_str_status_cleanup = '{"Almost done (cleaning up)"|alang|js}';
var campaign_str_status_send = '{"Sending emails (%s)"|alang|js}';
var campaign_str_status_send_paused = '{"Sending is currently paused at %s"|alang|js}';
var campaign_str_status_transfer = '{"Preparing subscribers before sending (%s)"|alang|js}';
var campaign_str_status_transfer_paused = '{"Preparing subscribers is currently paused at %s"|alang|js}';
var campaign_str_status_winner = '{"Sent %s, waiting for winner to send the rest"|alang|js}';
var campaign_str_status_approval = '{"We are analyzing your campaign before sending starts"|alang|js}';
var campaign_str_status_ticket = '{"We contacted you regarding this campaign"|alang|js}';
var campaign_str_status_recur = '{"This campaign is set to recur every %s"|alang|js}';
var campaign_str_status_redraft = '{"Was previously declined from sending."|alang|js}';
var campaign_str_status_redraft_ticket = '{"View Details"|alang|js}';
var campaign_str_status_lastsave = '{"Last saved %s ago"|alang|js}';
var campaign_str_folder_innone = '{"Not in any folder"|alang|js}';
var campaign_str_folder_inone = '{"In folder %s"|alang|js}';
var campaign_str_folder_inmany = '{"In %s folders"|alang|js}';
var campaign_str_filter_search = '{"Search Results"|alang|js}';
var campaign_str_filter_folder = '{"Folder %s"|alang|js}';

{jsvar var=$recur_intervals name=campaign_str_intervals}
{jsvar var=$folders name=folders forcearray=1}
{jsvar var=$listsList name=lists}

var campaign_listfilter = {jsvar var=$listfilter};

var showTicket = {if $__ishosted && !$hostedaccount.rsid and $admin.id == 1}1{else}0{/if};

var canSend = {jsvar var=$canSendCampaign};


var campaign_delete_id = 0;
var campaign_filter_id = 0;
var campaign_reuse_id = 0;
var campaign_list_id = 0;
var campaign_folder_id = 0;
var folder_delete_id = 0;
