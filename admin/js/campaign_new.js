var campaign_form_id = {jsvar var=$campaignid};

var isEdit = {jsvar var=$isEdit};
var showAllMessages = {jsvar var=$showAllMessages};
var isDemo = {jsvar var=$demoMode};

var campaign_actionid_readopen = 0;
var campaign_saving = false;

var campaign_str_andwillsend = '{"and will send"|alang|js}';
var campaign_str_remindernote = '{"(Each subscriber will receive an email when their date field matches the conditions set for this campaign)"|alang|js}';
var campaign_str_selectone = '{"Select the message that you would like to use for this campaign"|alang|js}';
var campaign_str_selectmul = '{"Select the messages that you would like to use for this campaign"|alang|js}';

var campaign_str_approximately = '{"approximately"|alang|js}';
var campaign_rightnow_str = '{"Immediately"|alang|js}';
var campaign_sendat_str = '{"at"|alang|js}';
var campaign_sendspecial_str = '{"This campaign will be sent to every subscriber individually based on their subscription date/time."|alang|js}';
var campaign_cancel_str = '{"This will delete this draft you are working on."|alang|js}\n\n' + jsAreYouSure;
var campaign_noname_str = '{"Please enter a name for this campaign before you continue"|alang|js}';
var campaign_nolist_str = '{"Please select at least one list before you continue"|alang|js}';
var campaign_nofilter_str =
	'{"You have indicated that you would use a list segment, but have not selected any."|alang|js}\n\n' +
	'{"- Press OK to continue WITHOUT applying a segment"|alang|js}\n' +
	'{"- Press CANCEL to stay and select a segment"|alang|js}'
;
var campaign_nomessage_str = '{"Please select a message to send before you continue"|alang|js}';
var campaign_nomessages_str = '{"Please select at least two messages to send before you continue"|alang|js}';
var campaign_splitsum_str = '{"Message ratios invalid. The combined value (sum) of all split messages in WINNER scenario has to be less than 100%."|alang|js}';
var campaign_messageedit_str = '{"This will edit the message in a new window.\n\nDo you wish to continue?"|alang|js}';
var campaign_actionscnt_str = '{"%s Action(s)"|alang|js}';
var campaign_bouncesall_str = '{"- Use All Bounce Addresses -"|alang|js}';

var campaign_nounsub_str = '{"Your unsubscription message should contain a personalization tag %UNSUBSCRIBELINK%.\n\nPlease add that before continuing."|alang|js}';
var campaign_nounsub_warn_str = '{"Your unsubscription message should contain a personalization tag %UNSUBSCRIBELINK%.\n\nAre you sure you wish to continue without it?"|alang|js}';

var campaign_approve_str  = '{"(Submit for approval)"|alang|js}';
var campaign_demomode_str = '{"(Disabled in demo)"|alang|js}';

var campaign_message_search_found = '{"Search returned %s results."|alang|js}';

var pageLoaded = false;

var stepb4 = null;

{if $__ishosted}
var ourcustomhostedflag = true;
{/if}

{literal}

function campaign_hosted_checkapproval() {
	ac_ajax_call_cb("api.php", "approval.approval_hostedstatus", ac_ajax_cb(campaign_hosted_checkapproval_cb), campaign_obj.id);
}

function campaign_hosted_checkapproval_cb(ary) {
	if (ary.waiting) {
		window.setTimeout('campaign_hosted_checkapproval()', 3000);
		return;
	}

	if (ary.approved) {
		$("approvalqueue_waiting").hide();
		$("approvalqueue_sending").show();
	} else {
		$("approvalqueue_waiting").hide();
		switch (ary.message) {
			case "approved":
				$("approvalqueue_sending").show();
				break;

			case "declined":
				$("approvalqueue_declined").show();
				break;

			case "moreinfo":
				$("approvalqueue_moreinfo").show();
				break;

			case "pending":
				$("approvalqueue_pending").show();
				break;

			default:		// Really shouldn't get here.
				$("approvalqueue_waiting").show();
				break;
		}
	}
}


function campaign_post_prepare() {
	var post = ac_form_post($("campaignform"));
	// add link/read actions
	if (campaign_actionid_readopen > 0)
		post.actionid = campaign_actionid_readopen;
	if ( typeof post.linkmessage == 'undefined' ) post.linkmessage = {};
	if ( typeof post.linkname    == 'undefined' ) post.linkname    = {};
	if ( typeof post.linkurl     == 'undefined' ) post.linkurl     = {};
	// add read (dummy) link
	if ( typeof post.p != "undefined" && post.p.length > 0 ) { // if lists are selected
		if ( ( post.campaign_type == 'split' && post.messageid.length > 0 ) || post.messageid > 0 ) { // if message(s) is/are selected
			if ( $('trackreads').checked && $('message_treads').style.display != 'none' ) {
				// add open link
				var linkid = campaign_actions_find(0, 'open');
				post.linkmessage[linkid] = 0;
				post.linkname[linkid] = '';
				post.linkurl[linkid] = 'open';
			}
		}
	}
	// add campaign id
	post.id = campaign_obj.id;
	// add sdate
	post.sdate = campaign_obj.sdate;
	// responder
	post.responder_offset = campaign_obj.responder_offset;
	post.responder_do_oldies = ( isEdit ? 0 : 1 );
	// link cleanup for mod_security probs
	post.htmlfetch = post.htmlfetch == 'http://' ? '' : ac_b64_encode(post.htmlfetch);
	post.textfetch = post.textfetch == 'http://' ? '' : ac_b64_encode(post.textfetch);
	for ( var i in post.linkurl ) {
		post.linkurl[i] = ac_b64_encode(post.linkurl[i]);
	}

	return post;
}



function campaign_type_set(type) {
	$('campaign_type').value = type;
	campaign_obj.type = type;
	$('campaign_type_single_radio').checked = ( type == 'single' ? true : false );
	$('campaign_type_responder_radio').checked = ( type == 'responder' ? true : false );
	if ( $('campaign_type_reminder_radio') ) {
		$('campaign_type_reminder_radio').checked = ( type == 'reminder' ? true : false );
	}
	if ( $('campaign_type_activerss_radio') ) {
		$('campaign_type_activerss_radio').checked = ( type == 'activerss' ? true : false );
	}
	$('campaign_type_split_radio').checked = ( type == 'split' ? true : false );
	$('campaign_type_text_radio').checked = (type == 'text' ? true : false);

	return;
}

function campaign_type_info(panel) {
	// hide already open ones
	var shown = $$('#typeinfo div');
	var ids = [ 'typeinfosingle', 'typeinforesponder', 'typeinfosplit', 'typeinforeminder', 'typeinfoactiverss' ];
	for ( var i = 0; i < shown.length; i++ ) {
		// if the ID of this <div> is in the allowed ids (array above), and it's display is NOT 'none', hide it
		if ( ac_array_has(ids, shown[i].id) && shown[i].style.display != 'none' ) {
			shown[i].hide();
		}
	}
	// show the requested one
	$('typeinfo' + panel).show();
	// show the modal
	$('typeinfo').show();
}

function campaign_validate() {
	if ($("campaign_name").value == "") {
		alert(campaign_noname_str);
		return false;
	}

	return true;
}

{/literal}
