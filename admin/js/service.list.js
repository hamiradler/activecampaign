{literal}
var service_table = new ACTable();
var service_list_sort = "0";
var service_list_offset = "0";
var service_list_filter = "0";
var service_list_sort_discerned = false;

service_table.setcol(0, function(row) {
	var edit = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsOptionEdit);
	var dele = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsOptionDelete);

	var ary = [];

	//if (ac_js_admin.__CAN_EDIT__) {
		ary.push(edit);
		ary.push(" ");
	//}

	//if (ac_js_admin.__CAN_DELETE__) {
		//ary.push(dele);
	//}

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});

service_table.setcol(1, function(row) {
		return row.name;
});

service_table.setcol(2, function(row) {
	return row.description;
});

function service_list_anchor() {
	return sprintf("list-%s-%s-%s", service_list_sort, service_list_offset, service_list_filter);
}

function service_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_block";
		/*
		if ($("list_delete_button") !== null)
			$("list_delete_button").className = "ac_hidden";
		*/
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	/*
	if ($("list_delete_button") !== null)
		$("list_delete_button").className = "ac_inline";
	$("list_delete_button").className = "ac_inline";
	*/
	$("loadingBar").className = "ac_hidden";
	//$("acSelectAllCheckbox").checked = false;
	$("selectXPageAllBox").className = "ac_hidden";
	ac_paginator_tabelize(service_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function service_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	/*
	if (service_list_filter > 0)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";
	*/

	service_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(service_list_anchor());
	$("loadingBar").className = "ac_block";
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, service_list_sort, service_list_offset, this.limit, service_list_filter);

	$("list").className = "ac_block";
}

function service_list_chsort(newSortId) {
	var oldSortId = ( service_list_sort.match(/D$/) ? service_list_sort.substr(0, 2) : service_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( service_list_sort.match(/D$/) ) {
			// was DESC
			newSortId = service_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = service_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old service_list_sort
		if ( oldSortObj ) oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	service_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(service_list_anchor());
	return false;
}

function service_list_discern_sortclass() {
	if (service_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", service_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (service_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	service_list_sort_discerned = true;
}

{/literal}
