var exclusion_form_str_cant_insert = '{"You do not have permission to add Exclusion Pattern"|alang|js}';
var exclusion_form_str_cant_update = '{"You do not have permission to edit Exclusion Pattern"|alang|js}';
var exclusion_form_str_cant_find   = '{"Exclusion Pattern not found."|alang|js}';
{literal}
var exclusion_form_id = 0;

function exclusion_form_defaults() {
	$("form_id").value = 0;
	$("form_address").value = "";
	$("form_address").disabled = false;
	$("matchtype").value = "exact";
	$("matchtype").disabled = false;

	ac_dom_boxclear("listid_field");
	ac_dom_radioset("target_field", "several");

	$("listbox").show();
}

function exclusion_form_load(id) {
	exclusion_form_defaults();
	exclusion_form_id = id;

	if (id > 0) {
		if (ac_js_admin.pg_list_edit != 1) {
			ac_ui_anchor_set(exclusion_list_anchor());
			alert(exclusion_form_str_cant_update);
			return;
		}

		ac_ui_api_call(jsLoading);
		$("form_submit").className = "ac_button_update";
		$("form_submit").value = jsUpdate;
		ac_ajax_call_cb("api.php", "exclusion.exclusion_select_row", exclusion_form_load_cb, id);
	} else {
		if (ac_js_admin.pg_list_edit != 1) {
			ac_ui_anchor_set(exclusion_list_anchor());
			alert(exclusion_form_str_cant_insert);
			return;
		}

		$("form_submit").className = "ac_button_add";
		$("form_submit").value = jsAdd;
		$("form").className = "ac_block";
	}
}

function exclusion_form_load_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if ( !ary.id ) {
		ac_error_show(exclusion_form_str_cant_find);
		ac_ui_anchor_set(exclusion_list_anchor());
		return;
	}

	exclusion_form_id = ary.id;

	$("form_id").value = ary.id;
	$("form_address").value = ary.email;
	$("form_address").disabled = true;
	$("matchtype").value = ary.matchtype;
	$("matchtype").disabled = true;

	if (ary.matchall == 1 && $("allradio")) {
		ac_dom_radioset("target_field", "all");
	} else {
		ary.lists = ary.lists.toString().split(",");

		ac_dom_radioset("target_field", "several");
		//$$('.listid_field').each(function(e) { e.checked = false; }); if there's a bug with more than allowed checks checked, uncomment this one
		ac_dom_boxset("listid_field", ary.lists);
		$("listbox").show();
	}

	$("form").className = "ac_block";
}

function exclusion_form_save(id) {
	var post = ac_form_post_alt($("form"));
	ac_ui_api_call(jsSaving);

	if (id > 0)
		ac_ajax_post_cb("api.php", "exclusion.exclusion_update_post", exclusion_form_save_cb, post);
	else
		ac_ajax_post_cb("api.php", "exclusion.exclusion_insert_post", exclusion_form_save_cb, post);
}

function exclusion_form_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(exclusion_list_anchor());
	} else {
		ac_error_show(ary.message);
	}
}
{/literal}
