{literal}

function service_form_load_cb_unbounce(ary) {
	$("service_webhook").show();
	$("service_webhook_url").show();

	$("service_webhook_customfields").show();
	$("service_webhook_instructions_unbounce").show();
	$("service_webhook_fields_savebutton").show();
	$("custom_fields_additional_unbounce1").show();
	if (typeof(ary.data[0]) != "undefined" && typeof(ary.data[0].lists) != "undefined") {
		// connected - values have been saved already
		$("service_webhook_customfields").hide();
		$("service_fields_connected").show();
		var unbounce_lists = ary.data[0].lists + "";
		if ( unbounce_lists.indexOf(",") != -1 ) {
			unbounce_lists = unbounce_lists.split(",");
		}
		else {
			unbounce_lists = [unbounce_lists];
		}
		for (var i = 0; i < unbounce_lists.length; i++) {
			// pre-check the lists they already chose/saved
			if ( $("p_" + unbounce_lists[i]) ) $("p_" + unbounce_lists[i]).checked = true;
		}
		service_form_fields_display_additional_fields(ary.data[0].additional_fields);
	}
	else {
		// not connected
		$("service_webhook_customfields").show();
		$("service_fields_connected").hide();
		var inputs = $("parentsList_div").getElementsByTagName("input");
		inputs[0].checked = true; // check the first list by default
		customFieldsObj.fetch(0);
	}

	// generate URL display based on the list that is checked by default
	service_form_gen_url( service_form_lists_checked() );
	if ( $("form_submit") ) $('form_submit').hide();
}

{/literal}