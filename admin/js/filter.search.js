{literal}
function filter_search_defaults() {
	$("search_content").value = '';

	var boxes = $("search").getElementsByTagName("input");

	for (var i = 0; i < boxes.length; i++) {
		if (boxes[i].type == "checkbox")
			boxes[i].checked = true;
	}
}

function filter_search_check() {
	ac_dom_display_block("search");
}

function filter_search() {
	var post = ac_form_post($("search"));

	$("list_search").value = post.content;

	ac_ajax_post_cb("api.php", "filter.filter_filter_post", filter_search_cb, post);
}

function filter_search_cb(xml) {
	ac_dom_toggle_display("search", "block");
	filter_list_search_cb(xml);
}
{/literal}
