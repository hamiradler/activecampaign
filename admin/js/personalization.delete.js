var personalization_delete_str = '{"Are you sure you want to delete Personalization Tag %s?"|alang|js}';
var personalization_delete_str_multi = '{"Are you sure you want to delete the following Personalization Tag?"|alang|js}';
var personalization_delete_str_cant_delete = '{"You do not have permission to delete Personalization Tag"|alang|js}';
{literal}
var personalization_delete_id = 0;
var personalization_delete_id_multi = "";

function personalization_delete_check(id) {
	if (ac_js_admin.pg_template_delete != 1) {
		ac_ui_anchor_set(personalization_list_anchor());
		alert(personalization_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		personalization_delete_check_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "personalization.personalization_select_row", personalization_delete_check_cb, id);
}

function personalization_delete_check_cb(xml) {
	var ary = ac_dom_read_node(xml);

	ac_dom_remove_children($("delete_list"));

	personalization_delete_id = ary.id;
	$("delete_message").innerHTML = sprintf(personalization_delete_str, ary.name);
	ac_dom_display_block("delete");	// can't use toggle here in IE
}

function personalization_delete_check_multi() {
	if (ac_js_admin.pg_template_delete != 1) {
		ac_ui_anchor_set(personalization_list_anchor());
		alert(personalization_delete_str_cant_delete);
		return;
	}

	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(personalization_list_anchor());
		return;
	}

	var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	ac_ajax_call_cb("api.php", "personalization.personalization_select_array", personalization_delete_check_multi_cb, 0, sel.join(","));
	personalization_delete_id_multi = sel.join(",");
}

function personalization_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = personalization_delete_str_multi;

	ac_dom_remove_children($("delete_list"));
	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].name ]));
	} else {
		$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}

	ac_dom_display_block("delete");
}

function personalization_delete(id) {
	if (personalization_delete_id_multi != "") {
		personalization_delete_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "personalization.personalization_delete", personalization_delete_cb, id);
}

function personalization_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(personalization_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
}

function personalization_delete_multi() {
	if (selectAllSwitch) {
		ac_ajax_call_cb("api.php", "personalization.personalization_delete_multi", personalization_delete_multi_cb, "_all", personalization_list_filter);
		return;
	}
	ac_ajax_call_cb("api.php", "personalization.personalization_delete_multi", personalization_delete_multi_cb, personalization_delete_id_multi);
	personalization_delete_id_multi = "";
}

function personalization_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(personalization_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
	$("acSelectAllCheckbox").checked = false;
	$('selectXPageAllBox').className = 'ac_hidden';
}
{/literal}
