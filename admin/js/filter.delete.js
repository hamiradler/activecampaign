var filter_delete_str = '{"Are you sure you want to delete filter %s?"|alang|js}';
var filter_delete_str_multi = '{"Are you sure you want to delete the following filter?"|alang|js}';
var filter_delete_str_cant_delete = '{"You do not have permission to delete filters"|alang|js}';
{literal}
var filter_delete_id = 0;
var filter_delete_id_multi = "";

function filter_delete_check(id) {
	if (ac_js_admin.pg_subscriber_filters != 1) {
		ac_ui_anchor_set(filter_list_anchor());
		alert(filter_delete_str_cant_delete);
		return;
	}

	if (id < 1) {
		filter_delete_check_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "filter.filter_select_row", filter_delete_check_cb, id);
}

function filter_delete_check_cb(xml) {
	var ary = ac_dom_read_node(xml);

	ac_dom_remove_children($("delete_list"));

	filter_delete_id = ary.id;
	$("delete_message").innerHTML = sprintf(filter_delete_str, ary.name);
	ac_dom_display_block("delete");	// can't use toggle here in IE
}

function filter_delete_check_multi() {
	if (ac_js_admin.pg_subscriber_filters != 1) {
		ac_ui_anchor_set(filter_list_anchor());
		alert(filter_delete_str_cant_delete);
		return;
	}

	if (!ac_form_check_selection_check($("list_table"), "multi[]", jsNothingSelected, jsNothingFound)) {
		ac_ui_anchor_set(filter_list_anchor());
		return;
	}

	var sel = ac_form_check_selection_get($("list_table"), "multi[]");
	ac_ajax_call_cb("api.php", "filter.filter_select_array", filter_delete_check_multi_cb, 0, sel.join(","));
	filter_delete_id_multi = sel.join(",");
}

function filter_delete_check_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);

	$("delete_message").innerHTML = filter_delete_str_multi;

	ac_dom_remove_children($("delete_list"));
	if (!selectAllSwitch) {
		for (var i = 0; i < ary.row.length; i++)
			$("delete_list").appendChild(Builder.node("li", [ ary.row[i].name ]));
	} else {
		$("delete_list").appendChild(Builder.node("li", [ jsAllItemsWillBeDeleted ]));
	}

	ac_dom_display_block("delete");
}

function filter_delete(id) {
	if (filter_delete_id_multi != "") {
		filter_delete_multi();
		return;
	}
	ac_ajax_call_cb("api.php", "filter.filter_hide", filter_delete_cb, id);
}

function filter_delete_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(filter_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
}

function filter_delete_multi() {
	if (selectAllSwitch) {
		ac_ajax_call_cb("api.php", "filter.filter_hide_multi", filter_delete_multi_cb, "_all", filter_list_filter);
		return;
	}
	ac_ajax_call_cb("api.php", "filter.filter_hide_multi", filter_delete_multi_cb, filter_delete_id_multi);
	filter_delete_id_multi = "";
}

function filter_delete_multi_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		ac_ui_anchor_set(filter_list_anchor());
	} else {
		ac_error_show(ary.message);
	}

	ac_dom_toggle_display("delete", "block");
	$("acSelectAllCheckbox").checked = false;
	$('selectXPageAllBox').className = 'ac_hidden';
}
{/literal}
