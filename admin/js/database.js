{literal}

function database_repair() {
	ac_ajax_call_cb("api.php", "database.database_repair", ac_ajax_cb(database_repair_cb));
}

function database_optimize() {
	ac_ajax_call_cb("api.php", "database.database_optimize", ac_ajax_cb(database_optimize_cb));
}

function database_repair_cb(ary) {
	alert(ary.message);
}

function database_optimize_cb(ary) {
	alert(ary.message);
}

{/literal}
