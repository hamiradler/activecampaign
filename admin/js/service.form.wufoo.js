{literal}

var customFieldsObj_wufoo = new ACCustomFields({
	sourceType: "CHECKBOX",
	sourceId: "parentsList_div_wufoo",
	sourceName: "p_wufoo[]",
	api: "service.service_list_change",
	responseIndex: "fields",
	includeGlobals: 0,
	additionalHandler: function(ary) {
		service_lists_change_wufoo(ary.lists, ary.fields);
		// loop through fields that were checked
		for (var i in ary.fields) {
			var f = ary.fields[i];
			if (typeof f != "function") {
				//alert(f.type);
				// get the input
				var rel = $("custom" + f.id + "Field");
				// get the parent <tr> (where this field is held)
				var this_tr = rel.parentNode.parentNode.parentNode;
				this_tr.show();
				//rel.checked = ac_array_has(this.selection, f.id);
			}
		}
	}
});

// Load custom fields onto the page
customFieldsObj_wufoo.addHandler("custom_fields_list_wufoo", "list");

function service_lists_change_wufoo(lists, fields) {
	var trs = $("custom_fields_list_wufoo").getElementsByTagName("tr");
	for (var i = 0; i < trs.length; i++) {
		var tds = trs[i].getElementsByTagName("td");
		var td1_inputs = tds[1].getElementsByTagName("input"); // the second <td> in the <tr> (the one on the right side)
		var rel_custom_field_domid = td1_inputs[0].id; // the corresponding AC custom field DOM ID (that exists in the same <tr>)
		var select_element = Builder.node(
		  "select",
			{id: rel_custom_field_domid + "_wufoo_select", name: "wufoo_fields_additional[]", className: "wufoo_fields_additional"}
		);
		tds[0].appendChild(select_element);
		service_form_wufoo_form_toggle($("service_wufoo_form").value);
	}
}

//if no listid is passed, then it will check the first list by default
function service_form_reset_lists_wufoo(listid) {
	var inputs = $("parentsList_div_wufoo").getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) {
		//if (inputs[i].value != listid) inputs[i].checked = false;
	}
	var checked = 0;
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].checked) checked++;
	}
	// make sure at least one is checked
	// also load custom fields for whatever was checked
	if (!checked) {
		inputs[0].checked = true;
		customFieldsObj_wufoo.fetch(0);
	}
}

function service_form_load_cb_wufoo(ary) {
	$("service_webhook").show();
	$("service_webhook_url").show();
	if ( $("form_submit") ) $("form_submit").hide();

	$("service_webhook").hide();
	$("service_webhook_url").hide();
	$("service_wufoo").show();
	$("service_wufoo_confirmed").hide();
	$("service_wufoo_form").show();
	$("service_wufoo_unconfirmed").show();
	$("service_webhook_instructions_wufoo").show();
	$("service_wufoo_disconnect").show();

	var old_version_saved = false;
	// try to capture users of the old version (of wufoo integration). they should only have lists and additional_fields mapped/saved
	// account and API key should be undefined
	if (
		typeof(ary.data[0]) != "undefined" &&
		typeof(ary.data[0].account) == "undefined" &&
		typeof(ary.data[0].api_key) == "undefined" &&
		typeof(ary.data[0].lists) != "undefined" &&
		ary.data[0].lists != "" &&
		typeof(ary.data[0].additional_fields) != "undefined" &&
		ary.data[0].additional_fields.length > 0
	) {
		old_version_saved = true;
	}

	if (old_version_saved) {
		$("service_wufoo_oldversion").show();
		$("service_wufoo_customfields_connected").show();
		$("service_wufoo_disconnect").hide();
		service_form_wufoo_display_additional_fields(ary.data[0].additional_fields);
	}

	if (typeof(ary.data[0]) != "undefined" && typeof(ary.data[0].account) != "undefined" && typeof(ary.data[0].api_key) != "undefined") {
		// account and api info already saved in database
		$("wufoo_account").value = ary.data[0].account; // hidden value to use when saving second time
		$("wufoo_api_key").value = ary.data[0].api_key;
		$("service_wufoo_confirmed").show();
		$("service_wufoo_unconfirmed").hide();
		$("service_wufoo_lists").show();
		$("service_wufoo_customfields").show();
		$("service_wufoo_account").value = ary.data[0].account;
		$("service_wufoo_apikey").value = ary.data[0].api_key;
		
		// if there are forms saved (on reset we make it an empty array, but still retain account and API key)
		if (typeof(ary.data[0].forms) != "undefined" && ary.data[0].forms.length > 0) {
			service_form_wufoo_display_form_options(ary.data[0].forms);
		}
		
		if (typeof(ary.data[0].form) != "undefined" && typeof(ary.data[0].lists) != "undefined" && typeof(ary.data[0].webhook_hash) != "undefined") {
			// wufoo form chosen; webhook, mapped fields, and list values have been saved already
			$("wufoo_webhook").value = ary.data[0].webhook_hash;
			$("service_wufoo_form").hide();
			$("service_wufoo_form").value = ary.data[0].form; // pre-select the form drop-down even though it is hidden (need this value when resetting)

			service_form_wufoo_display_forms(ary.data[0]);
			
			$("service_wufoo_lists").hide();
			$("service_wufoo_customfields").hide();
			$("service_wufoo_customfields_connected").show();
			var wufoo_lists = ary.data[0].lists + "";
			if ( wufoo_lists.indexOf(",") != -1 ) {
				wufoo_lists = wufoo_lists.split(",");
			}
			else {
				wufoo_lists = [wufoo_lists];
			}
			for (var i = 0; i < wufoo_lists.length; i++) {
				// pre-check the lists they already chose/saved
				if ( $("p_wufoo_" + wufoo_lists[i]) ) $("p_wufoo_" + wufoo_lists[i]).checked = true;
			}
			service_form_wufoo_display_additional_fields(ary.data[0].additional_fields);
		}
		else {
			// not connected
			$("service_wufoo_customfields_connected").hide();
			service_form_reset_lists_wufoo(0);
		}
	}
}

function service_form_wufoo_save() {
	// save the API connection details (and fetch their data from Wufoo) 
	var account = $("service_wufoo_account").value;
	var api_key = $("service_wufoo_apikey").value;
	if (!account || !api_key) {
		alert(service_form_str13);
		return;
	}
	ac_ui_api_call(service_form_str15);
	ac_ajax_call_cb("api.php", "service.service_wufoo_save_apiconnection", service_form_wufoo_save_cb, account, api_key);
}

function service_form_wufoo_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if (ary.succeeded && ary.succeeded == "1") {
		ac_result_show(ary.message);
		$("wufoo_account").value = ary.account; // hidden value to use when saving second time
		$("wufoo_api_key").value = ary.api_key;
		$("service_wufoo_confirmed").show();
		$("service_wufoo_form").show();
		$("service_wufoo_unconfirmed").hide();
		$("service_wufoo_lists").show();
		$("service_wufoo_customfields").show();
		service_form_wufoo_display_form_options(ary.forms);
		service_form_reset_lists_wufoo(0);
	}
	else {
		ac_error_show(ary.message);
	}
}

function service_form_wufoo_display_form_options(forms) {
	// clear out the select list, then add the default back in
	ac_dom_remove_children($("service_wufoo_form"));
	var default_option = Builder.node("option", {value: ""}, service_form_str14 + "...");
	$("service_wufoo_form").appendChild(default_option);
	ac_dom_remove_children($("service_wufoo_form_fields"));
	if (typeof(forms) != "undefined") {
		// connected to wufoo
		if (forms.length > 0) {
			for (var i = 0; i < forms.length; i++) {
				var current = forms[i];
				var form_option = Builder.node("option", { id: "wufoo_form_option_" + current["hash"], value: current["hash"] }, current["name"]);
				$("service_wufoo_form").appendChild(form_option);
				$("service_wufoo_form").value = "";
				
				if (typeof(current["fields"]) != "undefined" && current["fields"].length > 0) {
					for (var j = 0; j < current["fields"].length; j++) {
						var current_field = current["fields"][j];
						var field_option = Builder.node("option", { id: current["hash"] + "_" + current_field["id"], value: current_field["id"] }, current_field["title"]);
						$("service_wufoo_form_fields").appendChild(field_option);
					}
				}
			}
		}
	}
	else {
		// not connected
		$("service_wufoo_confirmed").hide();
		$("service_wufoo_unconfirmed").show();
	}
}

function service_form_wufoo_display_forms(data) {
	// displays the raw text (after mapping fields and saving everything)
	ac_dom_remove_children($("service_wufoo_form_chosen"));
	$("service_wufoo_form_chosen").show();
	var wufoo_form_options = $("service_wufoo_form").getElementsByTagName("option");
	for (var i = 0; i < wufoo_form_options.length; i++) {
		if (wufoo_form_options[i].selected) {
			
			var wufoo_lists = data.lists + "";
			if ( wufoo_lists.indexOf(",") != -1 ) {
				wufoo_lists = wufoo_lists.split(",");
			}
			else {
				wufoo_lists = [wufoo_lists];
			}
			for (var j = 0; j < wufoo_lists.length; j++) {
				if ( $("lists_all_" + wufoo_lists[j]) ) {
					// display the form name that was chosen (drop-down is hidden)
					var wufoo_form_name = wufoo_form_options[i].innerHTML;
					var form_list_display = Builder.node("div", {}, [
					  Builder.node("span", { id: "", className: "wufoo_form_display" }, wufoo_form_options[i].innerHTML),
					  Builder._text(" (" + service_form_str16 + ")"),
					  Builder._text(" => "),
					  Builder.node("span", {}, $("lists_all_" + wufoo_lists[j]).value),
					  Builder._text(" (" + service_form_str17 + ")")
					]);
					$("service_wufoo_form_chosen").appendChild(form_list_display);
				}
			}
			
		}
	}
}

function service_form_wufoo_display_additional_fields(fields) {
	if ( typeof(fields) != "undefined" ) {
		var fields_ul = $("service_wufoo_customfields_connected").getElementsByTagName("ul");
		fields_ul = fields_ul[0];
		// if there are fields to display
		if ( typeof(fields[0]) != "undefined" ) {
			var additional_fields = fields[0];
			ac_dom_remove_children(fields_ul);
			for (var i in additional_fields) {
				if (typeof(i) == "string") {
					// $fields_all_* is smarty vars written on page load containing all custom fields in the system
					if ( $("fields_all_" + additional_fields[i]) ) {
						var field_title = $("fields_all_" + additional_fields[i]).value;
						var fields_ul_li = Builder.node(
						  "li",
						  { style: "margin: 0 0 7px 0;" },
						  [
						    Builder.node("span", { id: "custom" + additional_fields[i] + "Field_external_value", className: "customfield_external_value" }, i),
						    Builder._text(" (" + service_form_str16 + ")"),
						    Builder._text(" => "),
						    Builder.node( "span", {}, field_title + " (" + service_form_str_shopify7 + " " + additional_fields[i] + ")" )
						  ]
						);
						fields_ul.appendChild(fields_ul_li);
					}
				}
			}
		}
		else {
			// before removing everything, re-fill textboxes with values that were just mapped, so its easier to EDIT
			var mapped_fields_external = $$(".customfield_external_value");
			for (var i = 0; i < mapped_fields_external.length; i++) {
				var current = mapped_fields_external[i];
				var regex = new RegExp("[0-9]+"); // find just the numeric portion of the string (IE: custom10Field_external_value)
				var old_mapped_field_id = regex.exec(current.id);
				$("custom" + old_mapped_field_id + "Field_wufoo_select").value = current.innerHTML;
			}
			ac_dom_remove_children(fields_ul);
		}
		/*
		var webhook_fields_inputs = $("service_webhook_fields_savebutton").getElementsByTagName("input");
		var save_button = webhook_fields_inputs[1];
		// count how many <li>'s were added
		var fields_ul_lis = fields_ul.getElementsByTagName("li");
		if (!fields_ul_lis.length) {
			// none added
			$("fields_form_submit_type").value = "save";
			$("service_webhook_customfields").show();
			$("service_fields_connected").hide();
			save_button.value = service_form_str11;
		}
		else {
			// some were added
			$("fields_form_submit_type").value = "reset";
			$("service_webhook_customfields").hide();
			$("service_fields_connected").show();
			save_button.value = service_form_str10;
		}
		*/
	}
}

function service_form_wufoo_form_toggle(form_hash) {
	if (form_hash == "") return;
	var additional_fields_selects = $$(".wufoo_fields_additional");
	var form_fields = $("service_wufoo_form_fields").getElementsByTagName("option");
	var regex = new RegExp("^" + form_hash + "_"); // IE: z7x3k7_EntryId - match all these options that start with the hash
	var options = [];
	// loop through the hidden select that contains all of the form fields (for all forms)
	for (var i = 0; i < form_fields.length; i++) {
		var current = form_fields[i];
		if ( current.id.match(regex) ) {
			//var option = Builder.node("option", {value: current.value}, current.innerHTML);
			var option = "<option value='" + current.innerHTML + " (" + current.value + ")'>" + current.innerHTML + "</option>";
			options.push(option);
		}
 	}
	for (var i = 0; i < additional_fields_selects.length; i++) {
		var current = additional_fields_selects[i];
		// remove all <option> elements first
		$J("#" + current.id).empty();
	}
	// loop through all select lists (under additional fields, in the left column)
	for (var i = 0; i < additional_fields_selects.length; i++) {
		var current = additional_fields_selects[i];
		// loop through <options> that need to be added to the select
		for (var j = 0; j < options.length; j++) {
			// append each <option> from the javascript object
			$J("#" + current.id).append(options[j]);
		}
	}
}

function service_form_wufoo_save2() {
	// 2nd save: saving the custom field mappings and list choice(s)
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);
	var all_custom_fields = [];
	var custom_fields_global_inputs = $("custom_fields_global_wufoo").getElementsByTagName("input");
	var custom_fields_list_inputs = $("custom_fields_list_wufoo").getElementsByTagName("input");
	for (var i = 0; i < custom_fields_global_inputs.length; i++) {
		var current = custom_fields_global_inputs[i];
		// only add to new array if checked
		if (current.checked) {
			all_custom_fields.push(current.value);
		}
		else {
			all_custom_fields.push(0);
		}
	}
	for (var i = 0; i < custom_fields_list_inputs.length; i++) {
		var current = custom_fields_list_inputs[i];
		// only add to new array if checked
		if (current.checked) {
			all_custom_fields.push(current.value);
		}
		else {
			all_custom_fields.push(0);
		}	
	}
	post.custom_fields_all = all_custom_fields;
	ac_ajax_post_cb("api.php", "service.service_wufoo_save_settings", service_form_wufoo_save2_cb, post);
}

function service_form_wufoo_save2_cb(xml) {
	// this function is the same stuff that's run on load_cb(), if there is data saved
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	
	if (ary.succeeded && ary.succeeded == "1") {
		ac_result_show(ary.message);
		if (ary.save_type == "add webhook") {
			// connected
			$("service_wufoo_oldversion").hide();
			$("wufoo_webhook").value = ary.webhook_id;
			$("service_wufoo_form").hide();
			$("service_wufoo_form").value = ary.form; // pre-select the form drop-down even though it is hidden (need this value when resetting)
			
			service_form_wufoo_display_forms(ary);
			
			$("service_wufoo_lists").hide();
			$("service_wufoo_customfields").hide();
			$("service_wufoo_customfields_connected").show();
			service_form_wufoo_display_additional_fields(ary.additional_fields);
		}
		else if (ary.save_type == "remove webhook") {
			// not connected
			$("wufoo_webhook").value = "";
			$("service_wufoo_confirmed").show();
			$("service_wufoo_lists").show();
			$("service_wufoo_customfields").show();
			$("service_wufoo_customfields_connected").hide();
		}
	}
	else {
		ac_error_show(ary.message);
	}
}

function service_form_wufoo_reset() {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsUpdating);
	ac_ajax_post_cb("api.php", "service.service_wufoo_delete_settings", service_form_wufoo_reset_cb, post);
}

function service_form_wufoo_reset_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if (ary.succeeded && ary.succeeded == "1") {
		service_form_load(4);
	}
	else {
		ac_error_show(ary.message);
	}
}

{/literal}