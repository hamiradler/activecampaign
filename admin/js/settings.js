{include file="settings.general.js"}
{if ac_admin_ismaingroup()}
{include file="settings.local.js"}
{include file="settings.mailsending.js"}
{include file="settings.delivery.js"}
{/if}
{include file="settings.api.js"}
{include file="settings.account.js"}

{literal}
function settings_form_submit() {
	$J('#accountSettings ul#navtabs a').each(function() {
		if ($J("#" + this.id).is(".selected")) {
			// if this element has the class "selected"
			// IE: "alias_tab_link" - split so we can obtain "alias"
			var id_array = this.id.split("_");
			var hash = "tab_" + id_array[0];
			$J("#accountSettings").attr("action", "main.php?action=settings#" + hash);
		}
	});
	// have to use prototype here becase jquery will keep trying to run this function (recursive)
	$("accountSettings").submit();
}
{/literal}