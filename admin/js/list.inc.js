var list_str_overlimit = '{"You have exceeded the number of allowed Lists"|alang|js}';
var list_str_filter_search = '{"Search Results"|alang|js}';
var list_str_bar_subs = '{"Subscribers"|alang|js}';
var list_str_bar_unsubs = '{"Unsubscriptions"|alang|js}';
var list_str_bar_interaction = '{"Interactions"|alang|js}';
var list_str_bar_nodata = '{"No Data Yet"|alang|js}';
var list_str_cnt_segments = '{"%s Segment(s)"|alang|js}';
var list_str_cnt_segments_none = '{"No segments yet"|alang|js}';
var list_str_cnt_segments_new = '{"New Segment"|alang|js}';
var list_str_opt_import = '{"Import"|alang|js}';
var list_str_opt_field = '{"Fields"|alang|js}';
var list_str_opt_auto = '{"Automations"|alang|js}';
var list_str_opt_form = '{"Forms"|alang|js}';
var list_str_opt_settings = '{"Advanced Settings"|alang|js}';
var list_str_opt_editlist = '{"List Name & Address"|alang|js}';
var list_str_opt_exclusion = '{"Exclusions"|alang|js}';
var list_str_opt_header = '{"Email Headers"|alang|js}';
var list_str_opt_emailaccount = '{"Subscribe by Email"|alang|js}';
var list_str_opt_sync = '{"Database Sync"|alang|js}';
var list_str_opt_copy = '{"Copy"|alang|js}';

var list_form_str_analytics1 = '{"Please include your Analytics Account Number in the proper format."|alang|js}';
var list_form_str_analytics2 = '{"Please include your Analytics Source Name."|alang|js}';

var list_form_str_twitter1 = '{"Could not obtain permission from Twitter. Please try again shortly."|alang|js}';
var list_form_str_twitter2 = '{"Successfully confirmed permission from Twitter."|alang|js}';
var list_form_str_twitter3 = '{"Twitter authentication error"|alang|js}';
var list_form_str_twitter4 = '{"Connecting"|alang|js}';
var list_form_str_twitter5 = '{"Retrieving your Twitter account details"|alang|js}';
var list_form_str_twitter6 = '{"Copying Twitter tokens to all lists"|alang|js}';
var list_form_str_twitter7 = '{"Successfully mirrored Twitter tokens to all lists"|alang|js}';
var list_form_str_twitter8 = '{"Failed to mirror Twitter tokens to all lists"|alang|js}';
var list_form_str_twitter9 = '{"Are you sure you want to mirror this Twitter account to all lists? This will overwrite any existing Twitter accounts."|alang|js}';
var list_form_str_senderinfo_company = '{"Your Company"|alang|js}';
var list_form_str_senderinfo_address = '{"Address"|alang|js}';
var list_form_str_senderinfo_apt = '{"Apt/Suite"|alang|js}';
var list_form_str_senderinfo_city = '{"City"|alang|js}';
var list_form_str_senderinfo_state = '{"State"|alang|js}';
var list_form_str_senderinfo_zip = '{"Zip"|alang|js}';
var list_form_str_senderinfo_country = '{"Country"|alang|js}';
var list_form_str_facebook_profile = '{"profile"|alang|js}';
var list_form_str_facebook_page = '{"page"|alang|js}';
var list_form_str_facebook1 = '{"Please select at least one Facebook account to update."|alang|js}';
var list_form_str_address1 = '{"Select Existing Address..."|alang|js}';
var list_form_str_address2 = '{"Add New Address..."|alang|js}';

var list_form_str_title_add = '{"Create New List"|alang|js}';
var list_form_str_title_edit = '{"List Name & Address"|alang|js}';
var list_form_str_button_add = '{"Create List"|alang|js}';
var list_form_str_button_edit = '{"Save List"|alang|js}';

var sender_fields_actual = ['sendernameField', 'senderaddr1Field', 'senderaddr2Field', 'sendercityField', 'senderstateField', 'senderzipField', 'sendercountryField'];
var sender_fields_preview = ['company', 'address1', 'address2', 'city', 'state', 'zip', 'country'];
var sender_fields_default = [list_form_str_senderinfo_company, list_form_str_senderinfo_address, list_form_str_senderinfo_apt, list_form_str_senderinfo_city, list_form_str_senderinfo_state, list_form_str_senderinfo_zip, list_form_str_senderinfo_country];

var list_form_str_deleteerror = '{"You must first check all the checkboxes (confirming what you are deleting) before you click delete list."|alang|js}';


{jsvar name=canAddList var=$canAddList}
{jsvar name=addresses var=$addresses}
{jsvar name=newform var=$newform}
{jsvar name=settingsid var=$settingsid}

var list_filter_id = 0;
var list_delete_id = 0;
var list_edit_id = 0;
var list_copy_id = 0;

