{literal}
var template_export_id = 0;

function template_export_check(id) {
	if (id < 1) {
		return;
	}
	template_export_id = id;
	ac_dom_display_block("export");
	//template_form_load(id); // make sure the form page loads on refresh
}

function template_export(id, type) {
	window.location = ac_js_site.p_link + "/admin/export.php?action=template&type=" + type + "&id=" + id;
	$("export").hide();
}

{/literal}