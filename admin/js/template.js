{include file="template.preview.js"}
{include file="template.list.js"}
{include file="template.form.js"}
{include file="template.import.js"}
{include file="template.delete.js"}
{include file="template.export.js"}
{include file="template.search.js"}

var template_listfilter = {jsvar var=$listfilter};

{literal}
function template_process(loc, hist) {
	if ( loc == '' ) {
		loc = 'preview'; // default
		//loc = 'list-' + template_list_sort + '-' + template_list_offset + '-' + template_list_filter;
		ac_ui_rsh_save(loc);
	}
	var args = loc.split("-");

	$("template_header1").hide();
	$("template_header2").hide();
	$("preview_").hide();
	$("list").className = "ac_hidden";
	$("form").className = "ac_hidden";
	//$("template_list_count").className = "ac_hidden";
	$("import").className = "ac_hidden";
	var func = null;
	try {
		var func = eval("template_process_" + args[0]);

	} catch (e) {
		if (typeof template_process_preview == "function")
			template_process_preview(args);
	}
	if (typeof func == "function")
		func(args);
}

function template_process_preview(args) {
	$("template_header2").show();
	$("preview_").show();
}

function template_process_list(args) {
	if (args.length < 2)
		args = ["list", template_list_sort, template_list_offset, template_list_filter];

	template_list_sort = args[1];
	template_list_offset = args[2];
	template_list_filter = args[3];

	if ( template_listfilter > 0 ) $('JSListManager').value = template_listfilter;

	$("template_header1").show();
	
	template_list_discern_sortclass();

	paginators[1].paginate(template_list_offset);
}

function template_process_form(args) {
	if (args.length < 2)
		args = ["form", "0"];

	$("template_header1").show();
	
	var id = parseInt(args[1], 10);

	template_form_load(id);
}

function template_process_import(args) {
	if (args.length < 1)
		args = ["import"];

	$("template_header1").show();
	
	template_import_load();
}

function template_process_delete(args) {
	if (args.length < 2) {
		template_process_preview(["preview"]);
		return;
	}

	$("template_header2").show();
	$("form").className = "ac_block";
	
	//$("list").className = "ac_block";
	var id = parseInt(args[1], 10);

	template_delete_check(id);
}

function template_process_delete_multi(args) {
	//$("list").className = "ac_block";
	$("template_header2").show();
	$("preview_").show();
	template_delete_check_multi();
}

function template_process_export(args) {
	var id = parseInt(args[1], 10);
	$("form").className = "ac_block";
	template_export_check(id);
}

function template_process_search(args) {
	$("list").className = "ac_block";
	template_search_check();
}
{/literal}
