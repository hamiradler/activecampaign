$J(document).ready(function() {
	// enable twipsy for links
	$J("a[rel=tip]").twipsy({gravity: 'sw'});
	// fetch campaigns
	paginators[1].paginate(0);
	// if they hit enter key in search box, run the filter
	$J('.filterSearch input[type=text]').keypress(function(event) {
		if ( event.which == 13 ) {
			campaign_list_search();
			event.preventDefault();
		}
	});
	$J("li.folder-name").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ":not(.ui-sortable-helper)",
		drop: function( event, ui ) {
			var fid = parseInt(this.id.replace('folder_', ''), 10);
			var cid = parseInt(ui.draggable.attr('id').replace('campaignrow_', ''), 10);
			campaign_folder_add(fid, cid);
		}
	});
	$J('#gravatarstream').masonry({
		itemSelector : '.avatarbox',
		isFitWidth: true,
		columnWidth: function( containerWidth ) {return containerWidth / 100;}
	});
});


function campaign_list_header() {
	var count = ac.str.number_format(paginators[1].total, 0, decimalDelim, commaDelim);
	var filter = "";
	var statusid = $J('#JSStatusManager').val();
	var typeid = $J('#JSTypeManager').val();
	var listid = $J('#JSListManager').val();
	if ( typeid ) {
		filter = campaign_types_strings[typeid];
	} else if ( statusid ) {
		filter = campaign_statuses_strings[statusid];
	} else if ( campaign_folder_id > 0 ) {
		for ( var i = 0; i < folders.length; i++ ) {
			if ( folders[i].id == campaign_folder_id ) {
				filter = sprintf(campaign_str_filter_folder, folders[i].folder);
				break;
			}
		}
	} else if ( listid ) {
		for ( var i = 0; i < lists.length; i++ ) {
			if ( lists[i].id == listid ) {
				filter = lists[i].name;
				break;
			}
		}
	} else if ( campaign_list_id ) {
		for ( var i = 0; i < lists.length; i++ ) {
			if ( lists[i].id == campaign_list_id ) {
				filter = lists[i].name;
				break;
			}
		}
	} else if ( campaign_filter_id ) {
		filter = campaign_str_filter_search;
	}
	if ( filter ) filter = ' - ' + filter;
	var str_header = filter + " (" + count + ")";
	return str_header;
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function campaign_list_paginate(offset) {
	$J("#campaign_list").hide();
	$J("#campaign_list_count").hide();
	$J("#list_paginator").hide();
	$J("#list_noresults").hide();
	$J("#list_loader").show();

	//ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, '', offset, this.limit, campaign_filter_id);
	ac.get(this.ajaxAction, [ this.id, '09', offset, this.limit, campaign_filter_id, 0, null, 0 ], paginateCBary);
}

// This function should not be called directly; it is called only by paginator's callback
function campaign_list_tabelize(rows, offset) {
	// hide the loader, we're done loading
	$J("#list_loader").hide();
	// set new count in title (h2)
	$J("#campaign_list_count").html(campaign_list_header()).show();
	// first remove old timers (progress bars)
	for ( var i in ac_progress_bars ) {
		ac_progressbar_unregister(i);
	}
	// if nothing found
	if (rows.length < 1) {
		/*
		if (!campaign_filter_id || campaign_filter_id == 0) {
			ac_ui_api_callback();
			window.location = 'main.php?action=campaign_new';
			return;
		}
		*/
		// We may have some rows left if we just deleted the last row.
		$J("#campaign_list").hide().empty();
		$J("#list_noresults").show();
		$J("#list_paginator").hide();
		return;
	}

	$J("#list_noresults").hide();
	$J("#list_paginator").show();

	// remove old rows
	$J("#campaign_list").show().empty();

	// append new rows
	for ( var i = 0; i < rows.length; i++ ) {
		$J("#campaign_list").append(campaign_row(rows[i]));

		// assign progress bars
		var divId = 'campaign_progress' + rows[i]['id'];
		if ( !$(divId) ) continue;
		//if ( rows[i]['status'] != 2 ) continue;
		if ( !ac_array_has([2, 3], rows[i]['status']) ) continue;
		//if ( rows[i]['processid'] != 0 ) continue;
		var processId = rows[i]['processid'];
		var percentage = rows[i]['send_amt'] / rows[i]['total_amt'];
		var remaining  = rows[i]['total_amt'] - rows[i]['send_amt'];

		ac_progressbar_register(
			divId,
			processId,
			percentage * 100,
			( remaining > 0 && processId != 0 ? 10 : 0 ), // second interval
			true, // spawn it if stalled
			campaign_progress_ihook,
			true // just value, not a bar
		);
		// turn off any completed ones
		if ( remaining == 0 ) {
			ac_progressbar_unregister(divId);
		}
	}

	$J('li.campaignrow:last').addClass('lastchild');

	// apply new
	$J("a[rel=tip]").twipsy({gravity: 'sw'});
	$J('.slashc-img-zoom-pan').slashcImgZoomPan();
	$J("#campaign_list li.campaignrow")
		//.hoverIntent(
		.hoverIntent(
			function() { campaign_hover_on(this); },
			function() { campaign_hover_off(this); }
		)
		.draggable({
			//appendTo: "body",
			//handle: "a.title",
			cancel: "div.folder",
			cursor: "move",
			cursorAt: { top: 0, left: 0 },
			helper: function( event ) {
				return $J("<div class='ui-widget-header'><span>" + $J("a.title", this).html() + "</span></div>");
			}
		})
	;
}


function campaign_row(row) {
	var url = campaign_row_url(row);
	var str_type = campaign_type_strings[row.type];
	var str_status = campaign_row_status(row);
	class_small = row.type;
	if ( row.status == 7 && showTicket && typeof row.approval_ticket != 'undefined' && row.approval_ticket ) {
		class_small = 'alert';
	} else if ( row.status == 1 && row.type == 'single' ) {
		class_small = 'scheduled';
	} else if ( row.status == 0 ) {
		class_small = 'draft';
	}
	var class_li = '';
	if ( row.status == 7 ) {
		class_li = 'status_pending_approval';
	} else if ( row.status == 1 && row.type == 'single' ) {
		class_li = 'status_scheduled';
	} else if ( row.status == 0 && row.sendid > 0 && typeof row.declined != 'undefined' && row.declined && showTicket ) {
		class_li = 'status_declined';
	} else if ( row.status == 0 ) {
		class_li = 'status_draft';
	}
	var ttip = '';
	var shortname = ac.str.shorten(row.name, 45);
	if ( shortname != row.name ) {
		ttip = ' data-placement="above" rel="tip" data-original-title="' + row.name + '"';
	}
	var buttons = campaign_row_buttons(row);
	var foldertitle = campaign_str_folder_innone;
	if ( row.folders ) {
		if ( row.folders.length > 1 ) {
			foldertitle = sprintf(campaign_str_folder_inmany, row.folders.length);
		} else if ( row.folders.length == 1 ) {
			var n = '';
			for ( var i = 0; i < folders.length; i++ ) {
				if ( folders[i].id == row.folders[0] ) {
					n = ac.str.shorten(folders[i].folder, 30);
					break;
				}
			}
			foldertitle = sprintf(campaign_str_folder_inone, n);
		}
	}
	var r  = '';
	r += '	<li id="campaignrow_' + row.id + '" class="campaignrow ' + class_li + '">';
	r += '		<div class="imgwrap">';
	r += '			<div class="img">';
	if ( row.hasimage ) {
	r += '			<span class="slashc-img-zoom-pan">';
	r += '				<img alt="' + row.name + '" src="' + row.screenshot + '" width="103" />';
	r += '				<img alt="' + row.name + '" src="' + row.screenshot + '" />';
	r += '			</span>';
	} else {
	r += '			<img alt="' + row.name + '" src="' + row.screenshot + '" width="103" height="73" border="0" />';
	}
	r += '			</div>';
	r += '			<div class="avatars">';
	r += stream_build(row.avatars, 'nw');
	r += '			</div>';
	r += '		</div>';
	r += '		<div class="campaigninfo">';
	r += '			<div class="folder" style="display:none;">';
	r += '				<a href="#" onclick="campaign_folder_show(this);return false;">' + foldertitle + '</a>';
	r += '				<ul class="folders" style="display:none;">';
	for ( var i = 0; i < folders.length; i++ ) {
		var f = folders[i];
		var infolder = ac_array_has(row.folders, f.id);
		var checked = infolder ? 'checked="checked"' : '';
	r += '					<li title="' + f.folder + '">';
	r += '						<label>';
	r += '							<input name="folder' + f.id + '" type="checkbox" ' + checked + ' onclick="if(this.checked) campaign_folder_add(' + f.id + ',' + row.id + ');else campaign_folder_remove(' + f.id + ',' + row.id + ');">';
	r += '							<span>' + ac.str.shorten(f.folder, 30) + '</span>';
	r += '						</label>';
	r += '					</li>';
	}
	r += '				</ul>';
	r += '			</div>';
	r += '			<a class="title" href="' + url + '"' + ttip + '>' + shortname + '</a>';
	r += '			<small class="' + class_small + '" data-placement="above" rel="tip" data-original-title="' + str_type + '">' + str_status + '</small>';
	r += '			<div class="buttons">';
	for ( var i = 0; i < buttons.length; i++ ) {
		var button = buttons[i];
		var burl = button.url;
		if ( typeof button.click  != 'undefined' && burl == '#'   ) burl  = '#" onclick="' + button.click;
		if ( typeof button.newwin != 'undefined' && button.newwin ) burl += '" target="_blank'
		//if ( ac_array_has(['report'], button.type) ) burl += '" style="display:none;'
	r += '				<a class="btnsmall ' + button.type + '" href="' + burl + '" id="btn_' + button.type + '_' + row.id + '"><span>' + button.label + '</span></a>';
	}
	r += '				<a class="btnsmall delete" href="#" onclick="campaign_delete_open(' + row.id + ');return false;" style="display:none;">X</a>';
	r += '			</div><!--//buttons-->';
	r += '		</div><!--//campaigninfo-->';
	r += '	</li>';
	return r;
}

function campaign_row_status(row) {
	// default string will be a status string
	var str_status = campaign_status_strings[row.status];
	// if draft (quick&simple)
	if ( row.status == 0 ) {
		// append "last saved at"
		var lsd = row.mdate ? row.ago_mdate : row.ago_cdate;
		str_status += ' - ' + sprintf(campaign_str_status_lastsave, lsd);
		// just check if it was redrafted
		if ( row.sendid > 0 && typeof row.declined != 'undefined' && row.declined && showTicket ) {
			str_status += ' (' + campaign_str_status_redraft;
			if ( typeof row.approval_ticket != 'undefined' && row.approval_ticket ) {
				str_status += ' <a href="' + row.approval_ticket + '" target="_blank">' + campaign_str_status_redraft_ticket + '</a>';
			}
			str_status += ')';
		}
		return str_status;
	}

	/* TYPE-BASED STATUSES */
	// if not a special-type, then list "completed"
	if ( !( ac_array_has([1, 7], row.status) && !ac_array_has(['responder', 'reminder'], row.type) ) ) {
		if ( row.total_amt > 0 && row.ldate ) {
			str_status = sprintf(campaign_str_status_completed, ac.str.number_format(row.total_amt, 0, decimalDelim, commaDelim), row.ago_ldate);
		}
	}
	// if scheduled single campaign
	if ( row.status == 1 && ac_array_has(['single', 'text', 'recurring'], row.type) && row.sdate ) {
		str_status = sprintf(campaign_str_status_scheduled, sql2date(row.sdate).format(dateformat), sql2date(row.sdate).format(timeformat));
	}
	// if reminder, show field & offsets
	if ( row.type == 'reminder' ) {
		var field = row.reminder_field_name;
		offset = ( row.reminder_offset > 0 ? row.reminder_offset_sign + row.reminder_offset + row.reminder_offset_type : '' );
		str_status = sprintf(campaign_str_status_reminder, field, offset);
	}
	// if responder, show offsets
	if ( row.type == 'responder' ) {
		if ( row.responder_offset == 0 ) {
			str_status = campaign_str_status_responder_instant;
		} else {
			offset = row.responder_offset + ' hour(s)';
			str_status = sprintf(campaign_str_status_responder_delayed, offset);
		}
	}
	// if activerss, show interval
	if ( row.type == 'activerss' && row.status != 5 ) {
		offset = campaign_str_intervals[row.activerss_interval].toLowerCase();
		str_status = sprintf(campaign_str_status_activerss, offset);
	}
	// if recurring, show the interval
	if ( row.type == 'recurring' && row.status != 5 ) {
		offset = campaign_str_intervals[row.recurring].toLowerCase();
		str_status = sprintf(campaign_str_status_recur, offset);
	}

	/* STATUS-BASED STATUSES */
	// stopped campaign
	if ( row.status == 4 ) {
		str_status = "This campaign was stopped before it completed sending.";
	}
	// currently sending
	//if ( row.status == 2 ) {
	if ( ac_array_has([2, 3], row.status) ) {
		var val = row.send_amt / row.total_amt * 100;
		if ( val < 0   ) val = 0;
		if ( val > 100 ) val = 100;
		var progressbar = '<span id="campaign_progress' + row.id + '">' + ( Math.round(val * 100) / 100 ) + '%</span>';
		if ( row.mail_send == 1 ) {
			var str_status = campaign_str_status_cleanup;
		} else if ( row.mail_transfer == 1 ) {
			if ( row.type == 'split' && row.split_type != 'even' && row.split_winner_awaiting == 1 ) {
				var str_status = sprintf(campaign_str_status_winner, progressbar);
			} else {
				if ( row.status == 3 ) {
					var str_status = sprintf(campaign_str_status_send_paused, progressbar);
				} else {
					var str_status = sprintf(campaign_str_status_send, progressbar);
				}
			}
		} else {
			if ( row.status == 3 ) {
			var str_status = sprintf(campaign_str_status_transfer_paused, progressbar);
			} else {
			var str_status = sprintf(campaign_str_status_transfer, progressbar);
			}
		}
	}
	/*
	// paused campaign
	if ( row.status == 3 ) {
		str_status = "This campaign is paused; you can continue sending it at any time by Resuming it";
	}
	// disabled (responder)
	if ( row.status == 6 ) {
		str_status = "This campaign is currently disabled.";
	}
	*/
	// pending approval
	if ( row.status == 7 && showTicket ) {
		if ( typeof row.approval_ticket != 'undefined' && row.approval_ticket ) {
			str_status = campaign_str_status_ticket;
		} else {
			str_status = campaign_str_status_approval;
		}
	}

	return str_status;
}

function campaign_row_continue(row) {
	var url_continue;
	switch (row.laststep) {
		case "type":
		default:
			url_continue = sprintf("main.php?action=campaign_new&id=%s", row.id);
			break;

		case "list":
			url_continue = sprintf("main.php?action=campaign_new_list&id=%s", row.id);
			break;

		case "template":
			url_continue = sprintf("main.php?action=campaign_new_template&id=%s", row.id);
			break;

		case "message":
			url_continue = sprintf("main.php?action=campaign_new_message&id=%s", row.id);
			break;

		case "text":
			url_continue = sprintf("main.php?action=campaign_new_text&id=%s", row.id);
			break;

		case "splitmessage":
			url_continue = sprintf("main.php?action=campaign_new_splitmessage&id=%s", row.id);
			break;

		case "splittext":
			url_continue = sprintf("main.php?action=campaign_new_splittext&id=%s", row.id);
			break;

		case "summary":
			url_continue = sprintf("main.php?action=campaign_new_summary&id=%s", row.id);
			break;

		case "finish":
			url_continue = sprintf("main.php?action=campaign_new_finish&id=%s", row.id);
			break;
	}
	return url_continue;
}

function campaign_row_url(row) {
	if ( row.status == 0 /*|| ( row.status == 1 && ac_array_has(['responder', 'reminder'], row.type) )*/ ) {
		return campaign_row_continue(row);
	}
	var url_report = sprintf("main.php?action=report_campaign&id=%d#general-01-0-0-0", row.id);
	return url_report;
}

function campaign_row_buttons(row) {
	var url_new = sprintf("main.php?action=campaign_new&campaignid=%d", row.id);
	var url_continue = campaign_row_continue(row);
	var url_report = sprintf("main.php?action=report_campaign&id=%d#general-01-0-0-0", row.id);

	var buttons = [];

	// REPORTS
	//if ( !ac_array_has([0, 7], row.status) && !( row.status == 1 && !ac_array_has(['responder', 'reminder'], row.type) ) ) {
	if ( row.status != 0 && !( ac_array_has([1, 7], row.status) && !ac_array_has(['responder', 'reminder'], row.type) ) ) {
		if (ac_js_admin.pg_reports_campaign) {
			buttons.push({
				type   : 'report',
				url    : url_report,
				label  : jsOptionReport
			});
		}
	}

	if (row.survey != "" && typeof(row.surveymonkey_reports_url) != "undefined" && row.surveymonkey_reports_url != "") {
		if (ac_js_admin.pg_reports_campaign) {
			buttons.push({
				type   : 'survey',
				url    : row.surveymonkey_reports_url,
				label  : campaign_str_survey1
			});
		}
	}

	return buttons;
}

function campaign_delete_open(id) {
	// save the id somewhere
	campaign_delete_id = id;
	// open modal
	$J('#deleteModal').modal('show');
}

function campaign_delete(id) {
	ac_ui_api_call(jsDeleting);
	ac.get(
		"campaign.campaign_delete",
		[ id ],
		function (data) {
			campaign_list_rebuild_callback(data);
			$J('#deleteModal').modal('hide');
		}
	);
}



function campaign_reuse_open(id) {
	campaign_reuse_id = id;
	$J('#reuseModal').modal('show');
	$J('#campaign_use_reuse').attr('checked','checked');
	return false;
}

function campaign_reuse() {
	ac_ui_api_call(jsLoading);
	var form = {
		action: $J('#reuseModal input[name=action]:checked').val(),
		id: campaign_reuse_id
	}
	// redirect
	var action = ( form.action == 'reuse' ? 'campaign_new' : 'campaign_use' );
	var go2 = 'main.php?action=' + action + '&copyid=' + form.id;
	go2 += ( form.action == 'reuse' ? '' : '&filter=' + form.action );
	//if ( form.action != 'reuse' ) go2 += '&filter=' + form.action;
	window.location.href = go2;
}


function campaign_resume(id) {
	return campaign_list_status(id, 2);
}

function campaign_pause(id) {
	return campaign_list_status(id, 3);
}

function campaign_stop(id) {
	return campaign_list_status(id, 4);
}

function campaign_run(id) {
	return campaign_list_status(id, 1);
}

function campaign_enable(id) {
	return campaign_list_status(id, 1, true);
}

function campaign_disable(id) {
	return campaign_list_status(id, 6);
}

function campaign_list_status(id, status, enable) {
	if ( status == 6 ) {
		if ( !confirm(campaign_str_alert_scheduled + '\n\n' + campaign_str_confirm_disable) ) return false;
		ac_ui_api_call(jsDisabling);
	} else if ( status == 4 ) {
		if ( !confirm(campaign_str_confirm_stop) ) return false;
		ac_ui_api_call(jsStopping);
	} else if ( status == 3 ) {
		if ( !confirm(campaign_str_confirm_pause) ) return false;
		ac_ui_api_call(jsPausing);
	} else if ( status == 2 ) {
		if ( !confirm(campaign_str_confirm_resume) ) return false;
		ac_ui_api_call(jsResuming);
	} else {
		if ( !enable ) {
			if ( !confirm(campaign_str_confirm_run) ) return false;
		} else {
			if ( !confirm(campaign_str_confirm_enable) ) return false;
		}
		ac_ui_api_call(jsStarting);
	}
	ac.get("campaign.campaign_status", [id, status], campaign_list_rebuild_callback);
	return false;
}

function campaign_list_rebuild_callback(ary) {
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		// rebuild this list
		paginators[1].paginate(paginators[1].offset);
		// start the process pickup tool just in case it should be picked up
		$J.get('process.php?dontlog=1');
	} else {
		ac_error_show(ary.message);
	}
}

function campaign_now(id) {
	if ( !confirm(campaign_str_confirm_send) ) return false;
	ac_ui_api_call(jsStarting);
	ac.get("campaign.campaign_now", [id], campaign_list_rebuild_callback);
	return false;
}


function campaign_list_clear() {
	/* listid  : */ $J('#JSListManager').val(0); $J('#sidemenu_list_filter').hide();
	/* status  : */ $J('#JSStatusManager').val(''); $J('#sidemenu_status_filter').hide();
	/* type    : */ $J('#JSTypeManager').val(''); $J('#sidemenu_type_filter').hide();
	/* qsearch : */ $J('.filterSearch input[type=text]').val('');
	/* folder  : */ campaign_folder_id = 0;
}

function campaign_list_search() {
	var post = {
		listid  : $J('#JSListManager').val(),
		status  : $J('#JSStatusManager').val(),
		type    : $J('#JSTypeManager').val(),
		qsearch : $J('.filterSearch input[type=text]').val(),
		folder  : campaign_folder_id
	};
	campaign_list_id = post.listid;
	// hit up filterer
	ac.post('campaign.campaign_filter_post', post, campaign_list_search_cb);
}

function campaign_list_search_cb(ary) {
	campaign_filter_id = ary.filterid;
	paginators[1].paginate(0);
}


function campaign_progress_ihook(ary) {
	ary.completed = parseInt(ary.completed, 10);
	ary.total = parseInt(ary.total, 10);
	if ( ary.completed < ary.total ) {
		// still running
		// do nothing
	} else {
		// completed
		// rebuild this list
		paginators[1].paginate(paginators[1].offset);
	}
}

function campaign_folder_add(fid, cid) {
	ac_ui_api_call(jsAdding);
	ac.get(
		'folder.folder_campaign_add',
		[ fid, cid ],
		function(ary) {
			ac_ui_api_callback();

			if (ary.succeeded != "0") {
				// update folder count
				var curcnt = parseInt($J("#folder_" + fid + " div.alert").html(), 10);
				var newcnt = curcnt + 1;
				//$J("#folder_" + fid + " > a div.alert").html(ac.str.number_format(newcnt, 0, decimalDelim, commaDelim));
				$J("#folder_" + fid + " > a div.alert").remove();
				$J("#folder_" + fid + " > a").append('<div class="alert grey">' + ac.str.number_format(newcnt, 0, decimalDelim, commaDelim) + '</div>');

				// update campaign count
				$J("#campaignrow_" + cid + " div.folder ul.folders input[name=folder" + fid + "]").attr('checked', 'checked');
				var newcnt = $J("#campaignrow_" + cid + " div.folder ul.folders input[type=checkbox]").filter(':checked').length;
				var str_folder = newcnt;
				if ( newcnt > 1 ) {
					str_folder = sprintf(campaign_str_folder_inmany, newcnt);
				} else {
					str_folder = sprintf(campaign_str_folder_inone, ac.str.shorten(ary.folder, 30));
				}
				$J("#campaignrow_" + cid + " div.folder a")
					.html(str_folder)
					.removeClass("foldershown")
				;
				$J("#campaignrow_" + cid + " div.folder ul.folders").hide();

				ac_result_show(ary.message);
			} else {
				ac_error_show(ary.message);
			}
		}
	);
}

function campaign_folder_remove(fid, cid) {
	ac_ui_api_call(jsRemoving);
	ac.get(
		'folder.folder_campaign_remove',
		[ fid, cid ],
		function(ary) {
			ac_ui_api_callback();

			if (ary.succeeded != "0") {
				// update folder count
				var curcnt = parseInt($J("#folder_" + fid + " div.alert").html(), 10);
				var newcnt = curcnt - 1;
				//$J("#folder_" + fid + " > a div.alert").html(ac.str.number_format(newcnt, 0, decimalDelim, commaDelim));
				$J("#folder_" + fid + " > a div.alert").remove();
				$J("#folder_" + fid + " > a").append('<div class="alert grey">' + ac.str.number_format(newcnt, 0, decimalDelim, commaDelim) + '</div>');

				// update campaign count
				$J("#campaignrow_" + cid + " div.folder ul.folders input[name=folder" + fid + "]").attr('checked', false);
				var newcnt = $J("#campaignrow_" + cid + " div.folder ul.folders input[type=checkbox]").filter(':checked').length;
				var str_folder = newcnt;
				if ( newcnt > 1 ) {
					str_folder = sprintf(campaign_str_folder_inmany, newcnt);
				} else if ( newcnt == 1 ) {
					var n = $J("span", $J("#campaignrow_" + cid + " div.folder ul.folders input[type=checkbox]").filter(':checked').parent()).html();
					str_folder = sprintf(campaign_str_folder_inone, ac.str.shorten(n, 12));
				} else {
					str_folder = campaign_str_folder_innone;
				}
				$J("#campaignrow_" + cid + " div.folder a")
					.html(str_folder)
					.removeClass("foldershown")
				;
				$J("#campaignrow_" + cid + " div.folder ul.folders").hide();

				ac_result_show(ary.message);
			} else {
				ac_error_show(ary.message);
			}
		}
	);
}

function campaign_folder_show(obj) {
	if ( $J("ul.folders input[type=checkbox]", obj.parentNode).length == 0 ) {
		folder_add_open();
		return;
	}
	$J("ul.folders", obj.parentNode).toggle();
	if ( $J(obj).hasClass("foldershown") ) {
		$J(obj).removeClass("foldershown");
	} else {
		$J(obj).addClass("foldershown");
	}
}

/*
statuses:
0 - not hovered
1 - fading in
2 - hovered
3 - fading out
*/
var campaign_hovered = {};
function campaign_hover_on(obj) {
	var id = obj.id.replace('campaignrow_');
	if ( typeof campaign_hovered[id] == 'undefined' ) {
		campaign_hovered[id] = { fadein : 0, fadeout : 0, hovered : false };
	}
	if ( campaign_hovered[id].hovered ) return; // if already hovered, stop
	campaign_hovered[id].hovered = true; // mark as hovered
	if ( campaign_hovered[id].fadein > 0 && campaign_hovered[id].fadein > campaign_hovered[id].fadeout ) {
		return;
	}
	campaign_hovered[id].fadein++; // add fading in
	// fade in
	if ( $J("ul.folders input[type=checkbox]", obj).length > 0 ) {
		$J("div.folder", obj).fadeIn(500);
	}
	//$J("a.btnsmall.report", obj).fadeIn(500);
	$J("a.btnsmall.delete", obj).fadeIn(500, function() { campaign_hovered[id].fadein--; }); // reduce fading in
}

function campaign_hover_off(obj) {
	var id = obj.id.replace('campaignrow_');
	if ( typeof campaign_hovered[id] == 'undefined' ) {
		campaign_hovered[id] = { fadein : 0, fadeout : 0, hovered : false };
	}
	if ( !campaign_hovered[id].hovered ) return; // if already hovered, stop
	campaign_hovered[id].hovered = false; // mark as hovered
	if ( campaign_hovered[id].fadeout > 0 && campaign_hovered[id].fadeout > campaign_hovered[id].fadein ) {
		return;
	}
	campaign_hovered[id].fadeout++; // add fading out
	// fade out
	if ( $J("ul.folders input[type=checkbox]", obj).length > 0 ) {
		$J("div.folder", obj).fadeOut(250);
	}
	//$J("a.btnsmall.report", obj).fadeOut(250);
	$J("a.btnsmall.delete", obj).fadeOut(250, function() { campaign_hovered[id].fadeout--; }); // reduce fading out
}
