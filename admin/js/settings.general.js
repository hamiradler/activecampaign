{literal}

function settings_general_load() {
	$("settings_general").className = "ac_block";
}

function public_rewrite_check(checked) {
	if (checked)
		$("public_rewrite_tbody").className = "ac_table_rowgroup";
	else
		$("public_rewrite_tbody").className = "ac_hidden";
}

function public_rewrite_htaccess() {
	$('htaccess').style.display = 'block';
}

var user = {};

user.resetapi = function(id) {
	ac.get("User::resetapi", [id], function(data) {
		$J("input[name=apikey]")
			.val(data)
			.css("background-color", "#1475a7")
			.animate({ backgroundColor: "#fff" }, 1500);
	});
};

{/literal}
