<?php
// require main include file
require_once(dirname(__FILE__) . '/prepend.inc.php');

/*
	WHITELIST
*/
$allowed = array(
	"open_pie" => true,
	"client_pie" => true,
	"link_bydate" => true,
	"link_byhour" => true,
	"link_rate" => true,
	"read_bydate" => true,
	"read_byhour" => true,
	"read_byweek" => true,
	"read_rate" => true,
	"subscribed_bydate" => true,
	"unsubscribed_bydate" => true,
	"unsubscribed_rate" => true,
	"emails_bydate" => true,
	"campaigns_bydate" => true,
	"interaction_bydate" => true,
	"interaction_rate" => true,
	"subscriber_rate" => true,
);

$shareallowed = array(
	"link_bydate" => true,
	"link_byhour" => true,
	"read_bydate" => true,
	"read_byhour" => true,
	"open_pie" => true,
);

// Preload the language file
ac_lang_get('admin');

if (isset($_GET["hash"]) && isset($_SESSION["em_sharedreport_hashes"]) && isset($_SESSION["em_sharedreport_hashes"][$_GET["hash"]])) {
	$_GLOBALS["admin"] = ac_admin_get_totally_unsafe(1);
	$allowed = $shareallowed;
} else {
	// check for basic admin privileges
	if ( !ac_admin_isadmin() ) {
		exit;
	}
}

// require ajax include
require_once ac_global_includes("graph.php");

?>
