<?php

class Config {
	// Static stuff, mainly for caching.

	private static $cache = array();
	private static $idcache = array();

	public static function lookup($keyname) {
		if (isset(self::$cache[$keyname]))
			return self::$cache[$keyname];

		$esc = ac_sql_escape($keyname);
		$row = ac_sql_select_row("SELECT id, val FROM #config WHERE keyname = '$esc'");

		self::$cache[$keyname] = $row['val'];
		self::$idcache[$keyname] = $row['id'];

		return $row['val'];
	}

	public static function assign($keyname, $val) {
		self::$cache[$keyname] = $val;

		if (!isset(self::$idcache[$keyname]))
			self::$idcache[$keyname] = 0;

		$expl = explode(".", $keyname);

		$up = array(
			"id" => self::$idcache[$keyname],
			"keyname" => $keyname,
			"section" => $expl[0],
			"item" => $expl[1],
			"val" => $val,
		);

		if (isset($GLOBALS['old_upgrade'])) {
			unset($up["section"]);
			unset($up["item"]);
		}

		ac_sql_insert_update("#config", $up);
	}	

	public static function assign_post() {
		self::assign(ac_http_param("key"), ac_http_param("val"));
	}
	
	// Instance vars
	
	private $keys;		// The array of key/val pairs
	private $ids;
	private $changed;
	private $section;

	// Instance methods

	public function __construct($section) {
		$this->section = $section;
		$this->keys = array();
		$this->changed = array();

		$section_esc = ac_sql_escape($section);
		$rs = ac_sql_query("SELECT id, item, val FROM #config WHERE section = '$section_esc'");

		while ($row = ac_sql_fetch_assoc($rs)) {
			$this->keys[$row['item']] = $row['val'];
			$this->ids[$row['item']] = $row['id'];
		}
	}

	public function __get($k) {
		return isset($this->keys[$k]) ? $this->keys[$k] : "";
	}

	public function __set($k, $v) {
		$this->keys[$k] = $v;
		$this->changed[$k] = $v;

		if (!isset($this->ids[$k]))
			$this->ids[$k] = 0;
	}

	public function export() {
		return $this->keys;
	}

	public function save() {
		foreach ($this->changed as $k => $v) {
			$up = array(
				"id" => $this->ids[$k],
				"keyname" => $this->section . "." . $k,
				"section" => $this->section,
				"item" => $k,
				"val" => $v,
			);

			$keyname = ac_sql_escape($this->section . "." . $k);
			ac_sql_insert_update("#config", $up);
		}

		$this->changed = array();
	}
}

?>
