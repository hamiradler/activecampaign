<?php

class Cache {
	private static $hit = array();
	private static $miss = array();

	private static $qhit = array();

	private static $avail = array();

	public static function lookup($class, $id) {
		$id = (int)$id;

		if (isset(self::$hit[$class]) && isset(self::$hit[$class][$id]))
			return self::$hit[$class][$id];

		// Ok -- we don't have it.  See if we know it's missing or not.
		if (isset(self::$miss[$class]) && isset(self::$miss[$class][$id]))
			return null;

		// We don't know that it's missing, so let's try looking it up.
		$sel = new Select($class);
		$sel->where("id = '$id'");

		$rval = $sel->next();

		if ($rval) {
			if (!isset(self::$hit[$class]))
				self::$hit[$class] = array();

			self::$hit[$class][$id] = $rval;
		} else {
			if (!isset(self::$miss[$class]))
				self::$miss[$class] = array();

			self::$miss[$class][$id] = true;
		}

		return $rval;
	}

	public static function query($class, $qstr) {
		if (isset(self::$qhit[$class]) && isset(self::$qhit[$class][$qstr]))
			return self::$qhit[$class][$qstr];

		return null;
	}

	public static function setquery($class, $qstr, $row) {
		if (!isset(self::$qhit[$class]))
			self::$qhit[$class] = array();
		if (!isset(self::$qhit[$class][$qstr]))
			self::$qhit[$class][$qstr] = array();

		self::$qhit[$class][$qstr][] = $row;
	}

	public static function clearquery($class, $qstr) {
		if (isset(self::$qhit[$class]) && isset(self::$qhit[$class][$qstr]))
			self::$qhit[$class][$qstr] = array();
	}

	// Return every row that's "available" to you, the user, based on what lists you have access to.
	public static function available($class, $limit = 0, $offset = 0) {
		if (isset(self::$avail[$class]))
			return self::$avail[$class];

		$table = class_table($class);
		$admin = ac_admin_get();

		$sel = new Select($class);
		$sel->from("$table t");

		if ($limit > 0)
			$sel->limit($limit, $offset);

		if ($admin["id"] != 1) {
			$relf = str_replace("#", "", $table) . "id";
			$liststr = implode("','", $admin["lists"]);

			$sel->from(
				"{$table}_list r"
			);
			$sel->where(
				"t.id = r.$relf",
				"r.listid IN ('$liststr')"
			);
		}

		self::$avail[$class] = $sel->rows();
		return self::$avail[$class];
	}

	// Assign one specific item in the cache, possibly overwriting anything that used to be there.
	public static function set($class, $id, $val) {
		if (!isset(self::$hit[$class]))
			self::$hit[$class] = array();

		self::$hit[$class][$id] = $val;
	}

	// Clear out any one value here (e.g. if you update some object).
	public static function clear($class, $id) {
		if (isset(self::$hit[$class]))
			unset(self::$hit[$class][$id]);
	}

	// Clear everything out for a certain class.
	public static function purge($class) {
		unset(self::$hit[$class]);
	}
}

?>
