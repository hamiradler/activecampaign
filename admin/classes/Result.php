<?php

class Result {
	private $table;
	private $fields;
	private $id;

	public function __construct($row = array()) {
		$this->table = class_table(get_class($this));
		foreach ($row as $k => $v) {
			if ($k == "id")
				$this->id = $v;
			else
				$this->fields[$k] = $v;
		}
	}

	// What to do if we assign an unknown member variable, or access an unknown one.

	function __get($k) {
		if ($k == "id")
			return $this->id;

		if (isset($this->fields[$k]))
			return $this->fields[$k];

		// Lazy access.  If we don't have this field, we'll look it up.  But try to avoid this if possible.
		$esc = ac_sql_escape($k);
		$this->fields[$k] = ac_sql_select_one("SELECT $esc FROM {$this->table} WHERE id = '{$this->id}'");
		return $this->fields[$k];
	}

	function __set($k, $v) {
		$this->fields[$k] = $v;
	}

	// Export the contents of this object as a PHP array suitable to be 
	// returned with JSON.
	public function export() {
		$rval = $this->fields;
		$rval["id"] = $this->id;

		return $rval;
	}

	// Common mysql functions...

	public function delete() {
		ac_sql_delete($this->table, "id = '{$this->id}'");
		Cache::clear(get_class($this), $this->id);
	}

	public function update() {
		ac_sql_update($this->table, $this->fields, "id = '{$this->id}'");
		Cache::clear(get_class($this), $this->id);
	}

	public function replace() {
		$repl = $this->fields;
		$repl["id"] = $this->id;

		ac_sql_replace($this->table, $this->fields);
		Cache::clear(get_class($this), $this->id);
	}

	public function insert() {
		ac_sql_insert($this->table, $this->fields);
		$this->id = ac_sql_insert_id();
		Cache::set(get_class($this), $this->id, $this);
	}

	// "Require" a variable number of fields, looking them up in batch if they aren't there.
	// E.g., $a->req("name", "email") will look both fields up in one query.
	// Doing $a->name and $a->email individually will force separate lookups if neither are there.

	public function req() {
		$args = func_get_args();
		$need = array();

		foreach ($args as $arg) {
			if ($arg == "id")
				continue;

			if (!isset($this->fields[$arg])) {
				$esc = ac_sql_escape($arg);
				$need[] = $esc;
			}
		}

		if (count($need) > 0) {
			$needstr = implode(", ", $need);
			$row = ac_sql_select_row("SELECT $needstr FROM {$this->table} WHERE id = '{$this->id}'");
			if ($row) {
				foreach ($row as $k => $v) {
					$this->fields[$k] = $v;
				}
			}
		}
	}
}

?>
