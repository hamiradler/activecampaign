<?php

class Screenshot extends Result {
	// Flags
	const F_BIG = 1;

	// Variables
	public $big;

	public function __construct($row = array()) {
		parent::__construct($row);
		$this->big = false;
	}
	
	// Static functions
	// --

	// Return the URL path to what any of our images should be.
	public static function urlpath($default = false) {
		return ac_site_plink("images/_screenshot_");
	}

	public static function defaultfile($target, $name = '') {
		if ($target == "template" && $name != "") {
			return self::urlpath(true) . "/template-$name.jpg";
		}
		return self::urlpath(true) . "/default-$target.gif";
	}

	public static function bytarget($target, $targetid) {
		$sel = new Select("Screenshot");
		$sel->limit(1);
		$sel->where(
			array("target = '%s'", $target),
			array("targetid = '%s'", $targetid)
		);

		return $sel->next();
	}

	public static function geturl($target, $targetid, $flags = 0) {
		$shot = self::bytarget($target, $targetid);

		if (!$shot) {
			if ($flags & self::F_BIG)
				$target .= "-big";
			return self::defaultfile($target);
		} else {
			return $shot->url();
		}
	}

	// Non-static functions
	// --

	public function file() {
		$this->req("target", "targetid");
		return md5(sprintf("%s%d", $this->target, $this->targetid)) . ".jpg";
	}

	public function exists() {
		return file_exists(ac_base("images/_screenshot_/{$this->file()}"));
	}

	public function url() {
		$this->req("target", "targetid", "importname");
		if ($this->exists())
			return Screenshot::urlpath() . "/" . $this->file();

		if ($this->big)
			return Screenshot::defaultfile($this->target . "-big", $this->importname);

		return Screenshot::defaultfile($this->target, $this->importname);
	}

	// Signal the target table that we don't need to request another screenshot.
	public function stopwait() {
		$this->req("target", "targetid");
		if (in_array($this->target, array("campaign", "template", "form"))) {
			$up = array(
				"waitpreview" => 0,
			);
			ac_sql_update('#' . $this->target, $up, "id = '{$this->targetid}'");
		}
	}

	public function writedata() {
			@file_put_contents(ac_base("images/_screenshot_/{$this->file()}"), $this->data);
	}

	public function insert() {
		$this->writedata();
		$this->data = "";
		parent::insert();
		$this->stopwait();
	}

	public function update() {
		if ($this->data) {
			$this->writedata();
			$this->data = "";
		}
		parent::update();
		$this->stopwait();
	}
}

?>
