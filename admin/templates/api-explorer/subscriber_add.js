<script type="text/javascript">

	{literal}

	function subscriber_add_list_check(is_checked, list_id) {
		$J("#subscriber_add_list_details_" + list_id).hide();
		if (is_checked) {
			$J("#subscriber_add_list_details_" + list_id).show();
		}
	}

	function subscriber_edit_list_check(is_checked, list_id) {
		$J("#subscriber_edit_list_details_" + list_id).hide();
		if (is_checked) {
			$J("#subscriber_edit_list_details_" + list_id).show();
		}
	}
	
	{/literal}

</script>