<body style="margin:0; padding:0; background:#f5f5f5; font-size:14px; font-family:Arial,Helvetica; color:#333333; line-height:20px;">
    <div style="padding:15px; background:#f5f5f5;">
        <p style="margin:0; margin-bottom:20px; padding:0; font-size:16px; font-family:Arial,Helvetica; line-height:26px;"><strong style="font-weight:bold;">{"Thank you for subscribing to %s!"|alang:'%LISTNAME%'}</strong></p>
        <p style="line-height:20px;">{"You or someone had subscribe to this list on %s using the address %s."|alang:'%SUBDATE%':'%EMAIL%'}</p>
        <p style="line-height:20px;">{"If you believe that this is a mistake and you did not intend on subscribing to this list, you can ignore this message and nothing else will happen."|alang}</p>
        <div style="margin:0; margin-top:35px; padding:0;">
        <div style="padding:5px; padding-bottom:20px; font-size:14px; background:#f4fbfe; border:1px solid #cbe1eb; color:#333333; font-family:Arial,Helvetica; text-align:center;">
            <p style="line-height:20px; margin-bottom:6px;"><strong style="font-weight:bold;">{"To confirm that you wish to be subscribed, please click the link below:"|alang}</strong></p>
            <div style="margin:10px; text-align:center;">
                <p style="width:100%; margin-top:20px; margin-right:auto; margin-bottom:0; margin-left:auto; width:272px;">
                    <a href="%CONFIRMLINK%" style="display:block; margin-top:20px; margin-right:auto; margin-bottom:0; margin-left:auto; padding-top:0px; width:270px; background:#83c700; border:1px solid #669b00; font-size:18px; color:#ffffff; text-decoration:none!important;">
                        <span style="display:block; width:100%; border-top:1px solid #e9ffbf; height:0px; line-height:0px;"></span>
                        <strong style="display:block; padding:10px; font-weight:bold; color:#ffffff;">{"Confirm My Subscription"|alang}</strong>
                    </a>
                    <span style="display:block; border-bottom:2px solid #bcd3dc; height:0px; line-height:0px;"></span>
                </p>
            </div>
        </div>
        </div>
    </div>
</body>