ALTER TABLE `em_campaign` ADD `activerss_url` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `activerss_interval` ;
ALTER TABLE `em_campaign` ADD `activerss_items` INT( 10 ) DEFAULT '10' NOT NULL AFTER `activerss_url` ;

ALTER TABLE `em_campaign_deleted` ADD `activerss_url` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `activerss_interval` ;
ALTER TABLE `em_campaign_deleted` ADD `activerss_items` INT( 10 ) DEFAULT '10' NOT NULL AFTER `activerss_url` ;