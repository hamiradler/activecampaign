# caching subscriber hashes from now on
ALTER TABLE `em_subscriber` ADD `hash` VARCHAR( 32 ) NOT NULL DEFAULT '' AFTER `ua`;
UPDATE `em_subscriber` SET `hash` = MD5(CONCAT(`id`, `email`)) WHERE 1;
ALTER TABLE `em_subscriber` ADD KEY `hash` (`hash`);

# adding new table for bounce logs
CREATE TABLE `em_bounce_log` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `tstamp` date NULL, `bounceid` INT( 10 ) NOT NULL DEFAULT '0', `subscriberid` INT ( 10 ) NOT NULL DEFAULT '0', `campaignid` INT ( 10 ) NOT NULL DEFAULT '0', `messageid` INT ( 10 ) NOT NULL DEFAULT '0', `codeid` INT( 10 ) NOT NULL DEFAULT '0', `email` VARCHAR( 128 ) NOT NULL DEFAULT '', `error` varchar(32) NOT NULL DEFAULT '', `source` longtext NULL, PRIMARY KEY (`id`), KEY `bounceid` (`bounceid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;

# adding new table for email account logs
CREATE TABLE `em_emailaccount_log` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `tstamp` date NULL, `emailid` INT( 10 ) NOT NULL DEFAULT '0', `email` VARCHAR( 128 ) NOT NULL DEFAULT '', `error` varchar(32) NOT NULL DEFAULT '', `source` longtext NULL, PRIMARY KEY (`id`), KEY `emailid` (`emailid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
