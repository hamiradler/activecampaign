<?php

// new tables have been added, do conversion stuff here

/*
	first backend and users,
	then lists and all related settings,
	then subscribers,
	then campaigns,
	then unsubscriptions,
*/

require_once(dirname(dirname(__FILE__)) . '/functions/em.php');
require_once ac_admin("functions/subscriber_action.php");	# For converting subscription rules

$completed = false;
$done = true;
$steps = 8;
$gd = false;

spit(sprintf(_a('Step %d out of %d'), ($updaterStep + 1), $steps), 'strong', 1);

// fetch old backend
$oldprefix = ( isset($GLOBALS['ac_updater_backend']) ? $GLOBALS['ac_updater_backend'] : '12all_' );
$sql = ac_sql_query("SELECT * FROM `{$oldprefix}backend`");
$backend = mysql_fetch_assoc($sql);



// find the first time the system was used
$sql = ac_sql_query("SELECT MIN(s_date) FROM `{$oldprefix}messages`");
list($cdate) = mysql_fetch_row($sql);
$cdate .= ' 00:00:00';



// fetch old lists
$lists = array();
$sql = ac_sql_query("SELECT * FROM `{$oldprefix}lists` ORDER BY `id`");
while ( $row = mysql_fetch_assoc($sql) ) {
	$lists[$row['id']] = $row;
}
$listids = implode("', '", array_keys($lists));



// fetch outlines (header/footer)
$outlines = ac_sql_select_box_array("
	SELECT
		`id`, `content`
	FROM
		`{$oldprefix}templates`
	WHERE
	(
		`type` = 'header'
	OR
		`type` = 'footer'
	)
	AND
	(
		`nl` IN ('$listids')
	OR
		`uni` != ''
	)
");



if ( $updaterStep == 0 ) {

	// some installs are missing this field
	@ac_sql_query("ALTER TABLE `12all_mod_sync` ADD `send_instant_responders` TINYINT( 1 ) NOT NULL DEFAULT '0';");

	// prepare blank groups
	$perms = array(
		// branding
		'brand_name' => '',
		'brand_logo' => '',
		'brand_copyright' => 0,
		'brand_links' => 0,
		'brand_footer' => '',
		'brand_footer_html' => '',
		'brand_version' => 0,
		'brand_demo'=> 0,

		// limits
		'lists_limit' => 0,
		'mails_limit' => 0,
		'mails_limit2' => 0,
		'users_limit' => 0,
		'dflt_subscribers_max' => 0,
		'dflt_upload_attach_max' => 0, // attachments

		// perms
		'list_create' => 0,
		'list_general' => 0,
		'list_optinoptout' => 0,
		'list_subscriberfields' => 0,
		'list_blockedaddr' => 0,
		'list_bounce_settings' => 0,
		'list_remove' => 0,
		'list_mailheaders' => 0,
		'list_popaccounts' => 0,
		'list_reports' => 0,
		'list_subscription_rules' => 0,
		'subscribers_view' => 0,
		'subscribers_add' => 0,
		'subscribers_edit' => 0,
		'subscribers_delete' => 0,
		'subscribers_search' => 0,
		'subscribers_import' => 0,
		'subscribers_export' => 0,
		'messages_create' => 0,
		'messages_delete' => 0,
		'messages_saved' => 0,
		'messages_queue' => 0,
		'messages_scheduled' => 0,
		'messages_archive' => 0,
		'presets_autorespond' => 0,
		'presets_templates' => 0,
		'presets_outline' => 0,
		'presets_filters' => 0,
		'presets_bodytags' => 0,
		'util_batchremove' => 0,
		'util_clone' => 0,
		'util_bouncecheck' => 0,
		'util_duplicates' => 0,
		'util_nonconfirmed' => 0,
		'util_unsubscribed' => 0,
		'util_subscribeforms' => 0,
		'admin_add' => 0,
		'admin_edit' => 0,
		'admin_delete' => 0,
		'admin_import' => 0,
		'admin_ab_split' => 0,
		'admin_auto_remind' => 0,
		'admin_email_check' => 0,
		'admin_flash_forms' => 0,
		'admin_sync' => 0,

		// reports-only
		'usertype' => 1, // 0=regular, 1=reportonly

		'force_optin' => 0,
		'force_unsubscribe_link' => 0,

		// lists
		'lists' => ''
	);

	// build groups
	$groups = array();
	// visitor group
	convert_admin4_group5($perms, $groups); // visitor
	// user group is report-only as well
	$groups[1] = $groups[0]; // user
	// main admin group should have all limits set to 0 and all perms to on



	// fetch old admin users
	$users = array();
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}admin` a, `{$oldprefix}admin_p` b WHERE a.id = b.id ORDER BY a.id");
	while ( $row = mysql_fetch_assoc($sql) ) {
		$rels = ac_sql_select_list("SELECT `list` FROM `{$oldprefix}admin_list` WHERE `admin` = '$row[id]' AND `list` IN ('$listids')");
		if ( !$rels ) $rels = array();
		$row['lists'] = implode(',', $rels);
		// ensure that admin group has no limits and all permissions set to on
		$row['_groupid'] = convert_admin4_group5($row, $groups) + 1; // function returns index, +1 is always auto_inc id
		$users[$row['id']] = $row;
	}




	// turning off indexes from important tables
	spit(_a('Turning off database indexes: '), 'em');
	$done = dbkeys(false);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}




	tz_init();
	$offset                        = tz_offset('America/Chicago');
	$t_offset_o                    = ($offset >= 0 ? "+" : "-");
	$t_offset                      = abs($offset);



	// what is our upload limit?
	$uploadLimit = ac_php_inisize(ini_get('upload_max_filesize'));
	// what is our post limit?
	$postLimit = ac_php_inisize(ini_get('post_max_size'));

	$minimumUpload = ( $uploadLimit < $postLimit ? $uploadLimit : $postLimit );
	$maxuploadfilesize = ( $minimumUpload < 10 * 1024 * 1024 ? (int)ac_file_humansize($minimumUpload) : 10 );

	$gd = (int)function_exists('gd_info');

	//$uploadHumanLimit = ac_file_humansize($uploadLimit);
	//$postHumanLimit = ac_file_humansize($postLimit);


	// backend table
	spit(_a('Moving application data: '), 'em');
	$backend['sdfreq'] = (int)$backend['sdfreq'];
	$backend['sdnum'] = (int)$backend['sdnum'];
	$insert = array(
		'id' => 1,
		'lang' => $lang,
		'serial' => $u['backend']['dl_s'],
		'av' => $av,
		'avo' => $u['backend']['dl_dd'],
		'ac' => $u['backend']['dr3292'],
		//'=acu' => 'NULL',
		//'=acec' => 'NULL',
		//'=acar' => 'NULL',
		//'=acad' => 'NULL',
		//'=acff' => 'NULL',
		'p_link' => ( isset($u['plink']) ? $u['plink'] : $siteurl ),
		'emfrom' => $users[1]['email'],
		'version' => '5.0.0.beta.0',
		'updatecheck' => (int)$backend['updatecheck'],
		'updateversion' => $thisVersion,
		'=updatedate' => 'SUBDATE(NOW(), INTERVAL 31 DAY)',
		't_offset' => $t_offset,
		't_offset_o' => $t_offset_o,
		//'datetimeformat' => $backend['dfltdateformat'],
		'stype' => (int)$backend['stype'],
		'maxuploadfilesize' => $maxuploadfilesize,
		'general_allow_rss' => (int)$backend['rss'],
		'general_passvars' => (int)!$backend['oth_mesg'],
		'general_public' => (int)!$backend['oth_public'],
		'admin_editor' => 'html',
		'sdfreq' => $backend['sdfreq'],
		'sdnum' => $backend['sdnum'],
	);
	//if ( $backend['murl'] != $insert['p_link'] ) $insert['p_link'] = $backend['murl'];
	$done = ac_sql_insert('#backend', $insert);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}


	// mailer table
	spit(_a('Assigning available mailers: '), 'em');
	$done = ac_sql_query("
		INSERT INTO
			#mailer
		(
				`id`,
				`type`,
				`host`,
				`port`,
				`user`,
				`pass`,
				`encrypt`,
				`pop3b4smtp`,
				`corder`,
				`threshold`,
				`frequency`,
				`pause`,
				`limit`,
				`limitspan`
		)
			SELECT
				x.id,
				x.type,
				x.host,
				x.port,
				x.user,
				x.pass,
				x.encrypt,
				x.pop3b4smtp,
				x.corder,
				x.threshold,
				{$backend['sdfreq']} AS `frequency`,
				{$backend['sdnum']} AS `pause`,
				0 AS `limit`,
				'hour' AS `limitspan`
			FROM
				`{$oldprefix}mailer` x
			ORDER BY x.id
	");
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// group table
	spit(_a('Adding groups: '), 'em', 1);
	foreach ( $groups as $i => $g ) {
		if ( $i == 0 ) {
			$insert = array(
				'title' => _a('Visitor'),
				'descript' => _a('This is a group for site visitors (people that are not logged in)'),
				'p_admin' => 0,
				'pg_user_add' => 0,
				'pg_user_edit' => 0,
				'pg_user_delete' => 0,
				'pg_group_add' => 0,
				'pg_group_edit' => 0,
				'pg_group_delete' => 0,
				'pg_reports_campaign' => 0,
				'pg_reports_list' => 0,
				'pg_reports_user' => 0,
			);
		} elseif ( $i == 1 ) {
			$insert = array(
				'title' => _a('User'),
				'descript' => _a('This is a default user group (people that are logged in)'),
				'p_admin' => 0,
				'pg_user_add' => 0,
				'pg_user_edit' => 0,
				'pg_user_delete' => 0,
				'pg_group_add' => 0,
				'pg_group_edit' => 0,
				'pg_group_delete' => 0,
				'pg_reports_campaign' => 0,
				'pg_reports_list' => 0,
				'pg_reports_user' => 0,
			);
		} elseif ( $i == 2 ) {
			$insert = array(
				'title' => _a('Admin'),
				'descript' => _a('This is a group for admin users (people that can manage content)'),
				'p_admin' => 1,
				'pg_user_add' => 1,
				'pg_user_edit' => 1,
				'pg_user_delete' => 1,
				'pg_group_add' => 1,
				'pg_group_edit' => 1,
				'pg_group_delete' => 1,
				'pg_reports_campaign' => 1,
				'pg_reports_list' => 1,
				'pg_reports_user' => 1,
			);
		} else {
			$insert = array(
				'title' => sprintf(_a('Custom Group %s'), $i - 2),
				'descript' => '',
				'p_admin' => 1,
				'pg_user_add' => 0,
				'pg_user_edit' => 0,
				'pg_user_delete' => 0,
				'pg_group_add' => 0,
				'pg_group_edit' => 0,
				'pg_group_delete' => 0,
				'pg_reports_campaign' => 0,
				'pg_reports_list' => 0,
				'pg_reports_user' => 0,
			);
		}
		spit(sprintf(_a("&bull; Adding group '%s': "), $insert['title']), 'em');
		foreach ( $g as $k => $v ) {
			if ( substr($k, 0, 3) == 'pg_' ) $insert[$k] = $v; // add old permissions
		}
		$insert['unsubscribelink'] = $g['unsubscribelink'];
		$insert['optinconfirm'] = $g['optinconfirm'];
		// add this group
		$done = ac_sql_insert('#group', $insert);
		if ( !$done ) {
			spit(_a('Error'), 'strong|error', 1);
			error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
			break;
		}
		$gid = (int)ac_sql_insert_id();
		if ( $gid > 2 ) { // 3+ are admins
			// add this group's limits
			$insert = array(
				'id'                  => 0,
				'groupid'             => $gid,
				'limit_mail'          => $g['limit_mail'],
				'limit_mail_type'     => $g['limit_mail_type'],
				'limit_subscriber'    => $g['limit_subscriber'],
				'limit_list'          => $g['limit_list'],
				'limit_campaign'      => $g['limit_campaign'],
				'limit_campaign_type' => $g['limit_campaign_type'],
				'limit_attachment'    => $g['limit_attachment'],
				'limit_user'          => $g['limit_user'],
			);
			$done = ac_sql_insert('#group_limit', $insert);
			if ( !$done ) {
				spit(_a('Error'), 'strong|error', 1);
				error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
				break;
			}
			// add this group's branding
			$sitename = $g['site_name'];
			if ( $sitename == '1-2-All Broadcast E-mail Software' ) {
				$sitename = 'Email Marketing Software';
			}
			$insert = array(
				'id'                => 0,
				'groupid'           => $gid,
				'site_name'         => $sitename,
				'site_logo'         => $g['site_logo'],
				'header_text'       => $g['header_text'],
				'header_text_value' => $g['header_text_value'],
				'header_html'       => $g['header_html'],
				'header_html_value' => $g['header_html_value'],
				'footer_text'       => $g['footer_text'],
				'footer_text_value' => $g['footer_text_value'],
				'footer_html'       => $g['footer_html'],
				'footer_html_value' => $g['footer_html_value'],
				'copyright'         => $g['copyright'],
				'version'           => $g['version'],
				'license'           => $g['license'],
				'links'             => $g['links'],
				'demo'              => $g['demo'],
			);
			$done = ac_sql_insert('#branding', $insert);
			if ( !$done ) {
				spit(_a('Error'), 'strong|error', 1);
				error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
				break;
			}
			// add this group's mailers
			$done = ac_sql_query("
				INSERT INTO
					#group_mailer
				(
					`id`, `groupid`, `mailerid`
				)
					SELECT
						0 AS `id`, $gid AS `groupid`, m.id AS `mailerid`
					FROM
						#mailer m
					ORDER BY m.id
			");
			if ( !$done ) {
				spit(_a('Error'), 'strong|error', 1);
				error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
				break;
			}
		}
		spit(_a('Done'), 'strong|done', 1);
	}
	if ( $fatal ) return;
	spit(_a('Done'), 'strong|done', 1);


	// user table
	spit(_a('Converting (admin) users: '), 'em');
	foreach ( $users as $v ) {
		$insert = array(
			'id' => $v['id'],
			'absid' => $v['absid'],
			'parentid' => 0,
			'=last_login' => 'NOW()',
			'lang' => $lang,
			't_offset' => $t_offset,
			't_offset_o' => $t_offset_o,
			'lists_per_page' => $v['listsperpage'],
			'messages_per_page' => $v['msgsperpage'],
			'subscribers_per_page' => $v['subsperpage'],
			'local_zoneid' => 'America/Chicago',
			'htmleditor' => 1,
			'autoupdate' => $v['autoupdate'],
			'autosave' => 0,
			'sdate' => $cdate, // application creation date
		);
		$done = ac_sql_insert('#user', $insert);
		if ( !$done ) {
			error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
			break;
		}
		// insert user into group
		$insert = array(
			'id' => 0,
			'userid' => $v['id'],
			'groupid' => $v['_groupid'],
		);
		$done = ac_sql_insert('#user_group', $insert);
		if ( !$done ) {
			error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
			break;
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// exclusion list
	// extract exclusion list
	spit(_a('Converting exclusion list: '), 'em');
	$done = ac_sql_query("
		INSERT INTO
			#exclusion
		(
			`id`, `email`
		)
			SELECT `id`, `email` FROM `{$oldprefix}exclusion` ORDER BY `id`
	");
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// CONVERT LISTS (list with dependants and list_group tables)
	spit(_a('Converting Lists: '), 'em', 1);
	foreach ( $lists as $list ) {
		$done = convert_list4_list5($list, $groups); // list id is returned, or false
		if ( !$done ) {
			spit(_a('Error'), 'strong|error', 1);
			error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
			return;
		} else {
			spit(_a('Done'), 'strong|done', 1);
		}
	}
	if ( $fatal ) return;
	spit(_a('Done'), 'strong|done', 1);



	// CONVERT LIST RELATED STUFF



	// custom headers
	spit(_a('Converting custom email headers '), 'em');
	$invalid = 0;
	$headers = ac_sql_select_array("SELECT * FROM `{$oldprefix}cheaders` WHERE `nl` IN ('$listids') ORDER BY `id`");
	foreach ( $headers as $header ) {
		// $header: id, nl, name, header, sdate
		$h = explode(':', $header['header'], 2);
		if ( count($h) == 2 ) {
			$insert = array(
				'id' => $header['id'], // same id
				'userid' => 1, // let's say admin made it
				'title' => $header['name'],
				'name' => trim($h[0]),
				'value' => trim($h[1]),
			);
			$done = ac_sql_insert('#header', $insert);
			if ( !$done ) break;
			$insert = array(
				'id' => 0,
				'headerid' => $header['id'],
				'listid' => $header['nl'],
			);
			$done = ac_sql_insert('#header_list', $insert);
			if ( !$done ) break;
		} else {
			$invalid++;
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}
	if ( $invalid ) {
		spit(sprintf(_a('Found %s invalid headers. Those were omitted.'), $invalid), 'strong|error', 1);
	}



	// email accounts
	spit(_a('Converting email accounts: '), 'em');
	$accounts = array();
	$pops = ac_sql_select_array("SELECT * FROM `{$oldprefix}pop` WHERE `nl` IN ('$listids') ORDER BY `id`");
	foreach ( $pops as $pop ) {
		// $pop: id, p_port, p_user, p_pass, p_host, p_email, action, nl, login_type
		if ( !isset($accounts[$pop['p_email']]) ) {
			$accounts[$pop['p_email']] = $pop['id'];
			$insert = array(
				'id' => $pop['id'], // same id
				'userid' => 1, // let's say admin made it
				'action' => ( ac_str_instr('unsub', strtolower($pop['action'])) ? 'unsub' : 'sub' ),
				'type' => 'pop3',
				'email' => $pop['p_email'],
				'host' => $pop['p_host'],
				'port' => $pop['p_port'],
				'user' => $pop['p_user'],
				'pass' => $pop['p_pass'],
				'method' => $pop['login_type'],
			);
			$done = ac_sql_insert('#emailaccount', $insert);
			if ( !$done ) break;
		} else {
			$pop['id'] = $accounts[$pop['p_email']];
		}
		$insert = array(
			'id' => 0,
			'emailid' => $pop['id'],
			'listid' => $pop['nl'],
		);
		$done = ac_sql_insert('#emailaccount_list', $insert);
		if ( !$done ) break;
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// bounce management
	spit(_a('Converting bounce settings: '), 'em');
	$accounts = array();
	$bounces = ac_sql_select_array("SELECT * FROM `{$oldprefix}bounce_settings` WHERE `nl` IN ('$listids', 0) OR `nl` IS NULL ORDER BY `id`");
	foreach ( $bounces as $bounce ) {
		if ( !( $bounce['id'] != 1 and $bounce['method'] == 'default' ) ) {
			// old type can be 'none', 'pop' and 'default'
			// new type can be 'none', 'pop3' and 'pipe'
			$type = ( $bounce['method'] == 'none' ? 'none' : 'pop3' );
			if ( $bounce['method'] == 'none' ) {
				$bounce['email'] = '';
			}
			if ( !isset($accounts[$bounce['email']]) ) {
				$accounts[$bounce['email']] = $bounce['id'];
				$insert = array(
					'id' => $bounce['id'], // same id
					'userid' => 1, // let's say admin made it
					'type' => $type,
					'email' => $bounce['email'],
					'host' => $bounce['host'],
					'port' => $bounce['port'],
					'user' => $bounce['username'],
					'pass' => $bounce['password'],
					'method' => $bounce['login_type'],
					'limit_hard' => $bounce['hard_bounce_times'],
					'limit_soft' => $bounce['soft_bounce_times'],
					'emails_per_batch' => $bounce['max_emails_per_batch'],
				);
				$done = ac_sql_insert('#bounce', $insert);
				if ( !$done ) break;
			} else {
				$bounce['id'] = $accounts[$bounce['email']];
			}
		} else {
			$bounce['id'] = 1; // use default
		}
		// default bounce has no lists (yet)
		if ( $bounce['nl'] ) {
			$insert = array(
				'id' => 0,
				'bounceid' => $bounce['id'],
				'listid' => $bounce['nl'],
			);
			$done = ac_sql_insert('#bounce_list', $insert);
			if ( !$done ) break;
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// bounce codes
	spit(_a('Converting known bounce codes: '), 'em');
	$done = ac_sql_query("
		INSERT INTO
			#bounce_code
		(
			`id`, `code`, `match`, `type`, `descript`
		)
			SELECT
				`id`, `code`, `matching_string` AS `match`, `bounce_type` AS `type`, `description` AS `descript`
			FROM
				`{$oldprefix}bounce_codes`
			WHERE
				code != ''
			ORDER BY `id`
	");
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// list/user templates
	spit(_a('Converting message templates: '), 'em');
	// type: FILTER, personalization, header/footer, text/html
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}templates` WHERE ( `type` = 'text' OR `type` = 'html' ) AND ( `nl` IN ('$listids') OR `uni` != '' )");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		$rels = array();
		$row['nl'] = (int)$row['nl'];
		if ( $row['nl'] and isset($lists[$row['nl']]) ) {
			$rels = array($row['nl']);
		} elseif ( $row['uni'] ) {
			// find user's group, then use all group's lists
			$rels = extract_userlists($row['uni'], $users, $groups);
		}
		$uid = 1;
		if ( count($rels) ) {
			$insert = array(
				'id' => $row['id'],
				'userid' => $uid,
				'name' => $row['name'],
				'content' => $row['content'],
				'format' => $row['type'],
			);
			$done = ac_sql_insert('#template', $insert);
			if ( !$done ) break;
			foreach ( $rels as $v ) {
				$insert = array(
					'id' => 0,
					'templateid' => $row['id'],
					'listid' => $v,
				);
				$done = ac_sql_insert('#template_list', $insert);
				if ( !$done ) break(2);
			}
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// personalization tags
	spit(_a('Converting sender personalization tags: '), 'em');
	// type: FILTER, personalization, header/footer, text/html
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}templates` WHERE `type` = 'personalization' AND ( `nl` IN ('$listids') OR `uni` != '' )");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		$rels = array();
		$row['nl'] = (int)$row['nl'];
		if ( $row['nl'] and isset($lists[$row['nl']]) ) {
			$rels = array($row['nl']);
		} elseif ( $row['uni'] ) {
			// find user's group, then use all group's lists
			$rels = extract_userlists($row['uni'], $users, $groups);
		}
		$uid = 1;
		if ( count($rels) ) {
			$insert = array(
				'id' => $row['id'],
				'userid' => $uid,
				'tag' => '',
				'name' => $row['name'],
				'content' => $row['content'],
				'format' => $row['format'],
			);
			$done = ac_sql_insert('#personalization', $insert);
			if ( !$done ) break;
			foreach ( $rels as $v ) {
				$insert = array(
					'id' => 0,
					'persid' => $row['id'],
					'listid' => $v,
				);
				$done = ac_sql_insert('#personalization_list', $insert);
				if ( !$done ) break(2);
			}
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// 2do peter : filters
	// can copy the one right above, the diff is in 'FILTER' instead of 'personalization' in WHERE
	spit(_a("Converting sending filters: "), 'em');
	$filters = array();
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}templates` WHERE `type` = 'FILTER' AND ( `nl` IN ('$listids') OR `uni` != '' )");
	while ($row = ac_sql_fetch_assoc($sql)) {
		$rels = array();
		$row['nl'] = (int)$row['nl'];
		if ( $row['nl'] and isset($lists[$row['nl']]) ) {
			$rels = array($row['nl']);
		} elseif ( $row['uni'] ) {
			// find user's group, then use all group's lists
			$rels = extract_userlists($row['uni'], $users, $groups);
		}
		$uid = 1;
		if ( count($rels) ) {
			$done = filter_importv4($row);
			if (!$done)
				break;
			foreach ( $rels as $v ) {
				$insert = array(
					'id' => 0,
					'filterid' => $row['id'],
					'listid' => $v,
				);
				$done = ac_sql_insert('#filter_list', $insert);
				if ( !$done ) break(2);
			}
			$filters[$row['id']] = 0;
		}
	}
	if (!$done) {
		spit(_a("Error"), "strong|error", 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a("Done"), "strong|done", 1);
	}
	$_SESSION['ac_updater']['filters'] = $filters;



	// subscriber rules
	spit(_a("Converting subscription rules to subscriber actions: "), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}subscription_rules` WHERE `list_id` IN ('$listids') AND `subscr_unsubscr_list_id` IN ('$listids') ORDER BY `id`");
	while ($row = ac_sql_fetch_assoc($sql)) {
		$done = subscriber_action_importv4($row);
		if ( !$done ) break;
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// database sync
	spit(_a('Converting database synchronizations: '), 'em');
	$query = "
		INSERT INTO
			`#sync`
		(
			id,
			sync_name,
			db_type,
			db_name,
			db_user,
			db_pass,
			db_host,
			db_table,
			field_from,
			field_to,
			relid,
			delete_all,
			rules,
			destination,
			sendresponder,
			instantresponder,
			sentresponders
	#		,sendoptinout
		)
			SELECT
				id,
				sync_name,
				'mysql' AS db_type,
				db_name,
				db_user,
				db_pass,
				db_host,
				db_table,
				field_from,
				REPLACE(field_to, 'field_', '_f') AS field_to,
				nl AS relid,
				delete_all,
				rules,
				IF(unsubscribed_list=0,1,2) AS destination,
				1 AS sendresponder,
				send_instant_responders AS instantresponder,
				NULL AS sentresponders
	#			,send_optin AS sendoptinout
			FROM
				`{$oldprefix}mod_sync`
	";
	$done = ac_sql_query($query);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// global redirection
	spit(_a('Converting global redirection URLs: '), 'em');
	// add1-4,unsub1-4,update1-2
	$insert = array(
		'id' => 1000,
		'name' => _a('Default Subscription Form'),
		'type' => 'both',
		'sub1_type'   => ( $backend['add1']    ? 'redirect' : 'default' ),
		'sub2_type'   => ( $backend['add2']    ? 'redirect' : 'default' ),
		'sub3_type'   => ( $backend['add3']    ? 'redirect' : 'default' ),
		'sub4_type'   => ( $backend['add4']    ? 'redirect' : 'default' ),
		'unsub1_type' => ( $backend['unsub1']  ? 'redirect' : 'default' ),
		'unsub2_type' => ( $backend['unsub2']  ? 'redirect' : 'default' ),
		'unsub3_type' => ( $backend['unsub3']  ? 'redirect' : 'default' ),
		'unsub4_type' => ( $backend['unsub4']  ? 'redirect' : 'default' ),
		'up1_type'    => ( $backend['update1'] ? 'redirect' : 'default' ),
		'up2_type'    => ( $backend['update2'] ? 'redirect' : 'default' ),
	);
	if ( $backend['add1']    ) $insert['sub1_value']   = $backend['add1'];
	if ( $backend['add2']    ) $insert['sub2_value']   = $backend['add2'];
	if ( $backend['add3']    ) $insert['sub3_value']   = $backend['add3'];
	if ( $backend['add4']    ) $insert['sub4_value']   = $backend['add4'];
	if ( $backend['unsub1']  ) $insert['unsub1_value'] = $backend['unsub1'];
	if ( $backend['unsub2']  ) $insert['unsub2_value'] = $backend['unsub2'];
	if ( $backend['unsub3']  ) $insert['unsub3_value'] = $backend['unsub3'];
	if ( $backend['unsub4']  ) $insert['unsub4_value'] = $backend['unsub4'];
	if ( $backend['update1'] ) $insert['up1_value']    = $backend['update1'];
	if ( $backend['update2'] ) $insert['up2_value']    = $backend['update2'];
	$done = ac_sql_insert('#form', $insert);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// custom subscription forms
	spit(_a('Converting custom subscription forms: '), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}subforms` ORDER BY id");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		preg_match_all('/, (\d+) ,/', $row['lists'], $match);
		if ( isset($match[1]) and count($match[1]) > 0 ) {
			// add1-4,unsub1-4,update1-2
			$insert = array(
				'id' => ( $row['id'] == 1000 ? 0 : $row['id'] ),
				'name' => $row['label'],
				'type' => 'both',
				'sub1_type'   => ( $row['add1']    ? 'redirect' : 'default' ),
				'sub2_type'   => ( $row['add2']    ? 'redirect' : 'default' ),
				'sub3_type'   => ( $row['add3']    ? 'redirect' : 'default' ),
				'sub4_type'   => ( $row['add4']    ? 'redirect' : 'default' ),
				'unsub1_type' => ( $row['unsub1']  ? 'redirect' : 'default' ),
				'unsub2_type' => ( $row['unsub2']  ? 'redirect' : 'default' ),
				'unsub3_type' => ( $row['unsub3']  ? 'redirect' : 'default' ),
				'unsub4_type' => ( $row['unsub4']  ? 'redirect' : 'default' ),
				'up1_type'    => ( $row['update1'] ? 'redirect' : 'default' ),
				'up2_type'    => ( $row['update2'] ? 'redirect' : 'default' ),
			);
			if ( $row['add1']    ) $insert['sub1_value']   = $row['add1'];
			if ( $row['add2']    ) $insert['sub2_value']   = $row['add2'];
			if ( $row['add3']    ) $insert['sub3_value']   = $row['add3'];
			if ( $row['add4']    ) $insert['sub4_value']   = $row['add4'];
			if ( $row['unsub1']  ) $insert['unsub1_value'] = $row['unsub1'];
			if ( $row['unsub2']  ) $insert['unsub2_value'] = $row['unsub2'];
			if ( $row['unsub3']  ) $insert['unsub3_value'] = $row['unsub3'];
			if ( $row['unsub4']  ) $insert['unsub4_value'] = $row['unsub4'];
			if ( $row['update1'] ) $insert['up1_value']    = $row['update1'];
			if ( $row['update2'] ) $insert['up2_value']    = $row['update2'];
			$done = ac_sql_insert('#form', $insert);
			if ( !$done ) break;
			$row['id'] = ac_sql_insert_id();
			foreach ( $match[1] as $v ) {
				$insert = array(
					'id' => 0,
					'formid' => $row['id'],
					'listid' => $v,
				);
				$done = ac_sql_insert('#form_list', $insert);
				if ( !$done ) break(2);
			}
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// cron manager
	spit(_a('Adding Scheduled Tasks: '), 'em');
	$insert = array (
		array (
			'id' => 1,
			'stringid' => 'process',
			'name' => _a('Stalled Processes Pickup Tool'),
			'descript' => _a('This picks up stalled processes triggered by the application.  Will ensure that your mailings continue to be sent.'),
			'active' => 1,
			'filename' => './admin/process.php',
			'loglevel' => 1,
			'minute' => 'a:1:{i:0;i:5;}',
			'hour' => -1,
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '2',
			'stringid' => 'sendingengine',
			'name' => _a('Sending Engine'),
			'descript' => _a('This will initiate scheduled mailings and enables the system to check if any message in a Split is a "winner"'),
			'active' => 1,
			'filename' => './admin/functions/crons/sendingengine.php',
			'loglevel' => 1,
			'minute' => 'a:1:{i:0;i:5;}',
			'hour' => -1,
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '3',
			'stringid' => 'autoresponder',
			'name' => _a('Auto-Responder'),
			'descript' => _a('This sends campaigns to subscribers scheduled against the subscription date.'),
			'active' => 1,
			'filename' => './admin/functions/crons/autoresponder.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:15;}',
			'hour' => -1,
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '4',
			'stringid' => 'autoreminder',
			'name' => _a('Auto-Reminder'),
			'descript' => _a('Auto-Reminder should run only once a day, since it uses dates only, not times. It sends campaigns scheduled against the anniversary of the date stored in subscriber\'s subscription date/custom field.'),
			'active' => 1,
			'filename' => './admin/functions/crons/autoreminder.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:0;}',
			'hour' => 0,
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '5',
			'stringid' => 'bounceparser',
			'name' => _a('Bounce Management'),
			'descript' => _a('This collects bounced emails via POP3 protocol. This cron job can be turned off if you can use email piping (preferred option).'),
			'active' => 1,
			'filename' => './admin/functions/crons/bounceparser.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:45;}',
			'hour' => -1,
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '6',
			'stringid' => 'emailparser',
			'name' => _a('Email (Un)Subscriptions Parser'),
			'descript' => _a('This will read all incoming emails via POP3 protocol, parse them and adds/remove senders from/to lists.'),
			'active' => 1,
			'filename' => './admin/functions/crons/emailparser.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:30;}',
			'hour' => -1,
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '7',
			'stringid' => 'dbsync',
			'name' => _a('Database Synchronization'),
			'descript' => _a('This will start a sync for all setup synchronization jobs in the system.'),
			'active' => 0, // turned off by default cuz a lot of clients ran it manually b4
			'filename' => './admin/functions/crons/dbsync.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:0;}',
			'hour' => '2',
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '8',
			'stringid' => 'dbbackup',
			'name' => _a('Database Backup'),
			'descript' => _a('Database Backup (a part of Database Utilities) saves a database backup to a location specified.'),
			'active' => 0,
			'filename' => './admin/functions/crons/dbbackup.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:0;}',
			'hour' => '3',
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
		array (
			'id' => '9',
			'stringid' => 'utilities',
			'name' => _a('Utilities'),
			'descript' => _a('This will cleanup redundant data (such as old logs), perform timed utility actions and optimize the tables.'),
			'active' => 1,
			'filename' => './admin/functions/crons/utilities.php',
			'loglevel' => 1,
			'minute' => 'a:2:{i:0;i:-2;i:1;i:0;}',
			'hour' => '4',
			'day' => -1,
			'weekday' => -1,
			'=lastrun' => 'NULL',
		),
	);
	$done = ac_sql_insert('#cron', $insert);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// old stuff
	// set trapperr
	spit(_a('Adding error handling settings: '), 'em');
	$done = (
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('xml_date_format', 'Y-m-d H:i:s (T)')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('sql_date_format', 'Y-m-d H:i:s')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('date_format', 'G:i:s, j. n. Y. (T)')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('db', '1')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('db_table', '{$GLOBALS['ac_prefix_use']}trapperrlogs')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('mail', '0')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('mail_to', 'bugs@activecampaign.com')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('mail_subject', '{$GLOBALS['ac_app_name']} PHP Error')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('screen', '1')") and
		ac_sql_query("INSERT INTO `#trapperr` (`id`, `value`) VALUES ('logfile', '0')")
	);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}


	// stop this upgrade step
	return;
}
if ( $updaterStep == 1 ) {



	// SUBSCRIBERS + CUSTOM FIELDS
	// fetch fields
	spit(_a('Fetching custom fields: '), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}fields` ORDER BY `dorder` ASC");
	if ( !$sql ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	} else {
		spit(_a('Done'), 'strong|done', 1);
		spit(_a('Converting custom fields: '), 'em');
		while ( $row = mysql_fetch_assoc($sql) ) {
			$rels = array_diff(array_map('intval', explode(',', $row['mirror'])), array(0));
			$rels[] = $row['nl'];
			$rels = array_unique($rels);
			sort($rels);
			$rels = array_intersect($rels, array_keys($lists));
			if ( count($rels) ) {
				// add to custom fields
				$insert = array(
					'id' => $row['id'],
					'title' => $row['title'],
					'type' => $row['type'],
					'expl' => $row['expl'],
					'req' => $row['req'],
					'onfocus' => $row['onfocus'],
					//'bubble_content' => '',
					'label' => $row['label'],
					'show_in_list' => $row['show_on_subscriber_list'],
					//'perstag' => '',
				);
				$done = ac_sql_insert('#list_field', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				foreach ( $rels as $v ) {
					// add to custom fields relations
					$insert = array(
						'id' => 0,
						'fieldid' => $row['id'],
						'relid' => $v,
						'dorder' => $row['dorder'],
					);
					$done = ac_sql_insert('#list_field_rel', $insert);
					if ( !$done ) {
						spit(_a('Error'), 'strong|error', 1);
						error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
						break(2);
					}
				}
			}
		}
	}
	if ( $fatal ) return;
	spit(_a('Done'), 'strong|done', 1);



	// SUBSCRIBERS
	// convert subscribers
	$subscriber_query = ac_sql_query("SELECT COUNT(*) FROM `{$oldprefix}listmembers` WHERE `nl` IN ('$listids')");
	list($subscriber_total) = ac_sql_fetch_row($subscriber_query);

	$step_count = 10000;

	$loopsteps = ceil($subscriber_total / $step_count);

	spit(sprintf(_a('Found %s subscribers.'), $subscriber_total), 'em', 1);

	for ($i = 1; $i <= $loopsteps; $i++) {

		$start = ($i - 1) * $step_count;

		spit(sprintf(_a('Converting next 10,000 subscribers (Step %s/%s): '), $i, $loopsteps), 'em');

		//$subs = array();
		$sql = ac_sql_query("SELECT * FROM `{$oldprefix}listmembers` WHERE `nl` IN ('$listids') ORDER BY `id` LIMIT $start, $step_count");
		while ( $row = ac_sql_fetch_assoc($sql) ) {
			$row['email'] = trim($row['email']);
			$ip = ( ac_str_is_ip($row['sip']) ? ac_sql_escape($row['sip']) : '127.0.0.1' );
			spit('. ');
			/*
			preg_match_all('/,(\d+),/', $row['respond'], $match);
			$r = array_unique(array_map('intval', $match[1]));
			sort($r);
			$r = array_intersect($r, array_keys($lists));
			*/
			//if ( !isset($subs[$row['email']]) ) {
			$id = sub_exists($row['email']);
			if ( !$id ) {

				$name = explode(' ', $row['name'], 2);
				if ( !isset($name[1]) ) $name[1] = '';

				// no emails like this one b4
				$insert = array(
					'id' => $row['id'], // reuse id
					'cdate' => $row['sdate'] . ' ' . $row['stime'],
					'email' => $row['email'],
					'first_name' => $name[0],
					'last_name' => $name[1],
					'bounced_hard' => $row['bounced'],
					'bounced_soft' => $row['soft_bounced'],
					'=ip' => "INET_ATON('$ip')",
					'ua' => $row['comp'],
					//'=hash' => "MD5(CONCAT(id, email))",
				);
				if ( $row['bounced_d'] and $row['bounced_d'] != '0000-00-00' ) $insert['bounced_date'] = $row['bounced_d'];
				$done = ac_sql_insert('#subscriber', $insert);
				if ( !$done ) break;
				$id = ac_sql_insert_id();
				//$subs[$row['email']] = array('id' => $id, 'lists' => array($row['nl']), 'respond' => $r);

			} else {
				// check if duplicate
				//if ( in_array($row['nl'], $subs[$row['email']]['lists']) ) {

				if ( sub_exists($row['email'], $row['nl']) ) {
					// duplicate check!
					if ( isset($lists[$row['nl']]) and $lists[$row['nl']]['a_duplicate'] ) {
						// no emails like this one b4
						$insert = array(
							'id' => $row['id'], // reuse id
							'cdate' => $row['sdate'] . ' ' . $row['stime'],
							'email' => $row['email'],
							'first_name' => $name[0],
							'last_name' => $name[1],
							'bounced_hard' => $row['bounced'],
							'bounced_soft' => $row['soft_bounced'],
							'=ip' => "INET_ATON('$ip')",
							'ua' => $row['comp'],
							//'=hash' => "MD5(CONCAT(id, email))",
						);
						if ( $row['bounced_d'] and $row['bounced_d'] != '0000-00-00' ) $insert['bounced_date'] = $row['bounced_d'];
						$done = ac_sql_insert('#subscriber', $insert);
						if ( !$done ) break;
						$id = ac_sql_insert_id();
						//$subs[$row['email']] = array('id' => $id, 'lists' => array($row['nl']), 'respond' => $r);
					} else {
						// 2do: unsupported case
						continue;
					}
				} else {
					// email moved already
					//$id = subid($row['id']); // already obtained the id before this IF
					//$id = $subs[$row['email']]['id'];
					// punch in this list
					//$subs[$row['email']]['lists'][] = $row['nl'];
				}

			}
			// add relation

			$insert = array(
				'id' => 0,
				'subscriberid' => $id,
				'listid' => $row['nl'],
				'formid' => $row['subscription_form_id'],
				'sdate' => $row['sdate'] . ' ' . $row['stime'],
				//'=udate' => 'NULL',
				'status' => (int)!$row['active'],
				'responder' => (int)!$row['no_autoresponders'],
				'sync' => (int)!$row['sync'],
				//'=unsubreason' => 'NULL',
				//'unsubcampaignid' => 0,
				//'unsubmessageid' => 0,
			);



			$done = ac_sql_insert('#subscriber_list', $insert);
			if ( !$done ) break;



			// fetching custom fields from v4 - old style
			require_once(ac_global_functions('custom_fields.php'));
			$fields     = ac_custom_fields_select_data("{$oldprefix}fields", "{$oldprefix}fieldsd", "d.`fid` = f.`id` AND d.`eid` = '$row[id]' AND d.`nl` = '$row[nl]'", "(f.`nl` = '$row[nl]' OR FIND_IN_SET($row[nl], f.`mirror`))");
			$values = array();
			foreach ( $fields as $v ) {
				$values[$v['id']] = $v['val'];
			}
			// adding custom fields to v5 - new style
			if ( count($values) ) ac_custom_fields_update_data($values, '#list_field_value', 'fieldid', array('relid' => $id));


			# We need to update their filter cache.
			//filter_cache_subscriber($id, false);


		}
		unset($row);
		unset($values);
		unset($fields);
		unset($insert);
		spit(_a('Completed'), 'strong', 1);
	}




	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// stop this upgrade step
	return;
}
if ( $updaterStep == 2 ) {



	// MESSAGES/RESPONDERS | CAMPAIGNS

	// fetch mailings
	spit(_a('Fetching past mailings: '), 'em');
	$campaigns = array();
	$attachments = array();
	// gotta push all drafts to the end
	$sql = ac_sql_query("
		SELECT
			m.*,
			( SELECT COUNT(c.id) FROM `{$oldprefix}mtbl_id` c WHERE c.id = m.mesg_id ) AS draft
		FROM
			`{$oldprefix}messages` m
		WHERE
			m.b = 0
		GROUP BY
			m.id
		ORDER BY
			draft DESC,
			m.id ASC
	");
	if ( !$sql ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	} else {
		spit(_a('Done'), 'strong|done', 1);
		spit(_a('Converting messages: '), 'em', 1);
		while ( $row = mysql_fetch_assoc($sql) ) {
			if ( !isset($campaigns[$row['mesg_id']]) ) {
				$mtbl = ac_sql_select_row("SELECT * FROM `{$oldprefix}mtbl_id` WHERE `id` = '$row[mesg_id]'");
				$b = ac_sql_select_row("SELECT * FROM `{$oldprefix}messages` WHERE `b` = '$row[id]'");
				// deal with lists/status
				$status = 0;
				$rels = array();
				preg_match_all('/, (\d+) ,/', $row['nl'], $match);
				if ( !$mtbl ) {
					// status is draft!
					$status = 0;
					// mtbl clone
					$mtbl = ac_sql_default_row("{$oldprefix}mtbl_id");
					$mtbl['mdate'] = $row['mdate'];
					$mtbl['mtime'] = $row['mtime'];
					if ( count($match) > 0 ) {
						sort($match[1]);
						$rels = array_intersect($match[1], array_keys($lists));
					} elseif ( $row['nl'] = (int)$row['nl'] ) {
						$rels = array($row['nl']);
					}
				} elseif ( $row['nl'] = (int)$row['nl'] ) {
					if ( isset($lists[$row['nl']]) ) {
						$rels = ac_sql_select_list("SELECT DISTINCT(`nl`) FROM `{$oldprefix}messages` WHERE `mesg_id` = '$row[mesg_id]' AND `b` = 0");
						//$rels = array($row['nl']);
						switch ( $row['status'] ) {
							case 4:
								// scheduled
								$status = 1;
							case 3:
								// paused
								$status = 3;
							case 0:
								// sending
								$status = 2;
							case 2:
							default:
								$status = 5;
						}
					}
				}

				// id, user, mdate, mtime, mfrom, mfromn, subject, message, textmesg, htmlmesg, nl, type, tlinks, amt, sent, completed, link1n, link1t, filter, d_check, mesg_id, status, s_date, s_time, init, l_time, l_date, last_send, reply2, charset, priority, encoding, treads, htmlhead, htmlfoot, texthead, textfoot, htmlunsub, textunsub, htmlfetch, textfetch, recurring, b, embed_images
				if ( count($rels) > 0 ) {
					spit(sprintf(_a("&bull; Adding campaign '%s': "), $row['subject']), 'em');
					$row['type'] = ( !in_array(strtolower($row['type']), array('text', 'html')) ? 'mime' : $row['type'] );
					$row['_uid'] = convert_admin2_username2id($row['user']);

					$row['send_amt'] = $mtbl['send_amt'];
					if ( $b ) {
						$send_amt = ceil( $mtbl['send_amt'] / 2 );
						$row['send_amt'] = ( $send_amt > 1 ? $send_amt : 1 );
						$b['send_amt'] = $mtbl['send_amt'] - $row['send_amt'];
					}

					// add to messages
					$insert = build_message_insert($row);
					$done = ac_sql_insert('#message', $insert);
					if ( !$done ) {
						spit(_a('Error'), 'strong|error', 1);
						error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
						break;
					}

					// add attachments
					$done = convert_attachments($attachments, $row['id'], $row['link1t']);
					if ( !$done ) {
						spit(_a('Error'), 'strong|error', 1);
						error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
						break;
					}

					// if a/b split
					if ( $b ) {
						$b['type'] = ( !in_array(strtolower($row['type']), array('text', 'html')) ? 'mime' : $row['type'] );
						$b['_uid'] = convert_admin2_username2id($row['user']);
						$insert = build_message_insert($b);
						$done = ac_sql_insert('#message', $insert);
						if ( !$done ) {
							spit(_a('Error'), 'strong|error', 1);
							error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
							break;
						}
						// add attachments
						$done = convert_attachments($attachments, $b['id'], $b['link1t']);
						if ( !$done ) {
							spit(_a('Error'), 'strong|error', 1);
							error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
							break;
						}
					}

					// set list relations
					foreach ( $rels as $v ) {
						$insert = array(
							'id' => 0,
							'messageid' => $row['id'],
							'listid' => $v,
						);
						// add this list relation
						$done = ac_sql_insert('#message_list', $insert);
						if ( !$done ) {
							spit(_a('Error'), 'strong|error', 1);
							error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
							break(2);
						}
						if ( $b ) {
							$insert = array(
								'id' => 0,
								'messageid' => $b['id'],
								'listid' => $v,
							);
							// add this list relation
							$done = ac_sql_insert('#message_list', $insert);
							if ( !$done ) {
								spit(_a('Error'), 'strong|error', 1);
								error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
								break(2);
							}
						}
					}

					// now add campaign
					$type = ( $row['recurring'] > 0 ? 'recurring' : ( $b ? 'split' : 'single' ) );
					$recurring = array(
						0 => 'day1', // not a recurring, set default
						1 => 'day1',
						2 => 'week1',
						3 => 'month1',
						4 => 'year1',
					);
					$mdate = $mtbl['mdate'] . ' ' . $mtbl['mtime'];
					$sdate = $row['s_date'] . ' ' . $row['s_time'];
					if ( $mdate > $sdate ) $sdate = $mdate;
					$stats = fetch_mailing_stats($mtbl['id']);
					$insert = array(
						'id' => $mtbl['id'],
						'type' => $type,
						'userid' => $row['_uid'],
						'filterid' => (int)$row['filter'],
						//'bounceid' => -1,
						//'realcid' => 0,
						'name' => $row['subject'],
						'cdate' => $mdate,
						'sdate' => $sdate,
						'ldate' => $mtbl['ldate'] . ' ' . $mtbl['ltime'],
						'send_amt' => $mtbl['send_amt'],
						'total_amt' => $mtbl['total_amt'],
						'opens' => $stats['reads_nonunique'],
						'uniqueopens' => $stats['reads_unique'],
						'linkclicks' => $stats['total_clicks'],
						'uniquelinkclicks' => $stats['unique_clicks'],
						'subscriberclicks' => $stats['unique_emails'],
						'forwards' => $stats['forwards'],
						'uniqueforwards' => $stats['unique_forwards'],
						'hardbounces' => $stats['bounces_hard'],
						'softbounces' => $stats['bounces_soft'],
						'unsubscribes' => $stats['unsubscribed'],
						'unsubreasons' => $stats['unsubscribed_wreason'],
						'status' => $status,
						//'public' => 1,
						'mail_transfer' => $mtbl['comp_transfer'],
						'mail_send' => $mtbl['comp_send'],
						'mail_cleanup' => $mtbl['comp_del'],
						'tracklinks' => ( $row['tlinks'] ? $row['type'] : 'none' ),
						'tracklinksanalytics' => 0,
						'trackreads' => (int)$row['treads'],
						'trackreadsanalytics' => 0,
						//'analytics_campaign_name' => '',
						'embed_images' => $row['embed_images'],
						'htmlunsub' => $row['htmlunsub'],
						'textunsub' => $row['textunsub'],
						//'=htmlunsubdata' => 'NULL',
						//'=textunsubdata' => 'NULL',
						'recurring' => $recurring[$row['recurring']],
						'split_type' => 'even',
						'split_offset' => 2,
						'split_offset_type' => 'day',
						//'responder_offset' => $mtbl[''],
						//'responder_type' => $mtbl[''],
						//'reminder_field' => $mtbl[''],
						//'reminder_format' => $mtbl[''],
						//'reminder_type' => $mtbl[''],
						//'reminder_offset' => $mtbl[''],
						//'reminder_offset_type' => $mtbl[''],
						//'reminder_offset_sign' => $mtbl[''],
						//'reminder_last_cron_run' => $mtbl[''],
					);
					// add this campaign
					if ($insert["status"] == 2)	# Pause all campaigns being sent.
						$insert["status"] = 3;
					$done = ac_sql_insert('#campaign', $insert);
					if ( !$done ) {
						spit(_a('Error'), 'strong|error', 1);
						error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
						break;
					}
					if ( !$mtbl['id'] ) $mtbl['id'] = ac_sql_insert_id();

					// set list relations
					foreach ( $rels as $v ) {
						$insert = array(
							'id' => 0,
							'campaignid' => $mtbl['id'],
							'listid' => $v,
						);
						// add this list relation
						$done = ac_sql_insert('#campaign_list', $insert);
						if ( !$done ) {
							spit(_a('Error'), 'strong|error', 1);
							error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
							break(2);
						}
					}

					$msgrels = array($row['id']);
					if ( $b ) $msgrels[] = $b['id'];
					// set list relations
					foreach ( $msgrels as $v ) {
						$insert = array(
							'id' => 0,
							'campaignid' => $mtbl['id'],
							'messageid' => $v,
							'percentage' => 100 / count($msgrels),
							//'sourcesize' => 0,
						);
						// add this list relation
						$done = ac_sql_insert('#campaign_message', $insert);
						if ( !$done ) {
							spit(_a('Error'), 'strong|error', 1);
							error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
							break(2);
						}
					}

					$row['send_amt'] = $mtbl['send_amt'];
					if ( $b ) {
						$send_amt = ceil( $mtbl['send_amt'] / 2 );
						$row['send_amt'] = ( $send_amt > 1 ? $send_amt : 1 );
						$b['send_amt'] = $mtbl['send_amt'] - $row['send_amt'];
					}

					spit(_a('Done'), 'strong|done', 1);
					$campaigns[$mtbl['id']] = array('lists' => $rels, 'messages' => $msgrels);
				} // if has list relations (should convert)
			} // if not converted already (in diff list)
		} // loop through all messages
	} // all messages fetched
	if ( $fatal ) return;
	spit(_a('Done'), 'strong|done', 1);
	$_SESSION['ac_updater']['campaigns'] = $campaigns;



	// fetch responders
	spit(_a('Fetching auto-responders/reminders: '), 'em');
	$responders = array();
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}respond` WHERE `nl` IN ('$listids') ORDER BY `id` ASC");
	if ( !$sql ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	} else {
		spit(_a('Done'), 'strong|done', 1);
		spit(_a('Converting auto-responders/reminders: '), 'em', 1);
		while ( $row = mysql_fetch_assoc($sql) ) {
			// deal with lists/status
			$row['nl'] = (int)$row['nl'];
			if ( isset($lists[$row['nl']]) ) {
				spit(sprintf(_a("&bull; Adding campaign '%s': "), $row['subject']), 'em');
				$row['type'] = ( !in_array(strtolower($row['type']), array('text', 'html')) ? 'mime' : $row['type'] );
				$row['_uid'] = 1;
				if ( $row['type'] == 'mime' ) {
					$row['textmesg'] = $row['content'];
					$row['htmlmesg'] = $row['mime_html_content'];
				} else {
					$row['textmesg'] = ( $row['type'] != 'html' ? $row['content'] : '' );
					$row['htmlmesg'] = ( $row['type'] != 'text' ? $row['content'] : '' );
				}
				// add to messages
				$insert = array(
					'id' => 0,
					'userid' => $row['_uid'],
					'cdate' => $cdate,
					'mdate' => $cdate,
					'fromname' => $row['fromn'],
					'fromemail' => $row['frome'],
					//'reply2' => '',
					//'priority' => 3,
					'charset' => $row['charset'],
					'encoding' => $row['encoding'],
					'format' => $row['type'],
					'subject' => $row['subject'],
					'text' => fix_message_text($row['textmesg'], $row['text_header'], $row['text_footer']),
					'html' => fix_message_html($row['htmlmesg'], $row['html_header'], $row['html_footer']),
					//'htmlfetch' => 'now',
					//'textfetch' => 'now',
					//'hidden' => 0,
				);
				$done = ac_sql_insert('#message', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				$mid = ac_sql_insert_id();

				// add attachments
				$done = convert_attachments($attachments, $mid, $row['attached']);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}

				// set list relation
				$insert = array(
					'id' => 0,
					'messageid' => $mid,
					'listid' => $row['nl'],
				);
				// add this list relation
				$done = ac_sql_insert('#message_list', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}

				// now add campaign
				$type = ( $row['is_date_message'] > 0 ? 'reminder' : 'responder' );
				$stats = fetch_responder_stats($row['id'], $row['nl']);
				$insert = array(
					'id' => 0,
					'type' => $type,
					'userid' => $row['_uid'],
					'filterid' => (int)$row['filter'],
					//'bounceid' => -1,
					//'realcid' => 0,
					'name' => $row['subject'],
					'cdate' => $cdate,
					'sdate' => $cdate,
					'ldate' => $cdate,
					'send_amt' => $stats['send_amt'],
					'total_amt' => $stats['total_amt'],
					'opens' => $stats['reads_nonunique'],
					'uniqueopens' => $stats['reads_unique'],
					'linkclicks' => $stats['total_clicks'],
					'uniquelinkclicks' => $stats['unique_clicks'],
					'subscriberclicks' => $stats['unique_emails'],
					'forwards' => $stats['forwards'],
					'uniqueforwards' => $stats['unique_forwards'],
					'hardbounces' => $stats['bounces_hard'],
					'softbounces' => $stats['bounces_soft'],
					'unsubscribes' => $stats['unsubscribed'],
					'unsubreasons' => $stats['unsubscribed_wreason'],
					'status' => 1, // all responders/reminders are always scheduled
					//'public' => 1,
					//'mail_transfer' => 0,
					//'mail_send' => 0,
					//'mail_cleanup' => 0,
					'tracklinks' => ( $row['clickthrough_tracking'] ? $row['type'] : 'none' ),
					'tracklinksanalytics' => 0,
					'trackreads' => (int)$row['reads_tracking'],
					'trackreadsanalytics' => 0,
					//'analytics_campaign_name' => '',
					//'embed_images' => 0,
					//'htmlunsub' => 1,
					//'textunsub' => 1,
					//'=htmlunsubdata' => 'NULL',
					//'=textunsubdata' => 'NULL',
					//'recurring' => 'day1',
					//'split_type' => 'even',
					//'split_offset' => 2,
					//'split_offset_type' => 'day',
					'responder_offset' => (int)$row['time'],
					'responder_type' => ( $row['subscription_type'] == 'unsubscribe' ? $row['subscription_type'] : 'subscribe' ),
					'reminder_field' => ( (int)$row['date_subscriber_field'] ? $row['date_subscriber_field'] : 'sdate' ),
					'reminder_format' => $row['date_format'],
					'reminder_type' => $row['date_type'],
					'reminder_offset' => (int)$row['date_offset_days'],
					'reminder_offset_type' => 'day',
					'reminder_offset_sign' => $row['date_offset_plus_minus'],
					//'reminder_last_cron_run' => $row['date_last_cron_run'],
				);
				if ( $row['date_last_cron_run'] ) {
					$insert['reminder_last_cron_run'] = $row['date_last_cron_run'];
				} else {
					$insert['=reminder_last_cron_run'] = 'NULL';
				}
				// add this campaign
				$done = ac_sql_insert('#campaign', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				$cid = ac_sql_insert_id();

				// set list relation
				$insert = array(
					'id' => 0,
					'campaignid' => $cid,
					'listid' => $row['nl'],
				);
				// add this list relation
				$done = ac_sql_insert('#campaign_list', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}

				// set list relation
				$insert = array(
					'id' => 0,
					'campaignid' => $cid,
					'messageid' => $mid,
					//'percentage' => 100,
					//'sourcesize' => 0,
				);
				// add this list relation
				$done = ac_sql_insert('#campaign_message', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				spit(_a('Done'), 'strong|done', 1);

				// save this responder
				$responders[$row['id']] = array('mid' => $mid, 'cid' => $cid);
			} // if has list relations (should convert)
		} // loop through all messages
	} // all messages fetched
	if ( $fatal ) return;
	spit(_a('Done'), 'strong|done', 1);
	$_SESSION['ac_updater']['responders'] = $responders;



	// stop this upgrade step
	return;
}

if ( isset($_SESSION['ac_updater']['filters']) ) {
	$filters = $_SESSION['ac_updater']['filters'];
} else {
	$filters = ac_sql_select_box_array("SELECT id, id FROM `{$oldprefix}templates` WHERE `type` = 'FILTER' AND ( `nl` IN ('$listids') OR `uni` != '' )");
}
if ( isset($_SESSION['ac_updater']['campaigns']) ) {
	$campaigns = $_SESSION['ac_updater']['campaigns'];
} else {
	$campaigns = ac_sql_select_box_array("SELECT id, id FROM `{$oldprefix}mtbl_id` WHERE `nl` IN ('$listids')");
}
if ( isset($_SESSION['ac_updater']['responders']) ) {
	$responders = $_SESSION['ac_updater']['responders'];
} else {
	$responders = array();
	// fetch an array with oldid => newid
	$responders = ac_sql_select_box_array("SELECT id, id FROM `#respond` WHERE `type` IN ('responder', 'reminder')");
}

if ( $updaterStep == 3 ) {



	// convert unsubscriptions
	spit(_a('Converting unsubscriptions: '), 'em');
	$sql = ac_sql_query("SELECT *, DATE_FORMAT(`date`, '%Y-%m-%d %H:%i:%s') AS `sqldate` FROM `{$oldprefix}listmembersu` WHERE `nl` IN ('$listids') ORDER BY `id`");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		spit('. ');
		$row['email'] = $row['em'] = trim($row['em']);
		$date = (string)$row['date_subscribed'];
		if ( $date and $row['time_subscribed'] ) $date .= ' ' . (string)$row['time_subscribed'];
		//if ( !isset($subs[$row['email']]) ) {
		$id = sub_exists($row['email']);
		if ( !$id ) {
			$ip = ( ac_str_is_ip($row['subscribe_ip']) ? ac_sql_escape($row['subscribe_ip']) : '127.0.0.1' );
			// no emails like this one b4
			$insert = array(
				'id' => 0,
				'cdate' => ( $date ? $date : $cdate ),
				'email' => $row['email'],
				'first_name' => '',
				'last_name' => '',
				//'bounced_hard' => 0,
				//'bounced_soft' => 0,
				//'=bounced_date' => 'NULL',
				'=ip' => "INET_ATON('$ip')",
				//'=ua' => 'NULL',
				//'=hash' => "MD5(CONCAT(id, email))",
			);
			$done = ac_sql_insert('#subscriber', $insert);
			if ( !$done ) break;
			$id = ac_sql_insert_id();
			//$subs[$row['email']] = array('id' => $id, 'lists' => array($row['nl']), 'respond' => array());
		} else {
			// email moved already
			//$id = $subs[$row['email']]['id'];
			// check if duplicate
			//if ( in_array($row['nl'], $subs[$row['email']]['lists']) ) {
			if ( sub_exists($row['email'], $row['nl']) ) {
				// duplicate check? no, just continue, this is unsub
				continue;
			} else {
				// punch in this list
				//$subs[$row['email']]['lists'][] = $row['nl'];
			}
		}
		$cid = 0;
		$mid = 0;
		$addRelation = false;
		if ( $row['mesg_id'] = (int)$row['mesg_id'] ) {
			$cid = $row['mesg_id'];
			if ( isset($campaigns[$cid]) ) {
				//$cid = $campaigns[$cid]['id'];
				$mid = $campaigns[$cid]['messages'][(int)( $row['b'] and isset($campaigns[$cid]['messages'][1]) )];
			}
		} elseif ( $row['respond_id'] = (int)$row['respond_id'] ) {
			if ( isset($responders[$row['respond_id']]) ) {
				$cid = $responders[$row['respond_id']]['cid'];
				$mid = $responders[$row['respond_id']]['mid'];
				// add to subscriber/responder relation
				$insert = array(
					'id' => 0,
					'subscriberid' => $id,
					'campaignid' => $cid,
					'messageid' => $mid,
					'sdate' => $cdate,
				);
				$done = ac_sql_insert('#subscriber_responder', $insert);
				if ( !$done ) break;
				//if ( !in_array($row['respond_id'], $subs[$row['email']]['respond']) ) {
					//$subs[$row['email']]['respond'][] = $row['respond_id'];
				//}
			}
		}
		// add relation
		$insert = array(
			'id' => 0,
			'subscriberid' => $id,
			'listid' => $row['nl'],
			//'formid' => 0,
			'sdate' => ( $date ? $date : $cdate ),
			'udate' => $row['sqldate'],
			'status' => 2, // unsub
			//'responder' => 1,
			//'sync' => 0,
			//'=unsubreason' => 'NULL',
			'unsubcampaignid' => $cid,
			'unsubmessageid' => $mid,
		);
		if ( $row['reason'] ) $insert['unsubreason'] = $row['reason'];
		$done = ac_sql_insert('#subscriber_list', $insert);
		if ( !$done ) break;
		# We need to update their filter cache.
		//filter_cache_subscriber($id, false);
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// stop this upgrade step
	return;
}
if ( $updaterStep == 4 ) {



	// subscriber_responder
	spit(_a('Saving auto-responder/reminder stats: '), 'em');
	$sql = ac_sql_query("
		SELECT
			id,
			nl,
			email,
			respond
		FROM
			`{$oldprefix}listmembers`
		WHERE
			`nl` IN ('$listids')
		AND
			`respond` IS NOT NULL
		AND
			`respond` != ''
		ORDER BY `id`
	");
	//foreach ( $subs as $email => $row ) {
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		spit('. ');
		$sid = sub_exists($row['email'], $row['nl']);
		preg_match_all('/,(\d+),/', $row['respond'], $match);
		$rels = array_unique(array_map('intval', $match[1]));
		sort($rels);
		if ( $sid ) {
			foreach ( $rels as $v ) {
				if ( isset($responders[$v]) ) {
					$insert = array(
						'id' => 0,
						'subscriberid' => $sid,
						'campaignid' => $responders[$v]['cid'],
						'messageid' => $responders[$v]['mid'],
						'sdate' => $cdate,
					);
					$done = ac_sql_insert('#subscriber_responder', $insert);
					if ( !$done ) break(2);
				}
			}
		}
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// stop this upgrade step
	return;
}
if ( $updaterStep == 5 ) {



	// fetch link tracking
	spit(_a('Converting link tracking data: '), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}links` WHERE `nl` > 0 OR `respond_id` > 0 ORDER BY `id`");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		spit('. ', 'strong');
		$found = false;
		if ( $row['nl'] = (int)$row['nl'] ) {
			$cid = $row['nl'];
			if ( isset($campaigns[$cid]) ) {
				$found = true;
				//$cid = $campaigns[$cid]['id'];
				$mid = $campaigns[$cid]['messages'][0];
			}
		} elseif ( $row['respond_id'] = (int)$row['respond_id'] ) {
			if ( isset($responders[$row['respond_id']]) ) {
				$found = true;
				$cid = $responders[$row['respond_id']]['cid'];
				$mid = $responders[$row['respond_id']]['mid'];
			}
		}
		if ( $found ) {
			$isLink = ( $row['link'] != '' and $row['link'] != 'open' );
			// figure out name based on link?
			if ( !$isLink ) {
				$name = _a('Read Tracking');
			} else {
				$name = '';
				// update subscriber
				if ( ac_str_instr('/p_m.php', $row['link']) ) {
					$name = _a("Update Subscriber Link");
					$tmpVar1 = strpos($row['link'], '?');
					if ( $tmpVar1 > 0 ) $row['link'] = substr($row['link'], 0, $tmpVar1);
				}
				// web copy
				if ( ac_str_instr('/p_v.php', $row['link']) ) {
					$tmpVar1 = strpos($row['link'], '?');
					if ( $tmpVar1 > 0 ) $row['link'] = substr($row['link'], 0, $tmpVar1);
					$name = _a("Web Copy Link");
				}
				// forward2friend
				if ( ac_str_instr('/p_f.php', $row['link']) ) {
					$tmpVar1 = strpos($row['link'], '?');
					if ( $tmpVar1 > 0 ) $row['link'] = substr($row['link'], 0, $tmpVar1);
					$name = _a("Forward to a Friend Link");
				}
			}
			$insert = array(
				'id' => 0,
				'campaignid' => $cid,
				'messageid' => $mid,
				'link' => $row['link'],
				'name' => $name,
			);
			$done = ac_sql_insert('#link', $insert);
			if ( !$done ) break;
			$lid = ac_sql_insert_id();

			if ( !$isLink and $mid ) {
				$insert = array(
					'id' => 0,
					'campaignid' => $cid,
					'messageid' => 0,
					'link' => $row['link'],
					'name' => $name,
				);
				$done = ac_sql_insert('#link', $insert);
				if ( !$done ) break;
				// get lid here! we use messageid=0 row from now on
				$lid = ac_sql_insert_id();
			}

			// fetch link data
			$sql2 = ac_sql_query("SELECT * FROM `{$oldprefix}linksd` WHERE `lid` = '$row[id]'");
			while ( $v = ac_sql_fetch_assoc($sql2) ) {
				spit('. ');
				// if subscriber is transfered
				$sid = sub_exists($v['email']);
				//if ( isset($subs[$v['email']]) ) {
				if ( $sid ) {
					//$sid = $subs[$v['email']]['id'];
					$ip = ( ac_str_is_ip($v['ip']) ? ac_sql_escape($v['ip']) : '127.0.0.1' );
					$insert = array(
						'id' => 0,
						'linkid' => $lid,
						'tstamp' => $v['sdate'] . ' ' . $v['stime'],
						'subscriberid' => $sid,
						'email' => $v['email'],
						'times' => $v['times'],
						'=ip' => "INET_ATON('$ip')",
						//'ua' => '',
						//'=referer' => 'NULL',
					);
					$done = ac_sql_insert('#link_data', $insert);
					if ( !$done ) break(2);

					// add to link log
					for ( $i = 1; $i <= $v['times']; $i++ ) {
						$insert = array(
							'id' => 0,
							'linkid' => $lid,
							'tstamp' => $v['sdate'] . ' ' . $v['stime'],
							'subscriberid' => $sid,
							'=ip' => "INET_ATON('$ip')",
							//'ua' => '',
							//'=referer' => 'NULL',
						);
						$done = ac_sql_insert('#link_log', $insert);
						if ( !$done ) break(2);
					}
				}
			}
		} // if not a convertible link
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// stop this upgrade step
	return;
}
if ( $updaterStep == 6 ) {



	// fetch bounce tracking
	spit(_a('Converting bounce tracking data: '), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}bounce` WHERE `nl` IN ('$listids') AND ( `mid` > 0 OR `respond_id` > 0 ) ORDER BY `id`");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		spit('. ');
		$found = false;
		$row['nl'] = (int)$row['nl'];
		if ( $row['mid'] = (int)$row['mid'] ) {
			$cid = $row['mid'];
			if ( isset($campaigns[$cid]) ) {
				$found = true;
				//$cid = $campaigns[$cid]['id'];
				$mid = $campaigns[$cid]['messages'][0];
			}
		} elseif ( $row['respond_id'] = (int)$row['respond_id'] ) {
			if ( isset($responders[$row['respond_id']]) ) {
				$found = true;
				$cid = $responders[$row['respond_id']]['cid'];
				$mid = $responders[$row['respond_id']]['mid'];
			}
		}
		// if subscriber is not transfered
		$sid = sub_exists($row['email']);
		//if ( !isset($subs[$row['email']]) ) {
		if ( !$sid ) {
			$found = false;
		}
		if ( $found ) {
			//$sid = $subs[$row['email']]['id'];
			$insert = array(
				'id' => $row['id'], // reuse bounce id
				'email' => $row['email'],
				'subscriberid' => $sid,
				'listid' => $row['nl'],
				'campaignid' => $cid,
				'messageid' => $mid,
				'tstamp' => $row['tdate'] . ' ' . $row['ttime'],
				'type' => $row['type'],
				'code' => $row['code'],
			);
			$done = ac_sql_insert('#bounce_data', $insert);
			if ( !$done ) break;
		} // if not a convertible bounce
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// stop this upgrade step
	return;
}
if ( $updaterStep == 7 ) {



	// fetch forwards tracking
	spit(_a('Converting forward-to-friend tracking data: '), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}forward_log` WHERE `nl` IN ('$listids') AND ( `mesg_id` > 0 OR `respond_id` > 0 ) ORDER BY `id`");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		spit('. ');
		$found = false;
		$row['nl'] = (int)$row['nl'];
		if ( $row['mesg_id'] = (int)$row['mesg_id'] ) {
			$cid = $row['mesg_id'];
			if ( isset($campaigns[$cid]) ) {
				$found = true;
				//$cid = $campaigns[$cid]['id'];
				$mid = $campaigns[$cid]['messages'][(int)( $row['b'] and isset($campaigns[$cid]['messages'][1]) )];
			}
		} elseif ( $row['respond_id'] = (int)$row['respond_id'] ) {
			if ( isset($responders[$row['respond_id']]) ) {
				$found = true;
				$cid = $responders[$row['respond_id']]['cid'];
				$mid = $responders[$row['respond_id']]['mid'];
			}
		}
		// if subscriber is not transfered
		$sid = sub_exists($row['from_email']);
		//if ( !isset($subs[$row['from_email']]) ) {
		if ( !$sid ) {
			$found = false;
		}
		if ( $found ) {
			//$sid = $subs[$row['from_email']]['id'];
			$ip = ( ac_str_is_ip($row['ip']) ? ac_sql_escape($row['ip']) : '127.0.0.1' );
			$insert = array(
				'id' => $row['id'], // reuse forward id
				'subscriberid' => $sid,
				'campaignid' => $cid,
				'messageid' => $mid,
				'email_from' => $row['from_email'],
				//'name_from' => '',
				'email_to' => $row['to_email'],
				//'name_to' => '',
				'brief_message' => $row['brief_message'],
				'tstamp' => $row['date'] . ' ' . $row['time'],
				'=ip' => "INET_ATON('$ip')",
			);
			$done = ac_sql_insert('#forward', $insert);
			if ( !$done ) break;
		} // if not a convertible bounce
	}
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// fetch activerss
	spit(_a('Fetching ActiveRSS: '), 'em');
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}rss` WHERE `nl` IN ('$listids') ORDER BY `id` ASC");
	if ( !$sql ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	} else {
		spit(_a('Done'), 'strong|done', 1);
		spit(_a('Converting ActiveRSS: '), 'em', 1);
		while ( $row = mysql_fetch_assoc($sql) ) {
			// deal with lists/status
			$row['nl'] = (int)$row['nl'];
			if ( isset($lists[$row['nl']]) ) {
				spit(sprintf(_a("&bull; Adding campaign '%s': "), $row['subject']), 'em');
				$row['type'] = ( !in_array(strtolower($row['type']), array('text', 'html')) ? 'mime' : $row['type'] );
				$row['_uid'] = 1;
				// add to messages
				$insert = array(
					'id' => 0,
					'userid' => $row['_uid'],
					'cdate' => $cdate,
					'mdate' => $cdate,
					'fromname' => $row['fromname'],
					'fromemail' => $row['frommail'],
					'reply2' => $row['reply2'],
					'priority' => $row['priority'],
					'charset' => $row['charset'],
					'encoding' => $row['encoding'],
					'format' => $row['type'],
					'subject' => $row['subject'],
					'text' => convert_activerss_personalization($row, 'text'),
					'html' => convert_activerss_personalization($row, 'html'),
					'htmlfetch' => $row['htmlfetch'],
					'textfetch' => $row['textfetch'],
					//'hidden' => 0,
				);
				$done = ac_sql_insert('#message', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				$mid = ac_sql_insert_id();

				// set list relation
				$insert = array(
					'id' => 0,
					'messageid' => $mid,
					'listid' => $row['nl'],
				);
				// add this list relation
				$done = ac_sql_insert('#message_list', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}

				// now add campaign
				$insert = array(
					'id' => 0,
					'type' => 'activerss',
					'userid' => $row['_uid'],
					'filterid' => (int)$row['filter'],
					//'bounceid' => -1,
					//'realcid' => 0,
					'name' => $row['subject'],
					'cdate' => $cdate,
					'sdate' => $cdate,
					'ldate' => $cdate,
					//'send_amt' => 0,
					//'total_amt' => 0,
					//'opens' => 0,
					//'uniqueopens' => 0,
					//'linkclicks' => 0,
					//'uniquelinkclicks' => 0,
					//'subscriberclicks' => 0,
					//'forwards' => 0,
					//'uniqueforwards' => 0,
					//'hardbounces' => 0,
					//'softbounces' => 0,
					//'unsubscribes' => 0,
					//'unsubreasons' => 0,
					'status' => 6, // activerss behaves like recurring, we're adding first one, next will be assigned
					//'public' => 1,
					//'mail_transfer' => 0,
					//'mail_send' => 0,
					//'mail_cleanup' => 0,
					'tracklinks' => ( $row['tlinks'] ? $row['type'] : 'none' ),
					'tracklinksanalytics' => 0,
					'trackreads' => (int)$row['treads'],
					'trackreadsanalytics' => 0,
					//'analytics_campaign_name' => '',
					'embed_images' => $row['embed_images'],
					'htmlunsub' => $row['htmlunsub'],
					'textunsub' => $row['textunsub'],
					//'=htmlunsubdata' => 'NULL',
					//'=textunsubdata' => 'NULL',
					'recurring' => 'day1',
					//'split_type' => 'even',
					//'split_offset' => 2,
					//'split_offset_type' => 'day',
					//'responder_offset' => 0,
					//'responder_type' => 'subscribe',
					//'reminder_field' => 'sdate',
					//'reminder_format' => $row['date_format'],
					//'reminder_type' => $row['date_type'],
					//'reminder_offset' => 0,
					//'reminder_offset_type' => 'day',
					//'reminder_offset_sign' => '+',
					//'=reminder_last_cron_run' => 'NULL',
					//'activerss_something' => 1,
				);
				// add this campaign
				$done = ac_sql_insert('#campaign', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				$cid = ac_sql_insert_id();

				// set list relation
				$insert = array(
					'id' => 0,
					'campaignid' => $cid,
					'listid' => $row['nl'],
				);
				// add this list relation
				$done = ac_sql_insert('#campaign_list', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}

				// set message relation
				$insert = array(
					'id' => 0,
					'campaignid' => $cid,
					'messageid' => $mid,
					//'percentage' => 100,
					//'sourcesize' => 0,
				);
				// add this message relation
				$done = ac_sql_insert('#campaign_message', $insert);
				if ( !$done ) {
					spit(_a('Error'), 'strong|error', 1);
					error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
					break;
				}
				spit(_a('Done'), 'strong|done', 1);
			} // if has list relations (should convert)
		} // loop through all messages
	} // all messages fetched
	if ( $fatal ) return;
	spit(_a('Done'), 'strong|done', 1);



	// ATTACHMENTS
	spit(_a('Converting message attachments: '), 'em');
	// instead of using $attachments
	$done = ac_sql_query("
		INSERT INTO
			`#message_file_data`
			SELECT
				a.id,
				a.fileid,
				a.sequence,
				a.data
			FROM
				`{$oldprefix}files_data` a,
				`#message_file` b
			WHERE
				a.fileid = b.id
			ORDER BY a.id
	");
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error());
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// turning off indexes from important tables
	spit(_a('Turning the database indexes back on: '), 'em');
	$done = dbkeys(true);
	if ( !$done ) {
		spit(_a('Error'), 'strong|error', 1);
		error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
		return;
	} else {
		spit(_a('Done'), 'strong|done', 1);
	}



	// at the end, delete old tables?
	$opEscaped = ac_sql_escape($oldprefix, true);
	$sql = ac_sql_query("SHOW TABLES LIKE '$opEscaped%'");
	while ( $row = mysql_fetch_row($sql) ) {
		//ac_sql_query("DROP TABLE `$row[0]`;");
	}

	$completed = true;



}








/*
	FUNCTIONS
*/


function optin_extract($list) {
	$opt = array(
		'optin_confirm' => (int)$list['confirmopt'],
		'optin_format' => ( $list['confirmoptt'] != 'html' ? 'text' : 'html' ),
		'optin_from_name' => $list['opt_in_from_name'],
		'optin_from_email' => $list['opt_in_from_email'],
		'optin_subject' => $list['isubject'],
		'optin_text' => ( $list['confirmoptt'] != 'html' ? $list['icontent'] : '' ),
		'optin_html' => ( $list['confirmoptt'] == 'html' ? $list['icontent'] : '' ),
		'optout_confirm' => (int)$list['confirmopt2'],
		'optout_format' => ( $list['confirmoptt'] != 'html' ? 'text' : 'html' ),
		'optout_from_name' => $list['opt_out_from_name'],
		'optout_from_email' => $list['opt_out_from_email'],
		'optout_subject' => $list['osubject'],
		'optout_text' => ( $list['confirmoptt'] != 'html' ? $list['ocontent'] : '' ),
		'optout_html' => ( $list['confirmoptt'] == 'html' ? $list['ocontent'] : '' ),
	);

	// then check if that type of array already exists
	if ( !isset($GLOBALS['ac_optin_cache']) ) $GLOBALS['ac_optin_cache'] = array();
	$found = false;
	foreach ( $GLOBALS['ac_optin_cache'] as $k => $v ) {
		if ( $k != 0 and $v === $opt ) {
			// found the group
			return $k;
		}
	}
	// save this optin
	// set name
	$addon = array(
		'name' => sprintf(_a('Confirmation Set %s'), count($GLOBALS['ac_optin_cache']) + 1),
	);
	$insert = array_merge($opt, $addon);
	if ( !ac_sql_insert('#optinoptout', $insert) ) return 1;
	$id = (int)ac_sql_insert_id();
	// then set new group
	$GLOBALS['ac_optin_cache'][$id] = $opt;
	// then return id
	return $id;
}


function convert_list4_list5($list, $groups) {
	global $oldprefix;
	// extract optin set
	$optin = optin_extract($list);
	spit(sprintf(_a("&bull; Adding list '%s': "), $list['name']), 'em');
	$insert = array(
		'id' => $list['id'],
		'stringid' => ac_sql_find_next_index('#list', 'stringid', ac_str_urlsafe($list['name'])),
		'userid' => (int)$list['admin'],
		'name' => trim($list['name']),
		'cdate' => $list['date'],
		'p_use_tracking' => $list['messages_linktrack'],
		'p_embed_image' => $list['embeded_images'],
		'p_use_captcha' => (int)($GLOBALS["gd"] and $list['use_captcha']),
		'p_duplicate_send' => $list['a_duplicate_s'],
		'p_duplicate_subscribe' => $list['a_duplicate'],
		'send_last_broadcast' => $list['send_last_broadcast'],
		'private' => $list['a_priv'],
		'carboncopy' => $list['messages_carboncopy'],
		'subscription_notify' => $list['alert_em'],
		'require_name' => $list['require_name'],
		'get_unsubscribe_reason' => $list['get_unsubscribe_reason'],
		'to_name' => _a("Subscriber"),
		'optinoptout' => $optin,
	);
	$done = ac_sql_insert('#list', $insert);
	if ( !$done ) return false;
	$id = ac_sql_insert_id(); // the same id

	// list mailers
	// not implemented yet

	// blocked emails
	$done = ac_sql_query("
		INSERT INTO
			#block
		(
			`id`, `listid`, `phrase`
		)
			SELECT
				0 AS `id`,
				`nl` AS `listid`,
				`phrase`
			FROM
				`{$oldprefix}blocked_emails`
			WHERE
				nl = '$id'
	");
	if ( !$done ) return false;

	// exclusion list
	// extract exclusion list relations
	$done = ac_sql_query("
		INSERT INTO
			#exclusion_list
		(
			`id`, `exclusionid`, `listid`, `sync`
		)
			SELECT
				0 AS `id`,
				`id` AS `exclusionid`,
				$id AS `listid`,
				0 AS `sync`
			FROM
				`#exclusion`
			ORDER BY `id`
	");
	if ( !$done ) return false;

	// list group
	$dflt = ac_sql_default_row('#list_group');
	foreach ( $groups as $i => $g ) {
		$gid = $i + 1;
		if ( $gid == 2 ) continue;
		if ( $gid == 1 and $list['a_priv'] ) continue;
		if ( $gid > 3 and !in_array($id, explode(',', $g['lists'])) ) continue;
		$insert = array(
			'id' => 0,
			'listid' => $id,
			'groupid' => $gid,
		);
		/*
		foreach ( $g as $p => $v ) {
			if ( substr($p, 0, 2) == 'p_' and isset($dflt[$p]) ) $insert[$p] = $v; // add old permissions
		}
		*/
		// add this group
		$done = ac_sql_insert('#list_group', $insert);
		if ( !$done ) return false;
	}
	list_rebuild_user_permissions($id);
	return $id;
}

// converts admin permissions to groups
function convert_admin4_group5($a, &$g) {
	// first define permission array
	$r = array();

	$r['pg_list_add']               = (int)$a['list_create'];
	$r['pg_list_edit']              = (int)$a['list_general'];
	$r['pg_list_delete']            = (int)$a['list_remove'];
	$r['pg_list_opt']               = (int)$a['list_optinoptout'];
	$r['pg_list_headers']           = (int)$a['list_mailheaders'];
	$r['pg_list_emailaccount']      = (int)$a['list_popaccounts'];
	$r['pg_list_bounce']            = (int)$a['list_bounce_settings'];
	$r['pg_message_add']            = (int)$a['messages_create'];
	$r['pg_message_edit']           = (int)$a['messages_saved'];
	$r['pg_message_delete']         = (int)$a['messages_delete'];
	$r['pg_message_send']           = (int)$a['messages_create'];
	$r['pg_subscriber_add']         = (int)$a['subscribers_add'];
	$r['pg_subscriber_edit']        = (int)$a['subscribers_edit'];
	$r['pg_subscriber_delete']      = (int)$a['subscribers_delete'];
	$r['pg_subscriber_import']      = (int)$a['subscribers_import'];
	$r['pg_subscriber_approve']     = (int)$a['subscribers_add'];
	$r['pg_subscriber_export']      = (int)$a['subscribers_export'];
	$r['pg_subscriber_sync']        = ( isset($a['id']) && $a['id'] == 1 ? 1 : (int)$a['admin_sync'] );
	$r['pg_subscriber_filters']     = (int)$a['presets_filters'];
	$r['pg_subscriber_actions']     = (int)$a['list_subscription_rules'];
	$r['pg_subscriber_fields']      = (int)$a['list_subscriberfields'];
	$r['pg_user_add']               = (int)$a['admin_add'];
	$r['pg_user_edit']              = (int)$a['admin_edit'];
	$r['pg_user_delete']            = (int)$a['admin_delete'];
	//$r['pg_group_add']              = (int)$a[''];
	//$r['pg_group_edit']             = (int)$a[''];
	//$r['pg_group_delete']           = (int)$a[''];
	$r['pg_template_add']           = (int)$a['presets_templates'];
	$r['pg_template_edit']          = (int)$a['presets_templates'];
	$r['pg_template_delete']        = (int)$a['presets_templates'];
	$r['pg_personalization_add']    = (int)$a['presets_bodytags'];
	$r['pg_personalization_edit']   = (int)$a['presets_bodytags'];
	$r['pg_personalization_delete'] = (int)$a['presets_bodytags'];
	$r['pg_form_add']               = (int)$a['util_subscribeforms'];
	$r['pg_form_edit']              = (int)$a['util_subscribeforms'];
	$r['pg_form_delete']            = (int)$a['util_subscribeforms'];
	$r['pg_reports_campaign']       = (int)$a['messages_archive'];
	$r['pg_reports_list']           = (int)$a['list_reports'];
	$r['pg_reports_user']           = (int)$a['list_reports'];
	$r['pg_startup_reports']        = (int)$a['messages_archive'];
	$r['pg_startup_gettingstarted'] = 1/*(int)$a['']*/;

	$r['unsubscribelink']           = (int)$a['force_unsubscribe_link'];
	$r['optinconfirm']              = (int)$a['force_optin'];


	$r['limit_mail']                = (int)$a['mails_limit'];
	$r['limit_mail_type']           = $a['mails_limit2'];
	$r['limit_subscriber']          = (int)$a['dflt_subscribers_max'];
	$r['limit_list']                = (int)$a['lists_limit'];
	$r['limit_campaign']            = 0/*$a['limit_campaign']*/;
	$r['limit_campaign_type']       = 'month'/*$a['limit_campaign_type']*/;
	$r['limit_attachment']          = ( (int)$a['dflt_upload_attach_max'] ? (int)$a['dflt_upload_attach_max'] : -1 );
	$r['limit_user']                = (int)$a['users_limit'];


	$r['site_name']                 = $a['brand_name'];
	$r['site_logo']                 = $a['brand_logo'];
	$r['header_text']               = 0;
	$r['header_text_value']         = '';
	$r['header_html']               = 0;
	$r['header_html_value']         = '';
	$r['footer_text']               = (int)( $a['brand_footer'] != '' );
	$r['footer_text_value']         = $a['brand_footer'];
	$r['footer_html']               = (int)( $a['brand_footer_html'] != '' );
	$r['footer_html_value']         = $a['brand_footer_html'];
	$r['copyright']                 = $a['brand_copyright'];
	$r['version']                   = $a['brand_version'];
	$r['license']                   = 1/*$a['brand_links']*/;
	$r['links']                     = $a['brand_links'];
	$r['demo']                      = $a['brand_demo'];

	$r['lists']                     = $a['lists'];

	// then check if that type of array already exists
	foreach ( $g as $k => $v ) {
		if ( $k != 0 and $v === $r ) {
			// found the group
			return $k;
		}
	}
	// then set new group
	$g[] = $r;
	// then return id
	return count($g) - 1;
}


function convert_admin2_username2id($username) {
	$oldprefix = ( isset($GLOBALS['ac_updater_backend']) ? $GLOBALS['ac_updater_backend'] : '12all_' );
	$u = mysql_real_escape_string($username, $GLOBALS['auth_db_link']);
	$sql = mysql_query("SELECT `id` FROM `acp_globalauth` WHERE `username` = '$u'", $GLOBALS['auth_db_link']);
	if ( !$sql ) return 1;
	if ( mysql_num_rows($sql) < 1 ) return 1;
	list($absID) = mysql_fetch_row($sql);
	$sql = mysql_query("SELECT `id` FROM `{$oldprefix}admin` WHERE `user_id` = '$absID'", $GLOBALS['db_link']);
	if ( !$sql ) return 1;
	if ( mysql_num_rows($sql) < 1 ) return 1;
	list($id) = mysql_fetch_row($sql);
	return $id;
}

function extract_userlists($username, $users, $groups) {
	$id = convert_admin2_username2id($username);
	if ( isset($users[$id]) ) {
		$user = $users[$id];
		$group = $groups[$user['_groupid']-1];
		// found admin's group, get it's lists
		return explode(',', $group['lists']);
	}
	return array();
	// old
	foreach ( $users as $user ) {
		if ( $user['user'] == $username ) {
			$group = $groups[$user['_groupid']-1];
			// found admin's group, get it's lists
			return explode(',', $group['lists']);
		}
	}
	return array();
}


function build_message_insert($row) {
	$r = array(
		'id' => $row['id'], // reuse id
		'userid' => $row['_uid'],
		'cdate' => $row['mdate'] . ' ' . $row['mtime'],
		'mdate' => $row['mdate'] . ' ' . $row['mtime'],
		'fromname' => $row['mfromn'],
		'fromemail' => $row['mfrom'],
		'reply2' => $row['reply2'],
		'priority' => $row['priority'],
		'charset' => $row['charset'],
		'encoding' => $row['encoding'],
		'format' => $row['type'],
		'subject' => $row['subject'],
		'text' => fix_message_text($row['textmesg'], $row['texthead'], $row['textfoot']),
		'html' => fix_message_html($row['htmlmesg'], $row['htmlhead'], $row['htmlfoot']),
		'htmlfetch' => $row['htmlfetch'],
		'textfetch' => $row['textfetch'],
		//'hidden' => $row[''],
	);
	return $r;
}

function fix_message_html($html, $header, $footer) {
	global $outlines;
	// try to cleanup HTML code
	if ( function_exists('ac_site_plink') ) {
		$murl = ac_site_plink();
	} elseif ( isset($GLOBALS['siteurl']) ) {
		$murl = $GLOBALS['siteurl'];
	} elseif ( isset($GLOBALS['backend']) ) {
		$murl = $GLOBALS['backend']['murl'];
	} else {
		$murl = '';
	}
	$baseURL = $murl . dirname(dirname(dirname(__FILE__)));
	// base url for images
	$pimgnow = $baseURL . '/';
	$replace_string = $baseURL . '/main.php';
	$pimgnow = str_replace('list_send2.php', 'main.php', $pimgnow);
	$file_loc = $murl . $pimgnow;
	$html = str_replace("$file_loc#", "#", $html);
	$file_loc = $murl . $replace_string;
	$html = str_replace("$file_loc#", "#", $html);
	$file_loc = $murl . $replace_string;
	$html = str_replace("$file_loc%", "%", $html);
	$file_loc = $murl . $pimgnow;
	$html = str_replace("$file_loc%", "%", $html);
	$html = str_replace("$baseURL%", "%", $html);
	$html = str_replace("$murl%", "%", $html);
	$html = str_replace("$murl/%", "%", $html);
	$html = str_replace("www.$murl/%", "%", $html);
	$html = str_replace("$murl/#", "#", $html);
	$html = str_replace("$murl/main.php#", "#", $html);
	$html = str_replace("$murl/admin/main.php#", "#", $html);
	$html = str_replace($murl . "/lt/t_go.php?i=currentmesg&e=subscriberid&l=", "", $html);
	$html = str_replace($murl . "/lt/t_go.php?i=currentmesg&amp;e=subscriberid&amp;l=", "", $html);
	$html = str_replace('&amp;', '&', $html);
	// postliminary message cleanup :)
	$html = str_replace('<link type="text/css" rel="stylesheet" href="" />', '', $html);
	$html = str_replace('<link href="" rel="stylesheet" type="text/css" />', '', $html);
	$html = str_replace('<title></title>', '', $html);
	$html = trim($html);
	$html = convert_system_personalization_tags($html);
	if ( isset($outlines[$header]) ) $html = ac_str_prepend_html($html, $outlines[$header]);
	if ( isset($outlines[$footer]) ) $html = ac_str_append_html( $html, $outlines[$footer]);
	return $html;
}

function fix_message_text($text, $header, $footer) {
	global $outlines;
	// try to cleanup TEXT code
	if ( function_exists('ac_site_plink') ) {
		$murl = ac_site_plink();
	} elseif ( isset($GLOBALS['siteurl']) ) {
		$murl = $GLOBALS['siteurl'];
	} elseif ( isset($GLOBALS['backend']) ) {
		$murl = $GLOBALS['backend']['murl'];
	} else {
		$murl = '';
	}
	$text = str_replace($murl . "/lt/t_go.php?i=currentmesg&e=subscriberid&l=", '', $text);
	$text = str_replace($murl . "/lt/t_go.php?i=currentmesg&amp;e=subscriberid&amp;l=", '', $text);
	$text = str_replace("\r", '', $text);
	$text = trim($text);
	$text = convert_system_personalization_tags($text);
	if ( isset($outlines[$header]) ) $text = ac_str_prepend_text($text, $outlines[$header]);
	if ( isset($outlines[$footer]) ) $text = ac_str_append_text( $text, $outlines[$footer]);
	return $text;
}

function convert_system_personalization_tags($str) {
	$str = str_replace('%PERS_UNSUB%', '%UNSUBSCRIBELINK%', $str);
	return $str;
}

function convert_activerss_personalization($rss, $type) {
	global $oldprefix;
	if ( $type == 'html' ) {
		$str = fix_message_html($rss['htmlmesg'], $rss['texthead'], $rss['textfoot']);
	} else {
		$str = fix_message_text($rss['textmesg'], $rss['htmlhead'], $rss['htmlfoot']);
	}
	// use $row['trimmer'], $row['id']
	$feeds = array();
	$sql = ac_sql_query("SELECT * FROM `{$oldprefix}rssfeeds` WHERE `templateid` = '$rss[id]'");
	while ( $row = ac_sql_fetch_assoc($sql) ) {
		$feeds[$row['id']] = $row['url'];
		$block = build_activerss_block($row['url'], $row['howmany'], $rss['trimmer']);
		if ( $type == 'html' ) $block = nl2br($block);
		$str = str_replace("%FEED_{$row['id']}%", $block, $str);
	}
	if ( count($feeds) ) {
		$block = build_activerss_block(implode('   ', $feeds), $row['howmany']/*use last from the loop above*/, $rss['trimmer']);
		if ( $type == 'html' ) $block = nl2br($block);
		$str = str_replace("%FEED%", $block, $str);
	}
	return $str;
}

function build_activerss_block($url, $loop = 0, $shorten = 0) {
	$trim = ( $shorten > 0 ? '|' . $shorten : '' );
	$show = ( $loop > 0 ? 'ALL' : 'NEW' );
	$code =
		"%RSS-FEED|URL:{$url}|SHOW:{$show}%\n\n" . // start feed section
		"%RSS:CHANNEL:TITLE{$trim}%\n\n" . // print out title
		"%RSS-LOOP|LIMIT:{$loop}%\n\n" . // start item section
		"%RSS:ITEM:DATE%\n" . // within a section
		"%RSS:ITEM:TITLE{$trim}%\n" .
		"%RSS:ITEM:SUMMARY{$trim}%\n" .
		"%RSS:ITEM:LINK%\n\n" .
		"%RSS-LOOP%\n\n" .
		"%RSS-FEED%\n" // end section
	;
	return $code;
}


// get mailing statistics (overalls)
function fetch_mailing_stats($mailingid) {
	global $oldprefix;
	// define output array
	$r = array();

	/*
		deal with general (mailing-based) stats
	*/

	// BOUNCED MESSAGES
	$query = "
		SELECT
			COUNT(DISTINCT(email)) AS total
		FROM
			`{$oldprefix}bounce`
		WHERE
			`mid` = '$mailingid'
		AND
			`type` = 'hard'
	";
	$r['bounces_hard'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );
	$query = "
		SELECT
			COUNT(DISTINCT(email)) AS total
		FROM
			`{$oldprefix}bounce`
		WHERE
			`mid` = '$mailingid'
		AND
			`type` = 'soft'
	";
	$r['bounces_soft'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	// TOTAL LINKS
	// unique emails
	$query = "
		SELECT
			COUNT(DISTINCT(email)) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.nl = '$mailingid'
		AND
			l.link NOT IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['unique_emails'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	// unique clicks
	$query = "
		SELECT
			COUNT(email) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.nl = '$mailingid'
		AND
			l.link NOT IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['unique_clicks'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	// total clicks
	$query = "
		SELECT
			SUM(times) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.nl = '$mailingid'
		AND
			l.link NOT IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['total_clicks'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	/*
		deal with message (a/b split distinction) stats
	*/
	// READ TRACKING
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.nl = '$mailingid'
		AND
			l.link IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['reads_unique'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );
	$query = "
		SELECT
			SUM(times) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.nl = '$mailingid'
		AND
			l.link IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['reads_nonunique'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	// UNSUBSCRIPTIONS
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}listmembersu`
		WHERE
			`mesg_id` = '$mailingid'
	";
	$r['unsubscribed'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}listmembersu`
		WHERE
			`mesg_id` = '$mailingid'
		AND
			`reason` != ''
		AND
			`reason` IS NOT NULL
	";
	$r['unsubscribed_wreason'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	// FORWARDS
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}forward_log`
		WHERE
			`mesg_id` = '$mailingid'
	";
	$r['forwards'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	$query = "
		SELECT
			COUNT(DISTINCT(from_email)) AS total
		FROM
			`{$oldprefix}forward_log`
		WHERE
			`mesg_id` = '$mailingid'
	";
	$r['unique_forwards'] = ( $mailingid ? (int)ac_sql_select_one($query) : 0 );

	return $r;
}

function fetch_responder_stats($responderid, $listid) {
	global $oldprefix;
	// define output array
	$r = array();

	/*
		deal with general (mailing-based) stats
	*/

	// BOUNCED MESSAGES
	$query = "
		SELECT
			COUNT(DISTINCT(email)) AS total
		FROM
			`{$oldprefix}bounce`
		WHERE
			`respond_id` = '$responderid'
		AND
			`type` = 'hard'
	";
	$r['bounces_hard'] = (int)ac_sql_select_one($query);
	$query = "
		SELECT
			COUNT(DISTINCT(email)) AS total
		FROM
			`{$oldprefix}bounce`
		WHERE
			`respond_id` = '$responderid'
		AND
			`type` = 'soft'
	";
	$r['bounces_soft'] = (int)ac_sql_select_one($query);

	// TOTAL LINKS
	// unique emails
	$query = "
		SELECT
			COUNT(DISTINCT(email)) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.respond_id = '$responderid'
		AND
			l.link NOT IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['unique_emails'] = (int)ac_sql_select_one($query);

	// unique clicks
	$query = "
		SELECT
			COUNT(email) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.respond_id = '$responderid'
		AND
			l.link NOT IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['unique_clicks'] = (int)ac_sql_select_one($query);

	// total clicks
	$query = "
		SELECT
			SUM(times) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.respond_id = '$responderid'
		AND
			l.link NOT IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['total_clicks'] = (int)ac_sql_select_one($query);

	/*
		deal with message (a/b split distinction) stats
	*/
	// READ TRACKING
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.respond_id = '$responderid'
		AND
			l.link IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['reads_unique'] = (int)ac_sql_select_one($query);
	$query = "
		SELECT
			SUM(times) AS total
		FROM
			`{$oldprefix}links` l,
			`{$oldprefix}linksd` d
		WHERE
			l.respond_id = '$responderid'
		AND
			l.link IN ('open', '')
		AND
			l.id = d.lid
	";
	$r['reads_nonunique'] = (int)ac_sql_select_one($query);

	// UNSUBSCRIPTIONS
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}listmembersu`
		WHERE
			`respond_id` = '$responderid'
	";
	$r['unsubscribed'] = (int)ac_sql_select_one($query);

	// unsubscribed with the reason
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}listmembersu`
		WHERE
			`respond_id` = '$responderid'
		AND
			`reason` != ''
		AND
			`reason` IS NOT NULL
	";
	$r['unsubscribed_wreason'] = (int)ac_sql_select_one($query);

	// FORWARDS
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}forward_log`
		WHERE
			`respond_id` = '$responderid'
	";
	$r['forwards'] = (int)ac_sql_select_one($query);

	$query = "
		SELECT
			COUNT(DISTINCT(from_email)) AS total
		FROM
			`{$oldprefix}forward_log`
		WHERE
			`respond_id` = '$responderid'
	";
	$r['unique_forwards'] = (int)ac_sql_select_one($query);

	// total_amt / send_amt
	$query = "
		SELECT
			COUNT(*) AS total
		FROM
			`{$oldprefix}listmembers`
		WHERE
			`respond` LIKE '%,$responderid,%'
	";
	$r['send_amt'] = $r['total_amt'] = (int)ac_sql_select_one($query);

	// if anyone unsubscribed, add him to counts
	$query = "SELECT COUNT(*) AS total FROM `{$oldprefix}listmembersu` WHERE `respond_id` = '$responderid'";
	$r['unsubscribed'] = (int)ac_sql_select_one($query);
	$r['send_amt'] += $r['unsubscribed'];
	$r['total_amt'] += $r['unsubscribed'];

	return $r;
}

// ATTACHMENTS
function convert_attachments(&$attachments, $messageid, $list) {
	global $oldprefix;
	$arr = array();
	preg_match_all('/, (\d+) ,/', $list, $match);
	if ( isset($match[1]) and count($match[1]) > 0 ) {
		$arr = array_diff(array_unique($match[1]), $attachments);
		sort($arr);
	}
	if ( count($arr) == 0 ) return true;
	foreach ( $arr as $v ) {
		$done = ac_sql_query("
			INSERT INTO
				`#message_file`
			(
				`id`, `name`, `size`, `mime_type`, `messageid`, `tstamp`
			)
				SELECT
					`id`,
					`rname` AS `name`,
					`size`,
					`mimetype` AS `mime_type`,
					'$messageid' AS `messageid`,
					DATE_FORMAT(`date`, '%Y-%m-%d %H:%i:%s') AS `tstamp`
				FROM
					`{$oldprefix}files_info`
				WHERE
					`id` = '$v'
		");
		if ( !$done ) return false;
		$attachments[] = $v;
	}
	return true;
}

function tablekeys($table, $enable = false) {
	$cmd = ( $enable ? 'ENABLE' : 'DISABLE' );
	return ac_sql_query("ALTER TABLE `$table` $cmd KEYS;");
}

function dbkeys($enable) {
	if ( !tablekeys('#campaign', $enable) ) return false;
	if ( !tablekeys('#campaign_list', $enable) ) return false;
	if ( !tablekeys('#campaign_message', $enable) ) return false;

	if ( !tablekeys('#link', $enable) ) return false;
	if ( !tablekeys('#link_data', $enable) ) return false;
	if ( !tablekeys('#link_log', $enable) ) return false;

	if ( !tablekeys('#list_field_value', $enable) ) return false;

	if ( !tablekeys('#message', $enable) ) return false;
	if ( !tablekeys('#message_list', $enable) ) return false;

	if ( !tablekeys('#subscriber', $enable) ) return false;
	if ( !tablekeys('#subscriber_list', $enable) ) return false;
	//if ( !tablekeys('#subscriber_filter', $enable) ) return false;
	if ( !tablekeys('#subscriber_responder', $enable) ) return false;

	return true;
}



/*
	replacement functions for $subs cache array
*/


// checks if the subscriber exists in the new system already
// input:  12all v4 subscriber email
// input:  optional subscriber's list (used for duplicates check)
// output: 12all v5 subscriber id if exists, zero (0) if not
function sub_exists($email, $list = null) {
	$email = ac_sql_escape($email);
	$list = (int)$list;
	if ( $list ) {
		$sql = ac_sql_query("SELECT s.id FROM #subscriber s, #subscriber_list l WHERE s.email = '$email' AND l.listid = '$list' AND s.id = l.subscriberid");
	} else {
		$sql = ac_sql_query("SELECT id FROM #subscriber WHERE `email` = '$email'");
	}
	if ( !$sql or !mysql_num_rows($sql) ) return 0;
	list($sub) = ac_sql_fetch_row($sql);
	unset($sql);
	return $sub;
}

?>
