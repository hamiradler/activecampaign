UPDATE em_branding SET public_template_htm = REPLACE(public_template_htm, '%HEADERNAV%', '') WHERE public_template_htm LIKE '%\%HEADERNAV\%%';
UPDATE em_branding SET public_template_htm = REPLACE(public_template_htm, '%FOOTERNAV%', '') WHERE public_template_htm LIKE '%\%FOOTERNAV\%%';
UPDATE em_branding SET public_template_htm = REPLACE(public_template_htm, '%LANGSELECT%', '') WHERE public_template_htm LIKE '%\%LANGSELECT\%%';
UPDATE em_branding SET admin_template_htm = REPLACE(admin_template_htm, '%SEARCHBAR%', '') WHERE admin_template_htm LIKE '%\%SEARCHBAR\%%';
