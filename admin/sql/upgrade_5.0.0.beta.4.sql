ALTER TABLE `em_backend` ADD `mailer_log_file` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign` ADD `mailer_log_file` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `mail_cleanup`;
UPDATE `em_trapperr` SET `value` = 'em_trapperrlogs' WHERE `id` = 'db_table' AND `value` = '12all_trapperrlogs';