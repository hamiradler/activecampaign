<?php

spit(_a('Converting single checkbox fields into checkbox groups: '), 'em');
$done = true;
$sql = ac_sql_query("SELECT * FROM `#field` WHERE type = 'checkbox';");
while ( $row = mysql_fetch_assoc($sql) ) {
	/*
	// change the title to empty string?
	$up = array(
		'title' => '',
	);
	$done = ac_sql_update("#field", $up, "id = '$row[id]'");
	if ( !$done ) break;
	*/

	$in = array(
		'id' => 0,
		'fieldid' => $row['id'],
		'orderid' => 1,
		'value' => $row['title'],
		'label' => $row['title'],
	);
	$done = ac_sql_insert("#field_option", $in);
	if ( !$done ) break;

	// update field values to match the new value
	ac_sql_update_one("em_field_value", "val", $row['title'], "fieldid = '$row[id]' AND val = 'checked'");

	// some may already be blank, but catch the ones that aren't
	ac_sql_update_one("em_field_value", "val", "~|", "fieldid = '$row[id]' AND val = 'unchecked'");

	// update segments that use it
	$esctitle = ac_sql_escape($row['title']);
	ac_sql_update_one("#filter_group_cond", "=rhs", "IF(rhs = 'checked', '$esctitle', '~|')", "type = 'custom' AND lhs = '$row[id]' AND op = 'equal'");

}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}



// at the end update the groups to use new name now
//ac_sql_update_one("#field", "type", "checkbox", "type = 'checkboxgroup'");


?>
