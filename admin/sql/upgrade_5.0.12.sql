# Add columns to store sync options
ALTER TABLE `em_sync` ADD `skipbounced` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1';
ALTER TABLE `em_sync` ADD `skipunsub` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1';
ALTER TABLE `em_sync` ADD `lastmessage` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';

# we have added DATE fields by mistake
ALTER TABLE `em_bounce_log` CHANGE `tstamp` `tstamp` DATETIME NULL DEFAULT NULL ;
ALTER TABLE `em_emailaccount_log` CHANGE `tstamp` `tstamp` DATETIME NULL DEFAULT NULL ;

# responder campaigns should still be tracked per list
ALTER TABLE `em_subscriber_responder` ADD `listid` INT( 10 ) NOT NULL DEFAULT '0' AFTER `subscriberid` ;

# we will try to address the dots problem
ALTER TABLE `em_campaign_mailer` ADD `dotfix` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `limitspan` ;
ALTER TABLE `em_list_mailer` ADD `dotfix` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `limitspan` ;
ALTER TABLE `em_mailer` ADD `dotfix` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `limitspan` ;
