ALTER TABLE `em_campaign` CHANGE `htmlunsub` `htmlunsub` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign` CHANGE `textunsub` `textunsub` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign_deleted` CHANGE `htmlunsub` `htmlunsub` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign_deleted` CHANGE `textunsub` `textunsub` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
