ALTER TABLE `em_sync` ADD `optin` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' ;

ALTER TABLE `em_message` ADD `name` VARCHAR( 250 ) NOT NULL AFTER `mdate` ;
UPDATE `em_message` SET `name` = `subject` ;
ALTER TABLE `em_backend` ADD `t_offset_min` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `t_offset` ;
ALTER TABLE `em_user` ADD `t_offset_min` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `t_offset` ;
