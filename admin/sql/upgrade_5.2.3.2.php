<?php

$bmesg = "This address no longer accepts mail.";
$c     = (int)ac_sql_select_one("SELECT COUNT(*) FROM #bounce_code WHERE match = '$bmesg'");

if ($c == 0) {
	# Only insert this code if one was never inserted before.
	$ins = array(
		"code"     => "9.1.2",
		"match"    => $bmesg,
		"type"     => "hard",
		"descript" => "Hard bounce with no bounce code found. It could be an invalid email or rejected email from your mail server (such as from a sending limit).",
	);

	ac_sql_insert("#bounce_code", $ins);
}

?>
