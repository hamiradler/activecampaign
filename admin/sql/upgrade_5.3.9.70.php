<?php

$done = true;
spit(_a('Copying link info into log table: '), 'em', 1);
$sql = ac_sql_query("SELECT * FROM #link");
while ( $row = mysql_fetch_assoc($sql) ) {
	spit('. ');
	$up = array(
		'campaignid' => $row['campaignid'],
		'messageid' => $row['messageid'],
		'isread' => (int)( $row['link'] == 'open' ),
	);
	$done = ac_sql_update("#link_data", $up, "linkid = '$row[id]'");
	if ( !$done ) break;
}

if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
