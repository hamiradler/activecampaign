ALTER TABLE `em_campaign` ADD `mdate` datetime DEFAULT NULL AFTER `cdate`;
ALTER TABLE `em_campaign_deleted` ADD `mdate` datetime DEFAULT NULL AFTER `cdate`;
ALTER TABLE `em_template` ADD `mdate` datetime DEFAULT NULL;
