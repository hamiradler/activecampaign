CREATE TABLE `12all_bounce_codes` (   `id` int(10) NOT NULL auto_increment,   `code` varchar(250) NOT NULL default '',   `matching_string` varchar(250) NOT NULL default '',   `bounce_type` enum('hard','soft') NOT NULL default 'hard',   `description` varchar(250) default NULL,   PRIMARY KEY  (`id`) ) ENGINE=MyISAM
INSERT INTO `12all_bounce_codes` VALUES (1, '5.1.0', '5.1.0', 'hard', 'Other address status');
INSERT INTO `12all_bounce_codes` VALUES (2, '5.1.1', '5.1.1', 'hard', 'Bad destination mailbox address');
INSERT INTO `12all_bounce_codes` VALUES (3, '5.1.2', '5.1.2', 'hard', 'Bad destination system address');
INSERT INTO `12all_bounce_codes` VALUES (4, '5.1.3', '5.1.3', 'hard', 'Bad destination mailbox address syntax');
INSERT INTO `12all_bounce_codes` VALUES (5, '5.1.4', '5.1.4', 'hard', 'Destination mailbox address ambiguous');
INSERT INTO `12all_bounce_codes` VALUES (6, '5.1.5', '5.1.5', 'hard', 'Destination mailbox address valid');
INSERT INTO `12all_bounce_codes` VALUES (7, '5.1.6', '5.1.6', 'hard', 'Mailbox has moved');
INSERT INTO `12all_bounce_codes` VALUES (8, '5.1.7', '5.1.7', 'hard', 'Bad sender''s mailbox address syntax');
INSERT INTO `12all_bounce_codes` VALUES (9, '5.1.8', '5.1.8', 'hard', 'Bad sender''s system address');
INSERT INTO `12all_bounce_codes` VALUES (10, '5.2.0', '5.2.0', 'soft', 'Other or undefined mailbox status');
INSERT INTO `12all_bounce_codes` VALUES (11, '5.2.1', '5.2.1', 'soft', 'Mailbox disabled, not accepting messages');
INSERT INTO `12all_bounce_codes` VALUES (12, '5.2.2', '5.2.2', 'soft', 'Mailbox full');
INSERT INTO `12all_bounce_codes` VALUES (13, '5.2.3', '5.2.3', 'hard', 'Message length exceeds administrative limit.');
INSERT INTO `12all_bounce_codes` VALUES (14, '5.2.4', '5.2.4', 'hard', 'Mailing list expansion problem');
INSERT INTO `12all_bounce_codes` VALUES (15, '5.3.0', '5.3.0', 'hard', 'Other or undefined mail system status');
INSERT INTO `12all_bounce_codes` VALUES (16, '5.3.1', '5.3.1', 'soft', 'Mail system full');
INSERT INTO `12all_bounce_codes` VALUES (17, '5.3.2', '5.3.2', 'hard', 'System not accepting network messages');
INSERT INTO `12all_bounce_codes` VALUES (18, '5.3.3', '5.3.3', 'hard', 'System not capable of selected features');
INSERT INTO `12all_bounce_codes` VALUES (19, '5.3.4', '5.3.4', 'hard', 'Message too big for system');
INSERT INTO `12all_bounce_codes` VALUES (20, '5.4.0', '5.4.0', 'hard', 'Other or undefined network or routing status');
INSERT INTO `12all_bounce_codes` VALUES (21, '5.4.1', '5.4.1', 'hard', 'No answer from host');
INSERT INTO `12all_bounce_codes` VALUES (22, '5.4.2', '5.4.2', 'hard', 'Bad connection');
INSERT INTO `12all_bounce_codes` VALUES (23, '5.4.3', '5.4.3', 'hard', 'Routing server failure');
INSERT INTO `12all_bounce_codes` VALUES (24, '5.4.4', '5.4.4', 'hard', 'Unable to route');
INSERT INTO `12all_bounce_codes` VALUES (25, '5.4.5', '5.4.5', 'soft', 'Network congestion');
INSERT INTO `12all_bounce_codes` VALUES (26, '5.4.6', '5.4.6', 'hard', 'Routing loop detected');
INSERT INTO `12all_bounce_codes` VALUES (27, '5.4.7', '5.4.7', 'hard', 'Delivery time expired');
INSERT INTO `12all_bounce_codes` VALUES (28, '5.5.0', '5.5.0', 'hard', 'Other or undefined protocol status');
INSERT INTO `12all_bounce_codes` VALUES (29, '5.5.1', '5.5.1', 'hard', 'Invalid command');
INSERT INTO `12all_bounce_codes` VALUES (30, '5.5.2', '5.5.2', 'hard', 'Syntax error');
INSERT INTO `12all_bounce_codes` VALUES (31, '5.5.3', '5.5.3', 'soft', 'Too many recipients');
INSERT INTO `12all_bounce_codes` VALUES (32, '5.5.4', '5.5.4', 'hard', 'Invalid command arguments');
INSERT INTO `12all_bounce_codes` VALUES (33, '5.5.5', '5.5.5', 'hard', 'Wrong protocol version');
INSERT INTO `12all_bounce_codes` VALUES (34, '5.6.0', '5.6.0', 'hard', 'Other or undefined media error');
INSERT INTO `12all_bounce_codes` VALUES (35, '5.6.1', '5.6.1', 'hard', 'Media not supported');
INSERT INTO `12all_bounce_codes` VALUES (36, '5.6.2', '5.6.2', 'hard', 'Conversion required and prohibited');
INSERT INTO `12all_bounce_codes` VALUES (37, '5.6.3', '5.6.3', 'hard', 'Conversion required but not supported');
INSERT INTO `12all_bounce_codes` VALUES (38, '5.6.4', '5.6.4', 'hard', 'Conversion with loss performed');
INSERT INTO `12all_bounce_codes` VALUES (39, '5.6.5', '5.6.5', 'hard', 'Conversion failed');
INSERT INTO `12all_bounce_codes` VALUES (40, '5.7.0', '5.7.0', 'hard', 'Other or undefined security status');
INSERT INTO `12all_bounce_codes` VALUES (41, '5.7.1', '5.7.1', 'hard', 'Delivery not authorized, message refused');
INSERT INTO `12all_bounce_codes` VALUES (42, '5.7.2', '5.7.2', 'hard', 'Mailing list expansion prohibited');
INSERT INTO `12all_bounce_codes` VALUES (43, '5.7.3', '5.7.3', 'hard', 'Security conversion required but not possible');
INSERT INTO `12all_bounce_codes` VALUES (44, '5.7.4', '5.7.4', 'hard', 'Security features not supported');
INSERT INTO `12all_bounce_codes` VALUES (45, '5.7.5', '5.7.5', 'hard', 'Cryptographic failure');
INSERT INTO `12all_bounce_codes` VALUES (46, '5.7.6', '5.7.6', 'hard', 'Cryptographic algorithm not supported');
INSERT INTO `12all_bounce_codes` VALUES (47, '5.7.7', '5.7.7', 'hard', 'Message integrity failure');
INSERT INTO `12all_bounce_codes` VALUES (48, '5.0.0', '5.0.0', 'hard', 'Address does not exist');
CREATE TABLE `12all_trapperr` (   `id` varchar(32) NOT NULL default '',   `value` varchar(255) NOT NULL default '',   `devdesc` text NOT NULL,   PRIMARY KEY  (`id`) ) ENGINE=MyISAM COMMENT='trapperr (error-handler) configuration'
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('xml_date_format', 'Y-m-d H:i:s (T)', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('sql_date_format', 'Y-m-d H:i:s', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('date_format', 'G:i:s, j. n. Y. (T)', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('db', '1', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('db_table', '12all_trapperrlogs', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('mail', '0', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('mail_to', 'bugs@activecampaign.com', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('mail_subject', '1-2-All PHP Error', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('screen', '1', '')
INSERT INTO `12all_trapperr` (`id`, `value`, `devdesc`) VALUES ('logfile', '0', '')
CREATE TABLE `12all_trapperrlogs` (   `id` int(10) unsigned NOT NULL auto_increment,   `tstamp` datetime NOT NULL default '0000-00-00 00:00:00',   `errnumber` int(6) unsigned NOT NULL default '0',   `errmessage` text NOT NULL,   `filename` varchar(255) NOT NULL default '',   `url` varchar(255) NOT NULL default '',   `linenum` int(10) unsigned NOT NULL default '0',   `vars` text NOT NULL,   `backtrace` text NOT NULL,   `session` varchar(32) NOT NULL default '',   `userid` int(10) unsigned NOT NULL default '0',   `ip` varchar(15) NOT NULL default '',   `host` varchar(100) NOT NULL default '',   `referer` varchar(255) NOT NULL default '',   PRIMARY KEY  (`id`) ) ENGINE=MyISAM COMMENT='run-time errors log'
CREATE TABLE `12all_hooks` (   `id` int(10) NOT NULL auto_increment,   `description` text NOT NULL,   `hook_group` varchar(250) NOT NULL default '',   `code` longtext NOT NULL,   `hook_call` varchar(250) NOT NULL default '',   `activation` text NOT NULL,   `status` int(1) NOT NULL default '0',   `tstamp` timestamp(14) NOT NULL,   PRIMARY KEY  (`id`) ) ENGINE=MyISAM
CREATE TABLE `12all_hooks_config` (   `id` int(10) NOT NULL auto_increment,   `hook_group` int(25) NOT NULL default '0',   `hook_key` varchar(250) NOT NULL default '',   `hook_val` longtext NOT NULL,   `status` int(1) NOT NULL default '0',   PRIMARY KEY  (`id`) ) ENGINE=MyISAM
CREATE TABLE `12all_hooks_groups` (   `id` int(10) NOT NULL auto_increment,   `name` varchar(250) NOT NULL default '',   `version` varchar(25) NOT NULL default '',   `description` longtext NOT NULL,   `status` int(1) NOT NULL default '0',   PRIMARY KEY  (`id`) ) ENGINE=MyISAM
