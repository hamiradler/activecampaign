|ALTER TABLE `em_campaign` CHANGE `processid` `sendid` int(10) not null default 0;
|ALTER TABLE `em_campaign_deleted` CHANGE `processid` `sendid` int(10) not null default 0;

CREATE TABLE `em_address` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `userid` int(10) NOT NULL DEFAULT '0', `sender_name` varchar(250) NOT NULL DEFAULT '', `sender_addr1` varchar(250) NOT NULL DEFAULT '', `sender_addr2` varchar(250) NOT NULL DEFAULT '', `sender_city` varchar(250) NOT NULL DEFAULT '', `sender_state` varchar(250) NOT NULL DEFAULT '', `sender_zip` varchar(250) NOT NULL DEFAULT '', `sender_country` varchar(250) NOT NULL DEFAULT '', `sender_phone` varchar(250) NOT NULL DEFAULT '', PRIMARY KEY (`id`), KEY `userid` (`userid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
INSERT INTO `em_address` (id, userid, sender_name, sender_addr1, sender_addr2, sender_city, sender_state, sender_zip, sender_country, sender_phone) SELECT 0 AS id, userid, sender_name, sender_addr1, sender_addr2, sender_city, sender_state, sender_zip, sender_country, sender_phone FROM em_list GROUP BY userid, sender_name, sender_addr1, sender_addr2, sender_city, sender_state, sender_zip, sender_country, sender_phone ;

#ALTER TABLE `em_bounce_data` ADD `sendid` int(10) NOT NULL DEFAULT '0' AFTER `messageid`;
#ALTER TABLE `em_bounce_log` ADD `sendid` int(10) NOT NULL DEFAULT '0' AFTER `messageid`;
ALTER TABLE `em_log` ADD `sendid` int(10) NOT NULL DEFAULT '0' AFTER `messageid`;
UPDATE `em_log` l, `em_campaign` c SET l.sendid = c.sendid WHERE l.campaignid = c.id;
UPDATE `em_log` l, `em_campaign_deleted` c SET l.sendid = c.sendid WHERE l.campaignid = c.id;