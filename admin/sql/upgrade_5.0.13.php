<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

spit(_a('Adding new bounce code - 9.1.1: '), 'em');
$insert = array(
	'id' => '0',
	'code' => '9.1.1',
	'match' => 'This is a permanent error.',
	'type' => 'hard',
	'descript' => _a('Permanent error'),
);
$done = ac_sql_insert('#bounce_code', $insert);
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
