ALTER TABLE `em_form_part` ADD `imagedata` MEDIUMBLOB NULL DEFAULT NULL;
ALTER TABLE `em_form_part` ADD `imagetype` VARCHAR(250) NOT NULL DEFAULT '';
ALTER TABLE `em_form_part` ADD `imagesize` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_form_part` ADD `imagealt` VARCHAR(250) NOT NULL DEFAULT '';
