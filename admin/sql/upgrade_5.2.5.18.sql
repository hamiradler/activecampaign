UPDATE `em_backend` SET `admin_editor` = 'html' WHERE `admin_editor` IS NULL ;
ALTER TABLE `em_backend` CHANGE `admin_editor` `admin_editor` ENUM( 'html', 'text' ) NOT NULL DEFAULT 'html' ;

UPDATE `em_campaign_deleted` SET `tracklinks` = 'all' WHERE `tracklinks` IS NULL ;
ALTER TABLE `em_campaign_deleted` CHANGE `tracklinks` `tracklinks` ENUM( 'all', 'mime', 'html', 'text', 'none' ) NOT NULL DEFAULT 'all' ;

UPDATE `em_exclusion` SET `matchtype` = 'exact' WHERE `matchtype` IS NULL ;
ALTER TABLE `em_exclusion` CHANGE `matchtype` `matchtype` ENUM( 'exact', 'begin', 'end' ) NOT NULL DEFAULT 'exact' ;

UPDATE `em_message` SET `format` = 'mime' WHERE `format` IS NULL ;
ALTER TABLE `em_message` CHANGE `format` `format` ENUM( 'html', 'text', 'mime' ) NOT NULL DEFAULT 'mime' ;

UPDATE `em_optinoptout` SET `optin_format` = 'mime' WHERE `optin_format` IS NULL ;
ALTER TABLE `em_optinoptout` CHANGE `optin_format` `optin_format` ENUM( 'html', 'text', 'mime' ) NOT NULL DEFAULT 'mime' ;
UPDATE `em_optinoptout` SET `optout_format` = 'mime' WHERE `optout_format` IS NULL ;
ALTER TABLE `em_optinoptout` CHANGE `optout_format` `optout_format` ENUM( 'html', 'text', 'mime' ) NOT NULL DEFAULT 'mime' ;

UPDATE `em_personalization` SET `format` = 'html' WHERE `format` IS NULL ;
ALTER TABLE `em_personalization` CHANGE `format` `format` ENUM( 'html', 'text' ) NOT NULL DEFAULT 'html' ;

UPDATE `em_template` SET `format` = 'html' WHERE `format` IS NULL ;
ALTER TABLE `em_template` CHANGE `format` `format` ENUM( 'html', 'text' ) NOT NULL DEFAULT 'html' ;

