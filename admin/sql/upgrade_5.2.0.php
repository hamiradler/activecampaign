<?php

require_once(dirname(dirname(__FILE__)) . '/functions/em.php');

$rs = ac_sql_query("SELECT * FROM #exclusion");
while ($row = ac_sql_fetch_assoc($rs)) {
	if (preg_match('/\*$/', $row["email"])) {
		$up = array(
			"email" => rtrim($row["email"], "*"),
			"matchtype" => "begin",
		);

		ac_sql_update("#exclusion", $up, "id = '$row[id]'");
	} elseif (preg_match('/^\*/', $row["email"])) {
		$up = array(
			"email" => ltrim($row["email"], "*"),
			"matchtype" => "end",
		);

		ac_sql_update("#exclusion", $up, "id = '$row[id]'");
	}
}

// message templates import
$templates_import = import_files("template", "xml");

?>
