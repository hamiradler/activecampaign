# cleaning up per-campaign mailer info
ALTER TABLE `em_campaign_mailer`  DROP `listid`,  DROP `corder`,  DROP `threshold`,  DROP `frequency`,  DROP `pause`,  DROP `limit`,  DROP `limitspan`,  DROP `dotfix`,  DROP `current`,  DROP `sent`;
ALTER TABLE `em_mailer` ADD `current` tinyint(1) NOT NULL DEFAULT '0' AFTER `dotfix` ;
ALTER TABLE `em_mailer` ADD `sent` int(10) NOT NULL DEFAULT '0' AFTER `current` ;
ALTER TABLE `em_mailer` ADD `tstamp` DATETIME NULL AFTER `sent` ;
UPDATE `em_mailer` SET `current` = 1 WHERE `id` = 1 ;
UPDATE `em_mailer` SET `limit` = 0 WHERE `limit` > 0 AND `frequency` > 0 AND `pause` > 0 ;
DROP TABLE `em_list_mailer` ;
ALTER TABLE `em_backend`  DROP `stype`,  DROP `sdfreq`,  DROP `sdnum`,  DROP `sdlim`,  DROP `sdspan` ;

# new field for text we need to store
ALTER TABLE `em_backend` ADD `acpow` VARCHAR( 200 ) NOT NULL DEFAULT '' AFTER `acff` ;