CREATE TABLE `em_tag` ( `id` int(10) unsigned NOT NULL AUTO_INCREMENT, `tag` varchar(250) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `tag` (`tag`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `em_template_tag` ( `id` int(10) unsigned NOT NULL AUTO_INCREMENT, `templateid` int(10) unsigned NOT NULL DEFAULT '0', `tagid` int(10) unsigned NOT NULL DEFAULT '0', PRIMARY KEY (`id`), KEY `templateid` (`templateid`), KEY `tagid` (`tagid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `em_campaign` ADD `basetemplateid` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `threadid`;
ALTER TABLE `em_campaign` ADD `basecampaignid` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `basetemplateid`;
