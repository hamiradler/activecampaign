ALTER TABLE `em_subscriber_data` ADD `ga_campaign_source` varchar(250) not null default '';
ALTER TABLE `em_subscriber_data` ADD `ga_campaign_name` varchar(250) not null default '';
ALTER TABLE `em_subscriber_data` ADD `ga_campaign_medium` varchar(250) not null default '';
ALTER TABLE `em_subscriber_data` ADD `ga_campaign_term` varchar(250) not null default '';
ALTER TABLE `em_subscriber_data` ADD `ga_campaign_content` varchar(250) not null default '';
ALTER TABLE `em_subscriber_data` ADD `ga_campaign_customsegment` varchar(250) not null default '';
ALTER TABLE `em_subscriber_data` ADD `ga_first_visit` datetime null default null;
ALTER TABLE `em_subscriber_data` ADD `ga_times_visited` int(10) not null default '0';
