<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/ac_global/functions/admin.php');
require_once(dirname(dirname(__FILE__)) . '/functions/em.php');

$admin = ac_admin_get();

if ($admin["brand_links"]) {
	// message templates import for SurveyMonkey integration
	spit(_a('Adding Survey email templates: '), 'em');
	$templates_import = import_files("template", "xml", array("survey.xml"));
	$templates_import = import_files("template", "xml", array("survey2.xml"));
	spit(_a('Done'), 'strong|done', 1);
}

?>
