<?php

require_once(dirname(dirname(__FILE__)) . '/functions/em.php');

// message templates import

$templates_import = import_files("template", "xml", array('bookkeepr.xml'));
$templates_import = import_files("template", "xml", array('eatery.xml'));
$templates_import = import_files("template", "xml", array('acme-orange.xml'));
$templates_import = import_files("template", "xml", array('vacation.xml'));
$templates_import = import_files("template", "xml", array('snowman-greeting.xml'));
$templates_import = import_files("template", "xml", array('business-event.xml'));
$templates_import = import_files("template", "xml", array('explore.xml'));

// Grab any orphaned template list rows, template tag rows, and get rid of them.
ac_sql_query("DELETE FROM em_template_list WHERE (SELECT COUNT(*) FROM em_template t WHERE t.id = em_template_list.templateid) = 0");
ac_sql_query("DELETE FROM em_template_tag WHERE (SELECT COUNT(*) FROM em_template t WHERE t.id = em_template_tag.templateid) = 0");

?>
