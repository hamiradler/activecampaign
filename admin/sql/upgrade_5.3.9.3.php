<?php

#examples in tickets: 799686, 802116, 801994

spit(_a('Removing duplicate campaign counts: '), 'em');
$done = true;
$removed = array();
$sql = ac_sql_query("SELECT * FROM `#campaign_count` ;");
while ( $row = mysql_fetch_assoc($sql) ) {
	if ( isset($removed[$row['id']]) ) continue;
	$sql2 = ac_sql_query("
		SELECT
			id
		FROM
			#campaign_count
		WHERE
			id != '$row[id]'
		AND
			campaignid = '$row[campaignid]'
		AND
			amt = '$row[amt]'
		AND
			tstamp BETWEEN SUBDATE('$row[tstamp]', INTERVAL 4 MINUTE) AND ADDDATE('$row[tstamp]', INTERVAL 4 MINUTE)
	");
	if ( !$sql2 ) {
		$done = false;
		break;
	}
	while ( $line = mysql_fetch_assoc($sql2) ) {
		ac_sql_delete("#campaign_count", "id = '$line[id]'");
		$removed[$line['id']] = $line['id'];
	}
	$removed[$row['id']] = $row['id'];
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}



return;


spit(_a('Fixing campaign counts: '), 'em');
$done = true;
$appenders = array_unique(array_merge(
	ac_sql_select_list("SELECT id FROM `#campaign` WHERE type IN ('responder', 'reminder', 'special') ;"),
	ac_sql_select_list("SELECT id FROM `#campaign_deleted` WHERE type IN ('responder', 'reminder', 'special') ;")
));
foreach ( $appenders as $cid ) {
	$realamt = 0;
	$prevamt = 0;
	$sql = ac_sql_query("SELECT * FROM `#campaign_count` WHERE campaignid = '$cid'");
	while ( $row = mysql_fetch_assoc($sql) ) {
		$rowamt = (int)$row['amt'];
		//dbg($row, 1);

		// if previous was not found, then this is set as first
		if ( !$realamt ) {
			$prevamt = $realamt = $rowamt;
			continue;
		}

		if ( $prevamt < $rowamt ) {
			$newc = $rowamt - $prevamt;
			//ac_sql_update_one("#campaign_count", "amt", $newc, "id = '$row[id]'");
			dbg($row, 1);
			dbg($newc, 1);
		} elseif ( $prevamt == $rowamt ) {
			//ac_sql_update_one("#campaign_count", "amt", $newc = 0, "id = '$row[id]'");
		} else {
			// if previous number was greater, then we are done with this campaign, we got to the fixed part
			break;
			$realamt += $rowamt;
		}

		$prevamt = $rowamt;
	}
	//dbg("prev:$prevamt\nreal:$realamt",1);
}

ac_sql_delete("#campaign_count", "amt = 0");

//dbg($appenders);

?>
