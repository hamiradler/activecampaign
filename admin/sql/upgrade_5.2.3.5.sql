CREATE TABLE `em_campaign_source_data` (  `id` int(10) NOT NULL auto_increment,  `sourceid` int(10) NOT NULL default '0',  `sequence` int(10) NOT NULL default '0',  `data` mediumblob,  PRIMARY KEY  (`id`),  KEY `sourceid` (`sourceid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `em_campaign_source` CHANGE `cid` `campaignid` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign_source` CHANGE `mid` `messageid` INT(10) UNSIGNED NOT NULL DEFAULT '0';
