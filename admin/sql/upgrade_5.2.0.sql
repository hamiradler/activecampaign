ALTER TABLE `em_exclusion` ADD `hidden` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_branding` ADD `twitter_consumer_key` VARCHAR( 128 ) NOT NULL DEFAULT 'JsjUb8QUaCg0fUDRfxnfcg';
ALTER TABLE `em_branding` ADD `twitter_consumer_secret` VARCHAR( 128 ) NOT NULL DEFAULT 'ufR6occzeroEg4QzDYDZbqL8vMC8bji1a7c8oAYVM';
ALTER TABLE `em_form` ADD `charset` VARCHAR(64) NOT NULL DEFAULT '' AFTER `type`;
CREATE TABLE `em_campaign_deleted` ( `id` int(10) NOT NULL DEFAULT '0', `type` enum('single','recurring','split','responder','reminder','special','activerss') NOT NULL default 'single', `userid` int(10) NOT NULL default '0', `filterid` int(10) NOT NULL default '0', `bounceid` int(10) NOT NULL default '-1', `realcid` int(10) NOT NULL default '0', `processid` int(10) NOT NULL default '0', `name` varchar(250) NOT NULL default '', `cdate` datetime default NULL, `sdate` datetime default NULL, `ldate` datetime default NULL, `send_amt` int(10) NOT NULL default '0', `total_amt` int(10) NOT NULL default '0', `opens` int(10) NOT NULL default '0', `uniqueopens` int(10) NOT NULL default '0', `linkclicks` int(10) NOT NULL default '0', `uniquelinkclicks` int(10) NOT NULL default '0', `subscriberclicks` int(10) unsigned NOT NULL default '0', `forwards` int(10) NOT NULL default '0', `uniqueforwards` int(10) NOT NULL default '0', `hardbounces` int(10) NOT NULL default '0', `softbounces` int(10) NOT NULL default '0', `unsubscribes` int(10) NOT NULL default '0', `unsubreasons` int(10) NOT NULL default '0', `updates` int(10) unsigned NOT NULL default '0', `status` tinyint(1) NOT NULL default '0', `public` tinyint(1) NOT NULL default '1', `mail_transfer` tinyint(1) NOT NULL default '0', `mail_send` tinyint(1) NOT NULL default '0', `mail_cleanup` tinyint(1) NOT NULL default '0', `mailer_log_file` tinyint(1) NOT NULL default '0', `tracklinks` enum('mime','html','text','none') default NULL, `tracklinksanalytics` tinyint(1) NOT NULL default '1', `trackreads` tinyint(1) NOT NULL default '1', `trackreadsanalytics` tinyint(1) NOT NULL default '1', `analytics_campaign_name` varchar(250) NOT NULL default '', `tweet` tinyint(1) NOT NULL default '0', `embed_images` tinyint(1) NOT NULL default '0', `htmlunsub` tinyint(1) NOT NULL default '1', `textunsub` tinyint(1) NOT NULL default '1', `htmlunsubdata` text, `textunsubdata` text, `recurring` varchar(16) NOT NULL default 'day1', `split_type` enum('even','read','click') NOT NULL default 'even', `split_offset` int(10) NOT NULL default '2', `split_offset_type` enum('hour','day','week','month') NOT NULL default 'day', `split_winner_messageid` int(10) NOT NULL default '0', `split_winner_awaiting` tinyint(1) NOT NULL default '0', `responder_offset` int(10) NOT NULL default '0', `responder_type` enum('subscribe','unsubscribe') NOT NULL default 'subscribe', `reminder_field` varchar(25) NOT NULL default 'sdate', `reminder_format` varchar(100) default NULL, `reminder_type` enum('month_day','year_month_day') NOT NULL default 'month_day', `reminder_offset` int(10) NOT NULL default '0', `reminder_offset_type` enum('day','week','month','year') NOT NULL default 'day', `reminder_offset_sign` enum('+','-') NOT NULL default '+', `reminder_last_cron_run` date default NULL, `activerss_interval` varchar(16) NOT NULL default 'day1', PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `em_list` ADD `sender_name` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_addr1` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_addr2` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_city` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_state` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_zip` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_country` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_list` ADD `sender_phone` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `em_group_limit` ADD `forcesenderinfo` TINYINT( 1 ) NOT NULL DEFAULT '0';

ALTER TABLE em_exclusion ADD matchtype ENUM('exact','begin','end') DEFAULT 'exact';

ALTER TABLE `em_feedbackloop` ADD `campaignid` INT( 10 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_feedbackloop` ADD `messageid` INT( 10 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_feedbackloop` ADD `listid` INT( 10 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_feedbackloop` ADD `subscriberid` INT( 10 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_mailer` ADD `name` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `id`;
UPDATE `em_mailer` SET `name` = IF(`type`=0,'Default', CONCAT('SMTP at ', `host`));
UPDATE `em_mailer` SET `type` = 0 WHERE `type` = 3;

ALTER TABLE  `em_message` ADD  `preview_mime` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE  `em_message` ADD  `preview_data` LONGBLOB NULL ;
ALTER TABLE  `em_template` ADD  `categoryid` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '1';
ALTER TABLE  `em_template` ADD  `preview_mime` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE  `em_template` ADD  `preview_data` LONGBLOB NULL ;

CREATE TABLE `em_sso_token` ( `id` int(10) unsigned NOT NULL  AUTO_INCREMENT, `token` varchar(32) NOT NULL DEFAULT '', `cdate` datetime NULL, `edate` datetime NULL, `userid` int(10) unsigned NOT NULL DEFAULT '0', `userip` int(10) unsigned NOT NULL DEFAULT '0', `appip` int(10) unsigned NOT NULL DEFAULT '0', `apphash` varchar(32) NOT NULL DEFAULT '', PRIMARY KEY (`id`), UNIQUE KEY `token` (`token`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;

ALTER TABLE `em_branding` ADD `admin_template_htm` TEXT DEFAULT NULL ;
ALTER TABLE `em_branding` ADD `admin_template_css` TEXT DEFAULT NULL ;
ALTER TABLE `em_branding` ADD `public_template_htm` TEXT DEFAULT NULL ;
ALTER TABLE `em_branding` ADD `public_template_css` TEXT DEFAULT NULL ;

|ALTER TABLE `em_subscriber_import` DROP INDEX `email` ;
|ALTER TABLE `em_subscriber_import` ADD INDEX `tstamp` ( `tstamp` ) ;

ALTER TABLE `em_backend` DROP `general_passvars`;

ALTER TABLE  `em_template` ADD  `subject` TEXT NULL AFTER  `name` ;
UPDATE `em_template` SET `subject` = `name` ;
