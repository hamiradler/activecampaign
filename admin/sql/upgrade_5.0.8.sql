ALTER TABLE `em_exclusion` ADD `wildcard` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0';
UPDATE `em_exclusion` SET `wildcard` = 1 WHERE LOCATE('%', `email`) > 0 OR LOCATE('_', `email`) > 0;
