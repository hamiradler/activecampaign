UPDATE `em_campaign` SET `sdate` = `cdate` WHERE `type` IN ('responder', 'reminder') AND ( `sdate` IS NULL OR `sdate` = '0000-00-00 00:00:00' );
ALTER TABLE `em_backend` ADD `stream_set_blocking` TINYINT(1) NOT NULL DEFAULT '0' AFTER `mailer_log_file`;
UPDATE `em_trapperr` SET `value` = 'em_trapperrlogs' WHERE `id` = 'db_value';
ALTER TABLE `em_user` ADD `ldate` DATE NULL;
ALTER TABLE `em_user` ADD `ltime` TIME NULL;
ALTER TABLE `em_message_archive` ADD `messageid` INT(10) NOT NULL DEFAULT '0' AFTER `campaignid`;
ALTER TABLE `em_message_archive` DROP INDEX `campaignid`;
ALTER TABLE `em_message_archive` ADD INDEX `campaignmessage` ( `campaignid` , `messageid` );
CREATE TABLE `em_subscriber_import` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `processid` int(10) NOT NULL DEFAULT '0', `email` varchar(250) NOT NULL DEFAULT '', `res` tinyint(1) NOT NULL DEFAULT '0', `msg` varchar(250) NOT NULL DEFAULT '', `tstamp` datetime NULL, PRIMARY KEY (`id`), KEY `processid` (`processid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
