ALTER TABLE `12all_respond` ADD `is_date_message` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `12all_respond` ADD `date_subscriber_field`  VARCHAR( 25 ) NOT NULL ;
ALTER TABLE `12all_respond` ADD `date_format` VARCHAR( 100 ) NULL;
ALTER TABLE `12all_respond` ADD `date_type` ENUM( 'month_day', 'year_month_day' ) NOT NULL;
ALTER TABLE `12all_respond` ADD `date_offset_days` INT( 10 ) NOT NULL ;
ALTER TABLE `12all_respond` ADD `date_offset_plus_minus` ENUM( '+', '-' ) NOT NULL;
ALTER TABLE `12all_respond` ADD `date_last_cron_run` DATE NOT NULL;