<?php

  // remove this when done testing
  //require_once(dirname(dirname(__FILE__)) . '/prepend.inc.php');

  require_once ac_admin("functions/template.php");
  require_once ac_admin("functions/user.php");
  require_once ac_admin("functions/permission.php");
  require_once ac_global_functions("ajax.php");

  $rs = ac_sql_query("SELECT * FROM #campaign WHERE managetext = 0");
  while ($row = ac_sql_fetch_assoc($rs)) {
	  $ms = ac_sql_select_list("SELECT messageid FROM #campaign_message WHERE campaignid = '$row[id]'");
	  $mstr = implode("','", $ms);
	  $c = (int)ac_sql_select_one("SELECT COUNT(*) FROM #message WHERE format = 'mime' AND id IN ('$mstr')");

	  if ($c > 0)
		  ac_sql_query("UPDATE #campaign SET managetext = 1 WHERE id = '$row[id]'");
  }

  $tags = array(
    "travel" => 0,
    "sale" => 0,
    "technology" => 0,
    "corporate" => 0,
    "services" => 0,
    "education" => 0,
    "real estate" => 0,
    "holiday" => 0,
    "simple" => 0,
    "creative" => 0,
    "event" => 0,
    "entertainment" => 0,
  );

  // insert tags if they are not there already
  foreach ($tags as $tag => $id) {
    $id = (int)ac_sql_select_one("SELECT id FROM #tag WHERE tag = '$tag'");
    if ($id) {
      $tags[$tag] = $id;
    }
    else {
      $insert = array(
        "tag" => $tag,
      );
      $sql = ac_sql_insert("#tag", $insert);
      $tags[$tag] = ac_sql_insert_id();
    }
  }

  $templates = ac_sql_select_array("SELECT * FROM #template");

  foreach ($templates as $template) {

    $tags_to_add = array();

    switch ($template["name"]) {

      case "Aqua Ocean" :
        $tags_to_add = array("travel");
      break;

      case "Beige Red Discount" :
        $tags_to_add = array("sale");
      break;

      case "Blue Computer" :
        $tags_to_add = array("technology", "corporate");
      break;

      case "Boats Fishing" :
        $tags_to_add = array("travel", "services", "corporate");
      break;

      case "Books Education" :
        $tags_to_add = array("education");
      break;

      case "Buildings Sky" :
        $tags_to_add = array("real estate");
      break;

      case "Christmas" :
        $tags_to_add = array("holiday");
      break;

      case "Clean Black" :
        $tags_to_add = array("simple");
      break;

      case "Clean Type" :
        $tags_to_add = array("simple");
      break;

      case "Creative Company" :
        $tags_to_add = array("creative", "services", "corporate");
      break;

      case "Event Template" :
        $tags_to_add = array("event");
      break;

      case "General Business" :
        $tags_to_add = array("corporate");
      break;

      case "Green Shades" :
        $tags_to_add = array("simple", "creative");
      break;

      case "Home Consulting" :
        $tags_to_add = array("corporate", "services");
      break;

      case "Hosting Company" :
        $tags_to_add = array("corporate", "services", "technology");
      break;

      case "Hotel" :
        $tags_to_add = array("corporate", "services");
      break;

      case "Night Dance" :
        $tags_to_add = array("creative", "entertainment");
      break;

      case "Ocean Beach" :
        $tags_to_add = array("services", "real estate");
      break;

      case "Orange Consulting" :
        $tags_to_add = array("corporate", "services");
      break;

      case "Red Consultant" :
        $tags_to_add = array("corporate", "services");
      break;

      case "Red Design Company" :
        $tags_to_add = array("corporate", "services", "creative");
      break;

      case "Red Postcard" :
        $tags_to_add = array("simple");
      break;

      case "School Donation" :
        $tags_to_add = array("event", "education");
      break;

      case "Simple Beige" :
        $tags_to_add = array("simple");
      break;

      case "Simple Grey Brown" :
        $tags_to_add = array("simple", "corporate");
      break;
    }

    foreach ($tags_to_add as $tag) {
      $rel_exists = (int)ac_sql_select_one("SELECT COUNT(*) FROM #template_tag WHERE templateid = '$template[id]' AND tagid = '$tags[$tag]'");
      if (!$rel_exists) {
        $insert = array(
          "templateid" => $template["id"],
          "tagid" => $tags[$tag],
        );
        $sql = ac_sql_insert("#template_tag", $insert);
        $template_tag_id = ac_sql_insert_id();
      }
    }
  }

?>
