# REPORT ABUSE
CREATE TABLE `em_abuse` ( `id` int(10) unsigned NOT NULL  AUTO_INCREMENT, `rdate` datetime NULL, `listid` int(10) unsigned NOT NULL DEFAULT '0', `campaignid` int(10) unsigned NOT NULL DEFAULT '0', `messageid` int(10) unsigned NOT NULL DEFAULT '0', `userid` int(10) unsigned NOT NULL DEFAULT '0', `groupid` int(10) unsigned NOT NULL DEFAULT '0', `subscriberid` int(10) unsigned NOT NULL DEFAULT '0', `email` varchar(250) NOT NULL DEFAULT '', PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
ALTER TABLE `em_group_limit` ADD `abuseratio` TINYINT( 3 ) NOT NULL DEFAULT '4';

# FIX FOR EMAIL INDEX IN SUBSCRIBER IMPORT TABLE
TRUNCATE TABLE `em_subscriber_import`;
|ALTER TABLE `em_subscriber_import` DROP INDEX `email`;
ALTER TABLE `em_subscriber_import` ADD INDEX ( `email` );

# CAMPAIGN APPROVAL
CREATE TABLE `em_approval` ( `id` int(10) unsigned NOT NULL  AUTO_INCREMENT, `campaignid` int(10) unsigned NOT NULL DEFAULT '0', `userid` int(10) unsigned NOT NULL DEFAULT '0', `groupid` int(10) unsigned NOT NULL DEFAULT '0', `approved` tinyint(1) NOT NULL DEFAULT '0', `adminid` int(10) unsigned NOT NULL DEFAULT '0', `sdate` datetime NULL, `adate` datetime NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
ALTER TABLE `em_group` ADD `req_approval` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_group` ADD `req_approval_1st` TINYINT( 3 ) NOT NULL DEFAULT '2';
ALTER TABLE `em_group` ADD `req_approval_notify` VARCHAR( 250 ) NOT NULL;
