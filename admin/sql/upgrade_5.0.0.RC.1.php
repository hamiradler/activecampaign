<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

$tables = array();
$rs = ac_sql_query("SHOW TABLES LIKE 'em_%'");
while ($row = ac_sql_fetch_row($rs)) {
	$tables[$row[0]] = array(
		"offset"    => 0,
		"totalrows" => ac_sql_select_one("SELECT COUNT(*) FROM $row[0]"),
	);
}

spit(_a('Setting a process to convert data to UTF-8: '), 'em');
// create it in db
$insert = array(
	'id' => 0,
	'userid' => 1,
	'rnd' => mt_rand(1201000, 9871000),
	'action' => 'iconv',
	'total' => count($tables),
	'completed' => 0,
	'percentage' => 0,
	'data' => serialize($tables),
	'=cdate' => 'SUBDATE(NOW(), INTERVAL 10 MINUTE)',
	'=ldate' => 'SUBDATE(NOW(), INTERVAL 10 MINUTE)', // will pick it up when they load admin page
);

$done = ac_sql_insert('#process', $insert);
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

# Assign the correct offsets for everyone based on the site settings.
spit(_a('Assigning the correct offsets for everyone based on the site settings: '), 'em');
if (isset($GLOBALS["site"])) {
	$site = $GLOBALS["site"];
	$ary  = array(
		"t_offset" => $site["t_offset"],
		"t_offset_o" => $site["t_offset_o"],
	);

	$done = ac_sql_update("#user", $ary, "t_offset = 0");
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}


foreach ($tables as $table => $info) {
	ac_sql_query("ALTER TABLE `$table` ENABLE KEYS");
}

?>
