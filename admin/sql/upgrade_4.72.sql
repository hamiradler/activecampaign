ALTER TABLE `12all_backend` CHANGE `valid` `id` INT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `12all_smtp` ADD `type` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `12all_smtp` ADD `encrypt` TINYINT( 1 ) NOT NULL DEFAULT '8';
ALTER TABLE `12all_smtp` ADD `pop3b4smtp` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `12all_smtp` ADD `corder` INT( 10 ) NOT NULL DEFAULT '0';
ALTER TABLE `12all_smtp` ADD `threshold` INT( 10 ) NOT NULL DEFAULT '50';
ALTER TABLE `12all_smtp` RENAME `12all_mailer` ;
UPDATE `12all_backend`, `12all_mailer` SET `12all_mailer`.`type` = `12all_backend`.`stype`, `12all_mailer`.`threshold` = `12all_backend`.`sdfreq` WHERE `12all_backend`.`id` = 1 AND `12all_mailer`.`id` = 1;
