ALTER TABLE `em_activity_cache` ADD `minid` int(10) not null default 0;
ALTER TABLE `em_activity_cache` ADD `maxid` int(10) not null default 0;
TRUNCATE TABLE `em_activity_cache`;