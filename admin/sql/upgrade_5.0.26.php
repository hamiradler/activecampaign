<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";


spit(_a('Moving subscriber names into list relations: '), 'em');
/* short version */
/*
$done = ac_sql_query("
	UPDATE
		#subscriber_list l
	SET
		first_name = ( SELECT first_name FROM #subscriber s WHERE s.id = l.subscriberid ),
		last_name = ( SELECT last_name FROM #subscriber s WHERE s.id = l.subscriberid )
");
*/
/* long version */
$done = true;
$sql = ac_sql_query("SELECT id, first_name, last_name FROM #subscriber");
while ( $row = ac_sql_fetch_assoc($sql) ) {
	spit('. ');
	$update = array(
		'first_name' => $row['first_name'],
		'last_name' => $row['last_name'],
	);
	$done = ac_sql_update('#subscriber_list', $update, "`subscriberid` = '$row[id]'");
	if ( !$done ) break;
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}


spit(_a('Removing old subscriber names: '), 'em');
$done = (
	ac_sql_query("ALTER TABLE `em_subscriber` DROP `first_name` ;")
and
	ac_sql_query("ALTER TABLE `em_subscriber` DROP `last_name` ;")
);

if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>