UPDATE `em_field` SET `type` = 'checkbox' WHERE `type` = 'checkboxgroup' ;
ALTER TABLE `em_field` CHANGE `type` `type` enum('text', 'textarea', 'checkbox', 'radio', 'dropdown', 'hidden', 'listbox', 'date') NOT NULL DEFAULT 'hidden' ;
