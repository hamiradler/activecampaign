ALTER TABLE em_campaign_count ADD KEY `tstamp` (`tstamp`);
ALTER TABLE em_config ADD `section` VARCHAR(250) NOT NULL DEFAULT '' AFTER `keyname`;
ALTER TABLE em_config ADD `item` VARCHAR(250) NOT NULL DEFAULT '' AFTER `section`;

|ALTER TABLE em_subscriber_action ADD KEY `linkid` (`linkid`), ADD KEY `campaignid` (`campaignid`);
ALTER TABLE em_config ADD KEY `section` (`section`);
