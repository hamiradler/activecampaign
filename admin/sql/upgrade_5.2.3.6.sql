CREATE TABLE `em_campaign_spamcheck` (  `id` int(10) NOT NULL auto_increment,  `campaignid` int(10) NOT NULL default '0',  `messageid` int(10) NOT NULL default '0',  `rule` varchar(250) NOT NULL default '',  `descript` text default NULL,  `score` float(5,2) NOT NULL default '0.00',  PRIMARY KEY  (`id`),  KEY `campaignmessage` (`campaignid`, `messageid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `em_campaign_message` ADD `spamcheck_score` INT(10) NOT NULL DEFAULT '-1';
ALTER TABLE `em_campaign_message` ADD `spamcheck_max` INT(10) UNSIGNED NOT NULL DEFAULT '0';
