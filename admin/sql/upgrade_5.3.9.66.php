<?php

$i = 1;
$up = array(
	array("name" => _a("Acme Orange"), "importnum" => $i++),
	array("name" => _a("Aqua Ocean"), "importnum" => $i++),
	array("name" => _a("Beige Red Discount"), "importnum" => $i++),
	array("name" => _a("Blue Computer"), "importnum" => $i++),
	array("name" => _a("Boats Fishing"), "importnum" => $i++),
	array("name" => _a("Bookkeepr"), "importnum" => $i++),
	array("name" => _a("Books Education"), "importnum" => $i++),
	array("name" => _a("Buildings Sky"), "importnum" => $i++),
	array("name" => _a("Business Event"), "importnum" => $i++),
	array("name" => _a("Christmas"), "importnum" => $i++),
	array("name" => _a("Clean Black"), "importnum" => $i++),
	array("name" => _a("Clean Type"), "importnum" => $i++),
	array("name" => _a("Creative Company"), "importnum" => $i++),
	array("name" => _a("Eatery"), "importnum" => $i++),
	array("name" => _a("Event Template"), "importnum" => $i++),
	array("name" => _a("Explore"), "importnum" => $i++),
	array("name" => _a("General Business"), "importnum" => $i++),
	array("name" => _a("Green Shades"), "importnum" => $i++),
	array("name" => _a("Home Consulting"), "importnum" => $i++),
	array("name" => _a("Hosting Company"), "importnum" => $i++),
	array("name" => _a("Hotel"), "importnum" => $i++),
	array("name" => _a("Night Dance"), "importnum" => $i++),
	array("name" => _a("Ocean Beach"), "importnum" => $i++),
	array("name" => _a("Orange Consulting"), "importnum" => $i++),
	array("name" => _a("Red Consultant"), "importnum" => $i++),
	array("name" => _a("Red Design Company"), "importnum" => $i++),
	array("name" => _a("Red Postcard"), "importnum" => $i++),
	array("name" => _a("School Donation"), "importnum" => $i++),
	array("name" => _a("Simple Beige"), "importnum" => $i++),
	array("name" => _a("Simple Grey Brown"), "importnum" => $i++),
	array("name" => _a("Snowman Greeting"), "importnum" => $i++),
	array("name" => _a("Survey"), "importnum" => $i++),
	array("name" => _a("Survey2"), "importnum" => $i++),
	array("name" => _a("Vacation"), "importnum" => $i++),
);

foreach ($up as $row) {
	$name = ac_sql_escape($row["name"]);
	$update = array("importnum" => $row["importnum"]);

	if ($name == _a("Survey2")) {
		$other = ac_sql_escape(_a("Surveymonkey"));
		$id = (int)ac_sql_select_one("SELECT id FROM #template WHERE name = '$name' OR name = '$other' LIMIT 1");
	} else {
		$id = (int)ac_sql_select_one("SELECT id FROM #template WHERE name = '$name'");
	}
	ac_sql_update("#template", $update, "id = '$id'");
	ac_sql_update("#screenshot", $update, "target = 'template' AND targetid = '$id'");
}

?>
