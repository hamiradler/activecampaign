<?php

$insert = array(
		'stringid' => 'socialsharing',
		'name' => _a('Social Sharing Report Monitor'),
		'descript' => _a('Periodically refreshes social media reports for campaigns sent within the last ten days.'),
		'active' => 1,
		'filename' => './admin/process.php',
		'loglevel' => 1,
		'minute' => 'a:2:{i:0;i:-2;i:1;i:45;}',
		'hour' => 101,
		'day' => -1,
		'weekday' => -1,
		'=lastrun' => 'NULL',
	);

ac_sql_insert("#cron", $insert);

// remove occurrences of "<title>Untitled document</title>" from tables that don't need "fullpage" plugin for TinyMCE.
// this is just for existing records - all new records will not have any <title> attribute at all, since fullpage will be OFF
$tables = array("form", "campaign");
$fields = array(
	"campaign" => array(
		"htmlunsubdata",
	),
	"form" => array(
		"sub1_value",
		"sub2_value",
		"sub3_value",
		"sub4_value",
		"unsub1_value",
		"unsub2_value",
		"unsub3_value",
		"unsub4_value",
		"up1_value",
		"up2_value",
	),
);
foreach ($tables as $table) {
	$set = array();
	foreach ($fields[$table] as $field) {
		$set[] = $field . " = REPLACE(" . $field . ", '<title>Untitled document</title>', '')";
	}
	$update = ac_sql_query("UPDATE #$table SET " . implode(", ", $set));
}

// em_service - "External Services" - insert default rows
$service_facebook = array("name" => "Facebook", "description" => "Configure Facebook application settings.");
$service_twitter = array("name" => "Twitter", "description" => "Configure Twitter application settings.");
ac_sql_insert("#service", $service_facebook);
ac_sql_insert("#service", $service_twitter);

# Fix campaigns with the fake "123" filterid that was inserted by those using the campaign_create 
# API call without changing the filterid field.
$filter123 = (int)ac_sql_select_one("SELECT COUNT(*) FROM #filter WHERE id = '123'");
if (!$filter123) {
	ac_sql_query("UPDATE #campaign SET filterid = '0' WHERE filterid = '123'");
}

# Fix campaigns with missing rows in em_campaign_message; first try and find the right message in 
# the em_link table.
$clist = ac_sql_select_list("SELECT id FROM #campaign");
foreach ($clist as $cid) {
	spit('. ');
	$mc = (int)ac_sql_select_one("SELECT COUNT(*) FROM #campaign_message WHERE campaignid = '$cid'");

	if ($mc == 0) {
		$msgs = ac_sql_select_list("SELECT messageid FROM #link WHERE campaignid = '$cid' AND messageid != '0'");
		if (count($msgs) > 0) {
			# Found some.  Add as 100%, however, so that we can't somehow send something out on 
			# accident...
			foreach ($msgs as $mid) {
				$ins = array(
					"campaignid" => $cid,
					"messageid"  => $mid,
					"percentage" => 100,
				);

				ac_sql_insert("#campaign_message", $ins);
			}
		} else {
			# Uh oh.  Well--the campaign is toast anyway, so we might as well delete it.  But 
			# only soft-delete it; push it into #campaign_deleted.
			ac_sql_query("
				INSERT INTO
					#campaign_deleted
				SELECT * FROM #campaign WHERE id = '$cid'
			");

			ac_sql_delete('#campaign', "id = '$cid'");
		}
	}
}

# Remove orphan subscriber rows, possibly created by bugs in subscriber_subscribe() where the 
# SMTP server dies during the opt-in send or (on hosted) created when the max subscriber limit 
# has been reached.
$rs = ac_sql_query("SELECT * FROM em_subscriber s WHERE (SELECT COUNT(*) FROM #subscriber_list l WHERE l.subscriberid = s.id) = 0");
while ($row = ac_sql_fetch_assoc($rs)) {
	spit('. ');
	$id = $row["id"];
	ac_sql_delete('#subscriber_list', "subscriberid = '$id'");
	ac_sql_delete('#subscriber_responder', "subscriberid = '$id'");
	if ( ac_sql_select_one('=COUNT(*)', '#subscriber_list', "subscriberid = '$id'") == 0 ) {
		ac_sql_delete('#subscriber', "id = '$id'");
		ac_sql_delete('#list_field_value', "relid = '$id'");
	}

	# Don't increment their deletedsubs backend field, since it's not entirely their fault that 
	# these subscribers are going away.
}

?>
