<?php

require_once(dirname(dirname(__FILE__)) . '/functions/em.php');

$rs = ac_sql_query("SELECT * FROM #campaign_message");
while ($row = ac_sql_fetch_assoc($rs)) {
	$ins = array(
		"campaignid" => $row["campaignid"],
		"messageid"  => $row["messageid"],
		"type"       => "preview",
		"len"        => $row["sourcesize"],
	);

	ac_sql_insert("#campaign_source", $ins);
	$sourceid = (int)ac_sql_insert_id();

	$ins = array(
		"sourceid" => $sourceid,
		"sequence" => 1,
		"data"     => $row["fullsource"],
	);

	ac_sql_insert("#campaign_source_data", $ins);
}

ac_sql_query("ALTER TABLE #campaign_message DROP fullsource");

?>
