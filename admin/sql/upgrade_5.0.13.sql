# Add columns to store virtually paused campaigns
ALTER TABLE `em_campaign` ADD `split_winner_awaiting` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `split_winner_messageid` ;

# we had INT(20) for some reason here
ALTER TABLE `em_subscriber` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT ;