ALTER TABLE `em_user` ADD `approved` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `parentid` ;
UPDATE em_exclusion SET email = REPLACE(REPLACE(email, '\\%', '%'), '\\_', '_');
UPDATE em_exclusion SET email = REPLACE(REPLACE(email, '%', '*'), '_', '?') WHERE wildcard = 1;
