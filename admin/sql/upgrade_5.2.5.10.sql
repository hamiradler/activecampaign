UPDATE `em_campaign` SET `tracklinks` = 'none' WHERE `tracklinks` IS NULL ;
ALTER TABLE `em_campaign` CHANGE `tracklinks` `tracklinks` ENUM( 'all',  'mime',  'html',  'text',  'none' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'all' ;

# update for all changes so far on campaign table that were not made on campaign_delete table
ALTER TABLE `em_campaign_deleted` ADD `laststep` VARCHAR(250) NOT NULL DEFAULT '';
ALTER TABLE `em_campaign_deleted` ADD `basetemplateid` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `threadid`;
ALTER TABLE `em_campaign_deleted` ADD `basemessageid` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `basetemplateid`;
ALTER TABLE `em_campaign_deleted` ADD `managetext` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign_deleted` ADD `schedule` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `em_campaign_deleted` CHANGE `type` `type` ENUM('single','recurring','split','responder','reminder','special','activerss','text') NOT NULL DEFAULT 'single';
UPDATE `em_campaign_deleted` SET `tracklinks` = 'none' WHERE `tracklinks` IS NULL ;
ALTER TABLE `em_campaign_deleted` CHANGE `tracklinks` `tracklinks` ENUM( 'all',  'mime',  'html',  'text',  'none' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
