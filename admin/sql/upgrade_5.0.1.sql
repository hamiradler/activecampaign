ALTER TABLE `em_user` ADD `local_dst` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `local_zoneid` ;
UPDATE `em_list_field_value` SET val = '~|' WHERE val = '__--blank--__';
UPDATE `em_sync` SET `field_to` = REPLACE(`field_to`, 'field_', '_f');
#UPDATE `em_cron` SET `active` = '0' WHERE `id` = '7';
