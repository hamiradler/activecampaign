<?php

$done = true;
spit(_a('Copying link info into log table: '), 'em', 1);
$sql = ac_sql_query("SELECT * FROM #link_data WHERE campaignid = 0");
while ( $row = mysql_fetch_assoc($sql) ) {
	spit('. ');
	$link = ac_sql_select_row("SELECT * FROM #link WHERE id = '$row[linkid]'");
	if ( !$link ) continue;
	$up = array(
		'campaignid' => $link['campaignid'],
		'messageid' => $link['messageid'],
		'isread' => (int)( $link['link'] == 'open' ),
	);
	$done = ac_sql_update("#link_data", $up, "id = '$row[id]'");
	if ( !$done ) break;
}

if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
