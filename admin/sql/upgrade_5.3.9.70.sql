ALTER TABLE `em_link_data` ADD `messageid` INT(10) NOT NULL DEFAULT '0' AFTER `linkid`;
ALTER TABLE `em_link_data` ADD `campaignid` INT(10) NOT NULL DEFAULT '0' AFTER `linkid`;
ALTER TABLE `em_link_data` ADD `isread` TINYINT(1) NOT NULL DEFAULT '0' AFTER `linkid`;

ALTER TABLE `em_socialshare` ADD KEY `campaignid` (`campaignid`);

UPDATE `em_activity_cache` SET `items` = NULL ;
