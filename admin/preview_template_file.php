<?php

# Despite the name of the file, we handle both messages and templates.

//if (!@ini_get("zlib.output_compression")) @ob_start("ob_gzhandler");

// require main include file
require_once(dirname(__FILE__) . '/prepend.inc.php');

if ( !ac_admin_isadmin() ) {
	echo 'You are not logged in.';
	exit;
}

$name = ac_http_param("name");
$expl = explode(".", $name);

if (strpos($name, "..") !== false)
	exit;

if (count($expl) < 2)
	exit;

switch (strtolower($expl[1])) {
	case "png":
		$mime = "image/png";
		break;

	case "gif":
		$mime = "image/gif";
		break;

	case "jpg":
	case "jpeg":
		$mime = "image/jpeg";
		break;

	default:
		exit;
}

$data = @file_get_contents(ac_base("cache/") . $expl[0] . "." . $expl[1]);

header("Content-Type: $mime");
echo $data;
exit;

?>
